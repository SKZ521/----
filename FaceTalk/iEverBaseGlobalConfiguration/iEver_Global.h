//
//  iEver_Global.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-15.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_Global : NSObject
//notification
extern NSString * const ReceiveRemoteNotification;

/*
 * 单例 iEver_Global
 */
+(iEver_Global *)Instance;


//检查网络状态
- (BOOL)isExistenceNetwork;
/*
 * 上传头像
 */
- (void)uploadImage:(UIImage *)image;



///*
// * 检查网络状态
// */
//- (BOOL)isExistenceNetwork;
//


/*
 * 解析数据成字典
 */
+ (NSDictionary *)parserToJsonDicData:(NSString *)responderStr;



/*
 * 解析数据成数组
 */
+ (NSArray *)parserToJsonArrayData:(NSString *)responderStr;

/*
 * 获取当前时间戳
 */
+ (NSString *)timeString;

/*
 * 计算时间间隔
 */
+ (NSString *)intervalSinceNow: (long long) theDate;

/*
 * 正则验证手机号
 */
- (BOOL)validateMobile:(NSString *)mobileNum;

/*
 * 验证错误类型
 */
- (NSString *)confirmationResultCode:(int)resultCode;

/*
 * 获得当前时间差
 */
- (NSInteger)requestCurrentDate:(NSString *)mothod;

/*
 * 计算发布时间间隔
 */
+ (NSString *)commentIntervalSinceNow: (long long) theDate;

/*
 * 十六进制的颜色转换为UIColor
 */
- (UIColor *)colorWithHexString: (NSString *)color;


/*
 函数描述：字符串筛选,去掉不需要的特殊字符串
 参数描述：target         原字符串
 replacement   需要替换的字符串
 options       默认传2:NSLiteralSearch,区分大小写
 _replaceArray 需要排除的Array
 返回值： 筛选完的String
 备注:   使用方法:replaceOccurrencesOfString:@"1(2*3" withString:@"" options:2 replaceArray:[NSArray arrayWithObjects:@"(",@"*", nil]
 输出:123
 */
+ (NSString *)replaceOccurrencesOfString:(NSString *)target withString:(NSString *)replacement options:(NSStringCompareOptions)options replaceArray:(NSArray *)_replaceArray;

/*
 * 判断是否开启推送
 */
+ (int)enabledRemoteNotificationTypes;

//邮箱
+ (BOOL) validateEmail:(NSString *)email;

//手机号码验证
+ (BOOL) validateMobile:(NSString *)mobile;

//车牌号验证
+ (BOOL) validateCarNo:(NSString *)carNo;

//车型
+ (BOOL) validateCarType:(NSString *)CarType;

//用户名
+ (BOOL) validateUserName:(NSString *)name;

//密码
+ (BOOL) validatePassword:(NSString *)passWord;

//昵称
+ (BOOL) validateNickname:(NSString *)nickname;

@end

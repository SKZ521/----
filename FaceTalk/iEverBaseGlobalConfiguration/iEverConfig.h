//
//  iEverConfig.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#ifndef FaceTalk_iEverConfig_h
#define FaceTalk_iEverConfig_h
// Comment the below line to get non-verbose logs
#define VERBOSE_LOGS

#ifdef DEBUG
#ifdef VERBOSE_LOGS
#	define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
# define DLog(...) NSLog(__VA_ARGS__)
#endif
#else
#	define DLog(...)
#endif

// Color picking helpers
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define GRAYACOLOR(g,a) [UIColor colorWithRed:(g)/255.0f green:(g)/255.0f blue:(g)/255.0f alpha:(a)]
#define GRAYCOLOR(g) [UIColor colorWithRed:(g)/255.0f green:(g)/255.0f blue:(g)/255.0f alpha:1]

#define BackkGroundColor RGBCOLOR(237, 238, 240)
#define CLEAR GRAYACOLOR(255, 0)
#define BLACK GRAYCOLOR(0)
#define WHITE GRAYCOLOR(255)
#define DAKEBLACK RGBCOLOR(85,85,85)
#define LOGIN_WHITE RGBCOLOR(249, 249, 249)
#define GRAY_LIGHT RGBCOLOR(142, 141, 146)
#define HEIGH_BLUE RGBCOLOR(44, 208, 231)
#define LINE_GRAY RGBCOLOR(200, 199, 204)

#define NAV RGBCOLOR(72, 197, 189)
/*try i-ever color set */
#define iever RGBCOLOR(245, 185, 175)
#define CZ RGBCOLOR(243, 207, 130)
#define HF RGBCOLOR(165, 206, 130)
#define JF RGBCOLOR(150, 205, 231)

#define BD RGBCOLOR(165, 207, 131)
#define MJ RGBCOLOR(168, 153, 196)
#define MF RGBCOLOR(198, 162, 171)
#define MT RGBCOLOR(148, 205, 232)

#define SY RGBCOLOR(243, 207, 129)
#define CH RGBCOLOR(229, 209, 197)
#define ALL RGBCOLOR(252, 174, 187)

/* 9宫格颜色 */
#define V_1 RGBCOLOR(245, 185, 175)
#define V_2 RGBCOLOR(123, 106, 173)
#define V_3 RGBCOLOR(249, 217, 160)
#define V_4 RGBCOLOR(229, 209, 197)
#define V_5 RGBCOLOR(72, 197, 189)
#define V_6 RGBCOLOR(198, 162, 171)
#define V_7 RGBCOLOR(150, 89, 68)
#define V_8 RGBCOLOR(148, 205, 231)
#define V_9 RGBCOLOR(228, 0, 101)

// list RGB
#define LIST_TITLE RGBCOLOR(144, 112, 112)
#define LIST_EDITOR RGBCOLOR(182, 176, 164)
#define DETAIL_TITLE RGBCOLOR(210, 187, 109)
#define DETAIL_CONTENT RGBCOLOR(210, 187, 109)
#define LIST_like_button RGBCOLOR(224, 200, 110)
/*main view Temporary data*/
#define NAVBAR RGBCOLOR(86, 185, 180)


// person model message
#define PERSON_green RGBCOLOR(146, 203, 229)
#define PERSON_yellow RGBCOLOR(241, 205, 128)
#define PERSON_red RGBCOLOR(251, 173, 176)
#define PERSON_lightYellow RGBCOLOR(160, 202, 128)
#define PERSON_lightBlue RGBCOLOR(72, 194, 186)
#define PERSON_lightRed RGBCOLOR(205, 185, 215)
#define PERSON_mask RGBCOLOR(245, 55, 88)

//rankR236 G224 B183 /R228 G212 B154 / R221 G200 B125
#define RANK_GOLD RGBCOLOR(236, 224, 183)
#define RANK_SILVER RGBCOLOR(228, 212, 154)
#define RANK_COOPER RGBCOLOR(221, 200, 125)

//手机注册
#define yanzheng_N RGBCOLOR(185,182,248)
#define yanzheng_C RGBCOLOR(188, 188, 188)
//V_detail
#define v_yellow RGBCOLOR(255, 190, 2)

//达人主页更新
#define SEABLUE RGBCOLOR(240, 240, 240)
#define ORANGECOLOR RGBCOLOR(102, 73, 150)
#define VNAME RGBCOLOR(28, 28, 28)
#define VINFO RGBCOLOR(108, 108, 108)
#define TIME RGBCOLOR(108, 108, 108)

//首页更新颜色
#define SpecialBannerCOLOR_1 RGBCOLOR(185, 182, 248)
#define SpecialBannerCOLOR_2 RGBCOLOR(221, 193, 171)
#define SpecialBannerCOLOR_3 RGBCOLOR(255, 202, 202)
#define SpecialBannerCOLOR_4 RGBCOLOR(201, 202, 202)
#define SpecialBannerCOLOR_5 RGBCOLOR(234, 219, 80)
#define SpecialBannerCOLOR_6 RGBCOLOR(146, 234, 209)

//榜单更新颜色
#define RANKCOLOR_1 RGBACOLOR(185, 182, 248, 0.5)
#define RANKCOLOR_2 RGBACOLOR(221, 193, 171, 0.5)
#define RANKCOLOR_3 RGBACOLOR(255, 202, 202, 0.5)
#define RANKCOLOR_4 RGBACOLOR(201, 202, 202, 0.5)
#define RANKCOLOR_5 RGBACOLOR(234, 219, 80, 0.5)
#define RANKCOLOR_6 RGBACOLOR(146, 234, 209, 0.5)
#define RANKCOLOR_7 RGBACOLOR(120, 206, 249, 0.5)
#define RANKCOLOR_8 RGBACOLOR(231, 97, 83, 0.5)
#define RANKCOLOR_9 RGBACOLOR(236, 175, 2, 0.5)
#define RANKCOLOR_10 RGBACOLOR(168, 196, 0, 0.5)

#define RANK_UPDATE_GOLD RGBCOLOR(190, 150, 98)
#define TRY_UPDATE_YELLOY RGBCOLOR(249, 186, 6)

#define MY_PURPLECOLOR RGBCOLOR(102, 73, 150)

#define TYR_BACKGRAYCOLOR RGBCOLOR(248, 248, 248)

#define V_COMMENTCOLOR RGBCOLOR(244, 132, 143)



// Application Fonts
//#define FONT_AVN_ULTRALIGHT(s) [UIFont fontWithName:@"AvenirNext-UltraLight" size:(s)]
//#define FONT_SYSTEM(s) [UIFont systemFontOfSize:(s)]
//#define TextLIST_EDITOR_Fonts  [UIFont systemFontOfSize:8];
//#define TextFonts  [UIFont systemFontOfSize:10];
//#define TextLIST_Fonts  [UIFont systemFontOfSize:11];
//#define TextLIST_TITLE_Fonts  [UIFont systemFontOfSize:35.0/2];
//#define TextDESCFonts  [UIFont systemFontOfSize:12];
//#define TextFont  [UIFont systemFontOfSize:14.0];
//#define TitleFont  [UIFont systemFontOfSize:17];
//#define TextBigFont  [UIFont systemFontOfSize:20];
//#define TextBigBoldFont [UIFont fontWithName:@"Helvetica-Bold" size:18]


// Application Fonts
#define FONT_AVN_ULTRALIGHT(s) [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:(s)]
#define FONT_SYSTEM(s) [UIFont systemFontOfSize:(s)]
#define TextLIST_EDITOR_Fonts  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:8];
#define TextFonts  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:10];
#define TextLIST_Fonts  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:11];
#define TextLIST_TITLE_Fonts  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:17.5];
#define TextDESCFonts  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:12];
#define TextFont  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:14];
#define TitleFont  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:17];
#define TextBigFont  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:20];
#define TextBigBoldFont [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:18];


#define Title_Font  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:14];
#define Title_Big2_Font  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:19];
#define Title_BigFont  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:20];
#define Title_V_Fonts  [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:17.5];

#define NetWorkError @"网络无连接"

//NSUserDefault
#define CurrentUserState    @"CurrentUserState"

//Api Url
#define api_news_list                @"/api/news_list"
#define api_reg_action               @"reg.action"

//判断iPhone5
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define iOS7 [[[UIDevice currentDevice]systemVersion] floatValue] >= 7.0 //判断iOS7

#define iOS7Status20
#define ScreenHeight    [[UIScreen mainScreen] bounds].size.height
#define ScreenWidth     [[UIScreen mainScreen] bounds].size.width
#define ScreenBounds	[[UIScreen mainScreen] bounds]
#define KMainViewRect	CGRectMake(0, 0, ScreenWidth, ScreenHeight)
#define NAVHeight	64.0

#define BARHeight	44.0


//static CGFloat marginOut = 15.0f;
//static CGFloat marginIn  = 10.0f;

#define DeviceToken @"DeviceToken"

#define CURRENTShortVERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#define CURRENTVERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]
#define V_CURRENTVERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]
#define person_CURRENTVERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]
#define rank_CURRENTVERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]
#define main_CURRENTVERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]

//
#define BottomHeight         49.0f
#define marginOut            15.0f
#define marginIn             10.0f
#define FIRSTLAUNCH            @"FirstLaunch"
#define PREVIOUSVERSION        @"PreviouVersion"
#define V_PREVIOUSVERSION      @"V_PreviouVersion"
#define person_PREVIOUSVERSION @"person_PreviouVersion"
#define rank_PREVIOUSVERSION   @"rank_PreviouVersion"
#define main_PREVIOUSVERSION   @"main_PreviouVersion"
#define ONEPAGECOUNT         10
#define APPID                918789910;
#define UMAPPKEY             @"545c3f73fd98c59302001b9b" //545c3f73fd98c59302001b9b  越狱市场友盟KEY:552cbc0cfd98c589ce0000be
#define APPUPDATEURL         @"https://itunes.apple.com/cn/app/i-ever/id918789910?l=zh&ls=1&mt=8"
#define USERAVATAR           @"USERAVATAR"

#define kWBSDKAppKey         @"2198108993"
#define kWBSDKAppSecret      @"6d139d2516fd69e20b1cfb25f1114c33"

#define kTENCENTSDKAppKey    @"801269915"
#define kTENCENTSDKAppSecret @"bebfbbd3319bc3f5c7f96f9702295723"

#ifdef __IPHONE_7_0
#define IOS7POP if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {\
self.navigationController.interactivePopGestureRecognizer.delegate = nil;\
}
#else
#define IOS7POP NULL
#endif

#endif

//
//  iEver_imageAFAPIClient.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "AFHTTPClient.h"

@interface iEver_imageAFAPIClient : AFHTTPClient
+ (iEver_imageAFAPIClient *)sharedClient:(NSString *)url;
@end

//
//  iEver_AppPostJsonAPIClient.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-11.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "AFHTTPClient.h"

@interface iEver_AppPostJsonAPIClient : AFHTTPClient
/*
 * 单例 iEver_AppDotNetAPIClient
 */
+ (iEver_AppPostJsonAPIClient *)sharedClient;

/*
 * 封装参数请求数据
 * 设置超时时间
 */
- (RACSignal *)enqueueRequestWithMethod:(NSString *)method
								   path:(NSString *)path
							 parameters:(NSDictionary *)parameters;

- (RACSignal *)enqueueRequestWithMethod:(NSString *)method
								   path:(NSString *)path
							 parameters:(NSDictionary *)parameters
								timeOut:(double)time;

@end

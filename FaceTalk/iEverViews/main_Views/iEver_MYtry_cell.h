//
//  iEver_MYtryItemObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/4/4.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_tryItemObject.h"

@interface iEver_MYtry_cell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_itemTryList_object *object;
@end

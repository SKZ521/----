//
//  iEver_itemCommentReply_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/18.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_itemCommentReply_object.h"


@interface iEver_itemCommentReply_cell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

@property (nonatomic, strong) iEver_icList_object *object;

@end

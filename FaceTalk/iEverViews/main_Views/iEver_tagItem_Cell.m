//
//  iEver_tagItem_Cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/5.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_tagItem_Cell.h"
#import "CWStarRateView.h"

@interface iEver_tagItem_Cell()

@property (nonatomic, strong) UIImageView   *itemImage;        /* 商品图片 */
@property (nonatomic, strong) UILabel       *titleLabel;       /* 商品标题 */
@property (nonatomic, strong) UILabel       *subTitleLabel;    /* 商品副标题 */
@property (nonatomic, strong) CWStarRateView *starView;        /* 评分视图 */
@property (nonatomic, strong) UILabel       *personTotalLabel; /* 使用人数 */
@property (nonatomic, strong) UILabel       *startGradeLabel;  /* 使用人数 */
@property (nonatomic, strong) UIImageView   *lineView;         /* 视图横线 */

@end

@implementation iEver_tagItem_Cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = CLEAR;
        [self.contentView addSubview:self.itemImage];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.subTitleLabel];
        [self.contentView addSubview:self.starView];
        [self.contentView addSubview:self.personTotalLabel];
        [self.contentView addSubview:self.startGradeLabel];
        [self.contentView addSubview:self.lineView];

    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setObject:(iEver_tagItemList_object *)object
{
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 步骤图片 */
    x = 10.0;
    y = 5.0;
    width = 95.0;
    height = 95.0;
    self.itemImage.frame           = CGRectMake(x, y, width, height);
    [self.itemImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/190x",object.itemImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    /* 商品标题 */
    x = 124.0;
    y = 10.0;
    width = 170.0;
    height = 40.0;
    self.titleLabel.frame          = CGRectMake(x, y, width, height);
    self.titleLabel.text           = object.itemName;
    NSDictionary * name_dic        = [NSDictionary dictionaryWithObjectsAndKeys:self.titleLabel.font, NSFontAttributeName,nil];
    CGSize name_silver_Size        = [object.itemName boundingRectWithSize:CGSizeMake(width, height)
                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                attributes:name_dic
                                                                   context:nil].size;
    self.titleLabel.frame          = CGRectMake(x, y, width, name_silver_Size.height);

    /* 商品副标题 */
    x = 124.0;
    y = 42.0;
    width = 150.0;
    height = 25.0;
    self.subTitleLabel.frame       = CGRectMake(x, y, width, height);
    self.subTitleLabel.text        = [NSString stringWithFormat:@"%@/%d￥",object.itemSpec,object.price];

    /* 评分视图 */
    self.starView.scorePercent     = object.startGrade/10.f;

    /* 分数 */
    x = 210.0;
    y = 62.0;
    width = 80;
    height = 15;
    self.startGradeLabel.frame     = CGRectMake(x, y, width, height);
    self.startGradeLabel.text      = [NSString stringWithFormat:@"%d",object.startGrade];

    /* 使用人数 */
    x = 124.0;
    y = 75.0;
    width = 150.0;
    height = 25.0;
    self.personTotalLabel.frame    = CGRectMake(x, y, width, height);
    self.personTotalLabel.text     = [NSString stringWithFormat:@"%d人关注",object.pvTotal];


    /* 视图横线 */
    x = 15.0;
    y = 104.0;
    height = 1.0;
    width = 290.0;
    self.lineView.frame            = CGRectMake(x, y, width, height);

}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    return 105.0;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
# pragma mark - view getters

/* 商品图片 */
- (UIImageView *)itemImage
{
    if (!_itemImage) {
        _itemImage = [[UIImageView alloc]init];
        _itemImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _itemImage;
}

/* 商品标题 */
- (UILabel *)titleLabel
{
    if (!_titleLabel){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.numberOfLines = 0;
        _titleLabel.backgroundColor = CLEAR;
        _titleLabel.textColor = BLACK;
        _titleLabel.font = TextFont;
    }
    return _titleLabel;
}
/* 商品容量&价格 */
- (UILabel *)subTitleLabel
{
    if (!_subTitleLabel){
        _subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.numberOfLines = 1;
        _subTitleLabel.backgroundColor = CLEAR;
        _subTitleLabel.textColor = DAKEBLACK;
        _subTitleLabel.font = TextDESCFonts;
    }
    return _subTitleLabel;
}

/* 分数 */
- (UILabel *)startGradeLabel
{
    if (!_startGradeLabel){
        _startGradeLabel = [[UILabel alloc] init];
        _startGradeLabel.numberOfLines = 1;
        _startGradeLabel.backgroundColor = CLEAR;
        _startGradeLabel.textColor = DAKEBLACK;
        _startGradeLabel.font = TextFont;
    }
    return _startGradeLabel;
}
/* 使用人数 */
- (UILabel *)personTotalLabel
{
    if (!_personTotalLabel){
        _personTotalLabel = [[UILabel alloc] init];
        _personTotalLabel.numberOfLines = 1;
        _personTotalLabel.backgroundColor = CLEAR;
        _personTotalLabel.textColor = GRAY_LIGHT;
        _personTotalLabel.font = TextDESCFonts;
    }
    return _personTotalLabel;
}

/* 视图横线 */
- (UIImageView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIImageView alloc]init];
        _lineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView;
}

/* 评分视图 */
- (CWStarRateView *)starView
{
    if (!_starView) {
        _starView = [[CWStarRateView alloc] initWithFrame:CGRectMake(124.0, 62, 80, 15) numberOfStars:5];
        
    }
    return _starView;
}


@end

//
//  iEver_itemRank_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-28.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_rankItemObject.h"

@interface iEver_itemRank_cell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_itemTopList_object *object;

@end

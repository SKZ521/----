
//
//  iEver_itemComment_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/16.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <CoreText/CoreText.h>
#import "iEver_itemComment_cell.h"
#import "iEver_likeActionObject.h"
#import "NSString+WPAttributedMarkup.h"
#import "WPHotspotLabel.h"
#import "WPAttributedStyleAction.h"
#import "UIView+FindViewController.h"
#import "iEver_itemCommentReplyDetailViewController.h"
#import "iEver_itemCommentViewController.h"
#import "MCFireworksButton.h"

@interface iEver_itemComment_cell()

@property (nonatomic, strong) iEver_itemCommentList_object *list_object;

@property (nonatomic, strong) UIView                 *bottomView;           /* 背景视图 */
@property (nonatomic, strong) UIImageView            *photoImage;           /* 用户图片 */
@property (nonatomic, strong) UILabel                *nameLabel;            /* 用户名称 */
@property (nonatomic, strong) UILabel                *featureLabel;         /* 定制类型 */
@property (nonatomic, strong) UILabel                *timeLabel;            /* 发布时间 */
@property (nonatomic, strong) UILabel                *contentLabel;         /* 消息内容 */

@property (nonatomic, strong) UIButton               *replyButton;          /* 回复按钮 */
@property (nonatomic, strong) MCFireworksButton      *likeButton;           /* 喜欢按钮 */
@property (nonatomic, strong) UILabel                *likeLabel;            /* 喜欢标签 */

@property (nonatomic, strong) UIImageView            *arrowImage;           /* 箭头图片 */
@property (nonatomic, strong) UIView                 *baseCommentView;      /* 用户评论灰色背景 */

/* 评论的评论第一条 */
@property (nonatomic, strong) UILabel                *nameLable_1;          /* 用户评论 */
@property (nonatomic, strong) UILabel                *timeLabel_1;          /* 发布时间 */
@property (nonatomic, strong) WPHotspotLabel         *contentLabel_1;       /* 消息内容 */
@property (nonatomic, strong) UIImageView            *lineView_1;           /* 视图横线 */

/* 评论的评论第二条 */
@property (nonatomic, strong) UILabel                *nameLable_2;          /* 用户评论 */
@property (nonatomic, strong) UILabel                *timeLabel_2;          /* 发布时间 */
@property (nonatomic, strong) WPHotspotLabel         *contentLabel_2;       /* 消息内容 */
@property (nonatomic, strong) UIImageView            *lineView_2;           /* 视图横线 */

/* 评论的评论第三条 */
@property (nonatomic, strong) UILabel                *nameLable_3;          /* 用户评论 */
@property (nonatomic, strong) UILabel                *timeLabel_3;          /* 发布时间 */
@property (nonatomic, strong) WPHotspotLabel         *contentLabel_3;       /* 消息内容 */
@property (nonatomic, strong) UIImageView            *lineView_3;           /* 视图横线 */

@property (nonatomic, strong) WPHotspotLabel         *queryAllCommLabel;    /* 查看更多评论_可点击标签 */

@property (nonatomic, strong) UIImageView            *lineView;             /* 视图横线 */

@end


@implementation iEver_itemComment_cell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];

        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.photoImage];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.featureLabel];
        [self.contentView addSubview:self.timeLabel];
        [self.contentView addSubview:self.contentLabel];

        [self.contentView addSubview:self.replyButton];
        [self.contentView addSubview:self.likeButton];
        [self.contentView addSubview:self.likeLabel];

        [self.contentView addSubview:self.arrowImage];
        [self.contentView addSubview:self.baseCommentView];

        [self.baseCommentView addSubview:self.nameLable_1];
        [self.baseCommentView addSubview:self.timeLabel_1];
        [self.baseCommentView addSubview:self.contentLabel_1];
        [self.baseCommentView addSubview:self.lineView_1];

        [self.baseCommentView addSubview:self.nameLable_2];
        [self.baseCommentView addSubview:self.timeLabel_2];
        [self.baseCommentView addSubview:self.contentLabel_2];
        [self.baseCommentView addSubview:self.lineView_2];

        [self.baseCommentView addSubview:self.nameLable_3];
        [self.baseCommentView addSubview:self.timeLabel_3];
        [self.baseCommentView addSubview:self.contentLabel_3];
        [self.baseCommentView addSubview:self.lineView_3];

        [self.baseCommentView addSubview:self.queryAllCommLabel];

        [self.contentView addSubview:self.lineView];

    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    /* 视图横线 */
    CGRect r = self.contentView.bounds;
    CGFloat x = 15.0;
    CGFloat y = r.size.height - 1;
    CGFloat height = 1.0;
    CGFloat width = 290.0;
    self.lineView.frame = CGRectMake(x, y, width, height);

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}


- (void)setObject:(iEver_itemCommentList_object *)object
{
    _list_object = object;
    /* 评论用户_头像 */
    CGFloat x = 15.0;
    CGFloat y = 10.0;
    CGFloat width = 36.0;
    CGFloat height = 36.0;
    CGFloat content0_SizeHeight = 0.0;
    CGFloat content1_SizeHeight = 0.0;
    CGFloat content2_SizeHeight = 0.0;
    CGFloat content3_SizeHeight = 0.0;
    CGFloat tryDetailArrow_SizeHeight = 0.0;
    self.photoImage.frame = CGRectMake(x, y, width, height);
    [self.photoImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/72x",object.headImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    /* 评论用户_名称 */
    x = 60.0;
    y = 10.0;
    width = 150.0;
    height = 20.0;
    self.nameLabel.frame = CGRectMake(x, y, width, height);
    self.nameLabel.text = object.nickName;

    /* 评论用户_个性化标签 */
    x = 60.0;
    y = 30.0;
    width = 200.0;
    height = 20.0;
    self.featureLabel.frame = CGRectMake(x, y, width, height);
    if (object.feature.length == 0) {
        self.featureLabel.text = @"该用户尚未定制";
    }else{

        self.featureLabel.text = object.feature;
    }

    /* 评论用户_发布时间 */
    x = 205.0;
    y = 10.0;
    width = 100.0;
    height = 20.0;
    NSString *str = [iEver_Global commentIntervalSinceNow:object.createTime/1000];
    self.timeLabel.frame = CGRectMake(x, y, width, height);
    self.timeLabel.text = str;

    /* 评论用户_评论 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[object.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    x = 15.0;
    y = 56.0;
    width = ScreenWidth - 15.0 - 15.0;
    height = contentSize.height;
    content0_SizeHeight = contentSize.height;
    self.contentLabel.frame = CGRectMake(x, y, width, height);
    self.contentLabel.text = object.commentContent;

    /*  回复评论按钮 */
    UIImage *detail_commentReplay = [UIImage imageNamed:@"detail_commentReplay"];
    x = 220.0;
    y = 56.0 + content0_SizeHeight + 5;
    width = detail_commentReplay.size.width;
    height = detail_commentReplay.size.height;
    [self.replyButton setImage:detail_commentReplay forState:UIControlStateNormal];
    [self.replyButton setImage:detail_commentReplay forState:UIControlStateHighlighted];
    self.replyButton.frame = CGRectMake(x, y, width, height);

    /*  点赞按钮 */
    UIImage *detail_commentLike       = [UIImage imageNamed:@"detail_commentLike"];
    UIImage *detail_commentLike_click = [UIImage imageNamed:@"detail_commentLike_click"];
    x = 250.0;
    y = 56.0 + content0_SizeHeight + 5;
    width = detail_commentLike.size.width;
    height = detail_commentLike.size.height;

    if (object._selected) {
        [self.likeButton setImage:detail_commentLike_click forState:UIControlStateNormal];
    }else{
        [self.likeButton setImage:detail_commentLike forState:UIControlStateNormal];
    }
    self.likeButton.frame = CGRectMake(x, y, width, height);

    /*  喜欢数量 */
    x = 270.0;
    y = 56.0 + content0_SizeHeight + 3;
    width = 40;
    height = 15;
    self.likeLabel.text = [NSString stringWithFormat:@"赞(%d)",object.likeTotal];
    self.likeLabel.frame = CGRectMake(x, y, width, height);


    /* 评论的评论最多展示三条 */
    if (object.replyTotal == 0) {

        self.arrowImage.hidden = YES;
        self.baseCommentView.hidden = YES;

        self.nameLable_1.hidden = YES;
        self.timeLabel_1.hidden = YES;
        self.contentLabel_1.hidden = YES;
        self.lineView_1.hidden = YES;

        self.nameLable_2.hidden = YES;
        self.timeLabel_2.hidden = YES;
        self.contentLabel_2.hidden = YES;
        self.lineView_2.hidden = YES;

        self.nameLable_3.hidden = YES;
        self.timeLabel_3.hidden = YES;
        self.contentLabel_3.hidden = YES;
        self.lineView_3.hidden = YES;

        self.queryAllCommLabel.hidden = YES;

        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = 56.0 + contentSize.height + 25.0;
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);

    }
    else if (object.replyTotal == 1) {

        self.arrowImage.hidden = NO;
        self.baseCommentView.hidden = NO;

        self.nameLable_1.hidden = NO;
        self.timeLabel_1.hidden = NO;
        self.contentLabel_1.hidden = NO;
        self.lineView_1.hidden = YES;

        {

            /* 评论的评论小箭头 */
            UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
            x = 35.0;
            y = y + height;
            width = tryDetailArrow.size.width;
            height = tryDetailArrow.size.height;
            tryDetailArrow_SizeHeight = tryDetailArrow.size.height;
            self.arrowImage.image = tryDetailArrow;
            self.arrowImage.frame = CGRectMake(x, y, width, height);


            iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_1 = [object._replyList objectAtIndex:0];

            /* 评论的评论第一条 用户昵称区域 */
            x = 10.0;
            y = 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name1_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};

            NSString *name1_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_detail_comment_ReplyList_object_1.nickName,_detail_comment_ReplyList_object_1.atNickName];
            self.nameLable_1.attributedText = [name1_strStyle attributedStringWithStyleBook:name1_Style];
            self.nameLable_1.frame = CGRectMake(x, y, width, height);

            /* 评论的评论第一条 时间 */
            x = 205.0;
            y = 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_1 = [iEver_Global commentIntervalSinceNow:_detail_comment_ReplyList_object_1.createTime/1000];
            self.timeLabel_1.text = replyCommentTimeStr_1;
            self.timeLabel_1.frame = CGRectMake(x, y, width, height);

            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

                iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

                superVC.comment_parentId = _detail_comment_ReplyList_object_1.parent_id;
                superVC.comment_atUserId = _detail_comment_ReplyList_object_1.userId;
                superVC.toReplyCommentName = _detail_comment_ReplyList_object_1.nickName;
                [superVC insertCommentToReplyComment];
                NSLog(@"insertComment");

            }]};
            NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize1 =[_detail_comment_ReplyList_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize1.height;
            content1_SizeHeight = contentSize1.height;
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_detail_comment_ReplyList_object_1.commentContent];
            self.contentLabel_1.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            self.contentLabel_1.frame = CGRectMake(x, y, width, height);

        }


        /* 评论的评论灰色块 */
        x = 15.0;
        y = self.arrowImage.frame.origin.y + tryDetailArrow_SizeHeight;
        width = ScreenWidth - 15.0 - 15.0;
        height = self.contentLabel_1.frame.origin.y + self.contentLabel_1.frame.size.height + 10.0;
        self.baseCommentView.frame = CGRectMake(x, y, width, height);



        self.nameLable_2.hidden = YES;
        self.timeLabel_2.hidden = YES;
        self.contentLabel_2.hidden = YES;
        self.lineView_2.hidden = YES;

        self.nameLable_3.hidden = YES;
        self.timeLabel_3.hidden = YES;
        self.contentLabel_3.hidden = YES;
        self.lineView_3.hidden = YES;

        self.queryAllCommLabel.hidden = YES;

        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = 56.0 + content0_SizeHeight + 25.0 + 35.0 + content1_SizeHeight + 10.0 + 10.0;
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);


    }
    else if (object.replyTotal == 2) {

        self.arrowImage.hidden = NO;
        self.baseCommentView.hidden = NO;

        self.nameLable_1.hidden = NO;
        self.timeLabel_1.hidden = NO;
        self.contentLabel_1.hidden = NO;
        self.lineView_1.hidden = NO;

        {

            /* 评论的评论小箭头 */
            UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
            x = 35.0;
            y = y + height ;
            width = tryDetailArrow.size.width;
            height = tryDetailArrow.size.height;
            tryDetailArrow_SizeHeight = tryDetailArrow.size.height;
            self.arrowImage.image = tryDetailArrow;
            self.arrowImage.frame = CGRectMake(x, y, width, height);


            iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_1 = [object._replyList objectAtIndex:0];

            /* 评论的评论第一条 用户昵称区域 */
            x = 10.0;
            y = 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name1_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};

            NSString *name1_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_detail_comment_ReplyList_object_1.nickName,_detail_comment_ReplyList_object_1.atNickName];
            self.nameLable_1.attributedText = [name1_strStyle attributedStringWithStyleBook:name1_Style];
            self.nameLable_1.frame = CGRectMake(x, y, width, height);

            /* 评论的评论第一条 时间 */
            x = 205.0;
            y = 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_1 = [iEver_Global commentIntervalSinceNow:_detail_comment_ReplyList_object_1.createTime/1000];
            self.timeLabel_1.text = replyCommentTimeStr_1;
            self.timeLabel_1.frame = CGRectMake(x, y, width, height);

            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

                iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

                superVC.comment_parentId = _detail_comment_ReplyList_object_1.parent_id;
                superVC.comment_atUserId = _detail_comment_ReplyList_object_1.userId;
                superVC.toReplyCommentName = _detail_comment_ReplyList_object_1.nickName;
                [superVC insertCommentToReplyComment];
                NSLog(@"insertComment");

            }]};
            NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize1 =[_detail_comment_ReplyList_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize1.height;
            content1_SizeHeight = contentSize1.height;
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_detail_comment_ReplyList_object_1.commentContent];
            self.contentLabel_1.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            self.contentLabel_1.frame = CGRectMake(x, y, width, height);


            /* 第一条评论的评论下的分割线 当有第二条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_1.frame.origin.y + self.contentLabel_1.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_1.frame = CGRectMake(x, y, width, height);


        }


        self.nameLable_2.hidden = NO;
        self.timeLabel_2.hidden = NO;
        self.contentLabel_2.hidden = NO;
        self.lineView_2.hidden = YES;

        {

            iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_2 = [object._replyList objectAtIndex:1];

            /* 评论的评论第二条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name2_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};

            NSString *name2_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_detail_comment_ReplyList_object_2.nickName,_detail_comment_ReplyList_object_2.atNickName];
            self.nameLable_2.attributedText = [name2_strStyle attributedStringWithStyleBook:name2_Style];
            self.nameLable_2.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第二条 时间 */
            x = 205.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_2 = [iEver_Global commentIntervalSinceNow:_detail_comment_ReplyList_object_2.createTime/1000];
            self.timeLabel_2.text = replyCommentTimeStr_2;
            self.timeLabel_2.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第二条 评论评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

                iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

                superVC.comment_parentId = _detail_comment_ReplyList_object_2.parent_id;
                superVC.comment_atUserId = _detail_comment_ReplyList_object_2.userId;
                superVC.toReplyCommentName = _detail_comment_ReplyList_object_2.nickName;
                [superVC insertCommentToReplyComment];
                NSLog(@"insertComment");

            }]};
            NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize2 =[_detail_comment_ReplyList_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize2.height;
            content2_SizeHeight = contentSize2.height;
            self.contentLabel_2.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_detail_comment_ReplyList_object_2.commentContent];
            self.contentLabel_2.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];

        }



        self.nameLable_3.hidden = YES;
        self.timeLabel_3.hidden = YES;
        self.contentLabel_3.hidden = YES;
        self.lineView_3.hidden = YES;

        self.queryAllCommLabel.hidden = YES;

        /* 评论的评论 背景灰色块 */
        x = 15.0;
        y = self.arrowImage.frame.origin.y + tryDetailArrow_SizeHeight;
        width = ScreenWidth - 15.0 - 15.0;
        height = self.contentLabel_2.frame.origin.y + self.contentLabel_2.frame.size.height + 10.0;
        self.baseCommentView.frame = CGRectMake(x, y, width, height);


        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = 56.0 + content0_SizeHeight + 25.0 + 35.0 + content1_SizeHeight  + 35.0 + content2_SizeHeight + 10.0 + 10.0 + 10.0;
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);

    }
    else if (object.replyTotal == 3) {

        self.arrowImage.hidden = NO;
        self.baseCommentView.hidden = NO;

        self.nameLable_1.hidden = NO;
        self.timeLabel_1.hidden = NO;
        self.contentLabel_1.hidden = NO;
        self.lineView_1.hidden = NO;

        {

            /* 评论的评论小箭头 */
            UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
            x = 35.0;
            y = y + height;
            width = tryDetailArrow.size.width;
            height = tryDetailArrow.size.height;
            tryDetailArrow_SizeHeight = tryDetailArrow.size.height;
            self.arrowImage.image = tryDetailArrow;
            self.arrowImage.frame = CGRectMake(x, y, width, height);


            iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_1 = [object._replyList objectAtIndex:0];

            /* 评论的评论第一条 用户昵称区域 */
            x = 10.0;
            y = 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name1_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};

            NSString *name1_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_detail_comment_ReplyList_object_1.nickName,_detail_comment_ReplyList_object_1.atNickName];
            self.nameLable_1.attributedText = [name1_strStyle attributedStringWithStyleBook:name1_Style];
            self.nameLable_1.frame = CGRectMake(x, y, width, height);

            /* 评论的评论第一条 时间 */
            x = 205.0;
            y = 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_1 = [iEver_Global commentIntervalSinceNow:_detail_comment_ReplyList_object_1.createTime/1000];
            self.timeLabel_1.text = replyCommentTimeStr_1;
            self.timeLabel_1.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

                iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

                superVC.comment_parentId = _detail_comment_ReplyList_object_1.parent_id;
                superVC.comment_atUserId = _detail_comment_ReplyList_object_1.userId;
                superVC.toReplyCommentName = _detail_comment_ReplyList_object_1.nickName;
                [superVC insertCommentToReplyComment];
                NSLog(@"insertComment");

            }]};
            NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize1 =[_detail_comment_ReplyList_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize1.height;
            content1_SizeHeight = contentSize1.height;
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_detail_comment_ReplyList_object_1.commentContent];
            self.contentLabel_1.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            self.contentLabel_1.frame = CGRectMake(x, y, width, height);


            /* 第一条评论的评论下的分割线 当有第二条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_1.frame.origin.y + self.contentLabel_1.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_1.frame = CGRectMake(x, y, width, height);


        }


        self.nameLable_2.hidden = NO;
        self.timeLabel_2.hidden = NO;
        self.contentLabel_2.hidden = NO;
        self.lineView_2.hidden = NO;

        {

            iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_2 = [object._replyList objectAtIndex:1];

            /* 评论的评论第二条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name2_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};

            NSString *name2_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_detail_comment_ReplyList_object_2.nickName,_detail_comment_ReplyList_object_2.atNickName];
            self.nameLable_2.attributedText = [name2_strStyle attributedStringWithStyleBook:name2_Style];
            self.nameLable_2.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第二条 时间 */
            x = 205.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_2 = [iEver_Global commentIntervalSinceNow:_detail_comment_ReplyList_object_2.createTime/1000];
            self.timeLabel_2.text = replyCommentTimeStr_2;
            self.timeLabel_2.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第二条 评论评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

                iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

                superVC.comment_parentId = _detail_comment_ReplyList_object_2.parent_id;
                superVC.comment_atUserId = _detail_comment_ReplyList_object_2.userId;
                superVC.toReplyCommentName = _detail_comment_ReplyList_object_2.nickName;
                [superVC insertCommentToReplyComment];
                NSLog(@"insertComment");

            }]};
            NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize2 =[_detail_comment_ReplyList_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize2.height;
            content2_SizeHeight = contentSize2.height;
            self.contentLabel_2.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_detail_comment_ReplyList_object_2.commentContent];
            self.contentLabel_2.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];

            /* 第二条评论的评论下的分割线 当有第三条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_2.frame.origin.y + self.contentLabel_2.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_2.frame = CGRectMake(x, y, width, height);

        }



        self.nameLable_3.hidden = NO;
        self.timeLabel_3.hidden = NO;
        self.contentLabel_3.hidden = NO;
        self.lineView_3.hidden = NO;

        {

            iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_3 = [object._replyList objectAtIndex:2];

            /* 评论的评论第三条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_2.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name3_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};

            NSString *name3_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_detail_comment_ReplyList_object_3.nickName,_detail_comment_ReplyList_object_3.atNickName];
            self.nameLable_3.attributedText = [name3_strStyle attributedStringWithStyleBook:name3_Style];
            self.nameLable_3.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第三条 时间 */
            x = 205.0;
            y = self.lineView_2.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_3 = [iEver_Global commentIntervalSinceNow:_detail_comment_ReplyList_object_3.createTime/1000];
            self.timeLabel_3.text = replyCommentTimeStr_3;
            self.timeLabel_3.frame = CGRectMake(x, y, width, height);

            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

                iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

                superVC.comment_parentId = _detail_comment_ReplyList_object_3.parent_id;
                superVC.comment_atUserId = _detail_comment_ReplyList_object_3.userId;
                superVC.toReplyCommentName = _detail_comment_ReplyList_object_3.nickName;
                [superVC insertCommentToReplyComment];
                NSLog(@"insertComment");

            }]};
            NSDictionary * contentdic3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize3 =[_detail_comment_ReplyList_object_3.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic3 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize3.height;
            content3_SizeHeight = contentSize3.height;
            self.contentLabel_3.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_detail_comment_ReplyList_object_3.commentContent];
            self.contentLabel_3.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];

        }


        self.queryAllCommLabel.hidden = YES;

        /* 评论的评论 背景灰色块 */
        x = 15.0;
        y = self.arrowImage.frame.origin.y + tryDetailArrow_SizeHeight;
        width = ScreenWidth - 15.0 - 15.0;
        height = self.contentLabel_3.frame.origin.y + self.contentLabel_3.frame.size.height + 10.0;
        self.baseCommentView.frame = CGRectMake(x, y, width, height);


        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = 56.0 + content0_SizeHeight + 25.0 + 35.0 + content1_SizeHeight  + 35.0 + content2_SizeHeight + 35.0 + content3_SizeHeight + 10.0 + 10.0 + 10.0 + 10.0 ;
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);


    }
    else if (object.replyTotal > 3){

        self.arrowImage.hidden = NO;
        self.baseCommentView.hidden = NO;

        self.nameLable_1.hidden = NO;
        self.timeLabel_1.hidden = NO;
        self.contentLabel_1.hidden = NO;
        self.lineView_1.hidden = NO;

        {

            /* 评论的评论小箭头 */
            UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
            x = 35.0;
            y = y + height ;
            width = tryDetailArrow.size.width;
            height = tryDetailArrow.size.height;
            tryDetailArrow_SizeHeight = tryDetailArrow.size.height;
            self.arrowImage.image = tryDetailArrow;
            self.arrowImage.frame = CGRectMake(x, y, width, height);


            iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_1 = [object._replyList objectAtIndex:0];

            /* 评论的评论第一条 用户昵称区域 */
            x = 10.0;
            y = 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name1_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};

            NSString *name1_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_detail_comment_ReplyList_object_1.nickName,_detail_comment_ReplyList_object_1.atNickName];
            self.nameLable_1.attributedText = [name1_strStyle attributedStringWithStyleBook:name1_Style];
            self.nameLable_1.frame = CGRectMake(x, y, width, height);

            /* 评论的评论第一条 时间 */
            x = 205.0;
            y = 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_1 = [iEver_Global commentIntervalSinceNow:_detail_comment_ReplyList_object_1.createTime/1000];
            self.timeLabel_1.text = replyCommentTimeStr_1;
            self.timeLabel_1.frame = CGRectMake(x, y, width, height);

            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

                iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

                superVC.comment_parentId = _detail_comment_ReplyList_object_1.parent_id;
                superVC.comment_atUserId = _detail_comment_ReplyList_object_1.userId;
                superVC.toReplyCommentName = _detail_comment_ReplyList_object_1.nickName;
                [superVC insertCommentToReplyComment];
                NSLog(@"insertComment");

            }]};
            NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize1 =[_detail_comment_ReplyList_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize1.height;
            content1_SizeHeight = contentSize1.height;
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_detail_comment_ReplyList_object_1.commentContent];
            self.contentLabel_1.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            self.contentLabel_1.frame = CGRectMake(x, y, width, height);


            /* 第一条评论的评论下的分割线 当有第二条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_1.frame.origin.y + self.contentLabel_1.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_1.frame = CGRectMake(x, y, width, height);


        }


        self.nameLable_2.hidden = NO;
        self.timeLabel_2.hidden = NO;
        self.contentLabel_2.hidden = NO;
        self.lineView_2.hidden = NO;

        {

            iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_2 = [object._replyList objectAtIndex:1];

            /* 评论的评论第二条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name2_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};

            NSString *name2_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_detail_comment_ReplyList_object_2.nickName,_detail_comment_ReplyList_object_2.atNickName];
            self.nameLable_2.attributedText = [name2_strStyle attributedStringWithStyleBook:name2_Style];
            self.nameLable_2.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第二条 时间 */
            x = 205.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_2 = [iEver_Global commentIntervalSinceNow:_detail_comment_ReplyList_object_2.createTime/1000];
            self.timeLabel_2.text = replyCommentTimeStr_2;
            self.timeLabel_2.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第二条 评论评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

                iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

                superVC.comment_parentId = _detail_comment_ReplyList_object_2.parent_id;
                superVC.comment_atUserId = _detail_comment_ReplyList_object_2.userId;
                superVC.toReplyCommentName = _detail_comment_ReplyList_object_2.nickName;
                [superVC insertCommentToReplyComment];
                NSLog(@"insertComment");

            }]};
            NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize2 =[_detail_comment_ReplyList_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize2.height;
            content2_SizeHeight = contentSize2.height;
            self.contentLabel_2.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_detail_comment_ReplyList_object_2.commentContent];
            self.contentLabel_2.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];

            /* 第二条评论的评论下的分割线 当有第三条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_2.frame.origin.y + self.contentLabel_2.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_2.frame = CGRectMake(x, y, width, height);

        }



        self.nameLable_3.hidden = NO;
        self.timeLabel_3.hidden = NO;
        self.contentLabel_3.hidden = NO;
        self.lineView_3.hidden = NO;

        {

            iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_3 = [object._replyList objectAtIndex:2];

            /* 评论的评论第三条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_2.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name3_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};

            NSString *name3_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_detail_comment_ReplyList_object_3.nickName,_detail_comment_ReplyList_object_3.atNickName];
            self.nameLable_3.attributedText = [name3_strStyle attributedStringWithStyleBook:name3_Style];
            self.nameLable_3.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第三条 时间 */
            x = 205.0;
            y = self.lineView_2.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_3 = [iEver_Global commentIntervalSinceNow:_detail_comment_ReplyList_object_3.createTime/1000];
            self.timeLabel_3.text = replyCommentTimeStr_3;
            self.timeLabel_3.frame = CGRectMake(x, y, width, height);


            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

                iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

                superVC.comment_parentId = _detail_comment_ReplyList_object_3.parent_id;
                superVC.comment_atUserId = _detail_comment_ReplyList_object_3.userId;
                superVC.toReplyCommentName = _detail_comment_ReplyList_object_3.nickName;
                [superVC insertCommentToReplyComment];
                NSLog(@"insertComment");

            }]};
            NSDictionary * contentdic3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize3 =[_detail_comment_ReplyList_object_3.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic3 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize3.height;
            content3_SizeHeight = contentSize3.height;
            self.contentLabel_3.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_detail_comment_ReplyList_object_3.commentContent];
            self.contentLabel_3.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];

            /* 第三条评论的评论下的分割线 当有超过三条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_3.frame.origin.y + self.contentLabel_3.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_3.frame = CGRectMake(x, y, width, height);

        }

        NSDictionary* queryAllCommLabel_StyleDic = @{@"queryAllComm":[WPAttributedStyleAction styledActionWithAction:^{

            iEver_itemCommentReplyDetailViewController *_CommentDetailViewController = [[iEver_itemCommentReplyDetailViewController alloc] init];
            _CommentDetailViewController.comment_id = object.Comment_id;
            _CommentDetailViewController.itemCommentList_object = object;
            UIViewController *superVC = (UIViewController *)[self firstAvailableUIViewController];
            [superVC.navigationController pushViewController:_CommentDetailViewController animated:YES];
            if ([superVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                superVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
            }
        }]};
        
        /* 第三条评论的评论下的查看更多评论评论 当有超过三条评论的评论才会出现 */
        x = 0.0;
        y = self.lineView_3.frame.origin.y + 1.0 ;
        width = ScreenWidth - 15.0 - 15.0 - 20.0;
        height = 38.0;
        self.queryAllCommLabel.frame = CGRectMake(x, y, width, height);
        NSString *queryAllCommLabel_strStyle = [NSString stringWithFormat:@"<queryAllComm>查看全部评论（%d）</queryAllComm>",object.replyTotal];
        self.queryAllCommLabel.attributedText = [queryAllCommLabel_strStyle attributedStringWithStyleBook:queryAllCommLabel_StyleDic];
        self.queryAllCommLabel.hidden = NO;
        
        
        /* 评论的评论 背景灰色块 */
        x = 15.0;
        y = self.arrowImage.frame.origin.y + tryDetailArrow_SizeHeight;
        width = ScreenWidth - 15.0 - 15.0;
        height = self.contentLabel_3.frame.origin.y + self.contentLabel_3.frame.size.height + 38.0 + 10.0 ;
        self.baseCommentView.frame = CGRectMake(x, y, width, height);
        
        
        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = 56.0 + content0_SizeHeight + 25.0 + 35.0 + content1_SizeHeight  + 35.0 + content2_SizeHeight + 35.0 + content3_SizeHeight + 10.0  + 38.0 + 10.0 + 10.0 + 10.0;
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);
    }
    
    self.bottomView.backgroundColor = WHITE;
    
    
    NSLog(@"self.bottomView.frame.size.height----->%f",self.bottomView.frame.size.height);
    
}



+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    float return_height = 0.0;

    iEver_itemCommentList_object *detail_Comment_Object = (iEver_itemCommentList_object *)object;

    /* 获取评论文字高度 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[detail_Comment_Object.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    if (detail_Comment_Object.replyTotal == 0) {

        /* 56.0是头像36+间隔20； contentSize.height 内容占据的高度；25.0 评论记录底部间距 */
        return_height = 56.0 + contentSize.height + 25.0;
    }
    else if (detail_Comment_Object.replyTotal == 1) {

        iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_1 = [detail_Comment_Object._replyList objectAtIndex:0];
        /* 评论的评论第一条 评论评论 */
        NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize1 =[_detail_comment_ReplyList_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;

        /* 56.0是头像36+间隔20； contentSize.height 内容占据的高度；25.0 评论记录底部间距/距离气泡 ；35.0 name部分占据的位置；contentSize1.height 第一条评论；10.0 第一条评论距离灰色色块间隔 ； 10 色块距离cell分割线 */
        return_height = 56.0 + contentSize.height + 25.0 + 35.0 + contentSize1.height + 10.0 + 10.0;

    }
    else if (detail_Comment_Object.replyTotal == 2) {

        iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_1 = [detail_Comment_Object._replyList objectAtIndex:0];
        /* 评论的评论第一条 评论评论 */
        NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize1 =[_detail_comment_ReplyList_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;

        iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_2 = [detail_Comment_Object._replyList objectAtIndex:1];
        /* 评论的评论第二条 评论评论 */
        NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize2 =[_detail_comment_ReplyList_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;

        /* 56.0是头像36+间隔20； contentSize.height 内容占据的高度；25.0 评论记录底部间距/距离气泡 ；35.0 name部分占据的位置；contentSize1.height 第一条评论；10.0 第一条评论距离灰色色块间隔 ； 10 色块距离cell分割线 */
        return_height = 56.0 + contentSize.height + 25.0 + 35.0 + contentSize1.height  + 35.0 + contentSize2.height + 10.0 + 10.0 + 10.0;
    }
    else if (detail_Comment_Object.replyTotal == 3) {

        iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_1 = [detail_Comment_Object._replyList objectAtIndex:0];
        /* 评论的评论第一条 评论评论 */
        NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize1 =[_detail_comment_ReplyList_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;

        iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_2 = [detail_Comment_Object._replyList objectAtIndex:1];
        /* 评论的评论第二条 评论评论 */
        NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize2 =[_detail_comment_ReplyList_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;

        iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_3 = [detail_Comment_Object._replyList objectAtIndex:2];
        /* 评论的评论第三条 评论评论 */
        NSDictionary * contentdic3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize3 =[_detail_comment_ReplyList_object_3.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic3 context:nil].size;

        /* 56.0是头像36+间隔20； contentSize.height 内容占据的高度；25.0 评论记录底部间距/距离气泡 ；35.0 name部分占据的位置；contentSize1.height 第一条评论；10.0 第一条评论距离灰色色块间隔 ； 10 色块距离cell分割线 */
        return_height = 56.0 + contentSize.height + 25.0 + 35.0 + contentSize1.height  + 35.0 + contentSize2.height + 35.0 + contentSize3.height + 10.0 + 10.0+ 10.0 + 10.0;
    }

    else if (detail_Comment_Object.replyTotal > 3) {

        iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_1 = [detail_Comment_Object._replyList objectAtIndex:0];
        /* 评论的评论第一条 评论评论 */
        NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize1 =[_detail_comment_ReplyList_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;

        iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_2 = [detail_Comment_Object._replyList objectAtIndex:1];
        /* 评论的评论第二条 评论评论 */
        NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize2 =[_detail_comment_ReplyList_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;

        iEver_itemComment_ReplyList_object *_detail_comment_ReplyList_object_3 = [detail_Comment_Object._replyList objectAtIndex:2];
        /* 评论的评论第三条 评论评论 */
        NSDictionary * contentdic3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize3 =[_detail_comment_ReplyList_object_3.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic3 context:nil].size;

        /* 56.0是头像36+间隔20； contentSize.height 内容占据的高度；25.0 评论记录底部间距/距离气泡 ；35.0 name部分占据的位置；contentSize1.height 第一条评论；10.0 第一条评论距离灰色色块间隔 ； 10 色块距离cell分割线 */
        return_height = 56.0 + contentSize.height + 25.0 + 35.0 + contentSize1.height  + 35.0 + contentSize2.height + 35.0 + contentSize3.height + 10.0  + 38.0 + 10.0+ 10.0 + 10.0;
    }
    
    NSLog(@"return_height----->%f",return_height);
    
    return return_height;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


/* 整块背景视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = WHITE;
    }
    return _bottomView;
}

/* 用户图片 */
- (UIImageView *)photoImage
{
    if (!_photoImage) {
        _photoImage = [[UIImageView alloc]init];
        CALayer *photoLayer = [_photoImage layer];
        [photoLayer setMasksToBounds:YES];
        [photoLayer setCornerRadius:18.0];
    }
    return _photoImage;
}

/* 用户名称 */
- (UILabel *)nameLabel
{
    if (!_nameLabel){
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.numberOfLines = 1;
        _nameLabel.backgroundColor = CLEAR;
        _nameLabel.textColor = MY_PURPLECOLOR;
        _nameLabel.font = TextFont;
    }
    return _nameLabel;
}

/* 用户定制信息 */
- (UILabel *)featureLabel
{
    if (!_featureLabel){
        _featureLabel = [[UILabel alloc] init];
        _featureLabel.numberOfLines = 1;
        _featureLabel.backgroundColor = CLEAR;
        _featureLabel.textColor = GRAY_LIGHT;
        _featureLabel.font = TextDESCFonts;
    }
    return _featureLabel;
}

/* 评论发布时间 */
- (UILabel *)timeLabel
{
    if (!_timeLabel){
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.numberOfLines = 1;
        _timeLabel.backgroundColor = CLEAR;
        _timeLabel.textColor = GRAY_LIGHT;
        _timeLabel.textAlignment = NSTextAlignmentRight;
        _timeLabel.font = TextDESCFonts;
    }
    return _timeLabel;
}
/* 评论内容 */
- (UILabel *)contentLabel
{
    if (!_contentLabel){
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.numberOfLines = 0;
        _contentLabel.backgroundColor = CLEAR;
        _contentLabel.textColor = BLACK;
        _contentLabel.font = TextFont;
    }
    return _contentLabel;
}
/*回复按钮*/
- (UIButton *)replyButton
{
    if (!_replyButton){
        _replyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_replyButton addTarget:self action:@selector(replyAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _replyButton;
}

-(void)replyAction:(id)sender{

    iEver_itemCommentViewController *superVC = (iEver_itemCommentViewController *)[self firstAvailableUIViewController];

    superVC.comment_parentId = _list_object.Comment_id;
    superVC.comment_atUserId = _list_object.userId;
    superVC.toReplyCommentName = _list_object.nickName;
    [superVC insertCommentToReplyComment];

}
/*喜欢按钮*/
- (MCFireworksButton *)likeButton
{
    if (!_likeButton){
        _likeButton = [MCFireworksButton buttonWithType:UIButtonTypeCustom];
        self.likeButton.particleImage = [UIImage imageNamed:@"Sparkle"];
        self.likeButton.particleScale = 0.05;
        self.likeButton.particleScaleRange = 0.02;
        [_likeButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeButton;
}

-(void)likeAction:(id)sender{

    iEver_likeActionObject * content = [[iEver_likeActionObject alloc] init];
    if (!_list_object._selected) {

        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_list_object.Comment_id],
                              @"type": [NSNumber numberWithInt:21],
                              };

        [[[content like:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {

             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                 _list_object._selected = !_list_object._selected;
                 _list_object.likeTotal = _list_object.likeTotal +1;
                 [self.likeButton popOutsideWithDuration:0.5];
                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike_click"] forState:UIControlStateNormal];
                 self.likeLabel.text = [NSString stringWithFormat:@"赞(%d)",_list_object.likeTotal];

                 [self.likeButton animate];

             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
    else {

        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_list_object.Comment_id],
                              @"type": [NSNumber numberWithInt:21],
                              };

        [[[content unlike:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {

             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                 _list_object._selected = !_list_object._selected;
                 _list_object.likeTotal = _list_object.likeTotal -1;
                 [self.likeButton popInsideWithDuration:0.4];
                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike"] forState:UIControlStateNormal];
                 self.likeLabel.text = [NSString stringWithFormat:@"赞(%d)",_list_object.likeTotal];

             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
}

/* 评论点赞数量  */
- (UILabel *)likeLabel
{
    if (!_likeLabel){
        _likeLabel = [[UILabel alloc] init];
        _likeLabel.numberOfLines = 1;
        _likeLabel.backgroundColor = CLEAR;
        _likeLabel.textColor = BLACK;
        _likeLabel.font = TextFonts;
    }
    return _likeLabel;
}

/* 评论下面箭头 */
- (UIImageView *)arrowImage
{
    if (!_arrowImage) {
        _arrowImage = [[UIImageView alloc]init];
        _arrowImage.backgroundColor = CLEAR;
    }
    return _arrowImage;
}

/* 对评论做评论的灰色背景 */
- (UIView *)baseCommentView
{
    if (!_baseCommentView) {
        _baseCommentView = [[UIView alloc]init];
        _baseCommentView.backgroundColor = TYR_BACKGRAYCOLOR;
    }
    return _baseCommentView;
}

/* 评论的评论者名称_1 */
- (UILabel *)nameLable_1
{
    if (!_nameLable_1){
        _nameLable_1 = [[UILabel alloc] init];
        _nameLable_1.numberOfLines = 1;
        _nameLable_1.backgroundColor = CLEAR;
        _nameLable_1.textColor = GRAY_LIGHT;
        _nameLable_1.textAlignment = NSTextAlignmentLeft;
        _nameLable_1.font = TextDESCFonts;
    }
    return _nameLable_1;
}

/* 评论的评论评论时间_1 */
- (UILabel *)timeLabel_1
{
    if (!_timeLabel_1){
        _timeLabel_1 = [[UILabel alloc] init];
        _timeLabel_1.numberOfLines = 0;
        _timeLabel_1.backgroundColor = CLEAR;
        _timeLabel_1.textColor = GRAY_LIGHT;
        _timeLabel_1.textAlignment = NSTextAlignmentRight;
        _timeLabel_1.font = TextDESCFonts;
    }
    return _timeLabel_1;
}

/* 评论的评论内容_1 */
- (WPHotspotLabel *)contentLabel_1
{
    if (!_contentLabel_1){
        _contentLabel_1 = [[WPHotspotLabel alloc] init];
        _contentLabel_1.numberOfLines = 0;
        _contentLabel_1.backgroundColor = CLEAR;
        _contentLabel_1.textColor = BLACK;
        _contentLabel_1.font = TextFont;
    }
    return _contentLabel_1;
}

/* 评论的评论视图横线_1 */
- (UIImageView *)lineView_1
{
    if (!_lineView_1) {
        _lineView_1 = [[UIImageView alloc]init];
        _lineView_1.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView_1;
}

/* 评论的评论者名称_2 */
- (UILabel *)nameLable_2
{
    if (!_nameLable_2){
        _nameLable_2 = [[UILabel alloc] init];
        _nameLable_2.numberOfLines = 1;
        _nameLable_2.backgroundColor = CLEAR;
        _nameLable_2.textColor = GRAY_LIGHT;
        _nameLable_2.textAlignment = NSTextAlignmentLeft;
        _nameLable_2.font = TextDESCFonts;
    }
    return _nameLable_2;
}

/* 评论的评论评论时间_2 */
- (UILabel *)timeLabel_2
{
    if (!_timeLabel_2){
        _timeLabel_2 = [[UILabel alloc] init];
        _timeLabel_2.numberOfLines = 0;
        _timeLabel_2.backgroundColor = CLEAR;
        _timeLabel_2.textColor = GRAY_LIGHT;
        _timeLabel_2.textAlignment = NSTextAlignmentRight;
        _timeLabel_2.font = TextDESCFonts;
    }
    return _timeLabel_2;
}

/* 评论的评论内容_2 */
- (WPHotspotLabel *)contentLabel_2
{
    if (!_contentLabel_2){
        _contentLabel_2 = [[WPHotspotLabel alloc] init];
        _contentLabel_2.numberOfLines = 0;
        _contentLabel_2.backgroundColor = CLEAR;
        _contentLabel_2.textColor = BLACK;
        _contentLabel_2.font = TextFont;
    }
    return _contentLabel_2;
}

/* 评论的评论视图横线_2 */
- (UIImageView *)lineView_2
{
    if (!_lineView_2) {
        _lineView_2 = [[UIImageView alloc]init];
        _lineView_2.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView_2;
}


/* 评论的评论者名称_3 */
- (UILabel *)nameLable_3
{
    if (!_nameLable_3){
        _nameLable_3 = [[UILabel alloc] init];
        _nameLable_3.numberOfLines = 1;
        _nameLable_3.backgroundColor = CLEAR;
        _nameLable_3.textColor = GRAY_LIGHT;
        _nameLable_3.textAlignment = NSTextAlignmentLeft;
        _nameLable_3.font = TextDESCFonts;
    }
    return _nameLable_3;
}

/* 评论的评论评论时间_3 */
- (UILabel *)timeLabel_3
{
    if (!_timeLabel_3){
        _timeLabel_3 = [[UILabel alloc] init];
        _timeLabel_3.numberOfLines = 0;
        _timeLabel_3.backgroundColor = CLEAR;
        _timeLabel_3.textColor = GRAY_LIGHT;
        _timeLabel_3.textAlignment = NSTextAlignmentRight;
        _timeLabel_3.font = TextDESCFonts;
    }
    return _timeLabel_3;
}

/* 评论的评论内容_3 */
- (WPHotspotLabel *)contentLabel_3
{
    if (!_contentLabel_3){
        _contentLabel_3 = [[WPHotspotLabel alloc] init];
        _contentLabel_3.numberOfLines = 0;
        _contentLabel_3.backgroundColor = CLEAR;
        _contentLabel_3.textColor = BLACK;
        _contentLabel_3.font = TextFont;
    }
    return _contentLabel_3;
}

/* 评论的评论视图横线_3 */
- (UIImageView *)lineView_3
{
    if (!_lineView_3) {
        _lineView_3 = [[UIImageView alloc]init];
        _lineView_3.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView_3;
}

/* 查看更多评论 */
- (WPHotspotLabel *)queryAllCommLabel
{
    if (!_queryAllCommLabel){
        _queryAllCommLabel = [[WPHotspotLabel alloc] init];
        _queryAllCommLabel.numberOfLines = 0;
        _queryAllCommLabel.backgroundColor = CLEAR;
        _queryAllCommLabel.textColor = BLACK;
        _queryAllCommLabel.font = TextFont;
        _queryAllCommLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _queryAllCommLabel;
}

/* 视图横线 */
- (UIImageView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIImageView alloc]init];
        _lineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView;
}



@end

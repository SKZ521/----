//
//  iEver_mainOfficialCell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-19.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_mainContent.h"
@class iEver_mainContent;
@interface iEver_mainOfficialCell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_mainContentList_object *object;
@property (nonatomic, strong) UIImageView   *coverImage;       /*封面图片*/
@end

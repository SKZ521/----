//
//  iEver_specialBanner.h
//  FaceTalk
//
//  Created by kevin on 15/3/29.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCPagerView.h"

@class iEver_mainContent;

@interface iEver_specialBanner : UIView

@property(nonatomic,strong)iEver_mainContent *object;

@property (strong, nonatomic) UIScrollView    *scrollView;

@property (strong, nonatomic) UIView          *baseColorView;
@property (strong, nonatomic) UILabel         *lastSpecialTitle;

@property (strong, nonatomic) UILabel         *crrentSpecialTitle;

@property (strong, nonatomic) UILabel         *nextSpecialTitle;
@property (strong, nonatomic) MCPagerView     *pagerView;

+ (CGFloat)heightForSpecialBanner;

@end

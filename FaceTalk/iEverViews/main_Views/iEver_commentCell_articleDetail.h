//
//  iEver_commentCell_articleDetail.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-24.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_mainDetail_Object.h"

@interface iEver_commentCell_articleDetail : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_detail_articleCommentList_object *object;

@end

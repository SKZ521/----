//
//  iEver_MainiAdView.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_MainiAdView.h"
#import "iEver_mainiADObject.h"
#import "iEver_IAD.h"
#import "iEver_OfficialDetailViewController.h"
#import "iEver_ItemDetailViewController.h"
#import "iEver_ItemTryDetailViewController.h"
#import "iEver_V_ListDetailViewController.h"
#import "iEver_mainContent.h"
#import "MCPagerView.h"

@interface iEver_MainiAdView() <UIScrollViewDelegate,MCPagerViewDelegate>
{
	NSInteger  _pages;
	NSTimer    *_timer;
}
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) MCPagerView *pagerView;

@end
@implementation iEver_MainiAdView

#define KScrollViewHeight 180.0f
#define KActivityHeight 44.0f

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initiaSubViews];
    }
    return self;
}
- (void)initiaSubViews
{
	[self addSubview:self.scrollView];
    [self addSubview:self.pagerView];
}
- (void)setObject:(iEver_mainContent *)object
{
	if (![object._bannerList count]) {
		return;
	}
	[self addImageWithName:[object._bannerList lastObject] atPosition:0];
    
	_pages = [object._bannerList count];

	for (int i = 1; i <= _pages; i++) {
		iEver_IAD *iAd = [object._bannerList objectAtIndex:i - 1];
		[self addImageWithName:iAd atPosition:i];

	}
	[self addImageWithName:[object._bannerList objectAtIndex:0] atPosition:_pages + 1];

	[self.scrollView setContentSize:CGSizeMake(ScreenWidth * [object._bannerList count] + ScreenWidth * 2, KScrollViewHeight - 20 )];
	[self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth, 0, ScreenWidth,KScrollViewHeight) animated:NO];
    
    // Pager
    [_pagerView setImage:[UIImage imageNamed:@"main_list_point"]
        highlightedImage:[UIImage imageNamed:@"main_list_point_"]
                  forKey:@"a"];
    
    NSMutableString * string = [[NSMutableString alloc ] init];
    
    for (int i = 0; i < [object._bannerList count]; i ++ ) {
        [string appendFormat:@"a"];
    }
    
    [_pagerView setPattern:string];
    
    _pagerView.delegate = self;

	//start timer
	if (!_timer) {
		_timer =  [NSTimer scheduledTimerWithTimeInterval:5.0
												   target:self
												 selector:@selector(tick)
												 userInfo:nil
												  repeats:YES];
	}
    
    [self.pagerView setCenter:CGPointMake((self.scrollView.frame.size.width / 2.0 - _pages * 18 /2), self.scrollView.frame.size.height - 15)];
}
- (void)tick
{

	int page = _pagerView.page;
    page++;
    page = page > _pages - 1 ? 0 : page;
    _pagerView.page  = page;

    if (page == 0) {
        [self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth * ( page + 1), 0, ScreenWidth, KActivityHeight) animated:NO];
    }else
    {

        [UIView beginAnimations:nil context:nil];
        [UIView    setAnimationCurve: UIViewAnimationCurveLinear];
        [UIView    setAnimationDelegate:self];
        [UIView    setAnimationDuration:1.0];
        [self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth * ( page + 1), 0, ScreenWidth, KActivityHeight) animated:NO];
        [UIView commitAnimations];

    }

}

- (void)addImageWithName:(iEver_IAD *)iAd atPosition:(int)position

{
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.frame = CGRectMake(ScreenWidth* position, 0, ScreenWidth, KScrollViewHeight - 20);
	button.tag = position;
	[button setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",iAd.coverImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"banner_defualt"]];
	[[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {
        /* 文章 */
		if (iAd.homeType == 10) {

            iEver_OfficialDetailViewController *_officialDetailViewController = [[iEver_OfficialDetailViewController alloc]init];
            _officialDetailViewController.type_id = iAd.businessId;
            _officialDetailViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:_officialDetailViewController animated:YES];
            IOS7POP;

       }/* 达人 */
        if (iAd.homeType == 11) {
//            iEver_V_PersonCenterViewController *_V_personCenterViewController = [[iEver_V_PersonCenterViewController alloc]init];
//            _V_personCenterViewController.userId = iAd.businessId;
//            _V_personCenterViewController.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:_V_personCenterViewController animated:YES];
//            IOS7POP;
       }/* 活动 */
        if (iAd.homeType == 12) {
            iEver_ItemTryDetailViewController *_itemTryDetailViewController = [[iEver_ItemTryDetailViewController alloc]init];
            _itemTryDetailViewController.type_id = iAd.businessId;
            _itemTryDetailViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:_itemTryDetailViewController animated:YES];
            IOS7POP;
       }/* 商品 */
        if (iAd.homeType == 13) {
            iEver_ItemDetailViewController *_itemlDetailViewController = [[iEver_ItemDetailViewController alloc]init];
            _itemlDetailViewController.type_id = iAd.businessId;
            _itemlDetailViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:_itemlDetailViewController animated:YES];
            IOS7POP;
       }
        /* 达人文章 */
        if (iAd.homeType == 17) {
            iEver_V_ListDetailViewController *_V_ListDetailViewController = [[iEver_V_ListDetailViewController alloc]init];
            _V_ListDetailViewController.type_id = iAd.businessId;
            _V_ListDetailViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:_V_ListDetailViewController animated:YES];
            IOS7POP;
        }
        /* 15榜单 */
        if (iAd.homeType == 15) {
            [iEverAppDelegate shareAppDelegate].tabBarController.selectedIndex = 2;
        }
        /* 16活动列表 */
        if (iAd.homeType == 16) {
            [iEverAppDelegate shareAppDelegate].tabBarController.selectedIndex = 3;
        }
	}];
	[self.scrollView addSubview:button];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#pragma mark -
#pragma mark UIScrollView Delegate


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {

    [_timer invalidate];
    _timer = nil;

    if (!_timer) {
        _timer =  [NSTimer scheduledTimerWithTimeInterval:5.0
                                                   target:self
                                                 selector:@selector(tick)
                                                 userInfo:nil
                                                  repeats:YES];
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    int page = floor((self.scrollView.contentOffset.x - ScreenWidth	/ (_pages + 2)) / ScreenWidth) + 1;
    page--;  // 默认从第二页开始
    _pagerView.page = page;

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {

	NSLog(@"%f",scrollView.contentOffset.x);
	// The key is repositioning without animation

    //	NSInteger pages = [self.object.adsList count];

	if (scrollView.contentOffset.x == 0) {
		// user is scrolling to the left from image 1 to image 4
		// reposition offset to show image 4 that is on the right in the scroll view
		[scrollView scrollRectToVisible:CGRectMake(ScreenWidth * _pages, 0, ScreenWidth, KScrollViewHeight) animated:NO];
	}
	else if (scrollView.contentOffset.x == ScreenWidth * (_pages + 1)) {
		// user is scrolling to the right from image 4 to image 1
		// reposition offset to show image 1 that is on the left in the scroll view
		[scrollView scrollRectToVisible:CGRectMake(ScreenWidth, 0, ScreenWidth, KScrollViewHeight) animated:NO];
	}
    

}
#pragma mark -
#pragma mark layoutSubviews (update view)

- (void)layoutSubviews
{
	[super layoutSubviews];
    
	self.scrollView.frame = CGRectMake(0, 0, ScreenWidth, KScrollViewHeight);
    
    [self.pagerView setCenter:CGPointMake((self.scrollView.frame.size.width / 2.0 - _pages * 9 /2), self.scrollView.frame.size.height - 10)];

}


#pragma mark -
#pragma mark SET
- (UIScrollView *)scrollView
{
	if (!_scrollView) {
		_scrollView = [[UIScrollView alloc] init];
		_scrollView.pagingEnabled = YES;
		_scrollView.showsHorizontalScrollIndicator = NO;
		_scrollView.delegate = self;
	}

	return _scrollView;
}


- (MCPagerView *)pagerView
{
    if (!_pagerView) {
        _pagerView = [[MCPagerView alloc] init];
        
        
    }
    return _pagerView;
}

+ (CGFloat)heightForiAd
{
	float h = 0;

    h = KScrollViewHeight;

	return  h;
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
    }
}

- (void)pageView:(MCPagerView *)pageView didUpdateToPage:(NSInteger)newPage
{

}
@end

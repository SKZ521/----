//
//  iEver_MainiAdView.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>
@class iEver_mainContent;
@interface iEver_MainiAdView : UIView
@property(nonatomic,strong)iEver_mainContent *object;
@property(nonatomic,strong)UINavigationController *navigationController;
+ (CGFloat)heightForiAd;
@end

//
//  iEver_itemCommentReply_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/18.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_itemCommentReply_cell.h"
#import "NSString+WPAttributedMarkup.h"
#import "WPHotspotLabel.h"
#import "WPAttributedStyleAction.h"
#import "UIView+FindViewController.h"
#import "iEver_itemCommentReplyDetailViewController.h"

@interface iEver_itemCommentReply_cell()

@property (nonatomic, strong) UIView               *bottomView;     /* 背景视图 */
@property (nonatomic, strong) UIImageView          *photoImage;     /* 用户图片 */
@property (nonatomic, strong) UILabel              *nameLabel;      /* 用户名称 */
@property (nonatomic, strong) UILabel              *featureLabel;   /* 定制类型 */
@property (nonatomic, strong) UILabel              *timeLabel;      /* 发布时间 */
@property (nonatomic, strong) WPHotspotLabel       *contentLabel;   /* 消息内容 */
@property (nonatomic, strong) UIImageView          *lineView;       /* 视图横线 */

@end


@implementation iEver_itemCommentReply_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) configureCell {
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.bottomView];
        [self.bottomView addSubview:self.photoImage];
        [self.bottomView addSubview:self.nameLabel];
        [self.bottomView addSubview:self.featureLabel];
        [self.bottomView addSubview:self.timeLabel];
        [self.bottomView addSubview:self.contentLabel];
        [self.bottomView addSubview:self.lineView];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    /* 视图横线 */
    CGRect r = self.contentView.bounds;
    CGFloat x = 0.0;
    CGFloat y = r.size.height - 1;
    CGFloat height = 1.0;
    CGFloat width = ScreenWidth - 15.0 - 15.0;
    self.lineView.frame = CGRectMake(x, y, width, height);
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}



- (void)setObject:(iEver_icList_object *)object
{
    /* 用户图片 */
    CGFloat x = 10.0;
    CGFloat y = 10.0;
    CGFloat width = 36.0;
    CGFloat height = 36.0;
    CGFloat contentSizeHeight = 0.0;
    self.photoImage.frame = CGRectMake(x, y, width, height);
    [self.photoImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/72x",object.headImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    /* 用户名称 */
    x = 56.0;
    y = 10.0;
    width = 180.0;
    height = 20.0;
    self.nameLabel.frame = CGRectMake(x, y, width, height);
    self.nameLabel.text = object.nickName;

    /* 用户个性化 */
    x = 56.0;
    y = 30.0;
    width = 200.0;
    height = 20.0;
    self.featureLabel.frame = CGRectMake(x, y, width, height);
    if (object.feature.length == 0) {
        self.featureLabel.text = @"该用户尚未定制";
    }else{

        self.featureLabel.text = object.feature;
    }

    /* 发布时间 */
    x = ScreenWidth - 100.0 - 15.0 - 15.0 - 5.0;
    y = 10.0;
    width = 100.0;
    height = 20.0;
    NSString *str = [iEver_Global commentIntervalSinceNow:object.createTime/1000];
    self.timeLabel.frame = CGRectMake(x, y, width, height);
    self.timeLabel.text = str;

    /* 步骤描述 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];

    NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{

        iEver_itemCommentReplyDetailViewController *superVC = (iEver_itemCommentReplyDetailViewController *)[self firstAvailableUIViewController];

        superVC.comment_parentId = object.parent_id;
        superVC.comment_atUserId = object.userId;
        superVC.toReplyCommentName = object.nickName;
        [superVC insertCommentToReplyComment];
        // NSLog(@"insertComment");

    }],
                                            @"gray": [UIColor lightGrayColor],
                                            @"black": [UIColor blackColor],
                                            @"blue": [UIColor blueColor]};
    NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment><gray> [ 回复</gray><blue>%@</blue><gray> ] </gray>%@</insertComment>",object.atNickName,object.commentContent];

    NSString *commentContentStr = [NSString stringWithFormat:@" [ 回复%@ ] %@",object.atNickName,object.commentContent];

    CGSize contentSize =[commentContentStr boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 10.0 - 10.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    x = 10.0;
    y = 56.0;
    width = ScreenWidth - 15.0 - 15.0 - 10.0 - 10.0;
    height = contentSize.height;
    contentSizeHeight = contentSize.height;
    self.contentLabel.frame = CGRectMake(x, y, width, height);
    self.contentLabel.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];

    /* 背景视图 */
    x = 15.0;
    height = 56.0 + contentSizeHeight + 10.0 ;
    y = 0.0;
    width = ScreenWidth - 15.0 - 15.0;
    self.bottomView.frame = CGRectMake(x, y, width, height);

    NSLog(@"self.bottomView.frame.size.height---->%f",self.bottomView.frame.size.height);
    
}


+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    float return_height = 0.0;

    iEver_icList_object *pic_object = (iEver_icList_object *)object;

    /* 获取描述文字高度 */
    NSString *commentContentStr = [NSString stringWithFormat:@" [ 回复%@ ] %@",pic_object.atNickName,pic_object.commentContent];
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[commentContentStr boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 10.0 - 10.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    return_height = 56.0 + contentSize.height + 10.0;

    NSLog(@"return_height---->%f",return_height);

    return return_height;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


# pragma mark - view getters

/* 背景视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = TYR_BACKGRAYCOLOR;
    }
    return _bottomView;
}

/* 用户图片 */
- (UIImageView *)photoImage
{
    if (!_photoImage) {
        _photoImage = [[UIImageView alloc]init];
        CALayer *photoLayer = [_photoImage layer];
        [photoLayer setMasksToBounds:YES];
        [photoLayer setCornerRadius:18.0];
    }
    return _photoImage;
}

/* 用户名称 */
- (UILabel *)nameLabel
{
    if (!_nameLabel){
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.numberOfLines = 1;
        _nameLabel.backgroundColor = CLEAR;
        _nameLabel.textColor = BLACK;
        _nameLabel.font = TextFont;
    }
    return _nameLabel;
}
/* 肤质类型 */
- (UILabel *)featureLabel
{
    if (!_featureLabel){
        _featureLabel = [[UILabel alloc] init];
        _featureLabel.numberOfLines = 0;
        _featureLabel.backgroundColor = CLEAR;
        _featureLabel.textColor = GRAY_LIGHT;
        _featureLabel.font = TextDESCFonts;
    }
    return _featureLabel;
}
/* 发布时间 */
- (UILabel *)timeLabel
{
    if (!_timeLabel){
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.numberOfLines = 0;
        _timeLabel.backgroundColor = CLEAR;
        _timeLabel.textColor = GRAY_LIGHT;
        _timeLabel.textAlignment = NSTextAlignmentRight;
        _timeLabel.font = TextDESCFonts;
    }
    return _timeLabel;
}
/* 消息内容 */
- (WPHotspotLabel *)contentLabel
{
    if (!_contentLabel){
        _contentLabel = [[WPHotspotLabel alloc] init];
        _contentLabel.numberOfLines = 0;
        _contentLabel.backgroundColor = CLEAR;
        _contentLabel.textColor = BLACK;
        _contentLabel.font = TextFont;
    }
    return _contentLabel;
}

/* 视图横线 */
- (UIImageView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIImageView alloc]init];
        _lineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView;
}


@end

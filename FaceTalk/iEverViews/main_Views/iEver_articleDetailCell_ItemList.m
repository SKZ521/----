//
//  iEver_articleDetailCell_ItemList.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-22.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_articleDetailCell_ItemList.h"
#import "iEver_buyViewController.h"
#import "iEver_OfficialDetailViewController.h"
@interface iEver_articleDetailCell_ItemList()
@property (nonatomic, strong) UIView        *bottomView;      /* 背景视图 */
@property (nonatomic, strong) UIImageView   *itemImage;       /* 商品图片 */
@property (nonatomic, strong) UILabel       *titleLabel;      /* 商品标题 */
@property (nonatomic, strong) UILabel       *subTitleLabel;   /* 商品副标题 */
@property (nonatomic, strong) UILabel       *channelLabel;    /* 渠道名称 */
@property (nonatomic, strong) UILabel       *personTotalLabel;/* 使用人数 */
@property (nonatomic, strong) UIButton      *tryButton;       /* 试用链接 */
@property (nonatomic, strong) UIButton      *buyButton;       /* 购买链接 */
@property (nonatomic, strong) UIImageView   *lineView;        /* 视图横线 */
@property (nonatomic, strong) UINavigationController *officialDetailVC; /*cell VC*/
@property (nonatomic, strong) iEver_detail_itemList_object *datail_item_object;
@end

@implementation iEver_articleDetailCell_ItemList

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setViewController:(UINavigationController *)mvc{
    _officialDetailVC = mvc;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.itemImage];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.subTitleLabel];
        [self.contentView addSubview:self.channelLabel];
        [self.contentView addSubview:self.personTotalLabel];
        [self.contentView addSubview:self.tryButton];
        self.tryButton.hidden = YES;
        [self.contentView addSubview:self.buyButton];
        [self.contentView addSubview:self.lineView];

    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
	[super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setObject:(iEver_detail_itemList_object *)object
{
    _datail_item_object = object;
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;
    /* 商品图片 */
    x = 10.0;
    y = 10.0;
    width = 50.0;
    height = 50.0;
    self.itemImage.frame = CGRectMake(x, y, width, height);
    [self.itemImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/100x",object.itemImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    /* 商品标题 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:self.titleLabel.font, NSFontAttributeName,nil];
    CGSize titleSize =[object.itemName boundingRectWithSize:CGSizeMake(210.0, 40) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    x = 70.0;
    y = 15.0;
    width = titleSize.width;
    height = titleSize.height;
    self.titleLabel.frame = CGRectMake(x, y, width, 20);
    self.titleLabel.text = object.itemName;

    /* 商品副标题 */
    x = 70.0;
    y = 25.0;
    width = 230.0;
    height = 25.0;
    self.subTitleLabel.frame = CGRectMake(x, y, width, height);
    self.subTitleLabel.hidden = YES;
    self.subTitleLabel.text = @"巴黎欧莱雅";

    /* 渠道名称 */
    x = 70.0;
    y = 35.0;
    width = 150.0;
    height = 25.0;
    self.channelLabel.frame = CGRectMake(x, y, width, height);
    self.channelLabel.text = [NSString stringWithFormat:@"RMB%d/%@",object.price,object.itemSpec];

    /* 使用人数 */
    x = 200.0;
    y = 35.0;
    width = 100.0;
    height = 25.0;
    self.personTotalLabel.frame = CGRectMake(x, y, width, height);
    self.personTotalLabel.text = [NSString stringWithFormat:@"%d人关注",object.pvTotal];

    /* 试用链接 */
    x = 240.0;
    y = 20.0;
    height = 30.0;
    width = 30.0;
    if (object.itemTryId >0) {
        _tryButton.userInteractionEnabled = YES;
        _tryButton.hidden = NO;
        [_tryButton setImage:[UIImage imageNamed:@"detail_try"] forState:UIControlStateNormal];
    }else {
        _tryButton.userInteractionEnabled = NO;
        _tryButton.hidden = YES;
        [_tryButton setImage:[UIImage imageNamed:@"detail_UnTry"] forState:UIControlStateNormal];
    }
    self.tryButton.frame = CGRectMake(x, y, width, height);

    /* 购买链接 */
    x = 275.0;
    y = 20.0;
    height = 30.0;
    width = 30.0;
    if (object.itemLink.length >0) {
        _buyButton.userInteractionEnabled = YES;
        [_buyButton setImage:[UIImage imageNamed:@"detail_buy"] forState:UIControlStateNormal];
    }else {
        _buyButton.userInteractionEnabled = NO;
        [_tryButton setImage:[UIImage imageNamed:@"detail_UnBuy"] forState:UIControlStateNormal];
    }
    self.buyButton.frame = CGRectMake(x, y, width, height);

    /* 视图横线 */
    x = 15.0;
    y = 69.0;
    height = 1.0;
    width = 290.0;
    self.lineView.frame = CGRectMake(x, y, width, height);

    /* 背景视图 */
    x = 0.0;
    y = 0.0;
    height = 70.0;
    width = 320.0;
    self.bottomView.frame = CGRectMake(x, y, width, height);


    if (object.endCell == 1) {

//        CALayer *bottomViewLayer = [self.bottomView layer];
//        [bottomViewLayer setMasksToBounds:YES];
//        [bottomViewLayer setCornerRadius:5.0];
        self.lineView.hidden = YES;

    }
    else if (object.endCell == 2) {
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bottomView.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
//        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//        maskLayer.frame = self.bottomView.bounds;
//        maskLayer.path = maskPath.CGPath;
//        self.bottomView.layer.mask=maskLayer;
//        self.bottomView.layer.masksToBounds=YES;
        self.lineView.hidden = NO;
    }
    else if (object.endCell == 3) {
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bottomView.bounds byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
//        CAShapeLayer *maskLayer             = [[CAShapeLayer alloc] init];
//        maskLayer.frame                     = self.bottomView.bounds;
//        maskLayer.path                      = maskPath.CGPath;
//        self.bottomView.layer.mask          =  maskLayer;
//        self.bottomView.layer.masksToBounds = YES;
        self.lineView.hidden = NO;
    }else if(object.endCell == 4){

        self.lineView.hidden = NO;
    }

}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
	return 70.0;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
# pragma mark - view getters

/* 背景视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = WHITE;
    }
    return _bottomView;
}

/* 商品图片 */
- (UIImageView *)itemImage
{
    if (!_itemImage) {
        _itemImage = [[UIImageView alloc]init];
        _itemImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _itemImage;
}

/* 商品标题 */
- (UILabel *)titleLabel
{
	if (!_titleLabel){
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.numberOfLines = 1;
		_titleLabel.backgroundColor = CLEAR;
		_titleLabel.textColor = BLACK;
		_titleLabel.font = TextFont;
	}
	return _titleLabel;
}
/* 商品副标题 */
- (UILabel *)subTitleLabel
{
	if (!_subTitleLabel){
		_subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.numberOfLines = 1;
		_subTitleLabel.backgroundColor = CLEAR;
        _subTitleLabel.textColor = DETAIL_TITLE;
        _subTitleLabel.font = TextFont;
	}
	return _subTitleLabel;
}
/* 渠道名称 */
- (UILabel *)channelLabel
{
	if (!_channelLabel){
		_channelLabel = [[UILabel alloc] init];
        _channelLabel.numberOfLines = 1;
		_channelLabel.backgroundColor = CLEAR;
        _channelLabel.textColor = BLACK;
        _channelLabel.font = TextFont;
	}
	return _channelLabel;
}
/* 使用人数 */
- (UILabel *)personTotalLabel
{
	if (!_personTotalLabel){
		_personTotalLabel = [[UILabel alloc] init];
        _personTotalLabel.numberOfLines = 1;
		_personTotalLabel.backgroundColor = CLEAR;
        _personTotalLabel.textColor = GRAY_LIGHT;
        _personTotalLabel.font = TextDESCFonts;
	}
	return _personTotalLabel;
}
/* 试用链接 */
- (UIButton *)tryButton
{
	if (!_tryButton){
		_tryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_tryButton addTarget:self action:@selector(tryAction:) forControlEvents:UIControlEventTouchUpInside];
	}
	return _tryButton;
}
-(void)tryAction:(id)sender{

}
/* 购买链接 */
- (UIButton *)buyButton
{
	if (!_buyButton){
		_buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_buyButton addTarget:self action:@selector(buyAction:) forControlEvents:UIControlEventTouchUpInside];
	}
	return _buyButton;
}
-(void)buyAction:(id)sender{

    iEver_buyViewController *buyViewController = [[iEver_buyViewController alloc]init];
    buyViewController.itemLink = _datail_item_object.itemLink;
    NSLog(@"");
    [_officialDetailVC pushViewController:buyViewController animated:YES];
    if ([_officialDetailVC respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        _officialDetailVC.interactivePopGestureRecognizer.delegate = nil;
    }
}

/* 视图横线 */
- (UIImageView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIImageView alloc]init];
        _lineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView;
}
@end

//
//  iEver_ItemDetail_PicList_TestCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/5.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_ItemDetail_PicList_TestCell.h"
#import "CWStarRateView.h"

@interface iEver_ItemDetail_PicList_TestCell()
@property (nonatomic, strong) UIView         *bottomView;     /* 背景视图 */
@property (nonatomic, strong) UIImageView    *processImage;   /* 步骤图片 */
@property (nonatomic, strong) UILabel        *titleLabel;     /* 步骤标题 */
@property (nonatomic, strong) UILabel        *contentLabel;   /* 步骤描述 */
@property (nonatomic, strong) UIImageView    *lineView;      /* 视图横线 */
@property (nonatomic, strong) CWStarRateView *starView;        /* 评分视图 */
@property (nonatomic, strong) UILabel        *startGradeLabel;  /* 分数 */

@end

@implementation iEver_ItemDetail_PicList_TestCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = WHITE;
        self.contentView.backgroundColor = WHITE;
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.processImage];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.contentLabel];
        [self.contentView addSubview:self.lineView];
        [self.contentView addSubview:self.starView];
        [self.contentView addSubview:self.startGradeLabel];

    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    /*单元格frame */
    CGFloat x = 15.0;
    CGFloat y = 0;
    CGFloat height = 1.0;
    CGFloat width = 290.0;
    self.lineView.frame = CGRectMake(x, y, width, height);

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setObject:(iEver_detail_itemPicList_object *)object
{

    if (object.endCell == 1) {
        CGFloat x = 15.0;
        CGFloat y = 10.0;
        CGFloat width = 115.0;
        CGFloat height = 115.0;
        self.processImage.frame = CGRectMake(x, y, width, height);
        self.processImage.hidden = NO;
        [self.processImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/230x",object.imgUrl]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

        x = 140.0;
        y = 10.0;
        width = 150.0;
        height = 20.0;
        self.titleLabel.text  = object.imgTitle;
        self.titleLabel.frame = CGRectMake(x, y, width, height);

        self.starView.scorePercent     = (object.startGrade *2)/10.f;
        self.starView.hidden           = NO;

        x = 225.0;
        y = 35.0;
        width = 80;
        height = 15;
        self.startGradeLabel.frame     = CGRectMake(x, y, width, height);
        self.startGradeLabel.hidden    = NO;
        self.startGradeLabel.text      = [NSString stringWithFormat:@"%.1f",object.startGrade];


        NSDictionary * ddic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize descSize =[object.imgDesc boundingRectWithSize:CGSizeMake(150, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:ddic context:nil].size;
        x = 140.0;
        y = 55.0;
        width = 150;
        height = descSize.height;
        self.contentLabel.frame = CGRectMake(x, y, width, height);
        self.contentLabel.text = object.imgDesc;

    }else{

        self.processImage.hidden = YES;
        self.starView.hidden     = YES;
        self.startGradeLabel.hidden = YES;

        CGFloat x = 15.0;
        CGFloat y = 10.0;
        CGFloat width = 250.0;
        CGFloat height = 20.0;
        self.titleLabel.text = object.imgTitle;
        self.titleLabel.frame = CGRectMake(x, y, width, height);

        NSDictionary * ddic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize descSize =[object.imgDesc boundingRectWithSize:CGSizeMake(300, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:ddic context:nil].size;
        x = 15.0;
        y = 35.0;
        width = 300;
        height = descSize.height;
        self.contentLabel.frame = CGRectMake(x, y, width, height);
        self.contentLabel.text = object.imgDesc;

    }

}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{

    float return_height = 135.0;

    iEver_detail_itemPicList_object *pic_object = (iEver_detail_itemPicList_object *)object;

    if (pic_object.endCell == 1) {

        NSDictionary * ddic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize descSize =[pic_object.imgDesc boundingRectWithSize:CGSizeMake(150, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:ddic context:nil].size;

        if (descSize.height + 55.0 >125) {
            return_height = descSize.height + 55.0 +10.0;
        }else{

            return_height = 135.0;
        }

    }else{

        NSDictionary * ddic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize descSize =[pic_object.imgDesc boundingRectWithSize:CGSizeMake(300, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:ddic context:nil].size;

        return_height = descSize.height + 35.0 +10.0;

    }

    return return_height;
}


/* 背景视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = WHITE;
    }
    return _bottomView;
}

/* 步骤图片 */
- (UIImageView *)processImage
{
    if (!_processImage) {
        _processImage = [[UIImageView alloc]init];
        _processImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _processImage;
}

/* 步骤标题 */
- (UILabel *)titleLabel
{
    if (!_titleLabel){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.numberOfLines = 1;
        _titleLabel.backgroundColor = CLEAR;
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.textColor = MY_PURPLECOLOR;
        _titleLabel.font = TitleFont;
    }
    return _titleLabel;
}
/* 步骤描述 */
- (UILabel *)contentLabel
{
    if (!_contentLabel){
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.numberOfLines = 0;
        _contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _contentLabel.backgroundColor = CLEAR;
        _contentLabel.textColor = BLACK;
        _contentLabel.font = TextFont;
    }
    return _contentLabel;
}
/* 视图横线 */
- (UIImageView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIImageView alloc]init];
        _lineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView;
}

/* 分数 */
- (UILabel *)startGradeLabel
{
    if (!_startGradeLabel){
        _startGradeLabel = [[UILabel alloc] init];
        _startGradeLabel.numberOfLines = 1;
        _startGradeLabel.backgroundColor = CLEAR;
        _startGradeLabel.textColor = RANK_UPDATE_GOLD;
        _startGradeLabel.font = TextFont;
    }
    return _startGradeLabel;
}

/* 评分视图 */
- (CWStarRateView *)starView
{
    if (!_starView) {
        _starView = [[CWStarRateView alloc] initWithFrame:CGRectMake(140.0, 35, 80, 15) numberOfStars:5];

    }
    return _starView;
}
@end

//
//  iEver_tryItemDetail_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/27.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"

@interface iEver_tryItemDetail_cell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) NSString *object;
@end

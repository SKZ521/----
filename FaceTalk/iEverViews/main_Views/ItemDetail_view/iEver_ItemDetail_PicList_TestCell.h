//
//  iEver_ItemDetail_PicList_TestCell.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/5.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_itemDetail_Object.h"

@interface iEver_ItemDetail_PicList_TestCell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_detail_itemPicList_object *object;

@end

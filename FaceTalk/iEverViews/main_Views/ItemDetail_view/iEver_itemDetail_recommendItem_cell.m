//
//  iEver_itemDetail_recommendItem_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-24.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_itemDetail_recommendItem_cell.h"
#import "iEver_ItemDetailViewController.h"

#define KScrollViewHeight 80.0f
#define KScrollViewWidth 80.0f
@interface iEver_itemDetail_recommendItem_cell()<UIScrollViewDelegate>
@property (nonatomic, strong) UIView        *bottomView;     /* 背景视图 */
@property (nonatomic, strong) UIImageView   *leftImage;      /* 左边图片 */
@property (nonatomic, strong) UIImageView   *rightImage;     /* 右边图片 */
@property (nonatomic, strong) UILabel       *titleLabel;     /* 步骤标题 */
@property (nonatomic, strong) UILabel       *contentLabel;   /* 步骤描述 */
@property (nonatomic, strong) iEver_ItemDetailViewController *officialDetailVC; /*cell VC*/
@property (strong, nonatomic) UIScrollView  *scrollView;
@end
@implementation iEver_itemDetail_recommendItem_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setViewController:(iEver_ItemDetailViewController *)mvc{
    _officialDetailVC = mvc;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.scrollView];
        [self.contentView addSubview:self.leftImage];
        [self.contentView addSubview:self.rightImage];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
	[super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)addImageWithName:(iEver_detail_recommendItemList_object *)iAd atPosition:(int)position

{
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.frame = CGRectMake(95.0 * position, 10.0, KScrollViewWidth, KScrollViewHeight);
	button.tag = position;
	[button setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/160x",iAd.itemImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
	[[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {

        iEver_ItemDetailViewController *_officialDetailViewController = [[iEver_ItemDetailViewController alloc]init];
        _officialDetailViewController.type_id = iAd.Item_id;
        [_officialDetailVC.navigationController pushViewController:_officialDetailViewController animated:YES];
        if ([_officialDetailVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            _officialDetailVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }

    }];
	[self.scrollView addSubview:button];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setObject:(iEver_itemDetail_Object *)object
{
    if (![object._recommendItemList count]) {
		return;
	}
	for (int i = 0; i < [object._recommendItemList count]; i++) {
		iEver_detail_recommendItemList_object *iAd = [object._recommendItemList objectAtIndex:i];
		[self addImageWithName:iAd atPosition:i];

	}
	[self.scrollView setContentSize:CGSizeMake(95.0 * [object._recommendItemList count] , KScrollViewHeight)];

    self.scrollView.frame = CGRectMake(27.0, 0.0, 272.0, 100.0);

    self.bottomView.frame = CGRectMake(0.0, 0.0, 320.0, 100.0);
//    CALayer *bottomViewLayer = [self.bottomView layer];
//    [bottomViewLayer setMasksToBounds:YES];
//    [bottomViewLayer setCornerRadius:5.0];
    self.bottomView.backgroundColor = WHITE;

    self.leftImage.frame = CGRectMake(10, 45, 7, 9);
    self.rightImage.frame = CGRectMake(303, 45, 7, 9);
    if ([object._recommendItemList count] > 3) {
        self.leftImage.hidden = YES;
        self.rightImage.hidden = NO;
    }else {
        self.leftImage.hidden = YES;
        self.rightImage.hidden = YES;
    }
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
	return 100.0;
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    if ( self.scrollView.contentSize.width > self.scrollView.contentOffset.x > 0.0 ) {
        self.leftImage.hidden = NO;
        self.rightImage.hidden = NO;
    }else if (self.scrollView.contentOffset.x <= 1 ){

        self.leftImage.hidden = YES;
        self.rightImage.hidden = NO;
    }
    else if (self.scrollView.contentOffset.x >= 202 ){

        self.leftImage.hidden = YES;
        self.rightImage.hidden = NO;
    }

}

# pragma mark - view getters

/* 背景视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = WHITE;
    }
    return _bottomView;
}
- (UIScrollView *)scrollView
{
	if (!_scrollView) {
		_scrollView = [[UIScrollView alloc] init];
		_scrollView.pagingEnabled = NO;
		_scrollView.showsHorizontalScrollIndicator = NO;
		_scrollView.delegate = self;
	}

	return _scrollView;
}
/* 商品图片 */
- (UIImageView *)leftImage
{
    if (!_leftImage) {
        _leftImage = [[UIImageView alloc]init];
        _leftImage.image = [UIImage imageNamed:@"detail_left"];
        _leftImage.hidden = YES;

    }
    return _leftImage;
}
- (UIImageView *)rightImage
{
    if (!_rightImage) {
        _rightImage = [[UIImageView alloc]init];
        _rightImage.image = [UIImage imageNamed:@"detail_right"];
        _rightImage.hidden = YES;
    }
    return _rightImage;
}


@end

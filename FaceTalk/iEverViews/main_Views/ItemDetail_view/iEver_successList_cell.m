//
//  iEver_successList_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/27.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_successList_cell.h"
#import "NSString+WPAttributedMarkup.h"

@interface iEver_successList_cell()
@property (nonatomic, strong) UIView        *bottomView;     /* 背景视图 */
@property (nonatomic, strong) UIImageView   *photoImage;     /* 用户图片 */
@property (nonatomic, strong) UILabel       *nameLabel;      /* 用户名称 */
@property (nonatomic, strong) UILabel       *timeLabel;      /* 申请时间 */
@property (nonatomic, strong) UILabel       *featureLable;   /* 用户特性 */
@property (nonatomic, strong) UIImageView   *arrowImage;     /* 箭头图片 */
@property (nonatomic, strong) UIView        *baseCommentView;/* 用户评论灰色背景 */
@property (nonatomic, strong) UILabel       *commentLable;   /* 用户评论 */
@property (nonatomic, strong) UIImageView   *cellLineView;   /* 单元格分割线 */

@end

@implementation iEver_successList_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = CLEAR;
        self.contentView.backgroundColor = CLEAR;
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.photoImage];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.timeLabel];
        [self.contentView addSubview:self.featureLable];
        [self.contentView addSubview:self.arrowImage];
        [self.contentView addSubview:self.baseCommentView];
        [self.baseCommentView addSubview:self.commentLable];
        [self.contentView addSubview:self.cellLineView];


    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setObject:(iEver_successList_object *)object
{


    /* 用户图片 */
    CGFloat x = 15.0;
    CGFloat y = 10.0;
    CGFloat width = 42.0;
    CGFloat height = 42.0;
    self.photoImage.frame = CGRectMake(x, y, width, height);
    [self.photoImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/84x",object.headImg]] placeholderImage:[UIImage imageNamed:@"like_image"]];

    /* 用户名称 */
    x = self.photoImage.frame.origin.x + self.photoImage.frame.size.width + 10;
    y = 15.0;
    width = 150.0;
    height = 15.0;
    self.nameLabel.frame = CGRectMake(x, y, width, height);
    self.nameLabel.text = object.nickName;


    /* 发布时间 */
    x = 225.0;
    y = 15.0;
    width = 80.0;
    height = 15.0;
    self.timeLabel.frame = CGRectMake(x, y, width, height);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY.MM.dd"];
    NSString *dateLoca = [NSString stringWithFormat:@"%lld",object.createTime/1000];
    NSTimeInterval time =[dateLoca doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timestr = [formatter stringFromDate:detaildate];
    self.timeLabel.text = timestr;

    /* 用户特性 */

    x = self.nameLabel.frame.origin.x;
    y = self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height + 3;
    width = ScreenWidth - x - 15;
    height = 15.0;
    self.featureLable.frame = CGRectMake(x, y, width, height);
    if (object.feature.length > 0) {
        self.featureLable.text = object.feature;
    }else {

        self.featureLable.text = @"该用户没有定制";
    }


    /* 评论小箭头 */
    UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
    x = self.photoImage.frame.size.width/2 - tryDetailArrow.size.width/2 + self.photoImage.frame.origin.x;
    y = self.photoImage.frame.origin.y + self.photoImage.frame.size.height + 5;
    width = tryDetailArrow.size.width;
    height = tryDetailArrow.size.height;
    self.arrowImage.image = tryDetailArrow;
    self.arrowImage.frame = CGRectMake(x, y, width, height);

    /* 评论内容 */
    NSDictionary* contentStyle = @{@"red":[UIColor redColor],
                                   @"black": [UIColor blackColor]};

    NSString *str_content = [NSString stringWithFormat:@"试用心得：%@",object.commentContent];
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:12.0], NSFontAttributeName,nil];
    CGSize contentSize =[str_content boundingRectWithSize:CGSizeMake(ScreenWidth - 34, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    NSString *contentStrStyle = [NSString stringWithFormat:@"<red>试用心得：</red><black>%@</black>",object.commentContent];
    self.commentLable.attributedText = [contentStrStyle attributedStringWithStyleBook:contentStyle];
    self.commentLable.frame = CGRectMake(10, 10, contentSize.width, contentSize.height);


    if ( object.commentContent.length > 0) {
        self.arrowImage.hidden = NO;
        self.baseCommentView.hidden = NO;

        /* 评论内容背景视图 */
        self.baseCommentView.frame = CGRectMake(12,  self.arrowImage.frame.origin.y + self.arrowImage.frame.size.height, ScreenWidth - 24, contentSize.height + 20.0);

        /* 视图横线 */
        self.cellLineView.frame = CGRectMake(15, self.baseCommentView.frame.origin.y + self.baseCommentView.frame.size.height + 15, ScreenWidth - 30, 1);

        /* 背景视图 */
        self.bottomView.frame = CGRectMake(0.0, 0.0, ScreenWidth, self.cellLineView.frame.origin.y + 1);



    }else {


        self.arrowImage.hidden = YES;
        self.baseCommentView.hidden = YES;


        /* 视图横线 */
        self.cellLineView.frame = CGRectMake(15, 64, ScreenWidth - 30, 1);

        /* 背景视图 */
        self.bottomView.frame = CGRectMake(0.0, 0.0, ScreenWidth, 65);

    }

}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    iEver_successList_object *_successList_object = (iEver_successList_object *)object;

    CGFloat height;
    NSString *str_content = [NSString stringWithFormat:@"试用心得:%@",_successList_object.commentContent];
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:12.0], NSFontAttributeName,nil];
    CGSize contentSize =[str_content boundingRectWithSize:CGSizeMake(ScreenWidth - 34, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    if (_successList_object.commentContent.length > 0) {

        height = 65 + contentSize.height + 20.0 + 15;
    }else{

        height = 65;
    }
    return height;
}
/* 背景视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = WHITE;
    }
    return _bottomView;
}

/* 用户图片 */
- (UIImageView *)photoImage
{
    if (!_photoImage) {
        _photoImage = [[UIImageView alloc]init];
        CALayer *photoLayer = [_photoImage layer];
        [photoLayer setMasksToBounds:YES];
        [photoLayer setCornerRadius:21.0];
    }
    return _photoImage;
}

/* 用户名称 */
- (UILabel *)nameLabel
{
    if (!_nameLabel){
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.numberOfLines = 1;
        _nameLabel.backgroundColor = CLEAR;
        _nameLabel.textColor = BLACK;
        _nameLabel.font = TextFont;
    }
    return _nameLabel;
}
/* 创建时间 */
- (UILabel *)timeLabel
{
    if (!_timeLabel){
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.numberOfLines = 1;
        _timeLabel.backgroundColor = CLEAR;
        _timeLabel.textColor = BLACK;
        _timeLabel.textAlignment = NSTextAlignmentRight;
        _timeLabel.font = TextFonts;
    }
    return _timeLabel;
}


/* 用户特性 */
- (UILabel *)featureLable
{
    if (!_featureLable){
        _featureLable = [[UILabel alloc] init];
        _featureLable.numberOfLines = 1;
        _featureLable.backgroundColor = CLEAR;
        _featureLable.textColor = TRY_UPDATE_YELLOY;
        _featureLable.font = TextDESCFonts;
    }
    return _featureLable;
}

/* 评论箭头 */
- (UIImageView *)arrowImage
{
    if (!_arrowImage) {
        _arrowImage = [[UIImageView alloc]init];
        _arrowImage.backgroundColor = CLEAR;
    }
    return _arrowImage;
}

/* 背景视图 */
- (UIView *)baseCommentView
{
    if (!_baseCommentView) {
        _baseCommentView = [[UIView alloc]init];
        _baseCommentView.backgroundColor = TYR_BACKGRAYCOLOR;
    }
    return _baseCommentView;
}


/*用户评论 */
- (UILabel *)commentLable
{
    if (!_commentLable){
        _commentLable = [[UILabel alloc] init];
        _commentLable.numberOfLines = 0;
        _commentLable.backgroundColor = CLEAR;
        _commentLable.textColor = BLACK;
        _commentLable.font = TextDESCFonts;
    }
    return _commentLable;
}

/* 视图横线 */
- (UIImageView *)cellLineView
{
    if (!_cellLineView) {
        _cellLineView = [[UIImageView alloc]init];
        _cellLineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _cellLineView;
}


@end

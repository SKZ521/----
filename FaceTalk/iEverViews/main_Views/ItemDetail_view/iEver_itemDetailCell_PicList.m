//
//  iEver_itemDetailCell_PicList.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-23.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_itemDetailCell_PicList.h"

@interface iEver_itemDetailCell_PicList()
@property (nonatomic, strong) UIView        *bottomView;     /* 背景视图 */
@property (nonatomic, strong) UIImageView   *processImage;   /* 步骤图片 */
@property (nonatomic, strong) UILabel       *titleLabel;     /* 步骤标题 */
@property (nonatomic, strong) UILabel       *contentLabel;   /* 步骤描述 */
@end

@implementation iEver_itemDetailCell_PicList

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = WHITE;
        self.contentView.backgroundColor = WHITE;
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.processImage];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.contentLabel];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
	[super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setObject:(iEver_detail_itemPicList_object *)object
{

    /* 步骤图片 */
    CGFloat x = 5.0;
    CGFloat y1 = 10.0;
    CGFloat y2 = 20.0;
    NSDictionary * ddic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:35.0/2], NSFontAttributeName,nil];

    CGRect frame = [self frame];

    if (object.imgUrl.length != 0 && object.imgTitle.length == 0 && object.imgDesc.length == 0) {

        int image_true_height = object.image_height;
        int image_true_width = object.image_width;
        int imageWidth = 310;
        float imageHeight = (image_true_height * imageWidth)/image_true_width;
        self.processImage.frame = CGRectMake(x, y1, imageWidth, imageHeight);
        [self.processImage setImageWithURL:[NSURL URLWithString:object.imgUrl] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

        self.titleLabel.frame = CGRectZero;
        self.contentLabel.frame = CGRectZero;
        frame.size.height = y1 + imageHeight + y1;

    }else if (object.imgUrl.length != 0 && object.imgTitle.length != 0 && object.imgDesc.length == 0) {

        int image_true_height = object.image_height;
        int image_true_width = object.image_width;
        int imageWidth = 310;
        float imageHeight = (image_true_height * imageWidth)/image_true_width;
        self.processImage.frame = CGRectMake(x, y1, imageWidth, imageHeight);
        [self.processImage setImageWithURL:[NSURL URLWithString:object.imgUrl] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

        CGSize titleSize =[object.imgTitle boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
        self.titleLabel.frame = CGRectMake(25, y1 + imageHeight + y2, titleSize.width, titleSize.height);
        self.titleLabel.text = object.imgTitle;

        self.contentLabel.frame = CGRectZero;
        frame.size.height = y1 + imageHeight + y2 + titleSize.height + y1;

    }else if (object.imgUrl.length != 0 && object.imgTitle.length == 0 && object.imgDesc.length != 0) {

        int image_true_height = object.image_height;
        int image_true_width = object.image_width;
        int imageWidth = 310;
        float imageHeight = (image_true_height * imageWidth)/image_true_width;
        self.processImage.frame = CGRectMake(x, y1, imageWidth, imageHeight);
        [self.processImage setImageWithURL:[NSURL URLWithString:object.imgUrl] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

        CGSize descSize =[object.imgDesc boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:ddic context:nil].size;
        self.contentLabel.frame = CGRectMake(25, y1 + imageHeight + y2, descSize.width, descSize.height);
        self.contentLabel.text = object.imgDesc;

        self.titleLabel.frame = CGRectZero;
        frame.size.height = y1 + imageHeight + y2 + descSize.height + y1;


    }else if (object.imgUrl.length != 0 && object.imgTitle.length != 0 && object.imgDesc.length != 0) {

        int image_true_height = object.image_height;
        int image_true_width = object.image_width;
        int imageWidth = 310;
        float imageHeight = (image_true_height * imageWidth)/image_true_width;
        self.processImage.frame = CGRectMake(x, y1, imageWidth, imageHeight);
        [self.processImage setImageWithURL:[NSURL URLWithString:object.imgUrl] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];


        CGSize titleSize =[object.imgTitle boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
        self.titleLabel.frame = CGRectMake(25, y1 + imageHeight + y2, titleSize.width, titleSize.height);
        self.titleLabel.text = object.imgTitle;

        CGSize descSize =[object.imgDesc boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:ddic context:nil].size;
        self.contentLabel.frame = CGRectMake(25, y1 + imageHeight + y2 + titleSize.height + y2, descSize.width, descSize.height);
        self.contentLabel.text = object.imgDesc;

        frame.size.height = y1 + imageHeight+ y2 + titleSize.height + y2 + descSize.height + y1;


    }else if (object.imgUrl.length == 0 && object.imgTitle.length == 0 && object.imgDesc.length == 0) {

        self.processImage.frame = CGRectZero;
        self.titleLabel.frame = CGRectZero;
        self.contentLabel.frame = CGRectZero;
        frame.size.height = 0.0;

    }else if (object.imgUrl.length == 0 && object.imgTitle.length != 0 && object.imgDesc.length == 0) {

        CGSize titleSize =[object.imgTitle boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
        self.titleLabel.frame = CGRectMake(25, y1, titleSize.width, titleSize.height);
        self.titleLabel.text = object.imgTitle;

        self.processImage.frame = CGRectZero;
        self.contentLabel.frame = CGRectZero;
        frame.size.height = y1 + titleSize.height + y1;

    }else if (object.imgUrl.length == 0 && object.imgTitle.length != 0 && object.imgDesc.length != 0) {

        CGSize titleSize =[object.imgTitle boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
        self.titleLabel.frame = CGRectMake(25, y1, titleSize.width, titleSize.height);
        self.titleLabel.text = object.imgTitle;

        CGSize descSize =[object.imgDesc boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:ddic context:nil].size;
        self.contentLabel.frame = CGRectMake(25, y1 + titleSize.height + y2, descSize.width, descSize.height);
        self.contentLabel.text = object.imgDesc;

        self.processImage.frame = CGRectZero;
        frame.size.height = y1 + titleSize.height+ y2 + descSize.height + y1;

    }else if (object.imgUrl.length == 0 && object.imgTitle.length == 0 && object.imgDesc.length != 0) {

        CGSize descSize =[object.imgDesc boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:ddic context:nil].size;
        self.contentLabel.frame = CGRectMake(25, y1, descSize.width, descSize.height);
        self.contentLabel.text = object.imgDesc;

        self.processImage.frame = CGRectZero;
        self.titleLabel.frame = CGRectZero;
        frame.size.height = y1 + descSize.height + y1;
    }
    
    self.bottomView.frame = frame;
    self.frame = frame;
    DLog(@"jisuan------%f", self.frame.size.height);

}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{

    float return_height = 10.0;
    CGFloat y1 = 10.0;
    CGFloat y2 = 20.0;

    /* 获取图片高度 */
    iEver_detail_itemPicList_object *pic_object = (iEver_detail_itemPicList_object *)object;
    float imageheight = (310 * pic_object.image_height)/pic_object.image_width;

    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:35.0/2], NSFontAttributeName,nil];
    CGSize titleSize =[pic_object.imgTitle boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    NSDictionary * ddic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize descSize =[pic_object.imgDesc boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:ddic context:nil].size;
    if (pic_object.imgUrl.length != 0 && pic_object.imgTitle.length == 0 && pic_object.imgDesc.length == 0) {
        return_height = y1 + imageheight + y1;
    }else if (pic_object.imgUrl.length != 0 && pic_object.imgTitle.length != 0 && pic_object.imgDesc.length == 0) {
        return_height = y1 + imageheight + y2 + titleSize.height + y1;
    }else if (pic_object.imgUrl.length != 0 && pic_object.imgTitle.length == 0 && pic_object.imgDesc.length != 0) {
        return_height = y1 + imageheight + y2 + descSize.height + y1;
    }else if (pic_object.imgUrl.length != 0 && pic_object.imgTitle.length != 0 && pic_object.imgDesc.length != 0) {
        return_height = y1 + imageheight + y2 + titleSize.height + y2 + descSize.height + y1;
    }else if (pic_object.imgUrl.length == 0 && pic_object.imgTitle.length == 0 && pic_object.imgDesc.length == 0) {
        return_height = 0.0;
    }else if (pic_object.imgUrl.length == 0 && pic_object.imgTitle.length != 0 && pic_object.imgDesc.length == 0) {
        return_height = y1 + titleSize.height + y1;
    }else if (pic_object.imgUrl.length == 0 && pic_object.imgTitle.length != 0 && pic_object.imgDesc.length != 0) {
        return_height = y1 + titleSize.height+ y2 + descSize.height + y1;
    }else if (pic_object.imgUrl.length == 0 && pic_object.imgTitle.length == 0 && pic_object.imgDesc.length != 0) {
        return_height = y1 + descSize.height + y1;
    }
    DLog(@"cell 高%f", return_height);
    return return_height;

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
# pragma mark - view getters

/* 背景视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = WHITE;
    }
    return _bottomView;
}

/* 步骤图片 */
- (UIImageView *)processImage
{
    if (!_processImage) {
        _processImage = [[UIImageView alloc]init];
        _processImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _processImage;
}

/* 步骤标题 */
- (UILabel *)titleLabel
{
	if (!_titleLabel){
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.numberOfLines = 0;
		_titleLabel.backgroundColor = CLEAR;
        _titleLabel.textAlignment = NSTextAlignmentLeft;
		_titleLabel.textColor = DETAIL_TITLE;
		_titleLabel.font = TextLIST_TITLE_Fonts;
	}
	return _titleLabel;
}
/* 步骤描述 */
- (UILabel *)contentLabel
{
	if (!_contentLabel){
		_contentLabel = [[UILabel alloc] init];
        _contentLabel.numberOfLines = 0;
        _contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
		_contentLabel.backgroundColor = CLEAR;
        _contentLabel.textColor = GRAY_LIGHT;
        _contentLabel.font = TextFont;
	}
	return _contentLabel;
}

@end

//
//  iEver_itemDetail_comment_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-26.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_itemDetail_Object.h"

@interface iEver_itemDetail_comment_cell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_detail_itemCommentList_object *object;

@end

//
//  iEver_tagItem_Cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/5.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_tagItemObject.h"

@interface iEver_tagItem_Cell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_tagItemList_object *object;
@end

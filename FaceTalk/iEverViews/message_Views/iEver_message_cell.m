//
//  iEver_message_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-7.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_message_cell.h"
@interface iEver_message_cell()
@property (nonatomic, strong) UIView        *redView;     /* 背景视图 */
@property (nonatomic, strong) UIImageView   *photoImage;     /* 用户图片 */
@property (nonatomic, strong) UILabel       *nameLabel;      /* 用户名称 */
@property (nonatomic, strong) UILabel       *skinLabel;      /* 肤质类型 */
@property (nonatomic, strong) UILabel       *pointLabel;     /* 用户积分 */
@property (nonatomic, strong) UILabel       *timeLabel;      /* 发布时间 */
@property (nonatomic, strong) UILabel       *contentLabel;   /* 消息内容 */
@property (nonatomic, strong) UILabel       *likeTotalLabel; /* 点赞个数 */
@property (nonatomic, strong) UIImageView   *lineView;       /* 视图横线 */
@end
@implementation iEver_message_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.redView];
        [self.contentView addSubview:self.photoImage];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.skinLabel];
        [self.contentView addSubview:self.pointLabel];
        [self.contentView addSubview:self.timeLabel];
        [self.contentView addSubview:self.contentLabel];
        [self.contentView addSubview:self.likeTotalLabel];
        [self.contentView addSubview:self.lineView];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
	[super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setObject:(iEver_messageList_object *)object
{
    /* 用户图片 */
    CGFloat x = 15.0;
    CGFloat y = 10.0;
    CGFloat width = 35.0;
    CGFloat height = 35.0;
    self.photoImage.frame = CGRectMake(x, y, width, height);
    [self.photoImage setImageWithURL:[NSURL URLWithString:object.userHeadImg] placeholderImage:[UIImage imageNamed:@"discover_V_A"]];

    /* 用户名称 */
    x = 60.0;
    y = 12.0;
    width = 150.0;
    height = 20.0;
    self.nameLabel.frame = CGRectMake(x, y, width, height);
    self.nameLabel.text = object.sendUserNickName;

    /* 是否已读 */
    x = 46.0;
    y = 13.0;
    width = 6.0;
    height = 6.0;
    self.redView.frame = CGRectMake(x, y, width, height);
    if (object.readStatus == 0) {
        self.redView.hidden = NO;
    }else{
        self.redView.hidden = YES;
    }

    /* 发布时间 */
    x = 200.0;
    y = 10.0;
    width = 100.0;
    height = 20.0;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY.MM.dd"];
    NSString *dateLoca = [NSString stringWithFormat:@"%lld",object.updateTime/1000];
    NSTimeInterval time =[dateLoca doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timestr = [formatter stringFromDate:detaildate];
    self.timeLabel.text = timestr;
    self.timeLabel.frame = CGRectMake(x, y, width, height);
    self.timeLabel.text = timestr;

    /* 消息描述 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[object.msgContent boundingRectWithSize:CGSizeMake(245.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    x = 60.0;
    y = 35.0;
    width = contentSize.width;
    height = contentSize.height;
    self.contentLabel.frame = CGRectMake(x, y, width, height);
    self.contentLabel.text = object.msgContent;

    y = y + height + 10.0;
    /* 单元格线 */
    x = 15.0;
    y = y - 1.0;
    width = 290.0;
    height = 1.0;
    self.lineView.frame = CGRectMake(x, y, width, height);

}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
   	iEver_messageList_object *message_object = (iEver_messageList_object *)object;

    CGFloat h = 35.0;
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[message_object.msgContent boundingRectWithSize:CGSizeMake(245.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    h = h + contentSize.height + 10.0;
    return h;

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
# pragma mark - view getters

/* 背景视图 */
- (UIView *)redView
{
    if (!_redView) {
        _redView = [[UIView alloc]init];
        _redView.backgroundColor = [UIColor redColor];
        CALayer *redViewLayer = [_redView layer];
        [redViewLayer setMasksToBounds:YES];
        [redViewLayer setCornerRadius:6.0/2];
    }
    return _redView;
}

/* 用户图片 */
- (UIImageView *)photoImage
{
    if (!_photoImage) {
        _photoImage = [[UIImageView alloc]init];
        CALayer *photoLayer = [_photoImage layer];
        [photoLayer setMasksToBounds:YES];
        [photoLayer setCornerRadius:35.0/2];
    }
    return _photoImage;
}

/* 用户名称 */
- (UILabel *)nameLabel
{
	if (!_nameLabel){
		_nameLabel = [[UILabel alloc] init];
		_nameLabel.numberOfLines = 1;
		_nameLabel.backgroundColor = CLEAR;
		_nameLabel.textColor = DETAIL_TITLE;
		_nameLabel.font = TextFont;
	}
	return _nameLabel;
}
/* 发布时间 */
- (UILabel *)timeLabel
{
	if (!_timeLabel){
		_timeLabel = [[UILabel alloc] init];
        _timeLabel.numberOfLines = 0;
		_timeLabel.backgroundColor = CLEAR;
        _timeLabel.textColor = GRAY_LIGHT;
        _timeLabel.textAlignment = NSTextAlignmentRight;
        _timeLabel.font = TextDESCFonts;
	}
	return _timeLabel;
}
/* 消息内容 */
- (UILabel *)contentLabel
{
	if (!_contentLabel){
		_contentLabel = [[UILabel alloc] init];
        _contentLabel.numberOfLines = 0;
		_contentLabel.backgroundColor = CLEAR;
        _contentLabel.textColor = GRAY_LIGHT;
        _contentLabel.font = TextFont;
	}
	return _contentLabel;
}

/* 视图横线 */
- (UIImageView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIImageView alloc]init];
        _lineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView;
}

@end

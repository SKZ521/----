//
//  iEver_person_FiveSection_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-4.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_person_FiveSection_cell.h"
@interface iEver_person_FiveSection_cell()
@property (nonatomic, strong) UIView        *bottom_View;         /* 视图背景*/
@property (nonatomic, strong) UILabel       *text_Name;         /* 设置定制信息标签 */
@property (nonatomic, strong) UIImageView   *icon_image;          /* 图标图片 */
@end
@implementation iEver_person_FiveSection_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = CLEAR;
        self.backgroundColor = CLEAR;
        [self.contentView addSubview:self.bottom_View];
        [self.contentView addSubview:self.text_Name];
        [self.contentView addSubview:self.icon_image];


    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
	[super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setObject:(iEver_personMessage_object *)object
{
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 白色圆角地图 */
    x = 0.0;
    y = 0.0;
    width = 320.0;
    height = 50.0;
    self.bottom_View.frame = CGRectMake(x, y, width, height);

    /* 用户名称 */
    x = 60.0;
    y = 12.0;
    width = 150.0;
    height = 25.0;
    self.text_Name.frame = CGRectMake(x, y, width, height);
    self.text_Name.text = @"退出登录";

    /* 图标 */
    x = 20.0;
    y = 10;
    width = 30;
    height = 30;
    self.icon_image.frame = CGRectMake(x, y, width, height);

}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
	return 50.0;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
# pragma mark - view getters

/* 底部白色视图 */
- (UIView *)bottom_View
{
    if (!_bottom_View) {
        _bottom_View = [[UIView alloc]init];
        _bottom_View.backgroundColor = WHITE;
    }
    return _bottom_View;
}

/* 用户昵称 */
- (UILabel *)text_Name
{
	if (!_text_Name){
		_text_Name = [[UILabel alloc] init];
		_text_Name.numberOfLines = 1;
		_text_Name.backgroundColor = CLEAR;
		_text_Name.textColor = BLACK;
        _text_Name.textAlignment = NSTextAlignmentLeft;
		_text_Name.font = TextFont;
	}
	return _text_Name;
}

/* 图标图片 */
- (UIImageView *)icon_image
{
    if (!_icon_image) {
        _icon_image = [[UIImageView alloc]init];
        _icon_image.image = [UIImage imageNamed:@"setting_SignOut"];
    }
    return _icon_image;
}

@end

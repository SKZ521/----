//
//  iEver_normalUserUnansweredCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_normalUserUnansweredCell.h"
#import "SJAvatarBrowser.h"
@interface iEver_normalUserUnansweredCell (){

}
@property (nonatomic, strong) UIButton      *A_PhotoButton;      /* 答者头像 */
@property (nonatomic, strong) UIImageView   *A_TypeImage;        /* 答者图片标示 */
@property (nonatomic, strong) UILabel       *A_nameLable;        /* 答者昵称 */
@property (nonatomic, strong) UILabel       *A_contentLabel;     /* 答案 */
@property (nonatomic, strong) UIImageView   *A_image1;           /* 答案图片1 */
@property (nonatomic, strong) UIImageView   *A_image2;           /* 答案图片2 */
@property (nonatomic, strong) UIImageView   *A_image3;           /* 答案图片3 */
@property (nonatomic, strong) UIImageView   *A_image4;           /* 答案图片4 */
@property (nonatomic, strong) UIImageView   *lineImage;          /* 单元格线 */
@property (nonatomic, strong) UINavigationController *navigation; /*cell VC*/
@property (nonatomic, strong) iEver_N_quesList_object *whole_object;
@end
@implementation iEver_normalUserUnansweredCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setViewController:(UINavigationController *)MVNavigation{

    self.navigation = MVNavigation;
}
- (void) configureCell {

}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.A_PhotoButton];
        [self.contentView addSubview:self.A_TypeImage];
        [self.contentView addSubview:self.A_nameLable];
        [self.contentView addSubview:self.A_contentLabel];
        [self.contentView addSubview:self.lineImage];
        [self.contentView addSubview:self.A_image1];
        [self.contentView addSubview:self.A_image2];
        [self.contentView addSubview:self.A_image3];
        [self.contentView addSubview:self.A_image4];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    [super layoutSubviews];
}
- (void)setObject:(iEver_N_quesList_object *)object
{
    _whole_object = object;

    self.A_image1.hidden = YES;
    self.A_image2.hidden = YES;
    self.A_image3.hidden = YES;
    self.A_image4.hidden = YES;

    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 单元格frame */
    //const CGRect r = self.contentView.bounds;

    /* 问者头像按钮 */
    x = 12.0;
    y = 12.0;
    width = 30.0;
    height = 30.0;
    self.A_PhotoButton.frame = CGRectMake(x, y, width, height);
    [_A_PhotoButton setImageWithURL:[NSURL URLWithString:object.aHeadImg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    /* 问着标示图标 */
    x = 50.0;
    y = 12.0;
    width = 15.0;
    height = 15.0;
    self.A_TypeImage.frame = CGRectMake(x, y, width, height);

    /* 问者昵称 */
    x = 50.0;
    y = 31.0;
    width = 54.0;
    height = 15.0;
    self.A_nameLable.frame = CGRectMake(x, y, width, height);
    self.A_nameLable.text = object.aNickName;

    /* 问题 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[object.qContent boundingRectWithSize:CGSizeMake(190.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    self.A_contentLabel.text = object.qContent;
    x = 110.0;
    y = 12.0;
    width = 200.0;
    height = contentSize.height;
    self.A_contentLabel.frame = CGRectMake(x, y, width, height);

    /* 问题图片 */
    if (contentSize.height < 35.0) {
        y = self.A_PhotoButton.frame.origin.y + self.A_PhotoButton.frame.size.height + 10.0;
    }else{
        y = self.A_contentLabel.frame.origin.y + self.A_contentLabel.frame.size.height + 10.0;
    }

    if ([object._QquesPicList count] >0) {

        x = 110.0;
        y = y;
        width = 40.0;
        height = 40.0;
        NSArray *imageView_array =@[self.A_image1,self.A_image2,self.A_image3,self.A_image4];
        for (int i = 0; i < [object._QquesPicList count]; i++) {
            UIImageView *imageview = imageView_array[i];
            imageview.hidden = NO;
            [imageview setImageWithURL:[NSURL URLWithString:[object._QquesPicList objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            imageview.frame = CGRectMake((40.0 + 5.0) * i + x, y, width, height);
            UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
            tapGesture.numberOfTapsRequired=1;
            imageview.userInteractionEnabled = YES;
            [imageview addGestureRecognizer:tapGesture];
        }

        y = self.A_image1.frame.origin.y + self.A_image1.frame.size.height + 10.0;
    }

    NSLog(@"---------y---%F",y);
    /* 单元格线 */
    x = 15.0;
    y = y - 1.0;
    width = 290.0;
    height = 1.0;
    self.lineImage.frame = CGRectMake(x, y, width, height);
}

-(void)handleTapGesture:(UIGestureRecognizer*)sender{
    [SJAvatarBrowser showImage:(UIImageView*)sender.view];
}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    iEver_N_quesList_object *V_answer_object = (iEver_N_quesList_object *)object;

    CGFloat h = 12.0;
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[V_answer_object.qContent boundingRectWithSize:CGSizeMake(190.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    if (contentSize.height < 35.0) {
        h = h + 30.0 + 10.0;
    }else{
        h = h + contentSize.height + 10.0;
    }
    if ([V_answer_object._QquesPicList count] >0) {
        h = h + 40.0 + 10.0;
    }
    NSLog(@"---------h---%F",h);
    return h;
}

# pragma mark - view getters

/* 答者头像 */
- (UIButton *)A_PhotoButton
{
    if (!_A_PhotoButton){
        _A_PhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_A_PhotoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:5.0];
    }
    return _A_PhotoButton;
}

/* 答者图片标示 */
-(UIImageView *)A_TypeImage{

    if (!_A_TypeImage) {
        _A_TypeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"discover_V_A"]];
    }
    return _A_TypeImage;
}

/* 答者昵称 */
- (UILabel *)A_nameLable
{
    if (!_A_nameLable){
        _A_nameLable = [[UILabel alloc] init];
        _A_nameLable.numberOfLines = 1;
        _A_nameLable.backgroundColor = CLEAR;
        _A_nameLable.textColor = LIST_TITLE;
        _A_nameLable.font = TextFonts;
    }
    return _A_nameLable;
}

/* 答案 */
- (UILabel *)A_contentLabel
{
    if (!_A_contentLabel){
        _A_contentLabel = [[UILabel alloc] init];
        _A_contentLabel.backgroundColor = CLEAR;
        _A_contentLabel.numberOfLines = 0;
        _A_contentLabel.textColor = LIST_TITLE;
        _A_contentLabel.font = TextFont;
    }
    return _A_contentLabel;
}

/* 答者图片标示 */
-(UIImageView *)lineImage{

    if (!_lineImage) {
        _lineImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"person_line"]];
    }
    return _lineImage;
}

/* 答案图片1 */
-(UIImageView *)A_image1{

    if (!_A_image1) {
        _A_image1 = [[UIImageView alloc] init];
        _A_image1.hidden = YES;
    }
    return _A_image1;
}
/* 答案图片2 */
-(UIImageView *)A_image2{

    if (!_A_image2) {
        _A_image2 = [[UIImageView alloc] init];
        _A_image2.hidden = YES;
    }
    return _A_image2;
}
/* 答案图片3*/
-(UIImageView *)A_image3{

    if (!_A_image3) {
        _A_image3 = [[UIImageView alloc] init];
        _A_image3.hidden = YES;
    }
    return _A_image3;
}
/* 答案图片4 */
-(UIImageView *)A_image4{
    
    if (!_A_image4) {
        _A_image4 = [[UIImageView alloc] init];
        _A_image4.hidden = YES;
    }
    return _A_image4;
}
@end


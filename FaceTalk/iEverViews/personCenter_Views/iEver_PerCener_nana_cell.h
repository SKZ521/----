//
//  iEver_PerCener_nana_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-16.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_discoverObject.h"
@class iEver_V_PersonCenterViewController;
@interface iEver_PerCener_nana_cell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) UIImageView   *coverImage;       /*封面图片*/
@property (nonatomic, strong) iEver_discoverContentList_object   *object;       /*封面图片*/
- (void)setViewController:(iEver_V_PersonCenterViewController *)MVNavigation;
@end

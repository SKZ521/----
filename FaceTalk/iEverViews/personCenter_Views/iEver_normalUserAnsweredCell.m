//
//  iEver_normalUserAnsweredCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_normalUserAnsweredCell.h"
#import "MCFireworksButton.h"
#import "iEver_likeActionObject.h"
#import "SJAvatarBrowser.h"

@interface iEver_normalUserAnsweredCell (){

}
@property (nonatomic, strong) UIView        *bottomView;         /* 背景视图 */
@property (nonatomic, strong) UIButton      *Q_PhotoButton;      /* 问者头像按钮 */
@property (nonatomic, strong) UIImageView   *Q_TypeImage;        /* 问着标示图标 */
@property (nonatomic, strong) UILabel       *Q_nameLable;        /* 问者昵称 */
@property (nonatomic, strong) UILabel       *Q_contentLabel;     /* 问题 */
@property (nonatomic, strong) UIImageView   *Q_image1;           /* 问题图片1 */
@property (nonatomic, strong) UIImageView   *Q_image2;           /* 问题图片2 */
@property (nonatomic, strong) UIImageView   *Q_image3;           /* 问题图片3 */
@property (nonatomic, strong) UIImageView   *Q_image4;           /* 问题图片4 */
@property (nonatomic, strong) UIButton      *A_PhotoButton;      /* 答者头像 */
@property (nonatomic, strong) UIImageView   *A_TypeImage;        /* 答者图片标示 */
@property (nonatomic, strong) UILabel       *A_nameLable;        /* 答者昵称 */
@property (nonatomic, strong) UILabel       *A_contentLabel;     /* 答案 */
@property (nonatomic, strong) UIImageView   *A_image1;           /* 答案图片1 */
@property (nonatomic, strong) UIImageView   *A_image2;           /* 答案图片2 */
@property (nonatomic, strong) UIImageView   *A_image3;           /* 答案图片3 */
@property (nonatomic, strong) UIImageView   *A_image4;           /* 答案图片4 */
@property (nonatomic, strong) MCFireworksButton *likeButton;     /* 喜欢按钮 */
@property (nonatomic, strong) UILabel       *likeNumLabel;       /* 喜欢次数 */
@property (nonatomic, strong) UIImageView   *lineImage;          /* 单元格线 */
@property (nonatomic, strong) UINavigationController *navigation; /*cell VC*/
@property (nonatomic, strong) iEver_N_quesList_object *whole_object;
@end

@implementation iEver_normalUserAnsweredCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setViewController:(UINavigationController *)MVNavigation{

    self.navigation = MVNavigation;
}
- (void) configureCell {

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.Q_PhotoButton];
        [self.contentView addSubview:self.Q_TypeImage];
        [self.contentView addSubview:self.Q_nameLable];
        [self.contentView addSubview:self.Q_contentLabel];
        [self.contentView addSubview:self.A_PhotoButton];
        [self.contentView addSubview:self.A_TypeImage];
        [self.contentView addSubview:self.A_nameLable];
        [self.contentView addSubview:self.A_contentLabel];
        [self.contentView addSubview:self.likeNumLabel];
        self.likeNumLabel.hidden = YES;
        [self.contentView addSubview:self.likeButton];
        self.likeButton.hidden = YES;
        [self.contentView addSubview:self.lineImage];
        [self.contentView addSubview:self.Q_image1];
        [self.contentView addSubview:self.Q_image2];
        [self.contentView addSubview:self.Q_image3];
        [self.contentView addSubview:self.Q_image4];
        [self.contentView addSubview:self.A_image1];
        [self.contentView addSubview:self.A_image2];
        [self.contentView addSubview:self.A_image3];
        [self.contentView addSubview:self.A_image4];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)setObject:(iEver_N_quesList_object *)object
{
    _whole_object = object;

    self.Q_image1.hidden = YES;
    self.Q_image2.hidden = YES;
    self.Q_image3.hidden = YES;
    self.Q_image4.hidden = YES;
    self.A_image1.hidden = YES;
    self.A_image2.hidden = YES;
    self.A_image3.hidden = YES;
    self.A_image4.hidden = YES;

    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 问者头像按钮 */
    x = 12.0;
    y = 12.0;
    width = 30.0;
    height = 30.0;
    self.Q_PhotoButton.frame = CGRectMake(x, y, width, height);
    [_Q_PhotoButton setImageWithURL:[NSURL URLWithString:object.qHeadImg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    /* 问着标示图标 */
    x = 50.0;
    y = 12.0;
    width = 15.0;
    height = 15.0;
    self.Q_TypeImage.frame = CGRectMake(x, y, width, height);

    /* 问者昵称 */
    x = 50.0;
    y = 31.0;
    width = 54.0;
    height = 15.0;
    self.Q_nameLable.frame = CGRectMake(x, y, width, height);
    self.Q_nameLable.text = object.qNickName;

    /* 问题 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[object.qContent boundingRectWithSize:CGSizeMake(190.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    self.Q_contentLabel.text = object.qContent;
    x = 110.0;
    y = 12.0;
    width = 190.0;
    height = contentSize.height;
    self.Q_contentLabel.frame = CGRectMake(x, y, width, height);

    /* 问题图片 */
    if (contentSize.height < 35.0) {
        y = self.Q_PhotoButton.frame.origin.y + self.Q_PhotoButton.frame.size.height + 10.0;
    }else{
        y = self.Q_contentLabel.frame.origin.y + self.Q_contentLabel.frame.size.height + 10.0;
    }

    if ([object._QquesPicList count] >0) {

        x = 110.0;
        y = y;
        width = 40.0;
        height = 40.0;
        NSArray *imageView_array =@[self.Q_image1,self.Q_image2,self.Q_image3,self.Q_image4];
        for (int i = 0; i < [object._QquesPicList count]; i++) {
            UIImageView *imageview = imageView_array[i];
            imageview.hidden = NO;
            [imageview setImageWithURL:[NSURL URLWithString:[object._QquesPicList objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            imageview.frame = CGRectMake((40.0 + 5.0) * i + x, y, width, height);
            UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
            tapGesture.numberOfTapsRequired=1;
            imageview.userInteractionEnabled = YES;
            [imageview addGestureRecognizer:tapGesture];
        }

        y = self.Q_image1.frame.origin.y + self.Q_image1.frame.size.height + 10.0;
    }

    /* 答者头像 */
    x = 12.0;
    y = y;
    width = 30.0;
    height = 30.0;
    self.A_PhotoButton.frame = CGRectMake(x, y, width, height);
    [_A_PhotoButton setImageWithURL:[NSURL URLWithString:object.aHeadImg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    /* 答者图片标示 */
    x = 50.0;
    y = y;
    width = 15.0;
    height = 15.0;
    self.A_TypeImage.frame = CGRectMake(x, y, width, height);

    y = self.A_TypeImage.frame.origin.y + self.A_TypeImage.frame.size.height + 10.0;

    /* 答者昵称 */
    x = 50.0;
    y = y;
    width = 54.0;
    height = 15.0;
    self.A_nameLable.frame = CGRectMake(x, y, width, height);
    self.A_nameLable.text = object.aNickName;

    /* 答案 */
    CGSize A_contentSize =[object.aContent boundingRectWithSize:CGSizeMake(190.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    self.A_contentLabel.text = object.aContent;
    x = 110;
    y = self.A_PhotoButton.frame.origin.y;
    width = 190.0;
    height = A_contentSize.height;
    self.A_contentLabel.frame = CGRectMake(x, y, width, height);


    /* 答案图片 */
    if (A_contentSize.height < 35.0) {
        y = self.A_PhotoButton.frame.origin.y + self.A_PhotoButton.frame.size.height + 10.0;
    }else{
        y = self.A_contentLabel.frame.origin.y + self.A_contentLabel.frame.size.height + 10.0;
    }

    if ([object._AquesPicList count] >0) {

        x = 110;
        y = y;
        width = 40.0;
        height = 40.0;
        NSArray *imageView_array =@[self.A_image1,self.A_image2,self.A_image3,self.A_image4];
        for (int i = 0; i < [object._AquesPicList count]; i++) {
            UIImageView *imageview = imageView_array[i];
            imageview.hidden = NO;
            [imageview setImageWithURL:[NSURL URLWithString:[object._AquesPicList objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            imageview.frame = CGRectMake((40.0 + 5.0) * i + x, y, width, height);
            UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
            imageview.userInteractionEnabled = YES;
            tapGesture.numberOfTapsRequired=1;
            [imageview addGestureRecognizer:tapGesture];
        }

        y = self.A_image1.frame.origin.y + self.A_image1.frame.size.height + 10.0;
    }

    /* 喜欢次数 */
    x = 220.0;
    y = y;
    width = 50.0;
    height = 20.0;
    self.likeNumLabel.frame = CGRectMake(x, y, width, height);
    self.likeNumLabel.text = [NSString stringWithFormat:@"%d",object.likeTotal];

    /* 喜欢按钮 */
    x = 280.0;
    y = y;
    width = 20.0;
    height = 20.0;
    self.likeButton.frame = CGRectMake(x, y, width, height);
    if (object._selected) {
        [_likeButton setImage:[UIImage imageNamed:@"detail_commentLike_click"] forState:UIControlStateNormal];
    }else{
        [_likeButton setImage:[UIImage imageNamed:@"detail_commentLike"] forState:UIControlStateNormal];
    }

    y = self.likeButton.frame.origin.y + self.likeButton.frame.size.height + 10.0;
    /* 单元格线 */
    x = 15.0;
    y =  y - 25;
    width = 290.0;
    height = 1.0;
    self.lineImage.frame = CGRectMake(x, y, width, height);
}

-(void)handleTapGesture:(UIGestureRecognizer*)sender{
    [SJAvatarBrowser showImage:(UIImageView*)sender.view];
}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    iEver_N_quesList_object *N_answer_object = (iEver_N_quesList_object *)object;

    CGFloat h = 12.0;
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[N_answer_object.qContent boundingRectWithSize:CGSizeMake(190.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    if (contentSize.height < 35.0) {
        h = h + 30.0 + 10.0;
    }else{
        h = h + contentSize.height + 10.0;
    }

    if ([N_answer_object._QquesPicList count] >0) {
        h = h + 40.0 + 10.0;
    }

    CGSize A_contentSize =[N_answer_object.aContent boundingRectWithSize:CGSizeMake(190.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    if (A_contentSize.height < 35.0) {
        h = h + 30.0 + 10.0;
    }else{
        h = h + A_contentSize.height + 10.0;
    }
    if ([N_answer_object._AquesPicList count] >0) {
        h = h + 40.0 + 10.0 + 10.0;
    }else{
        h = h + 10.0;

    }
   	return h;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
# pragma mark - view getters
/*背景视图*/
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

/* 问者头像按钮 */
- (UIButton *)Q_PhotoButton
{
    if (!_Q_PhotoButton){
        _Q_PhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_Q_PhotoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:5.0];
    }
    return _Q_PhotoButton;
}

/* 问者标示图标 */
-(UIImageView *)Q_TypeImage{

    if (!_Q_TypeImage) {
        _Q_TypeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"discover_V_Q"]];
    }
    return _Q_TypeImage;
}

/* 问者昵称 */
- (UILabel *)Q_nameLable
{
    if (!_Q_nameLable){
        _Q_nameLable = [[UILabel alloc] init];
        _Q_nameLable.numberOfLines = 1;
        _Q_nameLable.backgroundColor = CLEAR;
        _Q_nameLable.textColor = BLACK;
        _Q_nameLable.font = TextFonts;
    }
    return _Q_nameLable;
}

/* 问题 */
- (UILabel *)Q_contentLabel
{
    if (!_Q_contentLabel){
        _Q_contentLabel = [[UILabel alloc] init];
        _Q_contentLabel.backgroundColor = CLEAR;
        _Q_contentLabel.numberOfLines = 0;
        _Q_contentLabel.textColor = BLACK;
        _Q_contentLabel.font = TextFont;
    }
    return _Q_contentLabel;
}

/* 答者头像 */
- (UIButton *)A_PhotoButton
{
    if (!_A_PhotoButton){
        _A_PhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_A_PhotoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:5.0];
    }
    return _A_PhotoButton;
}

/* 答者图片标示 */
-(UIImageView *)A_TypeImage{

    if (!_A_TypeImage) {
        _A_TypeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"discover_V_A"]];
    }
    return _A_TypeImage;
}

/* 答者昵称 */
- (UILabel *)A_nameLable
{
    if (!_A_nameLable){
        _A_nameLable = [[UILabel alloc] init];
        _A_nameLable.numberOfLines = 1;
        _A_nameLable.backgroundColor = CLEAR;
        _A_nameLable.textColor = LIST_TITLE;
        _A_nameLable.font = TextFonts;
    }
    return _A_nameLable;
}

/* 答案 */
- (UILabel *)A_contentLabel
{
    if (!_A_contentLabel){
        _A_contentLabel = [[UILabel alloc] init];
        _A_contentLabel.backgroundColor = CLEAR;
        _A_contentLabel.numberOfLines = 0;
        _A_contentLabel.textColor = LIST_TITLE;
        _A_contentLabel.font = TextFont;
    }
    return _A_contentLabel;
}

/*喜欢次数*/
- (UILabel *)likeNumLabel
{
    if (!_likeNumLabel){
        _likeNumLabel = [[UILabel alloc] init];
        _likeNumLabel.backgroundColor = CLEAR;
        _likeNumLabel.textColor = LIST_TITLE;
        _likeNumLabel.textAlignment = NSTextAlignmentRight;
        _likeNumLabel.font = TextFont;
    }
    return _likeNumLabel;
}

/*喜欢按钮*/
- (MCFireworksButton *)likeButton
{
    if (!_likeButton){
        _likeButton = [MCFireworksButton buttonWithType:UIButtonTypeCustom];
        self.likeButton.particleImage = [UIImage imageNamed:@"Sparkle"];
        self.likeButton.particleScale = 0.05;
        self.likeButton.particleScaleRange = 0.02;
        [_likeButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeButton;
}
-(void)likeAction:(id)sender{

    iEver_likeActionObject * content = [[iEver_likeActionObject alloc] init];
    if (!_whole_object._selected) {

        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_whole_object.ques_id],
                              @"type": [NSNumber numberWithInt:10],
                              };

        [[[content like:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {

             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                 _whole_object._selected = !_whole_object._selected;
                 _whole_object.likeTotal = _whole_object.likeTotal +1;
                 [self.likeButton popOutsideWithDuration:0.5];
                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike_click"] forState:UIControlStateNormal];
                 self.likeNumLabel.text = [NSString stringWithFormat:@"%d",_whole_object.likeTotal];
                 [self.likeButton animate];

             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
    else {

        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_whole_object.ques_id],
                              @"type": [NSNumber numberWithInt:10],
                              };

        [[[content unlike:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {

             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                 _whole_object._selected = !_whole_object._selected;
                 _whole_object.likeTotal = _whole_object.likeTotal -1;
                 [self.likeButton popInsideWithDuration:0.4];
                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike"] forState:UIControlStateNormal];
                 self.likeNumLabel.text = [NSString stringWithFormat:@"%d",_whole_object.likeTotal];

             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
}

/* 答者图片标示 */
-(UIImageView *)lineImage{

    if (!_lineImage) {
        _lineImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"person_line"]];
    }
    return _lineImage;
}

/* 问题图片1 */
-(UIImageView *)Q_image1{

    if (!_Q_image1) {
        _Q_image1 = [[UIImageView alloc] init];
        _Q_image1.hidden = YES;
    }
    return _Q_image1;
}
/* 问题图片2 */
-(UIImageView *)Q_image2{

    if (!_Q_image2) {
        _Q_image2 = [[UIImageView alloc] init];
        _Q_image2.hidden = YES;
    }
    return _Q_image2;
}
/* 问题图片3 */
-(UIImageView *)Q_image3{

    if (!_Q_image3) {
        _Q_image3 = [[UIImageView alloc] init];
        _Q_image3.hidden = YES;
    }
    return _Q_image3;
}
/* 问题图片4 */
-(UIImageView *)Q_image4{

    if (!_Q_image4) {
        _Q_image4 = [[UIImageView alloc] init];
        _Q_image4.hidden = YES;
    }
    return _Q_image4;
}
/* 答案图片1 */
-(UIImageView *)A_image1{

    if (!_A_image1) {
        _A_image1 = [[UIImageView alloc] init];
        _A_image1.hidden = YES;
    }
    return _A_image1;
}
/* 答案图片2 */
-(UIImageView *)A_image2{

    if (!_A_image2) {
        _A_image2 = [[UIImageView alloc] init];
        _A_image2.hidden = YES;
    }
    return _A_image2;
}
/* 答案图片3*/
-(UIImageView *)A_image3{
    
    if (!_A_image3) {
        _A_image3 = [[UIImageView alloc] init];
        _A_image3.hidden = YES;
    }
    return _A_image3;
}
/* 答案图片4 */
-(UIImageView *)A_image4{
    
    if (!_A_image4) {
        _A_image4 = [[UIImageView alloc] init];
        _A_image4.hidden = YES;
    }
    return _A_image4;
}
@end

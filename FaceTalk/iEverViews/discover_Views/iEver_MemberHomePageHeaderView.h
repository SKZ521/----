//
//  iEver_MemberHomePageHeaderView.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/25.
//  Copyright (c) 2015年 iEver. All rights reserved.
//


@class iEver_V_userObject;

@interface iEver_MemberHomePageHeaderView : UIView

@property(nonatomic,strong)iEver_V_userObject *object;

@property (strong, nonatomic) UIButton        *backButton;

@property (strong, nonatomic) UIView          *baseView;

- (void)setSubViewPosition:(CGFloat)y;

@end

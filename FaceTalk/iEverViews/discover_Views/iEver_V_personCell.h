//
//  iEver_V_personCell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-11.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_V_groupUserObject.h"

@class iEver_V_person_ViewController;

@interface iEver_V_personCell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_V_user_object   *object;
- (void)setViewController:(iEver_V_person_ViewController *)mvc;

@end

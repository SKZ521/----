//
//  iEver_discoverUpdateCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/10.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_discoverUpdateCell.h"
#import "iEver_AskMeViewController.h"
#import "UIView+FindViewController.h"
@interface iEver_discoverUpdateCell (){

}
@property (nonatomic, strong) UIView        *bottomView;       /* 背景视图 */
@property (nonatomic, strong) UIButton      *photoButton;      /* 头像按钮 */
@property (nonatomic, strong) UIButton      *askMeButton;      /* askMe按钮 */
@property (nonatomic, strong) UILabel       *nameLable;        /* 名称标签 */
@property (nonatomic, strong) UILabel       *infoLable;        /* 简介 */
@property (nonatomic, strong) UILabel       *answerLable;      /* 回答量 */
@property (nonatomic, strong) iEver_discover_userList_object *user_object;
@end

@implementation iEver_discoverUpdateCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) configureCell {
    
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.photoButton];
        [self.contentView addSubview:self.askMeButton];
        [self.contentView addSubview:self.nameLable];
        [self.contentView addSubview:self.infoLable];
        [self.contentView addSubview:self.answerLable];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    /* 单元格frame */
    //const CGRect r = self.contentView.bounds;

    /* 背景视图 */
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 320.0;
    CGFloat height = 450.0;

    /* 达人头像按钮 */
    x = 175.0/2;
    y = 30.0;
    width = 145.0;
    height = 145.0;
    self.photoButton.frame = CGRectMake(x, y, width, height);

    /* askMe Button */
    x = 175.0/2 +145.0 -40.0;
    y = 30.0 + 145.0 -40.0;
    width = 45.0;
    height = 45.0;
    self.askMeButton.frame = CGRectMake(x, y, width, height);

    /* 达人名称 */
    x = 0.0;
    y = 195.0;
    width = 320.0;
    height = 20.0;
    self.nameLable.frame = CGRectMake(x, y, width, height);
    
}
- (void)setObject:(iEver_discover_userList_object *)object
{
    _user_object = object;

    /* 达人头像 */
    [_photoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/290x",object.headImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    /* 达人名称 */
    self.nameLable.text = object.nickName;

    /* 达人类型标签 */
    self.infoLable.text = object.intro;

    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[self.infoLable.text boundingRectWithSize:CGSizeMake(200.0, 40) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    /* 达人描述 */
    CGFloat x = 60.0;
    CGFloat y = 225.0;
    CGFloat width = 200.0;
    CGFloat height = contentSize.height;
    self.infoLable.frame = CGRectMake(x, y, width, height);

    /* 回答量 */
    x = 0.0;
    y = 225.0 + contentSize.height +10.0;
    width = 320.0;
    height = 25.0;
    self.answerLable.frame = CGRectMake(x, y, width, height);
    self.answerLable.text = [NSString stringWithFormat:@"%d回答",object.answerTotal];

}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
       	return 290.0;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
# pragma mark - view getters
/*背景视图*/
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}
/*头像按钮*/
- (UIButton *)photoButton
{
    if (!_photoButton){
        _photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _photoButton.titleLabel.numberOfLines = 0;
        _photoButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_photoButton setTitleColor:BLACK forState:UIControlStateNormal];
        _photoButton.titleLabel.font = TextLIST_Fonts;
        CALayer *photoButton = [_photoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:145.0/2];
        [photoButton setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [_photoButton addTarget:self action:@selector(V_personCenter:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _photoButton;
}
-(void)V_personCenter:(id)sender{

}
/* ask me button */
- (UIButton *)askMeButton
{
    if (!_askMeButton){
        _askMeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_askMeButton setImage:[UIImage imageNamed:@"V_list_ask"] forState:UIControlStateNormal];
        [_askMeButton setImage:[UIImage imageNamed:@"V_list_ask"] forState:UIControlStateHighlighted];
        [_askMeButton addTarget:self action:@selector(askMeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _askMeButton;
}
-(void)askMeAction:(id)sender{

    iEver_AskMeViewController *_askMeViewController = [[iEver_AskMeViewController alloc]init];
    _askMeViewController.userId = _user_object.User_id;
    _askMeViewController.image  = _user_object.headImg;
    _askMeViewController.name   =_user_object.nickName;
    _askMeViewController.hidesBottomBarWhenPushed = YES;
    UIViewController *superVC = (UIViewController *)[self firstAvailableUIViewController];
    [superVC.navigationController pushViewController:_askMeViewController animated:YES];
    if ([superVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        superVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

/*达人昵称*/
- (UILabel *)nameLable
{
    if (!_nameLable){
        _nameLable = [[UILabel alloc] init];
        _nameLable.numberOfLines = 1;
        _nameLable.textAlignment = NSTextAlignmentCenter;
        _nameLable.backgroundColor = CLEAR;
        _nameLable.textColor = NAV;
        _nameLable.font = TextLIST_TITLE_Fonts;
    }
    return _nameLable;
}
/* 达人类型 */
- (UILabel *)infoLable
{
    if (!_infoLable){
        _infoLable = [[UILabel alloc] init];
        _infoLable.backgroundColor = CLEAR;
        _infoLable.numberOfLines = 0;
        _infoLable.textAlignment = NSTextAlignmentCenter;
        _infoLable.textColor = NAV;
        _infoLable.font = TextFont;
    }
    return _infoLable;
}

/* 封面描述 */
- (UILabel *)answerLable
{
    if (!_answerLable){
        _answerLable = [[UILabel alloc] init];
        _answerLable.backgroundColor = CLEAR;
        _answerLable.numberOfLines = 1;
        _answerLable.textAlignment = NSTextAlignmentCenter;
        _answerLable.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapLable:)];
        tapGesture.numberOfTapsRequired = 1;
        [_answerLable addGestureRecognizer:tapGesture];
        _answerLable.textColor = CZ;
        _answerLable.font = TextFont;
    }
    return _answerLable;
}

- (void)TapLable:(UITapGestureRecognizer *)gesture {

}
@end

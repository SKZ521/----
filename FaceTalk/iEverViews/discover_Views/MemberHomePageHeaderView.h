//
//  MemberHomePageHeaderView.h
//  Volunteers
//
//  Created by 'Sooongz on 14/8/1.
//  Copyright (c) 2014年 Sooongz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXStretchableHeaderView.h"

@class iEver_V_userObject;


@interface MemberHomePageHeaderView : AXStretchableHeaderView <AXStretchableHeaderViewDelegate>

@property (strong, nonatomic) UIImageView  *imageView;

@property (strong, nonatomic) UIImageView  *avatarImageView;
@property (strong, nonatomic) UIImageView  *avatarBottomImageView;
@property (strong, nonatomic) UIImageView  *sexImageView;
@property (strong, nonatomic) UIImageView  *starImageView;
@property (strong, nonatomic) UIImageView  *refreshImageView;

@property (strong, nonatomic) UILabel      *userNameLabel;
@property (strong, nonatomic) UILabel      *starCountLabel;


@property (strong, nonatomic) UIButton     *backButton;

@property (strong, nonatomic) UIImageView *partyIcon;
@property (strong, nonatomic) UIImageView *groupIcon;

@property(nonatomic,strong)iEver_V_userObject *object;

- (void)setSubViewPosition:(CGFloat)y;

- (void)startAnimation;
- (void)stopAnimation;

@end

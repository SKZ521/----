//
//  iEver_V_answeredCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-18.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_V_answeredCell.h"
#import "MCFireworksButton.h"
#import "iEver_likeActionObject.h"
#import "MJPhotoBrowser.h"
#import "MJPhoto.h"
@interface iEver_V_answeredCell (){
    // 问题图片
    NSMutableArray *qUrlArray;
    NSMutableArray *qImageViewArray;

    // 回答图片
    NSMutableArray *aUrlArray;
    NSMutableArray *aImageViewArray;
}
@property (nonatomic, strong) UIView        *bottomView;         /* 背景视图 */
@property (nonatomic, strong) UIButton      *Q_PhotoButton;      /* 问者头像按钮 */
@property (nonatomic, strong) UIImageView   *Q_TypeImage;        /* 问着标示图标 */
@property (nonatomic, strong) UILabel       *Q_nameLable;        /* 问者昵称 */
@property (nonatomic, strong) UILabel       *Q_TimeLable;        /* 提问时间 */
@property (nonatomic, strong) UILabel       *Q_InfoLable;        /* 问者标签 */
@property (nonatomic, strong) UILabel       *Q_contentLabel;     /* 问题 */
@property (nonatomic, strong) UIButton      *Q_DetailButton;     /* 问题评论详情 */
@property (nonatomic, strong) UIImageView   *Q_image1;           /* 问题图片1 */
@property (nonatomic, strong) UIImageView   *Q_image2;           /* 问题图片2 */
@property (nonatomic, strong) UIImageView   *Q_image3;           /* 问题图片3 */
@property (nonatomic, strong) UIImageView   *Q_image4;           /* 问题图片4 */
@property (nonatomic, strong) UIImageView   *Q_image5;           /* 问题图片5 */
@property (nonatomic, strong) UIImageView   *Q_image6;           /* 问题图片6 */
@property (nonatomic, strong) UIImageView   *Q_image7;           /* 问题图片7 */
@property (nonatomic, strong) UIImageView   *Q_image8;           /* 问题图片8 */
@property (nonatomic, strong) UIImageView   *Q_image9;           /* 问题图片9 */
@property (nonatomic, strong) UIButton      *A_PhotoButton;      /* 答者头像 */
@property (nonatomic, strong) UIImageView   *A_TypeImage;        /* 答者图片标示 */
@property (nonatomic, strong) UILabel       *A_nameLable;        /* 答者昵称 */
@property (nonatomic, strong) UILabel       *A_TimeLable;        /* 回答时间 */
@property (nonatomic, strong) UILabel       *A_InfoLable;        /* 答者标签 */
@property (nonatomic, strong) UILabel       *A_contentLabel;     /* 答案 */
@property (nonatomic, strong) UIImageView   *A_image1;           /* 答案图片1 */
@property (nonatomic, strong) UIImageView   *A_image2;           /* 答案图片2 */
@property (nonatomic, strong) UIImageView   *A_image3;           /* 答案图片3 */
@property (nonatomic, strong) UIImageView   *A_image4;           /* 答案图片4 */
@property (nonatomic, strong) UIImageView   *A_image5;           /* 答案图片5 */
@property (nonatomic, strong) UIImageView   *A_image6;           /* 答案图片6 */
@property (nonatomic, strong) UIImageView   *A_image7;           /* 答案图片7 */
@property (nonatomic, strong) UIImageView   *A_image8;           /* 答案图片8 */
@property (nonatomic, strong) UIImageView   *A_image9;           /* 答案图片9 */
@property (nonatomic, strong) MCFireworksButton *likeButton;     /* 喜欢按钮 */
@property (nonatomic, strong) UILabel       *likeNumLabel;       /* 喜欢次数 */
@property (nonatomic, strong) UIImageView   *CutoffRuleImage;    /* 问答之间分割线 */
@property (nonatomic, strong) UIImageView   *lineImage;          /* 单元格线 */
@property (nonatomic, strong) UINavigationController *navigation; /*cell VC*/
@property (nonatomic, strong) iEver_V_quesList_object *whole_object;
@end

@implementation iEver_V_answeredCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {

    }
    return self;
}

- (void)setViewController:(UINavigationController *)MVNavigation{

    self.navigation = MVNavigation;
}

- (void)configureCell
{

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = CLEAR;
        self.contentView.backgroundColor = CLEAR;

        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.Q_PhotoButton];
        [self.contentView addSubview:self.Q_TypeImage];
        [self.contentView addSubview:self.Q_nameLable];
        [self.contentView addSubview:self.Q_TimeLable];
        [self.contentView addSubview:self.Q_InfoLable];
        [self.contentView addSubview:self.Q_DetailButton];
        [self.contentView addSubview:self.Q_contentLabel];
        [self.contentView addSubview:self.Q_DetailButton];

        [self.contentView addSubview:self.A_PhotoButton];
        [self.contentView addSubview:self.A_TypeImage];
        [self.contentView addSubview:self.A_nameLable];
        [self.contentView addSubview:self.A_TimeLable];
        [self.contentView addSubview:self.A_InfoLable];
        [self.contentView addSubview:self.A_contentLabel];
        [self.contentView addSubview:self.likeNumLabel];
        [self.contentView addSubview:self.likeButton];
        [self.contentView addSubview:self.lineImage];
//        [self.contentView addSubview:self.CutoffRuleImage];
        [self.contentView addSubview:self.Q_image1];
        [self.contentView addSubview:self.Q_image2];
        [self.contentView addSubview:self.Q_image3];
        [self.contentView addSubview:self.Q_image4];
        [self.contentView addSubview:self.Q_image5];
        [self.contentView addSubview:self.Q_image6];
        [self.contentView addSubview:self.Q_image7];
        [self.contentView addSubview:self.Q_image8];
        [self.contentView addSubview:self.Q_image9];
        [self.contentView addSubview:self.A_image1];
        [self.contentView addSubview:self.A_image2];
        [self.contentView addSubview:self.A_image3];
        [self.contentView addSubview:self.A_image4];
        [self.contentView addSubview:self.A_image5];
        [self.contentView addSubview:self.A_image6];
        [self.contentView addSubview:self.A_image7];
        [self.contentView addSubview:self.A_image8];
        [self.contentView addSubview:self.A_image9];
    }
    return self;
}

- (void)setObject:(iEver_V_quesList_object *)object
{
    _whole_object = object;

    self.Q_image1.hidden = YES;
    self.Q_image2.hidden = YES;
    self.Q_image3.hidden = YES;
    self.Q_image4.hidden = YES;
    self.Q_image5.hidden = YES;
    self.Q_image6.hidden = YES;
    self.Q_image7.hidden = YES;
    self.Q_image8.hidden = YES;
    self.Q_image9.hidden = YES;

    self.A_image1.hidden = YES;
    self.A_image2.hidden = YES;
    self.A_image3.hidden = YES;
    self.A_image4.hidden = YES;
    self.A_image5.hidden = YES;
    self.A_image6.hidden = YES;
    self.A_image7.hidden = YES;
    self.A_image8.hidden = YES;
    self.A_image9.hidden = YES;

    if (qUrlArray)
    {
        [qUrlArray removeAllObjects];
    } else {
        qUrlArray = [[NSMutableArray alloc] init];
    }
    if (qImageViewArray)
    {
        [qImageViewArray removeAllObjects];
    } else {
        qImageViewArray = [[NSMutableArray alloc] init];
    }

    if (aUrlArray)
    {
        [aUrlArray removeAllObjects];
    } else {
        aUrlArray = [[NSMutableArray alloc] init];
    }
    if (aImageViewArray)
    {
        [aImageViewArray removeAllObjects];
    } else {
        aImageViewArray = [[NSMutableArray alloc] init];
    }


    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 问题者用户头像 */
    x = 11.0;
    y = 11.0;
    width = 30.0;
    height = 30.0;
    [self.Q_PhotoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/60x",object.qHeadImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    self.Q_PhotoButton.frame = CGRectMake(x, y, width, height);

    /* 问题A标示 */
    x = x + (CGRectGetWidth(_Q_PhotoButton.frame)-20.0f)/2;
    y = CGRectGetMaxY(_Q_PhotoButton.frame) + 2.0f ;
    width = 20.0f;
    height = 14.0f;
    self.Q_TypeImage.frame = CGRectMake(x, y, width, height);

    /* 问者昵称 */
    x = CGRectGetMaxX(_Q_PhotoButton.frame) + 5.0f;
    y = 11.0;
    width = 175.0;
    height = 20.0;
    self.Q_nameLable.frame = CGRectMake(x, y, width, height);
    self.Q_nameLable.text = [NSString stringWithFormat:@"%@:",object.qNickName];

    /* 问题发布时间 */
    x = CGRectGetWidth(self.frame)-90.0f-11.0f;
    y = 11.0f;
    width = 90.0f;
    height = 20.0f;
    self.Q_TimeLable.frame = CGRectMake(x, y, width, height);
    self.Q_TimeLable.text = [self dateFormatWithTimeInterval:object.createTime];

    /* 提问者标签 */
    x = CGRectGetMaxX(_Q_PhotoButton.frame) + 5.0f;
    y = CGRectGetMaxY(_Q_nameLable.frame);
    width = 175.0f;
    height = 20.0f;
    self.Q_InfoLable.frame = CGRectMake(x, y, width, height);

    /* 问题内容 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"ZHSRXT--GBK1-0" size:14], NSFontAttributeName,nil];
    self.Q_contentLabel.text = object.qContent;
    x      = CGRectGetMaxX(_Q_PhotoButton.frame) + 5.0f;
    y      = CGRectGetMaxY(_Q_InfoLable.frame);
    width  = CGRectGetWidth(self.frame)-(CGRectGetMaxX(_Q_PhotoButton.frame) + 5.0f)-11.0f;
    CGSize contentSize =[object.qContent boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    height = contentSize.height;
    self.Q_contentLabel.frame = CGRectMake(x, y , width, height);

    /* 问题图片 */
    x = CGRectGetMaxX(_Q_PhotoButton.frame) + 5.0f;
    y = CGRectGetMaxY(self.Q_contentLabel.frame) + 3.0f;
    width = 72.0f;
    height = 72.0f;
    NSInteger count = 0;
    if ([object._QquesPicList count] >0) {
        NSArray *imageView_array =@[self.Q_image1,self.Q_image2,self.Q_image3,self.Q_image4,self.Q_image5,self.Q_image6,self.Q_image7,self.Q_image8,self.Q_image9];
        count = [object._QquesPicList count]<=[imageView_array count]?[object._QquesPicList count]:[imageView_array count];
        for (int i = 0; i < count; i++) {
            [qUrlArray addObject:[object._QquesPicList objectAtIndex:i]];
            UIImageView *imageview = imageView_array[i];
            [qImageViewArray addObject:imageview];
            imageview.hidden = NO;
            [imageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/315x",[object._QquesPicList objectAtIndex:i]]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            imageview.frame = CGRectMake(x+(72.0f+3.0f)*(i%3), y+(72.0f+3.0f)*(i/3), width, height);
            imageview.contentMode = UIViewContentModeScaleAspectFill;
            imageview.clipsToBounds = YES;
            UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleQuesTapGesture:)];
            tapGesture.numberOfTapsRequired=1;
            imageview.userInteractionEnabled = YES;
            [imageview addGestureRecognizer:tapGesture];
        }
        if (count%3==0)
        {
            y = y + (72.0f + 3.0f)*(count/3);
        } else {
            y = y + (72.0f + 3.0f)*(count/3 + 1);
        }
    }

    /* 回答详情按钮 */
    NSString *detailTitle = [NSString stringWithFormat:@"%d条回答>>",object.answerTotal];
    contentSize = [detailTitle sizeWithAttributes:tdic];
    width = contentSize.width;
    height = 20.0f;
    x = CGRectGetWidth(self.frame)-width-11.0f;
    [_Q_DetailButton setFrame:CGRectMake(x, y, width, height)];
    [_Q_DetailButton setTitle:detailTitle forState:UIControlStateNormal];

    /* 问题背景图 */
    x = 8.0f;
    y = 8.0f;
    width = CGRectGetWidth(self.frame)-16.0f;
    height = CGRectGetMaxY(_Q_DetailButton.frame) - 8.0f;
    [_bottomView setFrame:CGRectMake(x, y, width, height)];

    /* 回答者用户头像 */
    x = 11.0;
    y = CGRectGetMaxY(_bottomView.frame) + 8.0f;
    width = 30.0f;
    height = 30.0f;
    [self.A_PhotoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/60x",object.aHeadImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    self.A_PhotoButton.frame = CGRectMake(x, y, width, height);

    if (object.aUserType == 20)
    {
        /* 答案Q标示 */
        x = CGRectGetMaxX(_A_PhotoButton.frame)- 10.0f;
        y = CGRectGetMaxY(_A_PhotoButton.frame)- 10.0f;
        width = 14.0;
        height = 14.0;
        self.A_TypeImage.frame = CGRectMake(x, y, width, height);
        _A_TypeImage.hidden = NO;
    } else {
        _A_TypeImage.hidden = YES;
    }

    /* 答者昵称 */
    x = CGRectGetMaxX(_A_PhotoButton.frame)+5.0f;
    y = CGRectGetMaxY(_bottomView.frame) + 8.0f;
    width = 175.0;
    height = 20.0;
    self.A_nameLable.frame = CGRectMake(x, y, width, height);
    self.A_nameLable.text = [NSString stringWithFormat:@"%@ 回答:",object.aNickName];

    /* 回复时间 */
    x = CGRectGetWidth(self.frame)-90.0f-11.0f;
    y = CGRectGetMaxY(_bottomView.frame) + 8.0f;
    width = 90.0f;
    height = 20.0f;
    self.A_TimeLable.frame = CGRectMake(x, y, width, height);
    self.A_TimeLable.text = [self dateFormatWithTimeInterval:object.aCreateTime];

    /* 回答者标签 */
    x = CGRectGetMaxX(_A_PhotoButton.frame)+5.0f;
    y = CGRectGetMaxY(_A_nameLable.frame);
    width = 175.0f;
    height = 20.0f;
    self.A_InfoLable.frame = CGRectMake(x, y, width, height);

    /* 答案 */
    x = CGRectGetMaxX(_A_PhotoButton.frame)+5.0f;
    y = CGRectGetMaxY(_A_InfoLable.frame);
    width = CGRectGetWidth(self.frame)-(CGRectGetMaxX(_Q_PhotoButton.frame) + 5.0f)-11.0f;
    CGSize A_contentSize = [object.aContent boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    self.A_contentLabel.text = object.aContent;
    height = A_contentSize.height;
    self.A_contentLabel.frame = CGRectMake(x, y, width, height);
    _A_contentLabel.text = object.aContent;

    /* 答案图片 */
    x = CGRectGetMaxX(_A_PhotoButton.frame)+5.0f;
    y = CGRectGetMaxY(_A_contentLabel.frame)+3.0f;
    width = 72.0f;
    height = 72.0f;
    if ([object._AquesPicList count] >0)
    {
        NSArray *imageView_array =@[self.A_image1,self.A_image2,self.A_image3,self.A_image4,self.A_image5,self.A_image6,self.A_image7,self.A_image8,self.A_image9];
        NSInteger count = [object._AquesPicList count]<=[imageView_array count]?[object._AquesPicList count]:[imageView_array count];
        for (int i = 0; i < count; i++) {
            [aUrlArray addObject:[object._AquesPicList objectAtIndex:i]];
            UIImageView *imageview = imageView_array[i];
            [aImageViewArray addObject:imageview];
            imageview.hidden = NO;
            [imageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/315x",[object._AquesPicList objectAtIndex:i]]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            imageview.frame = CGRectMake(x+(72.0f+3.0f)*(i%3), y+(72.0f+3.0f)*(i/3), width, height);
            imageview.contentMode = UIViewContentModeScaleAspectFill;
            imageview.clipsToBounds = YES;
            UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleAnsTapGesture:)];
            imageview.userInteractionEnabled = YES;
            tapGesture.numberOfTapsRequired=1;
            [imageview addGestureRecognizer:tapGesture];
        }
        if (count%3==0)
        {
            y = y + (72.0f + 3.0f)*(count/3);
        } else {
            y = y + (72.0f + 3.0f)*(count/3 + 1);
        }
    }

    /* 喜欢数量 */
    NSString *likeString = [NSString stringWithFormat:@"赞(%d)",object.praiseTotal];
    contentSize = [likeString sizeWithAttributes:tdic];
    width = contentSize.width;
    height = 20.0f;
    x = CGRectGetWidth(self.frame)-width-11.0f;
    self.likeNumLabel.text = likeString;
    self.likeNumLabel.frame = CGRectMake(x, y, width, height);

    /*  点赞按钮 */
    UIImage *detail_commentLike       = [UIImage imageNamed:@"detail_commentLike"];
    UIImage *detail_commentLike_click = [UIImage imageNamed:@"detail_commentLike_click"];
    width = 55.0;
    height = 22.0;
    x = CGRectGetMinX(_likeNumLabel.frame)-width/2-detail_commentLike.size.width/2;

    if (object.isPraise) {
        [self.likeButton setImage:detail_commentLike_click forState:UIControlStateNormal];
    }else{
        [self.likeButton setImage:detail_commentLike forState:UIControlStateNormal];
    }
    self.likeButton.frame = CGRectMake(x, y, width, height);

    /* 单元格线 */
    x = 0.0;
    y = CGRectGetMaxY(_likeButton.frame)+10.0f;
    width = CGRectGetWidth(self.frame);
    height = 5.0f;
    self.lineImage.frame = CGRectMake(x, y, width, height);
    NSLog(@"_line maxY:%f",CGRectGetMaxY(_lineImage.frame));
}

- (void)handleQuesTapGesture:(UIGestureRecognizer*)sender
{
    int count = qUrlArray.count;
    NSMutableArray *photos = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        // 更换高清尺寸
        NSString *url = [NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",[qUrlArray objectAtIndex:i]];
        MJPhoto *photo = [[MJPhoto alloc] init];
        photo.url = [NSURL URLWithString:url]; // 图片路径
        photo.srcImageView = qImageViewArray[i]; // 来源于哪个UIImageView
        [photos addObject:photo];
    }

    // 2.显示相册
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.currentPhotoIndex = sender.view.tag; // 弹出相册时显示的第一张图片是？
    browser.photos = photos; // 设置所有的图片
    [browser show];
}

- (void)handleAnsTapGesture:(UIGestureRecognizer*)sender
{
    int count = aUrlArray.count;
    NSMutableArray *photos = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        // 更换高清尺寸
        NSString *url = [NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",[aUrlArray objectAtIndex:i]];
        MJPhoto *photo = [[MJPhoto alloc] init];
        photo.url = [NSURL URLWithString:url]; // 图片路径
        photo.srcImageView = aImageViewArray[i]; // 来源于哪个UIImageView
        [photos addObject:photo];
    }

    // 2.显示相册
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.currentPhotoIndex = sender.view.tag; // 弹出相册时显示的第一张图片是？
    browser.photos = photos; // 设置所有的图片
    [browser show];
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    iEver_V_quesList_object *V_answer_object = (iEver_V_quesList_object *)object;

    // 上边缘到提问者昵称间隙
    CGFloat height = 11.0f;

    /* 昵称 */
    height += 20.0f;

    // 标签
    height += 20.0f;

    /* 问题内容 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"ZHSRXT--GBK1-0" size:14], NSFontAttributeName,nil];
    CGSize contentSize = CGSizeZero;
    if (V_answer_object.qContent && [V_answer_object.qContent length]>0)
    {
        contentSize = [V_answer_object.qContent boundingRectWithSize:CGSizeMake(CGRectGetWidth(tableView.frame)-57.0f, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
        height = height + contentSize.height + 3.0;
    }

    /* 图片 */
    NSInteger count = [V_answer_object._QquesPicList count]<=9?[V_answer_object._QquesPicList count]:9;
    if (count%3 == 0)
    {
        height += (72.0f + 3.0f)*count/3;
    } else {
        height += (72.0f + 3.0f)*(count/3 + 1);
    }
    // n条回答
    height += 20.0f;
    // 间隙
    height += 8.0f;
    // 回答昵称
    height += 20.0f;
    // 标签
    height += 20.0f;
    /* 问题内容 */
    if (V_answer_object.aContent && [V_answer_object.aContent length]>0)
    {
        contentSize = [V_answer_object.aContent boundingRectWithSize:CGSizeMake(CGRectGetWidth(tableView.frame)-57.0f, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
        height = height + contentSize.height + 3.0;
    }
    /* 图片 */
    count = [V_answer_object._AquesPicList count]<=9?[V_answer_object._AquesPicList count]:9;
    if (count%3 == 0)
    {
        height += (72.0f + 3.0f)*count/3;
    } else {
        height += (72.0f + 3.0f)*(count/3 + 1);
    }
    height += 22.0f;
    height += 10.0f;
    height += 5.0f;
    NSLog(@"cell height:%f",height);
   	return height;
}

- (NSString *)dateFormatWithTimeInterval:(NSTimeInterval)time
{
    //获取当前时间
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:time];

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:date];

//    int year = [dateComponent year];
    int month = [dateComponent month];
    int day = [dateComponent day];
    int hour = [dateComponent hour];
    int minute = [dateComponent minute];
//    int second = [dateComponent second];

    return [NSString stringWithFormat:@"%d月%d日 %d:%d",month,day,hour,minute];
}

# pragma mark - view getters
/*背景视图*/
- (UIView *)bottomView
{
    if (!_bottomView)
    {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = RGBCOLOR(246.0f, 246.0f, 246.0f);
    }
    return _bottomView;
}

/* 问者头像按钮 */
- (UIButton *)Q_PhotoButton
{
    if (!_Q_PhotoButton){
        _Q_PhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_Q_PhotoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:15.0];
    }
    return _Q_PhotoButton;
}

/* 问者标示图标 */
-(UIImageView *)Q_TypeImage{

    if (!_Q_TypeImage) {
        _Q_TypeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"discover_V_Q"]];
    }
    return _Q_TypeImage;
}

/* 问者昵称 */
- (UILabel *)Q_nameLable
{
    if (!_Q_nameLable){
        _Q_nameLable = [[UILabel alloc] init];
        _Q_nameLable.numberOfLines = 1;
        _Q_nameLable.backgroundColor = CLEAR;
        _Q_nameLable.textColor = RGBCOLOR(109.0f, 77.0f, 154.0f);
        _Q_nameLable.font = TitleFont;
    }
    return _Q_nameLable;
}

/* 提问时间 */
- (UILabel *)Q_TimeLable
{
    if (!_Q_TimeLable){
        _Q_TimeLable = [[UILabel alloc] init];
        _Q_TimeLable.numberOfLines = 1;
        _Q_TimeLable.textAlignment = NSTextAlignmentRight;
        _Q_TimeLable.backgroundColor = CLEAR;
        _Q_TimeLable.textColor = RGBCOLOR(196.0f, 196.0f, 196.0f);
        _Q_TimeLable.font = TextFont;
    }
    return _Q_TimeLable;
}

/* 问者标签 */
- (UILabel *)Q_InfoLable
{
    if (!_Q_InfoLable){
        _Q_InfoLable = [[UILabel alloc] init];
        _Q_InfoLable.numberOfLines = 1;
        _Q_InfoLable.backgroundColor = CLEAR;
        _Q_InfoLable.textColor = BLACK;
        _Q_InfoLable.font = TitleFont;
    }
    return _Q_InfoLable;
}

- (UIButton *)Q_DetailButton
{
    if (!_Q_DetailButton)
    {
        _Q_DetailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_Q_DetailButton setBackgroundColor:CLEAR];
        [_Q_DetailButton setTitleColor:RGBCOLOR(109.0f, 77.0f, 154.0f) forState:UIControlStateNormal];
        [_Q_DetailButton.titleLabel setFont:[UIFont fontWithName:@"ZHSRXT--GBK1-0" size:14]];
    }
    return _Q_DetailButton;
}

/* 问题 */
- (UILabel *)Q_contentLabel
{
    if (!_Q_contentLabel){
        _Q_contentLabel = [[UILabel alloc] init];
        _Q_contentLabel.backgroundColor = CLEAR;
        _Q_contentLabel.numberOfLines = 0;
        _Q_contentLabel.textColor = BLACK;
        _Q_contentLabel.font = TextFont;
    }
    return _Q_contentLabel;
}

/* 答者头像 */
- (UIButton *)A_PhotoButton
{
    if (!_A_PhotoButton){
        _A_PhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_A_PhotoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:15.0];
    }
    return _A_PhotoButton;
}

/* 答者图片标示 */
-(UIImageView *)A_TypeImage{

    if (!_A_TypeImage) {
        _A_TypeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MY_VuserType"]];
    }
    return _A_TypeImage;
}

/* 答者昵称 */
- (UILabel *)A_nameLable
{
    if (!_A_nameLable){
        _A_nameLable = [[UILabel alloc] init];
        _A_nameLable.numberOfLines = 1;
        _A_nameLable.backgroundColor = CLEAR;
        _A_nameLable.textColor = RGBCOLOR(109.0f, 77.0f, 154.0f);
        _A_nameLable.font = TitleFont;
    }
    return _A_nameLable;
}

/* 答者昵称 */
- (UILabel *)A_TimeLable
{
    if (!_A_TimeLable){
        _A_TimeLable = [[UILabel alloc] init];
        _A_TimeLable.numberOfLines = 1;
        _A_TimeLable.textAlignment = NSTextAlignmentRight;
        _A_TimeLable.backgroundColor = CLEAR;
        _A_TimeLable.textColor = RGBCOLOR(196.0f, 196.0f, 196.0f);
        _A_TimeLable.font = TextFont;
    }
    return _A_TimeLable;
}

/* 标签 */
- (UILabel *)A_InfoLable
{
    if (!_A_InfoLable){
        _A_InfoLable = [[UILabel alloc] init];
        _A_InfoLable.backgroundColor = CLEAR;
        _A_InfoLable.numberOfLines = 0;
        _A_InfoLable.textColor = BLACK;
        _A_InfoLable.font = TextFont;
    }
    return _A_InfoLable;
}

/* 答案 */
- (UILabel *)A_contentLabel
{
    if (!_A_contentLabel){
        _A_contentLabel = [[UILabel alloc] init];
        _A_contentLabel.backgroundColor = CLEAR;
        _A_contentLabel.numberOfLines = 0;
        _A_contentLabel.textColor = BLACK;
        _A_contentLabel.font = TextFont;
    }
    return _A_contentLabel;
}
 /*喜欢次数*/
- (UILabel *)likeNumLabel
{
    if (!_likeNumLabel){
        _likeNumLabel = [[UILabel alloc] init];
        _likeNumLabel.backgroundColor = CLEAR;
        _likeNumLabel.textColor = RGBCOLOR(109.0f, 77.0f, 154.0f);
        _likeNumLabel.font = TextFont;
    }
    return _likeNumLabel;
}

/*喜欢按钮*/
- (MCFireworksButton *)likeButton
{
    if (!_likeButton)
    {
        _likeButton = [MCFireworksButton buttonWithType:UIButtonTypeCustom];
        self.likeButton.particleImage = [UIImage imageNamed:@"Sparkle"];
        self.likeButton.particleScale = 0.05;
        self.likeButton.particleScaleRange = 0.02;
        [_likeButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeButton;
}

- (void)likeAction:(id)sender
{
    iEver_likeActionObject * content = [[iEver_likeActionObject alloc] init];
    if (!_whole_object.isPraise) {

        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_whole_object.ques_id],
                              @"type": [NSNumber numberWithInt:10],
                              };

        [[[content like:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {

             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                 _whole_object.isPraise = !_whole_object.isPraise;
                 _whole_object.praiseTotal = _whole_object.praiseTotal +1;
                 [self.likeButton popOutsideWithDuration:0.5];
                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike_click"] forState:UIControlStateNormal];
                 self.likeNumLabel.text = [NSString stringWithFormat:@"赞(%d)",_whole_object.praiseTotal];
                 [self.likeButton animate];

             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
    else {

        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_whole_object.ques_id],
                              @"type": [NSNumber numberWithInt:10],
                              };

        [[[content unlike:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {

             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                 _whole_object.isPraise = !_whole_object.isPraise;
                 _whole_object.praiseTotal = _whole_object.praiseTotal -1;
                 [self.likeButton popInsideWithDuration:0.4];
                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike"] forState:UIControlStateNormal];
                 self.likeNumLabel.text = [NSString stringWithFormat:@"赞(%d)",_whole_object.praiseTotal];

             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
}

/* 单元格分割线图片标示 */
- (UIImageView *)lineImage
{
    if (!_lineImage)
    {
        _lineImage       = [[UIImageView alloc] init];
//        _lineImage.image = [UIImage imageNamed:@"V_colorLine"];
        _lineImage.backgroundColor = RGBCOLOR(239.0f, 239.0f, 239.0f);
    }
    return _lineImage;
}

/* 问答之间分割线 */
- (UIImageView *)CutoffRuleImage
{
    if (!_CutoffRuleImage)
    {
        _CutoffRuleImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"V_answerLine"]];
    }
    return _CutoffRuleImage;
}

/* 问题图片1 */
- (UIImageView *)Q_image1
{
    if (!_Q_image1)
    {
        _Q_image1 = [[UIImageView alloc] init];
        [_Q_image1 setBackgroundColor:CLEAR];
    }
    return _Q_image1;
}
/* 问题图片2 */
- (UIImageView *)Q_image2
{
    if (!_Q_image2)
    {
        _Q_image2 = [[UIImageView alloc] init];
        [_Q_image2 setBackgroundColor:CLEAR];
    }
    return _Q_image2;
}
/* 问题图片3 */
- (UIImageView *)Q_image3
{
    if (!_Q_image3)
    {
        _Q_image3 = [[UIImageView alloc] init];
        [_Q_image3 setBackgroundColor:CLEAR];
    }
    return _Q_image3;
}
/* 问题图片4 */
- (UIImageView *)Q_image4
{
    if (!_Q_image4)
    {
        _Q_image4 = [[UIImageView alloc] init];
        [_Q_image4 setBackgroundColor:CLEAR];
    }
    return _Q_image4;
}
/* 问题图片5 */
- (UIImageView *)Q_image5
{
    if (!_Q_image5)
    {
        _Q_image5 = [[UIImageView alloc] init];
        _Q_image5.backgroundColor = CLEAR;
    }
    return _Q_image5;
}

/* 问题图片6 */
- (UIImageView *)Q_image6
{
    if (!_Q_image6)
    {
        _Q_image6 = [[UIImageView alloc] init];
        _Q_image6.backgroundColor = CLEAR;
    }
    return _Q_image6;
}

/* 问题图片7 */
- (UIImageView *)Q_image7
{
    if (!_Q_image7)
    {
        _Q_image7 = [[UIImageView alloc] init];
        _Q_image7.backgroundColor = CLEAR;
    }
    return _Q_image7;
}

/* 问题图片8 */
- (UIImageView *)Q_image8
{
    if (!_Q_image8)
    {
        _Q_image8 = [[UIImageView alloc] init];
        _Q_image8.backgroundColor = CLEAR;
    }
    return _Q_image8;
}

/* 问题图片9 */
- (UIImageView *)Q_image9
{
    if (!_Q_image9)
    {
        _Q_image9 = [[UIImageView alloc] init];
        _Q_image9.backgroundColor = CLEAR;
    }
    return _Q_image9;
}
/* 答案图片1 */
- (UIImageView *)A_image1
{
    if (!_A_image1)
    {
        _A_image1 = [[UIImageView alloc] init];
        [_A_image1 setBackgroundColor:CLEAR];
    }
    return _A_image1;
}
/* 答案图片2 */
- (UIImageView *)A_image2
{
    if (!_A_image2)
    {
        _A_image2 = [[UIImageView alloc] init];
        [_A_image2 setBackgroundColor:CLEAR];
    }
    return _A_image2;
}
/* 答案图片3*/
- (UIImageView *)A_image3
{
    if (!_A_image3)
    {
        _A_image3 = [[UIImageView alloc] init];
        [_A_image3 setBackgroundColor:CLEAR];
    }
    return _A_image3;
}
/* 答案图片4 */
- (UIImageView *)A_image4
{
    if (!_A_image4)
    {
        _A_image4 = [[UIImageView alloc] init];
        [_A_image4 setBackgroundColor:CLEAR];
    }
    return _A_image4;
}
/* 答案图片5 */
- (UIImageView *)A_image5
{
    if (!_A_image5)
    {
        _A_image5 = [[UIImageView alloc] init];
        [_A_image5 setBackgroundColor:CLEAR];
    }
    return _A_image5;
}
/* 答案图片6 */
- (UIImageView *)A_image6
{
    if (!_A_image6)
    {
        _A_image6 = [[UIImageView alloc] init];
        [_A_image6 setBackgroundColor:CLEAR];
    }
    return _A_image6;
}
/* 答案图片3*/
- (UIImageView *)A_image7
{
    if (!_A_image7)
    {
        _A_image7 = [[UIImageView alloc] init];
        [_A_image7 setBackgroundColor:CLEAR];
    }
    return _A_image7;
}
/* 答案图片8 */
- (UIImageView *)A_image8
{
    if (!_A_image8)
    {
        _A_image8 = [[UIImageView alloc] init];
        [_A_image8 setBackgroundColor:CLEAR];
    }
    return _A_image8;
}
/* 答案图片4 */
- (UIImageView *)A_image9
{
    if (!_A_image9)
    {
        _A_image9 = [[UIImageView alloc] init];
        [_A_image9 setBackgroundColor:CLEAR];
    }
    return _A_image9;
}

@end

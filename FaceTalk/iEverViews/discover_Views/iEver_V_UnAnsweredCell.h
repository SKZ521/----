//
//  iEver_V_UnAnsweredCell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_V_answeredQuestionObject.h"
@interface iEver_V_UnAnsweredCell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_V_quesList_object   *object;
- (void)setViewController:(UINavigationController *)MVNavigation;
@end

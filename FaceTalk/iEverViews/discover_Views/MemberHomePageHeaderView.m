//
//  MemberHomePageHeaderView.m
//  Volunteers
//
//  Created by 'Sooongz on 14/8/1.
//  Copyright (c) 2014年 Sooongz. All rights reserved.
//

#import "MemberHomePageHeaderView.h"

@implementation MemberHomePageHeaderView


- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];

	if (self) {
        self.delegate                 = self;

        self.backButton               = [UIButton buttonWithType:UIButtonTypeCustom];

        self.imageView                = [[UIImageView alloc] init];
//        self.imageView.clipsToBounds  = YES;
        self.imageView.contentMode    = UIViewContentModeScaleAspectFill;
		[self.imageView  setNeedsDisplay];
		
		self.refreshImageView = [[UIImageView alloc] init];
		self.refreshImageView.image = [UIImage imageNamed:@"circle_loading"];
		self.refreshImageView.hidden = YES;

		self.avatarBottomImageView    = [[UIImageView alloc] init];
        self.avatarImageView          = [[UIImageView alloc] init];
        self.sexImageView             = [[UIImageView alloc] init];
        self.starImageView            = [[UIImageView alloc] init];

        self.userNameLabel            = [[UILabel alloc] init];
//        self.userNameLabel.font       = TextFont;
		self.userNameLabel.textColor  = WHITE;

        self.starCountLabel           = [[UILabel alloc] init];
        self.starCountLabel.font      = TextFont;
		self.starCountLabel.textColor = GRAY_LIGHT;

        self.partyIcon                = [[UIImageView alloc]init];
        self.groupIcon                = [[UIImageView alloc]init];
        
		[self addSubview:self.imageView];
		[self addSubview:_refreshImageView];
//		[self addSubview:self.backButton];
		[self addSubview:self.avatarImageView];
		[self addSubview:self.avatarBottomImageView];
		[self addSubview:self.sexImageView];
		[self addSubview:self.starImageView];
		[self addSubview:self.userNameLabel];
		[self addSubview:self.starCountLabel];
        [self addSubview:self.partyIcon];
        [self addSubview:self.groupIcon];
        
	}
	return self;
}

#pragma mark -

- (NSArray *)interactiveSubviewsInStretchableHeaderView:(AXStretchableHeaderView *)stretchableHeaderView
{
	return @[self.backButton];
}

#pragma mark -

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	self.backButton.frame = CGRectMake(0, 20 , 44, 44);

	self.refreshImageView.frame = CGRectMake(285, 20, 20, 20);
	
	self.avatarImageView.center = self.avatarBottomImageView.center;

    
}
- (void)setObject:(iEver_V_userObject *)object
{

    self.imageView.frame = CGRectMake(0, 0, ScreenWidth, 295 / 2);
    self.imageView.image = [UIImage imageNamed:@"defualt_icon"];
    
}

- (void)setSubViewPosition:(CGFloat)y
{
	if (y <= 0 ) {
		[self.imageView setFrame:CGRectMake(0, 0, ScreenWidth, 295 / 2 - y)];
	} else {
		[self.imageView setFrame:CGRectMake(0, - y, ScreenWidth, 295 / 2)];
	}
}

- (void)startAnimation
{
	CABasicAnimation *spinAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
	spinAnimation.byValue = [NSNumber numberWithFloat:2 * M_PI];
	spinAnimation.duration = 1.5;
	spinAnimation.repeatCount = MAX_CANON;
	spinAnimation.delegate = self;
	[_refreshImageView.layer addAnimation:spinAnimation forKey:@"spinAnimation"];
}

- (void)stopAnimation
{
	_refreshImageView.hidden = YES;
	[_refreshImageView.layer removeAllAnimations];
}

@end

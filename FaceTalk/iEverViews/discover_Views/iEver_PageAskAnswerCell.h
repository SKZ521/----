//
//  iEver_PageAskAnswerCell.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/23.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_expertQuestionObject.h"

@interface iEver_PageAskAnswerCell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

@property (nonatomic, strong) iEver_quesList_object *object;

@end

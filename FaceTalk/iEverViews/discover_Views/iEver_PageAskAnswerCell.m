//
//  iEver_PageAskAnswerCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/23.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_PageAskAnswerCell.h"
#import "iEver_V_person_ViewController.h"

#import "MJPhotoBrowser.h"
#import "MJPhoto.h"

@interface iEver_PageAskAnswerCell () {

    NSMutableArray *urlArray; //相册URL
    NSMutableArray *imageViewArray;//问题图片
}

@property (nonatomic, strong) UIView        *wholeView;          /* 整体背景 */

@property (nonatomic, strong) UIView        *headView;           /* 头部背景 */
@property (nonatomic, strong) UIImageView   *dateLLineImage;     /* 时间左横线图标 */
@property (nonatomic, strong) UIImageView   *dateRLineImage;     /* 时间右横线图标 */
@property (nonatomic, strong) UILabel       *dateLable;          /* 时间标签 */

@property (nonatomic, strong) UIView        *centerView;         /* 中间背景 */
@property (nonatomic, strong) UIButton      *photoButton;        /* 问者头像按钮 */
@property (nonatomic, strong) UILabel       *nameLable;          /* 问者昵称 */
@property (nonatomic, strong) UILabel       *answerTotalLabel;   /* 回复数量 */
@property (nonatomic, strong) UILabel       *answerTotalStrLabel;/* 回答 */
@property (nonatomic, strong) UILabel       *contentLabel;       /* 问题内容 */
@property (nonatomic, strong) UIImageView   *Q_image1;           /* 问题图片1 */
@property (nonatomic, strong) UIImageView   *Q_image2;           /* 问题图片2 */
@property (nonatomic, strong) UIImageView   *Q_image3;           /* 问题图片3 */
@property (nonatomic, strong) UIImageView   *Q_image4;           /* 问题图片4 */
@property (nonatomic, strong) UIImageView   *Q_image5;           /* 问题图片5 */
@property (nonatomic, strong) UIImageView   *Q_image6;           /* 问题图片6 */
@property (nonatomic, strong) UIImageView   *Q_image7;           /* 问题图片7 */
@property (nonatomic, strong) UIImageView   *Q_image8;           /* 问题图片8 */
@property (nonatomic, strong) UIImageView   *Q_image9;           /* 问题图片9 */

@property (nonatomic, strong) UIView        *footView;           /* 底部背景 */

@property (nonatomic, strong) UIImageView   *cell_arraw;         /* 单元格箭头 */

@end

@implementation iEver_PageAskAnswerCell


/* 相册浏览模式 */
- (void)tapImage:(UITapGestureRecognizer *)tap
{
    int count = urlArray.count;
    NSMutableArray *photos = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        // 更换高清尺寸
        NSString *url = [NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",[urlArray objectAtIndex:i]];
        MJPhoto *photo = [[MJPhoto alloc] init];
        photo.url = [NSURL URLWithString:url]; // 图片路径
        photo.srcImageView = imageViewArray[i]; // 来源于哪个UIImageView
        [photos addObject:photo];
    }

    // 2.显示相册
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.currentPhotoIndex = tap.view.tag; // 弹出相册时显示的第一张图片是？
    browser.photos = photos; // 设置所有的图片
    [browser show];
}


- (void) configureCell {

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.wholeView];

        [self.headView addSubview:self.dateLLineImage];
        [self.headView addSubview:self.dateRLineImage];
        [self.headView addSubview:self.dateLable];
        [self.wholeView addSubview:self.headView];

        [self.centerView addSubview:self.photoButton];
        [self.centerView addSubview:self.nameLable];
        [self.centerView addSubview:self.answerTotalLabel];
        [self.centerView addSubview:self.answerTotalStrLabel];
        [self.centerView addSubview:self.contentLabel];
        [self.centerView addSubview:self.Q_image1];
        [self.centerView addSubview:self.Q_image2];
        [self.centerView addSubview:self.Q_image3];
        [self.centerView addSubview:self.Q_image4];
        [self.centerView addSubview:self.Q_image5];
        [self.centerView addSubview:self.Q_image6];
        [self.centerView addSubview:self.Q_image7];
        [self.centerView addSubview:self.Q_image8];
        [self.centerView addSubview:self.Q_image9];
        [self.wholeView addSubview:self.centerView];

        [self.wholeView addSubview:self.footView];

        [self.centerView addSubview:self.cell_arraw];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)awakeFromNib
{
    // Initialization code
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    /*单元格frame */
   // const CGRect r = self.contentView.bounds;


}
- (void)setObject:(iEver_quesList_object *)object
{

    self.Q_image1.hidden = YES;
    self.Q_image2.hidden = YES;
    self.Q_image3.hidden = YES;
    self.Q_image4.hidden = YES;
    self.Q_image5.hidden = YES;
    self.Q_image6.hidden = YES;
    self.Q_image7.hidden = YES;
    self.Q_image8.hidden = YES;
    self.Q_image9.hidden = YES;
    
    urlArray = [[NSMutableArray alloc] init];
    imageViewArray = [[NSMutableArray alloc] init];

    /* Initialization size */
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 头部view */
    x = 0.0;
    y = 0.0;
    width =  ScreenWidth;
    height = (object.isSectionFirstData == YES) ? 0.0 : 25.0;
    self.headView.hidden = object.isSectionFirstData;
    self.headView.frame = CGRectMake(x, y, width, height);

    /* 头部view --时间左图标 */
    UIImage *askAnswerHead_line_L = [UIImage imageNamed:@"askAnswerHead_line_L"];
    x = ScreenWidth/2 - askAnswerHead_line_L.size.width/2 - 50.0;
    y = (object.isSectionFirstData == YES) ? 0.0 : self.headView.frame.size.height/2 - askAnswerHead_line_L.size.height/2;
    width =  askAnswerHead_line_L.size.width;
    height = (object.isSectionFirstData == YES) ? 0.0 : askAnswerHead_line_L.size.height;
    self.dateLLineImage.image = askAnswerHead_line_L;
    self.dateLLineImage.hidden = object.isSectionFirstData;
    self.dateLLineImage.frame = CGRectMake(x, y, width, height);

    /* 头部view --时间标签 */
    x = self.dateLLineImage.frame.origin.x + self.dateLLineImage.frame.size.width + 5;
    height = 20.0;
    y = (object.isSectionFirstData == YES) ? 0.0 : self.headView.frame.size.height/2 - height/2;
    width =  75.0;
    self.dateLable.text = object.creeatTimeFormatStr;
    self.dateLable.hidden = object.isSectionFirstData;
    self.dateLable.frame = CGRectMake(x, y, width, height);
    
    /* 头部view --时间右图标 */
    UIImage *askAnswerHead_line_R = [UIImage imageNamed:@"askAnswerHead_line_R"];
    x = self.dateLable.frame.origin.x + self.dateLable.frame.size.width + 2;
    y = (object.isSectionFirstData == YES) ? 0.0 : self.headView.frame.size.height/2 - askAnswerHead_line_R.size.height/2;
    width =  askAnswerHead_line_R.size.width;
    height = (object.isSectionFirstData == YES) ? 0.0 : askAnswerHead_line_R.size.height;
    self.dateRLineImage.image = askAnswerHead_line_R;
    self.dateRLineImage.hidden = object.isSectionFirstData;
    self.dateRLineImage.frame = CGRectMake(x, y, width, height);

    /* 中间view --用户头像 */
    x = 15.0;
    y = 15.0;
    width =  30.0;
    height = 30.0;
    [self.photoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/60x",object.qHeadImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    self.photoButton.frame = CGRectMake(x, y, width, height);

    /* 中间view --问者昵称 */
    x = 55.0;
    y = 25.0;
    width = 150.0;
    height = 15.0;
    self.nameLable.frame = CGRectMake(x, y, width, height);
    self.nameLable.text = object.qNickName;

    /* 中间view --内容 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[object.qContent boundingRectWithSize:CGSizeMake(240.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    x = 15.0;
    y = 60.0;
    width = 240.0;
    height = contentSize.height;
    self.contentLabel.frame = CGRectMake(x, y, width, height);
    self.contentLabel.text = object.qContent;

    /* 问题包含图片 */

    int totalLoc = 3; //总列数
    int totalRow = (object._quesPicList.count - 1) / totalLoc + 1; //总行数
    CGFloat imageViewW = 75;
    CGFloat imageViewH = 75;
    CGFloat margin = 5;

    if ([object._quesPicList count] > 0) {

        urlArray = object._quesPicList;
        
        NSArray *imageView_array =@[self.Q_image1,
                                    self.Q_image2,
                                    self.Q_image3,
                                    self.Q_image4,
                                    self.Q_image5,
                                    self.Q_image6,
                                    self.Q_image7,
                                    self.Q_image8,
                                    self.Q_image9];

        for (int i = 0; i < [object._quesPicList count]; i++) {

            int row = i / totalLoc;
            int loc = i % totalLoc;
            CGFloat imageViewX = 15 + ( margin + imageViewW ) * loc;
            CGFloat imageViewY = self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 10 + ( margin + imageViewH ) * row;

            UIImageView *imageview = imageView_array[i];
            [imageViewArray addObject:imageview];
            imageview.hidden = NO;
            [imageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/160x",[object._quesPicList objectAtIndex:i]]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            imageview.frame = CGRectMake(imageViewX, imageViewY, imageViewW, imageViewH);
            imageview.tag = i;
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapImage:)];
            tapGesture.numberOfTapsRequired = 1;
            imageview.userInteractionEnabled = YES;
            [imageview addGestureRecognizer:tapGesture];
        }
    }

    /* 中间view */
    x = 0.0;
    y = (object.isSectionFirstData == YES) ? 0.0 : self.headView.frame.size.height;
    width =  ScreenWidth;
    height =  (object._quesPicList.count == 0) ? self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 10.0 : (self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 10.0) + ( margin + imageViewH ) * totalRow + 10.0;
    self.centerView.frame = CGRectMake(x, y, width, height);

    //NSLog(@"self.centerView--->%f",self.centerView.frame.size.height);

    /* 箭头 */
    UIImage *askAnswerCell_arrow = [UIImage imageNamed:@"askAnswerCell_arrow"];

    self.cell_arraw.image = askAnswerCell_arrow;
    self.cell_arraw.frame = CGRectMake(ScreenWidth - 10.0 - askAnswerCell_arrow.size.width, 55 + ( self.centerView.frame.size.height - 60 ) / 2 - askAnswerCell_arrow.size.height/2, askAnswerCell_arrow.size.width, askAnswerCell_arrow.size.height);
    
    /* 中间view --回答量 */
    x = ScreenWidth - self.cell_arraw.frame.size.width - 10.0 - 10.0 - 30;
    y = self.cell_arraw.frame.origin.y;
    width = 40.0;
    height = 15.0;
    self.answerTotalLabel.frame = CGRectMake(x, y, width, height);
    self.answerTotalLabel.text = [NSString stringWithFormat:@"%d",object.answerTotal];
    NSLog(@"self.answerTotalLabel------>%@",self.answerTotalLabel);
    
    /* 中间view --回答STR */
    x = ScreenWidth - self.cell_arraw.frame.size.width - 10.0 - 10.0 - 20;
    y = self.cell_arraw.frame.origin.y + self.cell_arraw.frame.size.height/2;
    width = 30.0;
    height = 15.0;
    self.answerTotalStrLabel.frame = CGRectMake(x, y, width, height);
    self.answerTotalStrLabel.text = @"回答";
    

    //NSLog(@"self.cell_arraw-yy-->%f",self.cell_arraw.frame.origin.y);

    /* 整体view */
    x = 0.0;
    y = 0.0;
    width =  ScreenWidth;
    height = (object._quesPicList.count == 0) ? self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 10.0 + 10.0 + 5.0 : (self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 10.0) + ( margin + imageViewH ) * totalRow + 10.0 + 10.0 + 5.0;
    height = (object.isSectionFirstData == YES) ? height : height + 25.0;
    self.wholeView.frame = CGRectMake(x, y, width, height);

    /* 底部view */
    x = 0.0;
    y = self.wholeView.frame.size.height - 5.0;
    width =  ScreenWidth;
    height = 5.0;
    self.footView.frame = CGRectMake(x, y, width, height);

}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    iEver_quesList_object *content_object = (iEver_quesList_object *)object;

    CGFloat cellHieght = 0.0;

    CGFloat headViewHieght = 0.0;

    CGFloat centerViewHieght = 0.0;


    /* 判断是否是第一条数据  NO代表不是第一条数据 */
    headViewHieght = (content_object.isSectionFirstData == YES) ? 0.0 : 25.0;

    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[content_object.qContent boundingRectWithSize:CGSizeMake(240.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    int totalRow = (content_object._quesPicList.count - 1) / 3 + 1; //总行数
    CGFloat imageViewH = 75;
    CGFloat margin = 5;

    centerViewHieght = (content_object._quesPicList.count == 0) ? ( 60.0 + contentSize.height  + 10.0 + 10.0 + 5.0 ): ((60.0 + contentSize.height + 10.0) + ( margin + imageViewH ) * totalRow + 10.0 + 10.0 + 5.0);

    cellHieght = headViewHieght + centerViewHieght;

    return cellHieght;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
# pragma mark - view getters


/* 整体背景视图 */
- (UIView *)wholeView
{
    if (!_wholeView) {
        _wholeView = [[UIView alloc]init];
        _wholeView.backgroundColor = [UIColor whiteColor];
    }
    return _wholeView;
}

/* 头部时间背景视图 */
- (UIView *)headView
{
    if (!_headView) {
        _headView = [[UIView alloc]init];
        _headView.backgroundColor = TYR_BACKGRAYCOLOR;
    }
    return _headView;
}

/* 头部时间左图标 */
- (UIImageView *)dateLLineImage
{
    if (!_dateLLineImage) {
        _dateLLineImage = [[UIImageView alloc]init];
        _dateLLineImage.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _dateLLineImage;
}

/* 头部时间右图标 */
- (UIImageView *)dateRLineImage
{
    if (!_dateRLineImage) {
        _dateRLineImage = [[UIImageView alloc]init];
        _dateRLineImage.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _dateRLineImage;
}

/* 头部时间 */
- (UILabel *)dateLable
{
    if (!_dateLable){
        _dateLable = [[UILabel alloc] init];
        _dateLable.numberOfLines = 1;
        _dateLable.backgroundColor = CLEAR;
        _dateLable.textColor = DAKEBLACK;
        _dateLable.font = TextFonts;
        _dateLable.textAlignment = NSTextAlignmentLeft;
    }
    return _dateLable;
}

/* 中间背景视图 */
- (UIView *)centerView
{
    if (!_centerView) {
        _centerView = [[UIView alloc]init];
        _centerView.backgroundColor = [UIColor whiteColor];
    }
    return _centerView;
}

/* 头像按钮 */
- (UIButton *)photoButton
{
    if (!_photoButton){
        _photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_photoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:30.0/2];
    }
    return _photoButton;
}

/* 问者昵称 */
- (UILabel *)nameLable
{
    if (!_nameLable){
        _nameLable = [[UILabel alloc] init];
        _nameLable.numberOfLines = 1;
        _nameLable.backgroundColor = CLEAR;
        _nameLable.textColor = MY_PURPLECOLOR;
        _nameLable.font = TextFont;
    }
    return _nameLable;
}

/* 回答量 */
- (UILabel *)answerTotalLabel
{
    if (!_answerTotalLabel){
        _answerTotalLabel = [[UILabel alloc] init];
        _answerTotalLabel.backgroundColor = CLEAR;
        _answerTotalLabel.textColor = MY_PURPLECOLOR;
        _answerTotalLabel.textAlignment = NSTextAlignmentRight;
        _answerTotalLabel.font = TextFonts;
    }
    return _answerTotalLabel;
}

/* 回答 */
- (UILabel *)answerTotalStrLabel
{
    if (!_answerTotalStrLabel){
        _answerTotalStrLabel = [[UILabel alloc] init];
        _answerTotalStrLabel.backgroundColor = CLEAR;
        _answerTotalStrLabel.textColor = MY_PURPLECOLOR;
        _answerTotalStrLabel.textAlignment = NSTextAlignmentRight;
        _answerTotalStrLabel.font = TextFonts;
    }
    return _answerTotalStrLabel;
}


/* 问题主题 */
- (UILabel *)contentLabel
{
    if (!_contentLabel){
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.backgroundColor = CLEAR;
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = BLACK;
        _contentLabel.font = TextFont;
    }
    return _contentLabel;
}

/* 问题图片1 */
-(UIImageView *)Q_image1{
    
    if (!_Q_image1) {
        _Q_image1 = [[UIImageView alloc] init];
        _Q_image1.backgroundColor = BLACK;
        _Q_image1.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image1.hidden = YES;
    }
    return _Q_image1;
}

/* 问题图片2 */
-(UIImageView *)Q_image2{
    
    if (!_Q_image2) {
        _Q_image2 = [[UIImageView alloc] init];
        _Q_image2.backgroundColor = BLACK;
        _Q_image2.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image2.hidden = YES;
    }
    return _Q_image2;
}


/* 问题图片3 */
-(UIImageView *)Q_image3{
    
    if (!_Q_image3) {
        _Q_image3 = [[UIImageView alloc] init];
        _Q_image3.backgroundColor = BLACK;
        _Q_image3.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image3.hidden = YES;
    }
    return _Q_image3;
}


/* 问题图片4 */
-(UIImageView *)Q_image4{
    
    if (!_Q_image4) {
        _Q_image4 = [[UIImageView alloc] init];
        _Q_image4.backgroundColor = BLACK;
        _Q_image4.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image4.hidden = YES;
    }
    return _Q_image4;
}


/* 问题图片5 */
-(UIImageView *)Q_image5{
    
    if (!_Q_image5) {
        _Q_image5 = [[UIImageView alloc] init];
        _Q_image5.backgroundColor = BLACK;
        _Q_image5.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image5.hidden = YES;
    }
    return _Q_image5;
}

/* 问题图片6 */
-(UIImageView *)Q_image6{
    
    if (!_Q_image6) {
        _Q_image6 = [[UIImageView alloc] init];
        _Q_image6.backgroundColor = BLACK;
        _Q_image6.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image6.hidden = YES;
    }
    return _Q_image6;
}

/* 问题图片7 */
-(UIImageView *)Q_image7{
    
    if (!_Q_image7) {
        _Q_image7 = [[UIImageView alloc] init];
        _Q_image7.backgroundColor = BLACK;
        _Q_image7.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image7.hidden = YES;
    }
    return _Q_image7;
}

/* 问题图片8 */
-(UIImageView *)Q_image8{
    
    if (!_Q_image8) {
        _Q_image8 = [[UIImageView alloc] init];
        _Q_image8.backgroundColor = BLACK;
        _Q_image8.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image8.hidden = YES;
    }
    return _Q_image8;
}

/* 问题图片9 */
-(UIImageView *)Q_image9{
    
    if (!_Q_image9) {
        _Q_image9 = [[UIImageView alloc] init];
        _Q_image9.backgroundColor = BLACK;
        _Q_image9.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image9.hidden = YES;
    }
    return _Q_image9;
}

/* 底部背景视图 */
- (UIView *)footView
{
    if (!_footView) {
        _footView = [[UIView alloc]init];
        _footView.backgroundColor = TYR_BACKGRAYCOLOR;
    }
    return _footView;
}

/* cell 箭头 */
-(UIImageView *)cell_arraw{

    if (!_cell_arraw) {
        _cell_arraw = [[UIImageView alloc] init];
    }
    return _cell_arraw;
}

@end

//
//  iEverAppDelegate.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-11.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CustomTabBarViewController.h"

@interface iEverAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) CustomTabBarViewController *tabBarController;
@property(nonatomic, strong) UIButton *openDrawerButton;

+ (iEverAppDelegate *)shareAppDelegate;

- (NSString *)idfaString;
@end

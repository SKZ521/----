//
//  iEver_itemCommentReplyDetailViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/18.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"
#import "iEver_itemDetail_Object.h"

@interface iEver_itemCommentReplyDetailViewController : iEverBaseTableViewController


@property (nonatomic, assign) int                        comment_id;       /* comment id */

-(void)insertCommentToReplyComment;                                     /* 点击评论的评论，进行评论 */
@property (nonatomic, copy  ) NSString          *toReplyCommentName;    /* 评论回复 用户名 */
@property (nonatomic, assign) int               comment_parentId;       /* 评论回复 的parent ID */
@property (nonatomic, assign) int               comment_atUserId;       /* 评论回复 的atUser ID */


@property (nonatomic, retain) iEver_detail_itemCommentList_object    *itemCommentList_object; /* 商品详情传递过来的 */

@end

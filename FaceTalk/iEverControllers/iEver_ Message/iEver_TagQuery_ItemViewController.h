//
//  iEver_TagQuery_ItemViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/5.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverSSBaseTableViewController.h"

@interface iEver_TagQuery_ItemViewController : iEverSSBaseTableViewController
@property (nonatomic, assign) int                        tagId;             /* 标签查询*/
@end

//
//  iEver_itemCommentReplyDetailViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/18.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_itemCommentReplyDetailViewController.h"
#import "iEver_itemCommentReply_object.h"
#import "iEver_itemCommentReply_cell.h"
#import "iEver_commentObject.h"
#import "MCFireworksButton.h"
#import "iEver_likeActionObject.h"

@interface iEver_itemCommentReplyDetailViewController ()

{

    MCFireworksButton *likeButton;
    UILabel *likeLabel;
}
@property (retain, nonatomic) UIView                    *mainView;
@property (retain, nonatomic) UIButton                  *bottomButton;
@property (retain, nonatomic) UITextView                *myTextView;
@property (nonatomic, strong) UILabel                   *commentLab;

@end

@implementation iEver_itemCommentReplyDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"评论详情";
    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = YES;
    self.tableView.showsInfiniteScrolling = YES;
    self.tableView.frame = CGRectMake(0, 0.0, ScreenWidth, ScreenHeight - BARHeight - NAVHeight);
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    /* request data */
    [self creatCommentViewAsTableViweHeader];

    /* request data */
    [self fetchData];

    /* comment textview */
    [self creatTextView];
}


/* creat Comment As TableViweHeader */
-(void)creatCommentViewAsTableViweHeader{

    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = WHITE;

    /* 用户头像 */
    CGFloat x = 15.0;
    CGFloat y = 10.0;
    CGFloat width = 36.0;
    CGFloat height = 36.0;
    CGFloat content0_SizeHeight = 0.0;
    UIImageView *headImageview = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    CALayer *photoLayer = [headImageview layer];
    [photoLayer setMasksToBounds:YES];
    [photoLayer setCornerRadius:18.0];
    [headImageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",_itemCommentList_object.headImg]]
                  placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    [headerView addSubview:headImageview];

    /* 用户名称 */
    x = 60.0;
    y = 10.0;
    width = 150.0;
    height = 20.0;
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    nameLabel.numberOfLines = 1;
    nameLabel.backgroundColor = CLEAR;
    nameLabel.textColor = MY_PURPLECOLOR;
    nameLabel.font = TextFont;
    nameLabel.text = _itemCommentList_object.nickName;
    [headerView addSubview:nameLabel];

    /* 评论用户_个性化标签 */
    x = 60.0;
    y = 30.0;
    width = 200.0;
    height = 20.0;
    UILabel *featureLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    featureLabel.numberOfLines = 1;
    featureLabel.backgroundColor = CLEAR;
    featureLabel.textColor = GRAY_LIGHT;
    featureLabel.font = TextDESCFonts;

    if (_itemCommentList_object.feature.length == 0) {
        featureLabel.text = @"该用户尚未定制";
    }else{

        featureLabel.text = _itemCommentList_object.feature;
    }
    [headerView addSubview:featureLabel];

    /* 评论用户_发布时间 */
    x = 205.0;
    y = 10.0;
    width = 100.0;
    height = 20.0;
    NSString *str = [iEver_Global commentIntervalSinceNow:_itemCommentList_object.createTime/1000];
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    timeLabel.numberOfLines = 1;
    timeLabel.backgroundColor = CLEAR;
    timeLabel.textColor = GRAY_LIGHT;
    timeLabel.textAlignment = NSTextAlignmentRight;
    timeLabel.font = TextDESCFonts;
    timeLabel.frame = CGRectMake(x, y, width, height);
    timeLabel.text = str;
    [headerView addSubview:timeLabel];

    /* 评论用户_评论 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[_itemCommentList_object.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    x = 15.0;
    y = 56.0;
    width = ScreenWidth - 15.0 - 15.0;
    height = contentSize.height;
    content0_SizeHeight = contentSize.height;
    UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    contentLabel.numberOfLines = 0;
    contentLabel.backgroundColor = CLEAR;
    contentLabel.textColor = BLACK;
    contentLabel.font = TextFont;
    contentLabel.text = _itemCommentList_object.commentContent;
    [headerView addSubview:contentLabel];

    /*  点赞按钮 */
    UIImage *detail_commentLike       = [UIImage imageNamed:@"detail_commentLike"];
    UIImage *detail_commentLike_click = [UIImage imageNamed:@"detail_commentLike_click"];
    x = 250.0;
    y = 56.0 + content0_SizeHeight + 5;
    width = detail_commentLike.size.width;
    height = detail_commentLike.size.height;

    likeButton                     = [MCFireworksButton buttonWithType:UIButtonTypeCustom];
    likeButton.particleImage       = [UIImage imageNamed:@"Sparkle"];
    likeButton.particleScale       = 0.05;
    likeButton.particleScaleRange  = 0.02;
    likeButton.frame = CGRectMake(x, y, width, height);
    if (_itemCommentList_object._selected) {
        [likeButton setImage:detail_commentLike_click forState:UIControlStateNormal];
    }else{
        [likeButton setImage:detail_commentLike forState:UIControlStateNormal];
    }

    iEver_likeActionObject * content = [[iEver_likeActionObject alloc] init];
    likeButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(MCFireworksButton *input) {

        /* like modthod */
        if (!_itemCommentList_object._selected) {

            NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_itemCommentList_object.Comment_id],
                                  @"type": [NSNumber numberWithInt:21],
                                  };

            [[[content like:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
             subscribeNext:^(NSDictionary *object) {

                 if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                     _itemCommentList_object._selected = !_itemCommentList_object._selected;
                     _itemCommentList_object.likeTotal = _itemCommentList_object.likeTotal +1;
                     [likeButton popOutsideWithDuration:0.5];
                     [likeButton setImage:[UIImage imageNamed:@"detail_commentLike_click"] forState:UIControlStateNormal];
                     likeLabel.text = [NSString stringWithFormat:@"赞(%d)",_itemCommentList_object.likeTotal];
                     [likeButton animate];

                 }else{
                     NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                     [SVProgressHUD showImage:Nil status:codeStr];
                 }
             }];
        }
        else {

            NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_itemCommentList_object.Comment_id],
                                  @"type": [NSNumber numberWithInt:21],
                                  };

            [[[content unlike:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
             subscribeNext:^(NSDictionary *object) {

                 if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                     _itemCommentList_object._selected = !_itemCommentList_object._selected;
                     _itemCommentList_object.likeTotal = _itemCommentList_object.likeTotal -1;
                     [likeButton popInsideWithDuration:0.4];
                     [likeButton setImage:[UIImage imageNamed:@"detail_commentLike"] forState:UIControlStateNormal];
                     likeLabel.text = [NSString stringWithFormat:@"赞(%d)",_itemCommentList_object.likeTotal];

                 }else{
                     NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                     [SVProgressHUD showImage:Nil status:codeStr];
                 }
             }];
        }

        return [RACSignal empty];
    }];

    [headerView addSubview:likeButton];


    /*  喜欢数量 */
    x = 270.0;
    y = 56.0 + content0_SizeHeight + 3;
    width = 40;
    height = 15;
    likeLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    likeLabel.numberOfLines = 1;
    likeLabel.backgroundColor = CLEAR;
    likeLabel.textColor = BLACK;
    likeLabel.font = TextFonts;
    likeLabel.text = [NSString stringWithFormat:@"赞(%d)",_itemCommentList_object.likeTotal];
    likeLabel.frame = CGRectMake(x, y, width, height);
    [headerView addSubview:likeLabel];


    /* 背景视图 */
    x = 0.0;
    y = 0.0;
    height = 56.0 + contentSize.height + 25.0;
    width = 320.0;
    headerView.frame = CGRectMake(x, y, width, height);

    /* 评论的评论小箭头 */
    UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
    x = 35.0;
    y = height - tryDetailArrow.size.height;
    width = tryDetailArrow.size.width;
    height = tryDetailArrow.size.height;
    UIImageView *arrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    arrowImage.image = tryDetailArrow;
    arrowImage.frame = CGRectMake(x, y, width, height);
    [headerView addSubview:arrowImage];
    
    self.tableView.tableHeaderView = headerView;
    
}


/*request data*/
-(void)fetchData{

    @weakify(self);
    iEver_itemCommentReply_object *itemCommentReply_object = [[iEver_itemCommentReply_object alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"/itemComment/queryCommentReply/%d/%d",self.comment_id,self.page];
    [[[itemCommentReply_object queryItemCommentReply:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_itemCommentReply_object *object) {

         @strongify(self);
         if (object.pageSize < self.page) {
             self.tableView.showsInfiniteScrolling = NO;
         }else{

             [self addDataToDataSource:object._icList];
             [self.tableView reloadData];

         }
         
     }];
}



// 根据键盘状态，调整_mainView的位置
- (void) changeContentViewPoint:(NSNotification *)notification{

    NSDictionary *userInfo  = [notification userInfo];
    NSValue *value          = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyBoardEndY    = value.CGRectValue.origin.y;  // 得到键盘弹出后的键盘视图所在y坐标
    CGFloat kStateBarHeight = 0.0;

    NSNumber *duration      = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve         = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];

    // 添加移动动画，使视图跟随键盘移动
    [UIView animateWithDuration:duration.doubleValue animations:^{
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationCurve:[curve intValue]];
        self.mainView.center = CGPointMake(self.mainView.center.x, keyBoardEndY - kStateBarHeight - self.mainView.bounds.size.height/2.0 - NAVHeight);   // keyBoardEndY的坐标包括了状态栏的高度，要减去  隐藏导航 NAVHeight
    }];

}

-(void)changeKeyboardWillHide:(NSNotification *)notification {

    self.bottomButton.hidden     = YES;
    [self.myTextView resignFirstResponder];
    self.mainView.frame          = CGRectMake(0, ScreenHeight - NAVHeight, 320, 145.0);

}


-(void)insertCommentToReplyComment {

    [self.view bringSubviewToFront:self.mainView];
    self.bottomButton.hidden    = NO;
    self.commentLab.text        = [NSString stringWithFormat:@"回复 %@",self.toReplyCommentName];
    [self.myTextView becomeFirstResponder];

}


-(void)creatTextView {

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentViewPoint:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    /* mainView 灰黑 BG*/
    self.bottomButton             = [UIButton buttonWithType:UIButtonTypeCustom];
    self.bottomButton.frame       = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    self.bottomButton.hidden      = YES;
    self.bottomButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        self.comment_parentId = 0;
        self.bottomButton.hidden  = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame       = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    self.bottomButton.backgroundColor = BLACK;
    self.bottomButton.alpha           = 0.6;
    [self.view addSubview:self.bottomButton];

    /* mainView */
    self.mainView                     = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight, 320, 145.0)];
    self.mainView.backgroundColor     = BackkGroundColor;
    [self.view addSubview:self.mainView];

    /* textViewImage */
    UIImageView *textViewImage       = [[UIImageView alloc] initWithFrame:CGRectMake(15, 50, 290, 80)];
    textViewImage.image              = [UIImage imageNamed:@"commentContentBG"];
    [self.mainView addSubview:textViewImage];

    /* myTextView */
    self.myTextView                  = [[UITextView alloc]initWithFrame:CGRectMake(15, 50, 290, 80)];
    self.myTextView.font             = TextFont;
    self.myTextView.backgroundColor  = CLEAR;
    [self.mainView addSubview:self.myTextView];

    /* cancelBut */
    UIButton *cancelBut              = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBut.frame                  = CGRectMake(10, 10, 30, 30);
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateHighlighted];
    cancelBut.rac_command            = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        self.comment_parentId = 0;
        self.bottomButton.hidden     = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame          = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    [self.mainView addSubview:cancelBut];

    /* commentLab */
    self.commentLab             = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, 240, 25)];
    self.commentLab.text                 = @"评 论";
    self.commentLab.font                 = TextFont;
    self.commentLab.textAlignment        = NSTextAlignmentCenter;
    [self.mainView addSubview:self.commentLab];

    /* sendBut */
    UIButton *sendBut               = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBut.frame                   = CGRectMake(280, 10, 30, 30);
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateHighlighted];
    sendBut.rac_command             = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        /* insert Comment */
        [self insertCommentRequest];
        return [RACSignal empty];
    }];
    [self.mainView addSubview:sendBut];


    /* --- bottomView commentBut ------ */
    UIImage    * detail_bottomView = [UIImage imageNamed:@"detail_bottomView"];
    UIView *commentView            = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight - BARHeight - NAVHeight , detail_bottomView.size.width, detail_bottomView.size.height)];
    commentView.backgroundColor    = WHITE;
    [self.view addSubview:commentView];


    UIImageView *imageview         = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, detail_bottomView.size.width, detail_bottomView.size.height)];
    imageview.image                = detail_bottomView;
    [commentView addSubview:imageview];

    UIImage *detail_comment         = [UIImage imageNamed:@"detail_comment"];
    UIButton *commentBut            = [UIButton buttonWithType:UIButtonTypeCustom];
    commentBut.backgroundColor      = CLEAR;
    [commentBut setBackgroundImage:detail_comment forState:UIControlStateNormal];
    [commentBut setBackgroundImage:detail_comment forState:UIControlStateHighlighted];
    commentBut.frame = CGRectMake(ScreenWidth/2 - detail_comment.size.width/2, detail_bottomView.size.height/2 - detail_comment.size.height/2, detail_comment.size.width, detail_comment.size.height);
    commentBut.rac_command          = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        [self.view bringSubviewToFront:self.mainView];
        self.bottomButton.hidden    = NO;
        [self.myTextView becomeFirstResponder];
        self.commentLab.text        = [NSString stringWithFormat:@"回复 %@",self.itemCommentList_object.nickName];
        self.comment_parentId = self.itemCommentList_object.Comment_id;
        self.comment_atUserId = self.itemCommentList_object.userId;
        return [RACSignal empty];
    }];
    [commentView addSubview:commentBut];
    
}

/* insert Comment */
-(void)insertCommentRequest{


    NSString *temp = [self.myTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if ([temp isEqualToString:@""]) {
        [SVProgressHUD showImage:Nil status:@"请输评论内容"];
        return ;
    }

    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_commentObject * content = [[iEver_commentObject alloc] init];

    NSString *_pathUrl = [NSString stringWithFormat:@"/itemComment/insert"];
    NSDictionary *dic;

    /* 评论文章详情 */
    if (self.comment_parentId == 0) {

        dic = @{@"itemId": [NSNumber numberWithInt:self.itemCommentList_object.itemId],
                @"commentContent": self.myTextView.text,
                @"type": [NSNumber numberWithInt:10],
                };
    }
    /* 回复评论 */
    else {

        dic = @{@"itemId": [NSNumber numberWithInt:self.itemCommentList_object.itemId],
                @"parentId": [NSNumber numberWithInt:_comment_parentId],
                @"atUserId": [NSNumber numberWithInt:_comment_atUserId],
                @"commentContent": self.myTextView.text,
                @"type": [NSNumber numberWithInt:11],
                };
    }


    [[[content insertArticleComment:dic path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"提交成功"];

             self.page = 1;

             [self.tableView triggerPullToRefresh];

         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

     }];
    self.bottomButton.hidden = YES;
    [self.myTextView resignFirstResponder];
    self.commentLab.text = @"评 论";
    self.comment_parentId = 0;
    self.myTextView.text = @"";
    self.mainView.frame = CGRectMake(0, ScreenHeight, 320, 145.0);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"itemCommentReplyCell";
    UITableViewCell *cell = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = (iEver_itemCommentReply_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (!cell) {

        cell = [[iEver_itemCommentReply_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    iEver_itemCommentReply_cell *message_cell = (iEver_itemCommentReply_cell *)cell;
    iEver_icList_object * object = [self.dataSourceArray objectAtIndex:indexPath.row];
    [message_cell setObject:object];

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.dataSourceArray objectAtIndex:indexPath.row];
    return [iEver_itemCommentReply_cell heightForObject:object atIndexPath:indexPath tableView:tableView];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

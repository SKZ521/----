//
//  iEver_MY_ItemTryViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/4/4.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_MY_ItemTryViewController.h"
#import "iEver_MYtry_cell.h"
#import "iEver_tryItemObject.h"
#import "iEver_ItemTryDetailViewController.h"

@interface iEver_MY_ItemTryViewController ()

{

    iEver_tryItemObject *_tryobject;
}
@end

@implementation iEver_MY_ItemTryViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [GiFHUD dismiss];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    /* configure cell */
    [self configureCell];

    /* request data */
    [self fetchData];

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = YES;
    self.tableView.showsInfiniteScrolling = YES;

    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

/* configure cell */
-(void)configureCell{

    __typeof (self) __weak weakSelf = self;

    self.dataSource.cellConfigureBlock = ^(iEver_MYtry_cell *cell,
                                           NSNumber *exampleType,
                                           UITableView *tableView,
                                           NSIndexPath *indexPath) {

        [cell configureCell];
        iEver_itemTryList_object *listObject = (iEver_itemTryList_object *)[weakSelf.dataSource itemAtIndexPath:indexPath];
        cell.object                          = listObject;
    };
    self.dataSource.cellClass = [iEver_MYtry_cell class];
}


/*request data*/
-(void)fetchData{

    @weakify(self);

    [GiFHUD show];

    iEver_tryItemObject *detail_Object = [[iEver_tryItemObject alloc] init];
    NSString            *_pathUrl      = [NSString stringWithFormat:@"/itemTry/queryByUserId/%d",self.page];

    [[[detail_Object questTryItemData:nil
                                 path:_pathUrl]
      deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_tryItemObject *object) {

         @strongify(self);
         if (object.resultCode == 1) {

             if (object.pageSize < self.page) {

                 self.tableView.showsInfiniteScrolling = NO;

             }else{

                 [self            reloadDatasouce:object._itemTryList];
                 [self.tableView  reloadData];
             }


             if (object._itemTryList.count == 0) {

                 [self nothingView];
             }

         }else {

             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:object.resultCode];
             [SVProgressHUD showImage:Nil status:codeStr];

         }

          [GiFHUD  dismiss];
         
     }];
}

- (void)nothingView
{
    if (self.dataSource.allItems.count == 0) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_nothing_"]];
        self.tableView.tableFooterView = imageView;
    }else {
        self.tableView.tableFooterView = nil;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [iEver_MYtry_cell heightForObject:nil atIndexPath:indexPath tableView:tableView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    iEver_itemTryList_object *object                                = (iEver_itemTryList_object *)[self.dataSource itemAtIndexPath:indexPath];
    iEver_ItemTryDetailViewController *_itemTryDetailViewController = [[iEver_ItemTryDetailViewController alloc]init];
    _itemTryDetailViewController.type_id                            = object.T_ID;
    _itemTryDetailViewController.hidesBottomBarWhenPushed           = YES;
    [self.navigationController pushViewController:_itemTryDetailViewController animated:YES];
    IOS7POP;
}


#pragma mark -
#pragma mark UIInterfaceOrientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}

@end

//
//  iEver_SettingViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-16.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_SettingViewController.h"
#import "iEver_person_FourSection_cell.h"
#import "iEver_person_FiveSection_cell.h"
#import "iEver_EditUserMessageViewController.h"
#import "iEver_fixPassWordViewController.h"
#import "UMFeedback.h"
#import "iEver_LoginViewController.h"

#import "iEver_VuserAnsweredViewController.h"
#import "iEver_VuserUnAnswerdeViewController.h"

#import "iEver_UserMessageObject.h"
@interface iEver_SettingViewController ()
{

    NSMutableArray *arrayTitle;
}
@end

@implementation iEver_SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    self.title = @"设置";
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh     = NO;
    self.tableView.showsInfiniteScrolling = NO;

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image        = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.frame            = CGRectMake(0, 0, ScreenWidth, self.view.bounds.size.height );

    NSArray * title                 = @[@"修改密码",@"邀请朋友",@"意见反馈",@"清除缓存"];//,@"检测版本"
    NSArray * image                 = @[@"setting_fixpassword",@"setting_invitfirend",@"setting_suggest",@"setting_trash"];//,@"setting_Refresh"
    arrayTitle      = [[NSMutableArray alloc] initWithCapacity:5];

    for (int i = 0; i< [title count] ; i++) {
        iEver_personMessage_object *content_object = [[iEver_personMessage_object alloc] init];

        content_object.section4Title = title[i];
        content_object.section4image = image[i];
        content_object.section4EndCell = i;
        [arrayTitle addObject:content_object];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0) {
        return 4;
    }
    else if (section == 1) {
        return 1;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier;
    UITableViewCell *cell = nil;
    NSInteger section = indexPath.section;
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    switch (section) {
        case 0:
        {
            CellIdentifier = @"SettingFourSectionCell";
            cell = (iEver_person_FourSection_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
            break;
        case 1:
        {
            CellIdentifier = @"ExitFiveSectionCell";
            cell = (iEver_person_FiveSection_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
            break;
        default:
            break;
    }
    // Configure the cell...
	if (!cell) {

        switch (section) {
            case 0:
            {
                cell = [[iEver_person_FourSection_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
                break;
            case 1:
            {
                cell = [[iEver_person_FiveSection_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
                break;

            default:
                break;
        }
    }

    switch (section) {

        case 0:
        {
            iEver_person_FourSection_cell *item_cell = (iEver_person_FourSection_cell *)cell;
            iEver_personMessage_object *content_object = (iEver_personMessage_object *)[arrayTitle objectAtIndex:indexPath.row];
            [item_cell setObject:content_object];
        }
            break;
        case 1:
        {
            iEver_person_FiveSection_cell *item_cell = (iEver_person_FiveSection_cell *)cell;
            [item_cell setObject:nil];
        }
            break;

        default:
            break;
    }

	return cell;
}
#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;

    switch (section) {
        case 0:{
            return [iEver_person_FourSection_cell heightForObject:nil atIndexPath:indexPath tableView:tableView];
        }break;
        case 1:{
            return [iEver_person_FiveSection_cell heightForObject:nil atIndexPath:indexPath tableView:tableView];
        }break;

        default:
            break;
    }

    return 360.0;

}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    float height= 0.0;

    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    float height= 20.0;

    return height;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.height, 20.0)];
    view.backgroundColor = CLEAR;
    return view;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSInteger section = indexPath.section;
    switch (section) {
        /* 个人设置中心 */
        case 0:{
            /* 修改密码 */
            if (indexPath.row ==0 ) {

                iEver_fixPassWordViewController *_fixPassWordViewController = [[iEver_fixPassWordViewController alloc]init];
                _fixPassWordViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:_fixPassWordViewController animated:YES];
                IOS7POP;

            }
            /* 邀请朋友 */
            if (indexPath.row ==1 ) {

                [UMSocialSnsService presentSnsIconSheetView:self
                                                     appKey:UMAPPKEY
                                                  shareText:@"我正在使用 i-EVER，这里有好多实用的美妆技巧、最夯的日韩妆潮流教程还有产品内幕八卦爆料，很好看啊，一起来玩吧~~"
                                                 shareImage:[UIImage imageNamed:@"icon.png"]
                                            shareToSnsNames:[NSArray arrayWithObjects:UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToTencent,nil]
                                                   delegate:nil];
            }
            /* 意见反馈 */
            if (indexPath.row ==2 ) {

                [UMFeedback showFeedback:self withAppkey:UMAPPKEY];

            }
            /* 检测版本 */
//            if (indexPath.row ==3 ) {
//
//                [self userUpdate];
//            }
            /* 清除缓存 */
            if (indexPath.row == 3 ) {

                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                [[SDImageCache sharedImageCache] clearDisk];
                [self performSelector:@selector(delayMethod) withObject:nil afterDelay:2.0f];
            }

        }break;
            /* 退出登录 */
        case 1:{

            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginKey"] isKindOfClass:[NSString class]]) {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"loginKey"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userId"];
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"userType"];
                BOOL isLogin = NO;
                [[NSUserDefaults standardUserDefaults] setBool:isLogin forKey:@"isLogin"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                iEver_LoginViewController *_loginViewController = [[iEver_LoginViewController alloc]init];
                _loginViewController.delegate = [self.navigationController.viewControllers objectAtIndex:0];
                _loginViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:_loginViewController animated:YES];
            }
        }break;

        default:
            break;
    }
    
}


                 
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}
-(void)delayMethod{

    [self.tableView reloadData];
    [SVProgressHUD dismiss];
}
//-(void)userUpdate{
//
//    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
//    [[[content updateUserApp:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
//     subscribeNext:^(NSDictionary *object) {
//
//         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
//
//             //获取本地版本信息
//             NSString *currentVersion=[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
//
//             NSString * replaceStr = [iEver_Global replaceOccurrencesOfString:currentVersion withString:@"" options:2 replaceArray:@[@"."]];
//             NSString *appVersion = [object objectForKey:@"version"];
//             NSString * replaceStr1 = [iEver_Global replaceOccurrencesOfString:appVersion withString:@"" options:2 replaceArray:@[@"."]];
//
//             if ([replaceStr1 integerValue] > [replaceStr integerValue]) {
//                 UIAlertView* upgradeAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"版本提示", nil) message:NSLocalizedString(@"app有新版本！", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"前往更新", nil) otherButtonTitles:nil, nil];
//
//                 [upgradeAlert.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {
//                     if ([x integerValue] == 0) {
//                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/i-ever/id918789910?l=zh&ls=1&mt=8"]];
//                     }
//                 }];
//                 [upgradeAlert show];
//             }else{
//
//                 UIAlertView* upgradeAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"版本提示", nil) message:NSLocalizedString(@"您的版本是最新！", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"确定", nil) otherButtonTitles:nil, nil];
//
//                 [upgradeAlert show];
//             }
//             
//         }else{
//             
//         }
//         
//         
//     }];
//}

@end

//
//  iEver_MYViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/4/2.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_MYViewController.h"
#import "iEver_SettingViewController.h"
#import "iEver_EditUserMessageViewController.h"
#import "iEver_PersonalTagSettingViewController.h"
#import "iEver_MessageViewController.h"
#import "iEver_OfficialListViewController.h"
#import "iEver_normalUserAnsweredQesViewController.h"
#import "iEver_normalUserUnAnswerQesViewController.h"
#import "iEver_VuserAnsweredViewController.h"
#import "iEver_VuserUnAnswerdeViewController.h"
#import "iEver_VuserShareArticleViewController.h"
#import "iEver_MYcell.h"
#import "iEver_UserMessageObject.h"
#import "iEver_MY_ItemTryViewController.h"

@interface iEver_MYViewController () {

    NSMutableArray *arrayTitle;

    UIImageView  *userPhoto;
    UILabel      *name;
    UILabel      *personTag;
    UILabel      *tagStr;
}

@property (nonatomic, strong)iEver_personMessage_object *person_object;

@end

@implementation iEver_MYViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"我";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@""
                                                        image:[UIImage imageNamed:@"tabbar_personCenter_normal"]
                                                selectedImage:[UIImage imageNamed:@"tabbar_personCenter_click"]];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchData1];
    self.navigationController.navigationBarHidden = NO;
    [iEverAppDelegate shareAppDelegate].tabBarController.tabBar.hidden = NO;

    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"userType"] == 10) {

        NSArray * title                 = @[@"我的消息",@"我的试用",@"赞过的文章",@"赞过的经验",@"已回答的问题",@"未回答的问题"];
        NSArray * image                 = @[@"MY_icon1",@"MY_icon2",@"MY_icon3",@"MY_icon4",@"MY_icon6",@"MY_icon7"];
        arrayTitle      = [[NSMutableArray alloc] initWithCapacity:6];

        for (int i = 0; i< [title count] ; i++) {
            iEver_personMessage_object *content_object = [[iEver_personMessage_object alloc] init];

            content_object.section4Title = title[i];
            content_object.section4image = image[i];
            [arrayTitle addObject:content_object];
        }

    }else if ([[NSUserDefaults standardUserDefaults] integerForKey:@"userType"] == 20){

        NSArray * title                 = @[@"我的消息",@"我的经验分享",@"我的试用",@"赞过的文章",@"赞过的经验",@"已回答的问题",@"未回答的问题"];
        NSArray * image                 = @[@"MY_icon1",@"MY_icon8",@"MY_icon2",@"MY_icon3",@"MY_icon4",@"MY_icon6",@"MY_icon7"];
        arrayTitle      = [[NSMutableArray alloc] initWithCapacity:6];

        for (int i = 0; i< [title count] ; i++) {
            iEver_personMessage_object *content_object = [[iEver_personMessage_object alloc] init];

            content_object.section4Title = title[i];
            content_object.section4image = image[i];
            [arrayTitle addObject:content_object];
        }
        
        
    }

}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [iEverAppDelegate shareAppDelegate].tabBarController.tabBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    /* add left  right bar */
    [self addBar];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = NO;

    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    CGRect tFrame        = self.view.bounds;
    tFrame.size.height   = tFrame.size.height - 44.0 - 50.0;
    self.tableView.frame = tFrame;
    [self creatCoverViewAsTableViweHeader];
    // Do any additional setup after loading the view.
}

//添加左右 bar
- (void)addBar
{
    self.navigationItem.leftBarButtonItem = nil;

    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *MY_set       = [UIImage imageNamed:@"MY_set"];
    rightButton.frame     = CGRectMake(0, 0, MY_set.size.width, MY_set.size.height);
    [rightButton setImage:MY_set forState:UIControlStateNormal];
    rightButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        iEver_SettingViewController *_settingViewController = [[iEver_SettingViewController alloc]init];
        _settingViewController.hidesBottomBarWhenPushed     = YES;
        [self.navigationController pushViewController:_settingViewController animated:YES];
        IOS7POP;
        return [RACSignal empty];
    }];
    UIBarButtonItem *rightBarButton        = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
}

/*request data*/
-(void)fetchData1{

    @weakify(self);
    iEver_UserMessageObject *detail_Object = [[iEver_UserMessageObject alloc] init];
    [[[detail_Object queryUserById:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_personMessage_object *object) {
         @strongify(self);

         self.person_object = object;

         /* 头像 */
         [userPhoto setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/90x",object.headImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

         /* 昵称 */
         name.text      = object.nickName;

         /* 定制 */
         personTag.text = object.feature;

         /* 修改定制文案 */
         if ([object.feature isEqualToString:@""]) {
             tagStr.text    = @"定制>>";
         }else {

             tagStr.text    = @"修改定制>>";
         }

     }];
}


/* creat CoverView As TableViweHeader */
-(void)creatCoverViewAsTableViweHeader{

    /* Initialization */
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    UIImage *MY_head = [UIImage imageNamed:@"MY_head"];
    /* custom tableView headerView */
    /* headerView */
    self.tableView.tableHeaderView = ({

        UIView *headerView = [[UIView alloc] init];
        [headerView setBackgroundColor:CLEAR];
        /* 白色背景 */
        [headerView addSubview:({

            UIView *bottomView = [[UIView alloc] init];
            [bottomView setBackgroundColor:CLEAR];

            /* 表头 封面图片 */
            [bottomView addSubview:({


                x = 0.0 ;
                y = 0.0;
                width = MY_head.size.width;
                height = MY_head.size.height;
                UIImageView  *MY_head_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                MY_head_image.backgroundColor = CLEAR;
                MY_head_image.image = MY_head;
                MY_head_image;
            })];

            /* 个人资料 view */
            [bottomView addSubview:({

                x = 0.0;
                y = MY_head.size.height / 2 ;
                width = ScreenWidth;
                height = MY_head.size.height / 2;
                UIView * user_BaseView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                user_BaseView.backgroundColor = CLEAR;

                /* 用户头像*/
                [user_BaseView addSubview:({

                    x = 12.0 ;
                    y = 0.0;
                    width = 56.0;
                    height = 56.0;
                    userPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    userPhoto.backgroundColor = CLEAR;
                    CALayer *photoButton = [userPhoto layer];
                    [photoButton setBorderWidth:1.0];
                    [photoButton setBorderColor:[WHITE CGColor]];
                    [photoButton setMasksToBounds:YES];
                    [photoButton setCornerRadius:56.0/2];
                    userPhoto.contentMode = UIViewContentModeScaleAspectFill;
                    userPhoto;
                })];

                /* 用户名称 */
                [user_BaseView addSubview:({

                    x = 85.0 ;
                    y = 10.0;
                    width = 160.0;
                    height = 20.0;
                    name = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    name.numberOfLines = 1;
                    name.textColor = WHITE;
                    name.font = TextBigFont;
                    name.backgroundColor = CLEAR;
                    //titleLabel.text = _datailOblect._itemDetailObject.itemName;
                    name;
                })];

                /* personTag */
                [user_BaseView addSubview:({

                    x = 85.0;
                    y = 30.0;
                    width = 215.0;
                    height = 25.0;
                    personTag = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    personTag.textColor = WHITE;
                    personTag.font = TextDESCFonts;
                    personTag.backgroundColor = CLEAR;
                    personTag;
                })];

                /* userTYPE */
                [user_BaseView addSubview:({

                    x = 25.0;
                    y = user_BaseView.frame.size.height - 25.0;
                    width = 50.0;
                    height = 25.0;
                    UILabel *user_type = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    user_type.textColor = WHITE;
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"userType"] == 10) {

                        user_type.text = @"普通用户";
                    }else {

                        user_type.text = @"达人用户";
                    }

                    user_type.font = TextLIST_EDITOR_Fonts;
                    user_type.textAlignment = NSTextAlignmentLeft;
                    user_type.backgroundColor = CLEAR;
                    user_type;
                })];


                /* 进入详情箭头图片 */
                [user_BaseView addSubview:({

                    UIImage * MY_into = [UIImage imageNamed:@"MY_into"];
                    x = ScreenWidth - 20 - MY_into.size.width ;
                    y = MY_head.size.height / 4 - MY_into.size.height/2;
                    width = MY_into.size.width;
                    height = MY_into.size.height;

                    UIImageView  *itemCoverImageview = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    itemCoverImageview.backgroundColor = CLEAR;
                    [itemCoverImageview setImage:MY_into];
                    itemCoverImageview;
                })];

                /* 进入详情按钮 */
                [user_BaseView addSubview:({


                    UIButton *entereditUser = [UIButton buttonWithType:UIButtonTypeCustom];
                    entereditUser.frame = user_BaseView.bounds;
                    entereditUser.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

                        iEver_EditUserMessageViewController *_EditUserMessageViewController = [[iEver_EditUserMessageViewController alloc]init];
                        _EditUserMessageViewController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:_EditUserMessageViewController animated:YES];
                        IOS7POP;
                        return [RACSignal empty];
                    }];
                    entereditUser;
                })];
                user_BaseView;
            })];

            UIImage *MY_dingzi = [UIImage imageNamed:@"MY_dingzih"];
            [bottomView addSubview:({

                x = 0;
                y = MY_head.size.height;
                width = ScreenWidth;
                height = 245 - MY_head.size.height;

                UIView *personTagBaseView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                personTagBaseView.backgroundColor = CLEAR;

                [personTagBaseView addSubview:({

                    x = ScreenWidth/2 - MY_dingzi.size.width/2;
                    y = personTagBaseView.frame.size.height/2 - MY_dingzi.size.height/2;
                    width = MY_dingzi.size.width;
                    height = MY_dingzi.size.height;

                    UIImageView *MY_dingzi_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    MY_dingzi_image.image = MY_dingzi;

                    [MY_dingzi_image addSubview:({

                        x = MY_dingzi_image.frame.size.width - 70;
                        y = MY_dingzi_image.frame.size.height/2 - 2 ;
                        width = 60;
                        height = 12;
                        tagStr = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                        tagStr.backgroundColor = [UIColor redColor];
                        tagStr.numberOfLines = 1;
                        tagStr.backgroundColor = CLEAR;
                        tagStr.textColor = MY_PURPLECOLOR;
                        tagStr.textAlignment = NSTextAlignmentCenter;
                        tagStr.font = TextFonts;
                        tagStr;


                    })];
                    MY_dingzi_image;

                })];

                [personTagBaseView addSubview:({

                    UIButton *personTag_button = [UIButton buttonWithType:UIButtonTypeCustom];
                    personTag_button.frame = personTagBaseView.bounds;
                    personTag_button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

                        iEver_PersonalTagSettingViewController *_PersonalTagSettingViewController = [[iEver_PersonalTagSettingViewController alloc]init];
                        if ([tagStr.text isEqualToString:@"修改定制>>"]) {
                            
                            _PersonalTagSettingViewController.userIsCustom =  @"DID";
                        }else
                        {
                            _PersonalTagSettingViewController.userIsCustom =  @"NOT";

                        }
                        _PersonalTagSettingViewController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:_PersonalTagSettingViewController animated:YES];
                        IOS7POP;
                        return [RACSignal empty];
                    }];
                    personTag_button;


                })];

                personTagBaseView;


            })];
            x = 0.0;
            y = 0.0;
            width = 320.0;
            height = 245;
            bottomView.frame = CGRectMake(x, y, width, height);
            bottomView;
            
        })];
        x = 0.0;
        y = 0.0;
        width = 320.0;
        height = 245;
        headerView.frame = CGRectMake(x, y, width, height);
        headerView;
    });
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return arrayTitle.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MYcell";
    iEver_MYcell *cell = (iEver_MYcell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    // Configure the cell...
    if (!cell) {

        cell = [[iEver_MYcell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

    }

        iEver_MYcell *my_cell = (iEver_MYcell *)cell;
        iEver_personMessage_object *content_object = (iEver_personMessage_object *)[arrayTitle objectAtIndex:indexPath.row];
        [my_cell setObject:content_object];


    return cell;
}
#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return [iEver_MYcell heightForObject:nil atIndexPath:indexPath tableView:tableView];


}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"userType"] == 10) {

        if (indexPath.row == 0) {
            iEver_MessageViewController *_messageViewController = [[iEver_MessageViewController alloc]init];
            _messageViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:_messageViewController animated:YES];
            IOS7POP;
        }
        else if (indexPath.row == 1) {

            iEver_MY_ItemTryViewController *_MY_ItemTryViewController = [[iEver_MY_ItemTryViewController alloc]init];
            _MY_ItemTryViewController.title = @"我的试用";
            _MY_ItemTryViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:_MY_ItemTryViewController animated:YES];
            IOS7POP;

        }
        else if (indexPath.row == 2) {
            
            iEver_OfficialListViewController *_OfficialListViewController = [[iEver_OfficialListViewController alloc]init];
            _OfficialListViewController.userLike                          = 1;
            _OfficialListViewController.title                             = @"赞过的文章";
            _OfficialListViewController.category_id                       = 1;
            _OfficialListViewController.hidesBottomBarWhenPushed          = YES;
            [self.navigationController pushViewController:_OfficialListViewController animated:YES];
            IOS7POP;

        }
        else if (indexPath.row == 3) {

            iEver_VuserShareArticleViewController *_VuserShareArticleViewController = [[iEver_VuserShareArticleViewController alloc]init];
            _VuserShareArticleViewController.title                         = @"赞过的经验";
            _VuserShareArticleViewController.v_dataType                    = myPage_NuserLikeVuser_dataType;
            _VuserShareArticleViewController.hidesBottomBarWhenPushed      = YES;
            _VuserShareArticleViewController.navigationController          = self.navigationController;
            [self.navigationController pushViewController:_VuserShareArticleViewController animated:YES];
            IOS7POP;

        }
        else if (indexPath.row == 4) {

            iEver_normalUserAnsweredQesViewController *_normalUserAnsweredQesViewController = [[iEver_normalUserAnsweredQesViewController alloc]init];
            _normalUserAnsweredQesViewController.hidesBottomBarWhenPushed      = YES;
            [self.navigationController pushViewController:_normalUserAnsweredQesViewController animated:YES];
            IOS7POP;

        }
        else if (indexPath.row == 5) {

            iEver_normalUserUnAnswerQesViewController *_normalUserUnAnswerQesViewController = [[iEver_normalUserUnAnswerQesViewController alloc]init];
            _normalUserUnAnswerQesViewController.hidesBottomBarWhenPushed      = YES;
            [self.navigationController pushViewController:_normalUserUnAnswerQesViewController animated:YES];
            IOS7POP;

        }

    }
    else if ([[NSUserDefaults standardUserDefaults] integerForKey:@"userType"] == 20){


        if (indexPath.row == 0) {
            iEver_MessageViewController *_messageViewController = [[iEver_MessageViewController alloc]init];
            _messageViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:_messageViewController animated:YES];
            IOS7POP;
        }
        else if (indexPath.row == 1) {

            iEver_VuserShareArticleViewController *_VuserShareArticleViewController = [[iEver_VuserShareArticleViewController alloc]init];
            _VuserShareArticleViewController.title                         = @"经验分享";
            _VuserShareArticleViewController.v_dataType                    = myPage_VuserShare_dataType;
            _VuserShareArticleViewController.navigationController          = self.navigationController;
            _VuserShareArticleViewController.hidesBottomBarWhenPushed      = YES;
            [self.navigationController pushViewController:_VuserShareArticleViewController animated:YES];
            IOS7POP;

        }
        else if (indexPath.row == 2) {

            iEver_MY_ItemTryViewController *_MY_ItemTryViewController = [[iEver_MY_ItemTryViewController alloc]init];
            _MY_ItemTryViewController.title = @"我的试用";
            _MY_ItemTryViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:_MY_ItemTryViewController animated:YES];
            IOS7POP;


        }
        else if (indexPath.row == 3) {

            iEver_OfficialListViewController *_OfficialListViewController = [[iEver_OfficialListViewController alloc]init];
            _OfficialListViewController.userLike                          = 1;
            _OfficialListViewController.title                             = @"赞过的文章";
            _OfficialListViewController.category_id                       = 1;
            _OfficialListViewController.hidesBottomBarWhenPushed          = YES;
            [self.navigationController pushViewController:_OfficialListViewController animated:YES];
            IOS7POP;



        }
        else if (indexPath.row == 4) {

            iEver_VuserShareArticleViewController *_VuserShareArticleViewController = [[iEver_VuserShareArticleViewController alloc]init];
            _VuserShareArticleViewController.title                         = @"赞过的经验";
            _VuserShareArticleViewController.v_dataType                    = myPage_NuserLikeVuser_dataType;
            _VuserShareArticleViewController.navigationController          = self.navigationController;
            _VuserShareArticleViewController.hidesBottomBarWhenPushed      = YES;
            [self.navigationController pushViewController:_VuserShareArticleViewController animated:YES];
            IOS7POP;



        }
        else if (indexPath.row == 5) {

            iEver_VuserAnsweredViewController *_VuserAnsweredViewController = [[iEver_VuserAnsweredViewController alloc]init];
            _VuserAnsweredViewController.title = @"已回答问题";
            _VuserAnsweredViewController.hidesBottomBarWhenPushed      = YES;
            [self.navigationController pushViewController:_VuserAnsweredViewController animated:YES];
            IOS7POP;

        }

        else if (indexPath.row == 6) {

            iEver_VuserUnAnswerdeViewController *_VuserUnAnswerdeViewController = [[iEver_VuserUnAnswerdeViewController alloc]init];
            _VuserUnAnswerdeViewController.hidesBottomBarWhenPushed      = YES;
            _VuserUnAnswerdeViewController.title = @"未回答问题";
            [self.navigationController pushViewController:_VuserUnAnswerdeViewController animated:YES];
            IOS7POP;

        }

    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];


}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationPortrait;
}

@end

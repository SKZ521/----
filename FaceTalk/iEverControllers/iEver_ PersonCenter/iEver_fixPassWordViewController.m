//
//  iEver_fixPassWordViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-5.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_fixPassWordViewController.h"
#import "iEver_UserMessageObject.h"
@interface iEver_fixPassWordViewController ()
@property (strong) IBOutlet UIScrollView *scrollView;
@property (strong) IBOutlet UITextField  *originalPassword_textField;
@property (strong) IBOutlet UITextField  *fixPassword_textField;
@property (strong) IBOutlet UITextField  *confimPassword__textField;
@property (strong) IBOutlet UIButton     *confimed_button;
- (IBAction)buttonAction:(id)sender;
@end

@implementation iEver_fixPassWordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"修改密码";

    self.view.backgroundColor = CLEAR;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /* scrollView  */
    [self backScrollView];

    /* fix custom text Font */

    [self customButtonFont];

    
}

-(void)customButtonFont {

    self.originalPassword_textField.font = TextFont;
    self.fixPassword_textField.font = TextFont;
    self.confimPassword__textField.font = TextFont;
    self.confimed_button.titleLabel.font = Title_V_Fonts;
    self.confimed_button.backgroundColor = MY_PURPLECOLOR;
    CALayer *coverButton = [self.confimed_button layer];
    [coverButton setMasksToBounds:YES];
    [coverButton setCornerRadius:5.0];

}

-(void)backScrollView{

    _scrollView.scrollEnabled = YES;
    CGSize newSize = CGSizeMake(self.view.frame.size.width, 700.0);
    [_scrollView setContentSize:newSize];
    
}
#pragma mark
#pragma mark - button Action
- (IBAction)buttonAction:(id)sender
{

    if (self.originalPassword_textField.text.length == 0) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的密码"];
        return;
    }
    if (20 < self.fixPassword_textField.text.length) {
        [SVProgressHUD showImage:Nil status:@"密码太长"];
        return;
    }
    if ( 6 > self.fixPassword_textField.text.length) {
        [SVProgressHUD showImage:Nil status:@"密码太短"];
        return;
    }
    if ([self.fixPassword_textField.text isEqualToString:self.originalPassword_textField.text]) {
        [SVProgressHUD showImage:Nil status:@"验新密码与旧密码一致"];
        return;
    }
    if (![self.fixPassword_textField.text isEqualToString:self.confimPassword__textField.text]) {
        [SVProgressHUD showImage:Nil status:@"两次输入不一致"];
        return;
    }
    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSDictionary *dic = @{@"originalPassword": self.originalPassword_textField.text,
                          @"newPassword": self.fixPassword_textField.text,
                          };

    [[[content updatePasswd:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {

         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"修改成功"];
             [self.navigationController popToRootViewControllerAnimated:YES];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

     }];


}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;

}


#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    [self.originalPassword_textField resignFirstResponder];
    [self.fixPassword_textField resignFirstResponder];
    [self.confimPassword__textField resignFirstResponder];
}
@end

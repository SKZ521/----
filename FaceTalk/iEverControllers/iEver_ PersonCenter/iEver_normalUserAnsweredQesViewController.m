//
//  iEver_normalUserAnsweredQesViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_normalUserAnsweredQesViewController.h"
#import "iEver_normalUserAnsweredCell.h"
#import "iEver_normalUserUnAnswerObject.h"
@interface iEver_normalUserAnsweredQesViewController ()

@end

@implementation iEver_normalUserAnsweredQesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"已被回答问题";
    /* NO show pull refresh */
    self.tableView.showsPullToRefresh =NO;
    self.tableView.showsInfiniteScrolling = NO;

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    /* request data */
    [self fetchData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*request data*/
-(void)fetchData{

    @weakify(self);
    iEver_normalUserUnAnswerObject *question_Object = [[iEver_normalUserUnAnswerObject alloc] init];
    NSString *urlPath = [NSString stringWithFormat:@"/user/queryQuestByAnswered/%d",self.page];
    [[[question_Object queryQuestByUnanswered:nil path:urlPath] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_normalUserUnAnswerObject *object) {

         @strongify(self);
         [self addDataToDataSource:object._quesList];
         [self.tableView reloadData];

     }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"normalUserAnsweredCell";
    UITableViewCell *cell = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = (iEver_normalUserAnsweredCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (!cell) {
        cell = [[iEver_normalUserAnsweredCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    iEver_normalUserAnsweredCell *dic_cell = (iEver_normalUserAnsweredCell *)cell;
    iEver_N_quesList_object * object = [self.dataSourceArray objectAtIndex:indexPath.row];
    [dic_cell setViewController:self.navigationController];
    [dic_cell setObject:object];
    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    id object = [self.dataSourceArray objectAtIndex:indexPath.row];
    return [iEver_normalUserAnsweredCell heightForObject:object atIndexPath:indexPath tableView:tableView];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end

//
//  iEver_VuserUnAnswerdeViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_VuserUnAnswerdeViewController.h"
#import "iEver_V_UnAnsweredCell.h"
#import "iEver_V_answeredQuestionObject.h"
#import "iEver_AnswerYouViewController.h"
@interface iEver_VuserUnAnswerdeViewController ()

@end

@implementation iEver_VuserUnAnswerdeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"未回答提问";
    /* NO show pull refresh */
    self.tableView.showsPullToRefresh =NO;
    self.tableView.showsInfiniteScrolling = NO;

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    /* request data */
    [self fetchData];
}
/*request data*/
-(void)fetchData{

    @weakify(self);
    iEver_V_answeredQuestionObject *question_Object = [[iEver_V_answeredQuestionObject alloc] init];
    int userId =  [[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] intValue];
    NSString *urlPath = [NSString stringWithFormat:@"/expertQuestion/queryUnanswered/%d/%d",userId,self.page];
    [[[question_Object queryAnsweredByUser:nil path:urlPath] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_V_answeredQuestionObject *object) {

         @strongify(self);
         [self addDataToDataSource:object._quesList];
         [self.tableView reloadData];

     }];
}
-(void)delegateMethod{

    [self.dataSourceArray removeAllObjects];
    [self fetchData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"VUserUnAnsweredCell";
    UITableViewCell *cell = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = (iEver_V_UnAnsweredCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (!cell) {
        cell = [[iEver_V_UnAnsweredCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    iEver_V_UnAnsweredCell *dic_cell = (iEver_V_UnAnsweredCell *)cell;
    iEver_V_quesList_object * object = [self.dataSourceArray objectAtIndex:indexPath.row];
    [dic_cell setObject:object];
    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    id object = [self.dataSourceArray objectAtIndex:indexPath.row];
    return [iEver_V_UnAnsweredCell heightForObject:object atIndexPath:indexPath tableView:tableView];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    iEver_V_quesList_object *object = [self.dataSourceArray objectAtIndex:indexPath.row];
    iEver_AnswerYouViewController *_answerYouViewController = [[iEver_AnswerYouViewController alloc] init];
    _answerYouViewController.userId = object.ques_id;
    _answerYouViewController.image  = object.qHeadImg;
    _answerYouViewController.name   = object.qNickName;
    _answerYouViewController.question = object.qContent;
    
    _answerYouViewController.delegate = self;
    _answerYouViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:_answerYouViewController animated:YES];
    IOS7POP;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end

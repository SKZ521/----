//
//  iEver_LoginViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-15.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_LoginViewController.h"
#import "iEver_OfficialListViewController.h"
#import "iEver_RegisterViewController.h"
#import "iEver_PhoneRegistViewController.h"
#import "iEver_FindPonePassWordViewController.h"
#import "iEver_FindMailPassWordViewController.h"
#import "iEver_UserMessageObject.h"


#import "UMSocialWechatHandler.h"

#define Mode_viewHeight 200
#define registerMode_viewY   self.view.frame.size.height - Mode_viewHeight
@interface iEver_LoginViewController ()
@property (strong) IBOutlet UIScrollView *scrollView;
@property (strong) IBOutlet UITextField  *userName_textField;
@property (strong) IBOutlet UITextField  *password_textField;
@property (strong) IBOutlet UIView       *registerMode_view;
@property (strong) IBOutlet UIButton     *background_button;

@property (strong) IBOutlet UIButton     *login_button;
@property (strong) IBOutlet UIButton     *selectRegister_button;
@property (strong) IBOutlet UIButton     *lostPassword_button;

@property (strong) IBOutlet UILabel      *selectThirdLogin_lable;
@property (strong) IBOutlet UIButton     *WXLogin_button;
@property (strong) IBOutlet UIButton     *sinaLogin_button;
@property (strong) IBOutlet UILabel      *WXLogin_lable;
@property (strong) IBOutlet UILabel      *sinaLogin_lable;

@property (strong) IBOutlet UILabel      *selectRegister_lable;
@property (strong) IBOutlet UIButton     *mailRegister_button;
@property (strong) IBOutlet UIButton     *phoneRegister_button;
@property (strong) IBOutlet UILabel     *phoneRegister_lable;
@property (strong) IBOutlet UILabel     *mailRegister_lable;


- (IBAction)buttonAction:(id)sender;

- (IBAction)colseViewbuttonAction:(id)sender;

- (IBAction)selectRegisterAction:(id)sender;

- (IBAction)findCertificationCodeAction:(id)sender;
@end

@implementation iEver_LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey: @"account"];
    if (userName.length>0) {
        self.userName_textField.text = userName;
    }
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self Mode_viewDisappear];
    self.navigationController.navigationBarHidden = NO;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /* scrollView  */
    [self backScrollView];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"登录";
    
    self.view.backgroundColor = CLEAR;
    
    /* fix custom text Font */
    
    [self customButtonFont];
    
    
}

-(void)customButtonFont {
    
    self.userName_textField.font = TitleFont;
    self.password_textField.font = TitleFont;
    self.login_button.titleLabel.font = Title_V_Fonts;
    self.login_button.backgroundColor = MY_PURPLECOLOR;
    CALayer *coverButton = [self.login_button layer];
    [coverButton setMasksToBounds:YES];
    [coverButton setCornerRadius:5.0];
    self.lostPassword_button.titleLabel.font = TextDESCFonts;
    self.selectRegister_button.titleLabel.font = TextDESCFonts;
    
    self.selectRegister_lable.font = Title_Font;
    self.phoneRegister_lable.font = TextFonts;
    self.mailRegister_lable.font = TextFonts;
    
    self.selectThirdLogin_lable.font = Title_Font;
    self.WXLogin_lable.font = TextFonts;
    self.sinaLogin_lable.font = TextFonts;
    
}

-(void)backScrollView{
    
    _scrollView.scrollEnabled = YES;
    CGSize newSize = CGSizeMake(self.view.frame.size.width, 700.0);
    [_scrollView setContentSize:newSize];
    
}

/**
 *  关闭当前控制器
 *
 */


- (IBAction)colseViewbuttonAction:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/**
 *   选择注册方式
 *
 */
- (IBAction)selectRegisterAction:(id)sender{
    
    [self Mode_viewAppear];
    
}
/**
 *   找回密码
 *
 */

- (IBAction)findCertificationCodeAction:(id)sender{
    
    [self.view endEditing:YES];
    
    @weakify(self);
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"找回手机注册密码",@"找回邮箱注册密码", nil];
    [actionSheet.rac_buttonClickedSignal subscribeNext:^(id x) {
        @strongify(self);
        
        NSInteger aIndex = [x integerValue];
        
        if (aIndex == 0) {
            
            iEver_FindPonePassWordViewController * _FindPonePassWordViewController = [[iEver_FindPonePassWordViewController alloc] init];
            [self.navigationController pushViewController:_FindPonePassWordViewController animated:YES];
            IOS7POP;
            
        }else if (aIndex == 1) {
            
            iEver_FindMailPassWordViewController * _FindMailPassWordViewController = [[iEver_FindMailPassWordViewController alloc] init];
            [self.navigationController pushViewController:_FindMailPassWordViewController animated:YES];
            IOS7POP;
        }
    }];
    
    [actionSheet showInView:self.view.window];
}

/**
 *   @param    ;1 登录;    3 微信登录; 4 新浪登录  5 手机注册；6邮箱注册 7.叉号关闭；8，背景关闭
 *
 */

#pragma mark
#pragma mark - button Action
- (IBAction)buttonAction:(id)sender
{
    
    [self.userName_textField resignFirstResponder];
    [self.password_textField resignFirstResponder];
    
    UIButton *button = (UIButton *)sender;
    NSInteger index = button.tag - 10;
    switch (index) {
        case 1:/* 登录 */
        {
            
            BOOL isNetworkWorking = [[iEver_Global Instance] isExistenceNetwork];
            if (!isNetworkWorking) {
                UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:@"警告"
                                                                  message:NetWorkError
                                                                 delegate:nil
                                                        cancelButtonTitle:@"确认"
                                                        otherButtonTitles:nil];
                [myalert show];
            }else {
                
                
                if (self.userName_textField.text.length == 0) {
                    [SVProgressHUD showImage:Nil status:@"请输入正确的登录名"];
                    return;
                }
                if (self.password_textField.text.length < 1) {
                    [SVProgressHUD showImage:Nil status:@"输入正确的密码"];
                    return;
                }
                [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
                
                // changed by CommaWang 2015-05-31
                NSDictionary *dic = @{@"account": self.userName_textField.text,
                                      @"password": self.password_textField.text,
                                      @"pushToken": [APService registrationID],
                                      @"deviceType": @"ios"
                                      };
                [self loginWithMode:@"Normal" parameters:dic];
            }
            
            
        }
            break;
        case 3:/* 微信登录 */
        {
            
            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
            
            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
                
                if (response.responseCode == UMSResponseCodeSuccess) {
                    
                    UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
                    
                    NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
                    
                    // changed by CommaWang 2015-05-31
                    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                    [parameters setObject:@"wechar" forKey:@"origin"];// 第三方来源 wechar或者weibo
                    [parameters setObject:@"ios" forKey:@"deviceType"];// 手机类型 ios或者android
                    if (snsAccount.usid)// 第三方获取的用户标识
                    {
                        [parameters setObject:snsAccount.usid forKey:@"uid"];
                    }
                    if (snsAccount.userName)// 第三方获取的用户昵称
                    {
                        [parameters setObject:snsAccount.userName forKey:@"nickName"];
                    }
                    if (snsAccount.iconURL)// 第三方获取的用户头像
                    {
                        [parameters setObject:snsAccount.iconURL forKey:@"headImg"];
                    }
                    // 设备标识
                    NSString *idfaStr = [[iEverAppDelegate shareAppDelegate] idfaString];
                    if (idfaStr)
                    {
                        [parameters setObject:idfaStr forKey:@"deviceId"];
                        
                        // 签名 md5(uid+deviceId)
                        NSString *signature = [[NSString stringWithFormat:@"%@%@",snsAccount.usid,idfaStr] MD5Digest];
                        [parameters setObject:signature forKey:@"signature"];
                    }
                    
                    // jpush推送registerId(获取不到的情况下不填写)
                    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"];
                    if (token)
                    {
                        [parameters setObject:token forKey:@"pushToken"];
                    }
                    
                    [self loginWithMode:@"Third" parameters:parameters];
                }
                
            });
        }
            break;
        case 4:/* 微博登录 */
        {
            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
            
            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
                
                //          获取微博用户名、uid、token等
                
                if (response.responseCode == UMSResponseCodeSuccess) {
                    
                    UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
                    
                    NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
                    
                    // changed by CommaWang 2015-05-31
                    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                    [parameters setObject:@"weibo" forKey:@"origin"];// 第三方来源 wechar或者weibo
                    [parameters setObject:@"ios" forKey:@"deviceType"];// 手机类型 ios或者android
                    if (snsAccount.usid)// 第三方获取的用户标识
                    {
                        [parameters setObject:snsAccount.usid forKey:@"uid"];
                    }
                    if (snsAccount.userName)// 第三方获取的用户昵称
                    {
                        [parameters setObject:snsAccount.userName forKey:@"nickName"];
                    }
                    if (snsAccount.iconURL)// 第三方获取的用户头像
                    {
                        [parameters setObject:snsAccount.iconURL forKey:@"headImg"];
                    }
                    // 设备标识
                    NSString *idfaStr = [[iEverAppDelegate shareAppDelegate] idfaString];
                    if (idfaStr)
                    {
                        [parameters setObject:idfaStr forKey:@"deviceId"];
                        
                        // 签名 md5(uid+deviceId)
                        NSString *signature = [[NSString stringWithFormat:@"%@%@",snsAccount.usid,idfaStr] MD5Digest];
                        [parameters setObject:signature forKey:@"signature"];
                    }
                    
                    // jpush推送registerId(获取不到的情况下不填写)
                    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"];
                    if (token)
                    {
                        [parameters setObject:token forKey:@"pushToken"];
                    }
                    
                    [self loginWithMode:@"Third" parameters:parameters];
                }});
        }
            break;
            
        case 5:/* 手机注册 */
        {
            
            iEver_PhoneRegistViewController * _phoneConfirmationViewController = [[iEver_PhoneRegistViewController alloc] init];
            _phoneConfirmationViewController.delegate = self.delegate;
            [self.navigationController pushViewController:_phoneConfirmationViewController animated:YES];
            IOS7POP;
        }
            break;
        case 6:/* 邮箱注册 */
        {
            iEver_RegisterViewController * _mailRegisterViewController = [[iEver_RegisterViewController alloc] init];
            _mailRegisterViewController.delegate = self.delegate;
            [self.navigationController pushViewController:_mailRegisterViewController animated:YES];
            IOS7POP;
            
        }
            break;
            
        case 7:/* 叉号关闭 */
        {
            [self Mode_viewDisappear];
        }
            break;
        case 8:/* 背景关闭 */
        {
            [self Mode_viewDisappear];
        }
            break;
            
        default:
            break;
    }
    
}

/**
 * mode:Normal  Third
 */
- (void)loginWithMode:(NSString *)mode parameters:(NSDictionary *)paras
{
    NSString *path = @"user/loginVerify";
    if ([mode isEqualToString:@"Third"])
    {
        path = @"user/thirdLogin";
    }
    
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    
    [[[content userLogin:paras path:path] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             
             BOOL isLogin = YES;
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setBool:isLogin forKey:@"isLogin"];
             [defaults setObject:[object objectForKey:@"loginKey"] forKey:@"loginKey"];
             [defaults setObject:[object objectForKey:@"userId"] forKey:@"userId"];
             [defaults setInteger:[[object objectForKey:@"userType"] integerValue] forKey:@"userType"];
             [defaults setObject:self.userName_textField.text forKey:@"account"];
             [defaults synchronize];
             
             if ([self.delegate respondsToSelector:@selector(delegateMethod)]) {
                 [self.delegate delegateMethod];
             }
             
             [SVProgressHUD showImage:Nil status:@"登录成功"];
             [self dismissViewControllerAnimated:YES completion:nil];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }
     }];
}

#pragma mark
#pragma mark - photo view appear/disappear
- (void)Mode_viewDisappear
{
    self.background_button.hidden = YES;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.registerMode_view.frame = CGRectMake(0, ScreenHeight, ScreenWidth, Mode_viewHeight);
    [UIView commitAnimations];
}

- (void)Mode_viewAppear
{
    self.background_button.hidden = NO;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.registerMode_view.frame = CGRectMake(0, registerMode_viewY, ScreenWidth, Mode_viewHeight);
    [UIView commitAnimations];
}


#pragma mark
#pragma mark - Certification iEverUser _login
-(void)iEverUserLogin{
    
    if (self.userName_textField.text.length == 0) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的登录名"];
        return;
    }
    if (self.password_textField.text.length < 6) {
        [SVProgressHUD showImage:Nil status:@"输入正确的密码"];
        return;
    }
    
}


#pragma mark - Configuring the view’s layout behavior

- (UIStatusBarStyle)preferredStatusBarStyle
{
    
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    [self.userName_textField resignFirstResponder];
    [self.password_textField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end

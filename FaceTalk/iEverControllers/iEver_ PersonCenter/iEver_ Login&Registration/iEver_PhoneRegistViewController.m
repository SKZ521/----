//
//  iEver_PhoneRegistViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-15.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_PhoneRegistViewController.h"
#import "iEver_PhoneRegister_NextViewController.h"
#import "iEver_UserMessageObject.h"
@interface iEver_PhoneRegistViewController (){

}
@property (strong) IBOutlet UIScrollView *scrollView;
@property (strong) IBOutlet UITextField *userPhone_textField;
@property (strong) IBOutlet UITextField *certificationCode_textField;
@property (strong) IBOutlet UIButton    *C_button;
@property (strong) IBOutlet UIButton    *next_button;
@property(nonatomic ,copy)NSString *verifyCode;

- (IBAction)requestCertificationCodeAction:(id)sender;
- (IBAction)nextAction:(id)sender;
@end

@implementation iEver_PhoneRegistViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    self.title = @"手机注册";

    /* fix custom text Font */

    [self customButtonFont];
}

-(void)customButtonFont {

    self.userPhone_textField.font = TitleFont;

    CALayer *nextButton = [self.next_button layer];
    [nextButton setMasksToBounds:YES];
    [nextButton setCornerRadius:5.0];

    CALayer *certifiButton = [self.C_button layer];
    [certifiButton setMasksToBounds:YES];
    [certifiButton setCornerRadius:10.0];

    self.userPhone_textField.font = Title_Font;
    self.certificationCode_textField.font = Title_Font;
    self.next_button.titleLabel.font = Title_V_Fonts;
    self.C_button.titleLabel.font = TextDESCFonts;
    self.C_button.backgroundColor = yanzheng_N;
    self.next_button.backgroundColor = MY_PURPLECOLOR;

}


- (IBAction)requestCertificationCodeAction:(id)sender{

    UIButton *CertifieyButton =(UIButton *)sender;

    if (![[iEver_Global Instance] validateMobile:self.userPhone_textField.text]) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的手机号"];
        return;
    }

    /*Confirmation code  request mothod*/
    [self requestCertificationCodeInterface:CertifieyButton];


}

-(void)requestCertificationCodeInterface:(UIButton *)sender{

    //_verifyCode = [object objectForKey:@"verifyCode"];
    __block int timeout= 60; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setTitle:@"发送验证码" forState:UIControlStateNormal];
                sender.userInteractionEnabled = YES;
                sender.backgroundColor = yanzheng_N;
            });
        }else{
            int seconds = timeout % 100;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setTitle:[NSString stringWithFormat:@"%@秒后重发",strTime] forState:UIControlStateNormal];
                sender.backgroundColor = yanzheng_C;
                sender.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);

    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSDictionary *dic = @{@"mobilePhone": self.userPhone_textField.text,
                          };

    [[[content postSendVerifyCode:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {


         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];

             if ([[object objectForKey:@"resultCode"] integerValue] == -3008) {
                 timeout = 0;
                 [sender setTitle:@"发送验证码" forState:UIControlStateNormal];
                 sender.userInteractionEnabled = YES;
                 sender.backgroundColor = yanzheng_N;

             }

         }
         
     }];
    

}
- (IBAction)nextAction:(id)sender{


    if (![[iEver_Global Instance] validateMobile:self.userPhone_textField.text]) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的手机号"];
        return;
    }

    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSDictionary *dic = @{@"mobilePhone": self.userPhone_textField.text,
                          @"verifyCode": self.certificationCode_textField.text,
                          };

    [[[content postVerifyMobile:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

             /*go  next viewcontroller */
             iEver_PhoneRegister_NextViewController * _PhoneRegisterViewController = [[iEver_PhoneRegister_NextViewController alloc] init];
             _PhoneRegisterViewController.iphoneNum = self.userPhone_textField.text;
             _PhoneRegisterViewController.verifyCode = self.certificationCode_textField.text;
             _PhoneRegisterViewController.delegate = self.delegate;
             [self.navigationController pushViewController:_PhoneRegisterViewController animated:YES];
             IOS7POP;

         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
             return;
         }
     }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end

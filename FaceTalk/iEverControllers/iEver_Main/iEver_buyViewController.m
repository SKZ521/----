//
//  iEver_buyViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-22.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_buyViewController.h"

@interface iEver_buyViewController ()<UIWebViewDelegate>
{
    UIWebView *webView;
    UIActivityIndicatorView *activityIndicator;
}
@property (strong, nonatomic) UIImageView *backImageView;
@end

@implementation iEver_buyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"相关链接";
    /* back imageview*/
    [self creatBackImageView];

    /* add left  right bar */
    [self addBar];

    /* creatWebview */
    [self creatWebview];
}
-(void)creatBackImageView{

    _backImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    _backImageView.image = [UIImage imageNamed:@"backImage"];
    [self.view addSubview:_backImageView];
    
}

-(void)creatWebview{

    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64.0)];
    webView.delegate = self;
    NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:self.itemLink]];
    [self.view addSubview: webView];
    [webView loadRequest:request];
}
//添加左右 bar
- (void)addBar
{
    __weak iEver_buyViewController *weakSelf = self;
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage * return_image = [UIImage imageNamed:@"return"];
    leftButton.frame = CGRectMake(0, 0, return_image.size.width, return_image.size.height);
    [leftButton setImage:return_image forState:UIControlStateNormal];
	leftButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {


        [weakSelf.navigationController popViewControllerAnimated:YES];
		return [RACSignal empty];

	}];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
	self.navigationItem.leftBarButtonItem = leftBarButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void) webViewDidStartLoad:(UIWebView *)webView
{
    //创建UIActivityIndicatorView背底半透明View
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [view setTag:108];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha:0.5];
    [self.view addSubview:view];

    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [activityIndicator setCenter:view.center];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [view addSubview:activityIndicator];

    [activityIndicator startAnimating];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator stopAnimating];
    UIView *view = (UIView*)[self.view viewWithTag:108];
    [view removeFromSuperview];
    NSLog(@"webViewDidFinishLoad");

}
- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [activityIndicator stopAnimating];
    UIView *view = (UIView*)[self.view viewWithTag:108];
    [view removeFromSuperview];
}
@end

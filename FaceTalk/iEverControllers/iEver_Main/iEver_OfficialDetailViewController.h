//
//  iEver_OfficialDetailViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-6.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"
#import "iEver_mainContent.h"
#import <MediaPlayer/MediaPlayer.h>

@class iEver_OfficialListViewController;


@interface iEver_OfficialDetailViewController : iEverBaseTableViewController

@property (nonatomic, assign) int                        type_id;       /* id */
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) iEver_mainContentList_object *list_Object;
@property (nonatomic, retain) iEver_OfficialListViewController *delegate;
@property (nonatomic, assign) int                        didSelectIndexPath;/* 选中cell的indexpath  */

-(void)insertCommentToReplyComment;                                     /* 点击评论的评论，进行评论 */
@property (nonatomic, copy  ) NSString       *toReplyCommentName;       /* 评论回复 用户名 */
@property (nonatomic, assign) int               comment_parentId;       /* 评论回复 的parent ID */
@property (nonatomic, assign) int               comment_atUserId;       /* 评论回复 的atUser ID */

@end

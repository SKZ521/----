//
//  iEver_buyViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-22.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

@interface iEver_buyViewController : iEverBaseViewController
@property (nonatomic, copy  ) NSString                   *itemLink;        /* 商品链接 */
@end

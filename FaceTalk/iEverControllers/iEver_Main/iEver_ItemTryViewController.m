//
//  iEver_ItemTryViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-30.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_ItemTryViewController.h"
#import "iEver_LoginViewController.h"
#import "iEver_ItemTryDetailViewController.h"
#import "iEver_tryItemObject.h"
#import "iEver_itemTry_cell.h"

@interface iEver_ItemTryViewController ()
{

    iEver_tryItemObject *_tryobject;
}
@end

@implementation iEver_ItemTryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        self.title = @"活动";

        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@""
                                                        image:[UIImage imageNamed:@"tabbar_gift_normal"]
                                                selectedImage:[UIImage imageNamed:@"tabbar_gift_click"]];
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

//    /* request data */
//    [self fetchData];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [GiFHUD dismiss];
    [super viewWillDisappear:animated];
}
/* creat left right bar button */
-(void)addBar{

    self.navigationItem.leftBarButtonItem  = nil;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    /* creat left right bar button */
    [self addBar];

    /* configure cell */
    [self configureCell];

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });


    [self fetchData];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = YES;
    self.tableView.showsInfiniteScrolling = YES;

    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

}

/* configure cell */
-(void)configureCell{

    __typeof (self) __weak weakSelf = self;

    self.dataSource.cellConfigureBlock = ^(iEver_itemTry_cell *cell,
                                           NSNumber *exampleType,
                                           UITableView *tableView,
                                           NSIndexPath *indexPath) {

        [cell configureCell];
        iEver_itemTryList_object *listObject = (iEver_itemTryList_object *)[weakSelf.dataSource itemAtIndexPath:indexPath];
        cell.object                          = listObject;
    };
    self.dataSource.cellClass = [iEver_itemTry_cell class];
}
-(void)delegateMethod{
    [self fetchData];
}

/*request data*/
-(void)fetchData{

    @weakify(self);

    [GiFHUD show];

    iEver_tryItemObject *detail_Object = [[iEver_tryItemObject alloc] init];
    NSString            *_pathUrl      = [NSString stringWithFormat:@"/itemTry/queryItemTry/%d",self.page];

    [[[detail_Object questTryItemData:nil
                                 path:_pathUrl]
                            deliverOn:[RACScheduler mainThreadScheduler]]
                        subscribeNext:^(iEver_tryItemObject *object) {

        @strongify(self);
         if (object.resultCode == 1) {

             if (object.pageSize < self.page) {

                 self.tableView.showsInfiniteScrolling = NO;

             }else{

                 [self            reloadDatasouce:object._itemTryList];
                 [self.tableView  reloadData];
             }

             [GiFHUD  dismiss];
         }
         else{

             iEver_LoginViewController *_loginViewController = [[iEver_LoginViewController alloc]init];
             _loginViewController.delegate                   = self;
             _loginViewController.hidesBottomBarWhenPushed   = YES;
             [self.navigationController pushViewController:_loginViewController animated:YES];
             IOS7POP;
         }

     }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [iEver_itemTry_cell heightForObject:nil atIndexPath:indexPath tableView:tableView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

    iEver_itemTryList_object *object                                = (iEver_itemTryList_object *)[self.dataSource itemAtIndexPath:indexPath];
    iEver_ItemTryDetailViewController *_itemTryDetailViewController = [[iEver_ItemTryDetailViewController alloc]init];
    _itemTryDetailViewController.type_id                            = object.T_ID;
    _itemTryDetailViewController.hidesBottomBarWhenPushed           = YES;
    [self.navigationController pushViewController:_itemTryDetailViewController animated:YES];
    IOS7POP;
}


#pragma mark -
#pragma mark UIInterfaceOrientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}

@end

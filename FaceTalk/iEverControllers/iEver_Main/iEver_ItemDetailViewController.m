//
//  iEver_ItemDetailViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-23.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_ItemDetailViewController.h"
#import "iEver_itemDetail_Object.h"
#import "iEver_itemCommentViewController.h"
#import "MCFireworksButton.h"
#import "iEver_likeActionObject.h"
#import "iEver_itemDetailCell_PicList.h"
#import "iEver_ItemDetail_PicList_TestCell.h"
#import "iEver_itemDetail_recommendItem_cell.h"
#import "iEver_itemDetail_comment_cell.h"
#import "iEver_commentObject.h"
#import "iEver_buyViewController.h"
#import "DWTagList.h"
#import "iEver_Item_TagList.h"
#import "CWStarRateView.h"
#import <CoreText/CoreText.h>
#import "NSString+WPAttributedMarkup.h"

@interface iEver_ItemDetailViewController ()
{

    iEver_itemDetail_Object *_datailOblect;
    UILabel *likeLabel;
    UIImageView *coverImageview;
    UIWebView *webView;
    MCFireworksButton *likeBut;
    MCFireworksButton *likeButton;

    /* 导航悬浮按钮 */
    UIButton                 *itemDetail_returnButton;
    MCFireworksButton        *itemDetail_likeButton;
    UIButton                 *itemDetail_shareButton;

    /* 表头放置按钮视图 */
    UIView *buttonView;
}
@property (retain, nonatomic) UIView *mainView;
@property (retain, nonatomic) UIButton *bottomButton;
@property (retain, nonatomic) UITextView *myTextView;
@property (nonatomic, strong) UILabel     *commentLab;
@property (nonatomic,retain)MPMoviePlayerController *moviePlayer;
@property (nonatomic, strong) NSURL *contentURL;
@property (nonatomic, strong) UIView *fullscreenOverlay;

@end

@implementation iEver_ItemDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    itemDetail_returnButton.hidden = NO;
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];

}
- (void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    if (!self.moviePlayer.fullscreen) {
        [self createAndConfigureMoviePlayer];
        MPMoviePlayerController *player = self.moviePlayer;
        if (self.contentURL) {
            player.contentURL = self.contentURL;
            [player prepareToPlay];
            [self resizeMovieView];
        }
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    itemDetail_returnButton.hidden = YES;
    itemDetail_likeButton.hidden = YES;
    itemDetail_shareButton.hidden = YES;
    self.navigationController.navigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];

}

- (void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    itemDetail_returnButton.hidden = YES;
    itemDetail_likeButton.hidden = YES;
    itemDetail_shareButton.hidden = YES;
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];

    // viewWillDisappear/viewDidDisappear is called when entering fullscreen on iOS 6+.
    if (!self.moviePlayer.fullscreen) {
        [self removeMovieViewFromViewHierarchy];
        [self deleteMoviePlayerAndNotificationObservers];
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self resizeMovieView];
}

#pragma mark - Setters and Getters

- (UIWindow *)appWindow
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if (!window) {
        window = ([UIApplication sharedApplication].windows)[0];
    }

    return window;
}
- (void)resizeMovieView
{
    //self.moviePlayer.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 320.0);

    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        if (self.view.frame.size.width == 736) {
            self.moviePlayer.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 414);
        }else if(self.view.frame.size.width == 667){
            self.moviePlayer.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 375);
        }else{
            self.moviePlayer.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 320);
        }
    }else{
        self.moviePlayer.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 320);
    }
}


#pragma mark - Create and Configure Movie Player

- (void)createAndConfigureMoviePlayer
{

    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] init];

    if (player) {
        self.moviePlayer = player;

        [self installMovieNotificationObservers];

        player.movieSourceType = MPMovieSourceTypeStreaming;
        player.view.hidden = YES;
        player.shouldAutoplay = NO;
        player.allowsAirPlay = YES;

        [self.tableView.tableHeaderView addSubview:player.view];
        [self.tableView.tableHeaderView bringSubviewToFront:player.view];
        
    }
}

- (void)installMovieNotificationObservers
{
    MPMoviePlayerController *player = self.moviePlayer;


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:player];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didEnterFullscreen:)
                                                 name:MPMoviePlayerDidEnterFullscreenNotification
                                               object:player];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willExitFullscreen:)
                                                 name:MPMoviePlayerWillExitFullscreenNotification
                                               object:player];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didExitFullscreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:player];
}
#pragma mark - Movie Notification Handlers

- (void)moviePlayBackDidFinish:(NSNotification *)notification
{
    NSLog(@"moviePlayBackDidFinish");
    NSNumber *reason = [notification userInfo][MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    switch ([reason integerValue]) {
        case MPMovieFinishReasonPlaybackEnded:
            break;
        case MPMovieFinishReasonPlaybackError:
            break;
        case MPMovieFinishReasonUserExited:
            [self removeMovieViewFromViewHierarchy];
            break;
        default:
            break;
    }
}
- (void)didEnterFullscreen:(NSNotification *)notification
{
    /* 添加通知监测设备方向 */
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(layoutFullscreenOverlayForOrientation:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = UIInterfaceOrientationLandscapeLeft;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }    // This needs to be called in the next run loop for it to work properly in iOS 6+
    [self performSelector:@selector(addFullscreenOverlayForCurrentInterfaceOrientation) withObject:nil afterDelay:0];
}
- (void)willExitFullscreen:(NSNotification*)notification
{
    [GiFHUD dismiss];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];

    // Remove the fullscreen overlay view
    if (self.fullscreenOverlay) {
        [self.fullscreenOverlay removeFromSuperview];
        self.fullscreenOverlay = nil;
    }
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = UIInterfaceOrientationPortrait;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
}
- (void)didExitFullscreen:(NSNotification*)notification
{
    // Remove the fullscreen overlay view
    // This was called already in willExitFullscreen: but we'll call it again to ensure that it's removed.
    if (self.fullscreenOverlay) {
        [self.fullscreenOverlay removeFromSuperview];
        self.fullscreenOverlay = nil;
    }
}
#pragma mark - Fullscreen Overlay  1111

- (void)addFullscreenOverlayForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{

    interfaceOrientation = 3;
    // Get the screen dimensions based on the orientation
    CGRect screenRect = [UIScreen mainScreen].bounds;
    if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
        CGRect temp = CGRectZero;
        temp.size.width = screenRect.size.height;
        temp.size.height = screenRect.size.width;
        screenRect = temp;
    }

    CGFloat screenWidth = CGRectGetWidth(screenRect);
    CGFloat screenHeight = CGRectGetHeight(screenRect);

    // Create a sample overlay view centered the fullscreen view
    CGFloat overlayHeight = 100;
    CGFloat overlayWidth = 200;
    CGRect overlayRect = CGRectMake(screenWidth/2.0 - overlayWidth/2.0,
                                    screenHeight/2.0 - overlayHeight/2.0,
                                    overlayWidth, overlayHeight);

    UIView *fullscreenOverlay = [[UIView alloc] initWithFrame:overlayRect];
    fullscreenOverlay.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0];

    // Rotation and position the view based on the orientation
    // If your overlay isn't centered you'll need to adjust the CGAffineTransformTranslate
    // transform for PortraitUpsideDown, LandscapeLeft and LandscapeRight orientations.
    CGAffineTransform transform = CGAffineTransformIdentity;

    switch (interfaceOrientation) {
        case UIInterfaceOrientationPortrait:
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
        case UIInterfaceOrientationLandscapeLeft:
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            transform = CGAffineTransformTranslate(transform,
                                                   -(screenWidth/2.0 - screenHeight/2.0),
                                                   -(screenWidth/2.0 - screenHeight/2.0));
            break;
        case UIInterfaceOrientationLandscapeRight:
            transform = CGAffineTransformRotate(transform, M_PI_2);
            transform = CGAffineTransformTranslate(transform,
                                                   screenWidth/2.0 - screenHeight/2.0,
                                                   screenWidth/2.0 - screenHeight/2.0);
            break;
        default:
            break;
    }

    [fullscreenOverlay setTransform:transform];

    [self.appWindow addSubview:fullscreenOverlay];
    self.fullscreenOverlay = fullscreenOverlay;
}
/* 增加全屏根据当前方向 */
- (void)addFullscreenOverlayForCurrentInterfaceOrientation
{
    [self addFullscreenOverlayForInterfaceOrientation:self.interfaceOrientation];
}

- (void)layoutFullscreenOverlay
{
    UIInterfaceOrientation statusBarOrientation = [[UIApplication sharedApplication] statusBarOrientation];

    // If there's an overlay view, so remove it and add it in the new orientation
    if (self.fullscreenOverlay) {
        [self.fullscreenOverlay removeFromSuperview];
        self.fullscreenOverlay = nil;

        [self addFullscreenOverlayForInterfaceOrientation:statusBarOrientation];
    }
}
#pragma mark - UIDeviceOrientationDidChangeNotification Handler

- (void)layoutFullscreenOverlayForOrientation:(UIDeviceOrientation)orientation
{
    // This needs to be called in the next run loop for it to work properly in iOS 6+
    [self performSelector:@selector(layoutFullscreenOverlay) withObject:nil afterDelay:0];
}

#pragma mark - Remove Movie Player

- (void)removeMovieNotificationHandlers
{
    MPMoviePlayerController *player = self.moviePlayer;

    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerDidEnterFullscreenNotification object:player];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerWillExitFullscreenNotification object:player];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:player];
}

- (void)deleteMoviePlayerAndNotificationObservers
{
    [self removeMovieNotificationHandlers];

    // Need to nil the contentURL in iOS 5 otherwise the AVPlayerItem, AVURLAsset,
    // and other related objects don't get released.
    self.moviePlayer.contentURL = nil;
    self.moviePlayer = nil;
}

- (void)removeMovieViewFromViewHierarchy
{
    MPMoviePlayerController *player = self.moviePlayer;
    [player.view removeFromSuperview];
}

- (void)loadView
{
    [super loadView];
}
- (void)viewDidUnload
{
    self.moviePlayer = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"商品详情";

    self.view.backgroundColor = CLEAR;

    /* creat left right bar button */
    [self addBar];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = NO;
    self.tableView.frame = CGRectMake(0, 0.0, ScreenWidth, ScreenHeight - BARHeight );
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });


    /* request data */
    [self fetchData];

    /* comment textview */
    [self creatTextView];

    /* 返回按钮 */
    [self creatReturnButton];

}

-(void)creatReturnButton {

    UIImage *return_image = [UIImage imageNamed:@"return"];

    if (!itemDetail_returnButton) {
        itemDetail_returnButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    itemDetail_returnButton.frame  = CGRectMake(15.0, 25.0, return_image.size.width, return_image.size.height);
    [itemDetail_returnButton setImage:return_image forState:UIControlStateNormal];
    [[iEverAppDelegate shareAppDelegate].tabBarController.view addSubview:itemDetail_returnButton];
    [[iEverAppDelegate shareAppDelegate].tabBarController.view bringSubviewToFront:itemDetail_returnButton];
    itemDetail_returnButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        [self.navigationController popViewControllerAnimated:YES];

        return [RACSignal empty];
    }];
    
}

/* 导航条处三个按钮 */
-(void)navigationButton{


    /* 喜欢按钮 */

    UIImage *main_list_liked = [UIImage imageNamed:@"main_list_liked"];
    UIImage *main_list_like = [UIImage imageNamed:@"main_list_like"];

    if (!itemDetail_likeButton) {
        itemDetail_likeButton = [MCFireworksButton buttonWithType:UIButtonTypeCustom];
    }

    itemDetail_likeButton.particleImage          = [UIImage imageNamed:@"Sparkle"];
    itemDetail_likeButton.particleScale          = 0.05;
    itemDetail_likeButton.particleScaleRange     = 0.02;
    itemDetail_likeButton.hidden = YES;
    itemDetail_likeButton.frame  = CGRectMake(103.0, 25.0, main_list_liked.size.width, main_list_liked.size.height);
    [[iEverAppDelegate shareAppDelegate].tabBarController.view addSubview:itemDetail_likeButton];
    [[iEverAppDelegate shareAppDelegate].tabBarController.view bringSubviewToFront:itemDetail_likeButton];
    if (_datailOblect._itemCover._selected) {
        [itemDetail_likeButton setImage:main_list_liked forState:UIControlStateNormal];
    }else{
        [itemDetail_likeButton setImage:main_list_like forState:UIControlStateNormal];
    }
    iEver_likeActionObject * content                = [[iEver_likeActionObject alloc] init];
    itemDetail_likeButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(MCFireworksButton *input) {

        /* like modthod */
        if (!_datailOblect._itemCover._selected) {

            NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_datailOblect._itemCover.Cover_id],
                                  @"type": [NSNumber numberWithInt:20],
                                  };

            [[[content like:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
             subscribeNext:^(NSDictionary *object) {

                 if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                     _datailOblect._itemCover._selected = !_datailOblect._itemCover._selected;
                     _datailOblect._itemCover.likeTotal = _datailOblect._itemCover.likeTotal +1;
                     [input popOutsideWithDuration:0.5];
                     [input setImage:main_list_liked forState:UIControlStateNormal];
                     [likeButton setImage:main_list_liked forState:UIControlStateNormal];
                     likeLabel.text = [NSString stringWithFormat:@"%d",_datailOblect._itemCover.likeTotal];
                     [input animate];

                 }else{
                     NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                     [SVProgressHUD showImage:Nil status:codeStr];
                 }
             }];
        }
        else {

            NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_datailOblect._itemCover.Cover_id],
                                  @"type": [NSNumber numberWithInt:20],
                                  };

            [[[content unlike:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
             subscribeNext:^(NSDictionary *object) {

                 if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                     _datailOblect._itemCover._selected = !_datailOblect._itemCover._selected;
                     _datailOblect._itemCover.likeTotal = _datailOblect._itemCover.likeTotal -1;
                     [input popInsideWithDuration:0.4];
                     [input setImage:main_list_like forState:UIControlStateNormal];
                     [likeButton setImage:main_list_like forState:UIControlStateNormal];
                     likeLabel.text = [NSString stringWithFormat:@"%d",_datailOblect._itemCover.likeTotal];
                 }else{
                     NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                     [SVProgressHUD showImage:Nil status:codeStr];
                 }
             }];
        }
        
        
        return [RACSignal empty];
    }];



    /* 分享按钮 */

    UIImage *main_list_share = [UIImage imageNamed:@"main_list_share"];

    if (!itemDetail_shareButton) {
        itemDetail_shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    itemDetail_shareButton.hidden = YES;
    itemDetail_shareButton.frame  = CGRectMake(180, 25.0, main_list_share.size.width, main_list_share.size.height);
    [itemDetail_shareButton setImage:main_list_share forState:UIControlStateNormal];
    [[iEverAppDelegate shareAppDelegate].tabBarController.view addSubview:itemDetail_shareButton];
    [[iEverAppDelegate shareAppDelegate].tabBarController.view bringSubviewToFront:itemDetail_shareButton];
    itemDetail_shareButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        NSString *shareUrl = _datailOblect._itemCover.webUrl;
        NSString *title = _datailOblect._itemCover.itemName;
        UIImage *shareImage = coverImageview.image;

        //设置分享消息类型
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;

        //微信好友
        [UMSocialData defaultData].extConfig.wechatSessionData.url = shareUrl;
        [UMSocialData defaultData].extConfig.wechatSessionData.title = title;

        //微信朋友圈
        [UMSocialData defaultData].extConfig.wechatTimelineData.url = shareUrl;
        [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;

        //Qzone QQ空间分享只支持图文分享（图片文字缺一不可）
        [UMSocialData defaultData].extConfig.qzoneData.url = shareUrl;
        [UMSocialData defaultData].extConfig.qzoneData.title = title;


        NSString *shareText = [NSString stringWithFormat:@"美课美妆给予你们：%@, 详情请见：%@",  title, shareUrl];            //分享内嵌文字

        //分享到新浪微博内容
        [UMSocialData defaultData].extConfig.sinaData.shareText = shareText;


        //如果得到分享完成回调，需要设置delegate为self
        [UMSocialSnsService presentSnsIconSheetView:self appKey:UMAPPKEY shareText:shareText shareImage:shareImage shareToSnsNames:@[UMShareToWechatSession, UMShareToWechatTimeline,UMShareToSina, UMShareToTencent ] delegate:nil];
        
        return [RACSignal empty];
    }];
    
    
    
}


// 根据键盘状态，调整_mainView的位置
- (void) changeContentViewPoint:(NSNotification *)notification{

    NSDictionary *userInfo  = [notification userInfo];
    NSValue *value          = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyBoardEndY    = value.CGRectValue.origin.y;  // 得到键盘弹出后的键盘视图所在y坐标
    CGFloat kStateBarHeight = 0.0;

    NSNumber *duration      = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve         = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];

    // 添加移动动画，使视图跟随键盘移动
    [UIView animateWithDuration:duration.doubleValue animations:^{
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationCurve:[curve intValue]];
        self.mainView.center = CGPointMake(self.mainView.center.x, keyBoardEndY - kStateBarHeight - self.mainView.bounds.size.height/2.0 );   // keyBoardEndY的坐标包括了状态栏的高度，要减去
    }];

}

-(void)changeKeyboardWillHide:(NSNotification *)notification{

    self.bottomButton.hidden     = YES;
    [self.myTextView resignFirstResponder];
    self.mainView.frame          = CGRectMake(0, ScreenHeight, 320, 145.0);
    
}

-(void)insertCommentToReplyComment {

    [self.view bringSubviewToFront:self.mainView];
    self.bottomButton.hidden    = NO;
    self.commentLab.text        = [NSString stringWithFormat:@"回复 %@",self.toReplyCommentName];
    [self.myTextView becomeFirstResponder];
    
}

-(void)creatTextView{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentViewPoint:) name:UIKeyboardWillShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    /* mainView 灰黑 BG*/
    self.bottomButton             = [UIButton buttonWithType:UIButtonTypeCustom];
    self.bottomButton.frame       = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    self.bottomButton.hidden      = YES;
    self.bottomButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        self.bottomButton.hidden  = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame       = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    self.bottomButton.backgroundColor = BLACK;
    self.bottomButton.alpha           = 0.6;
    [self.view addSubview:self.bottomButton];

    /* mainView */
    self.mainView                     = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight, 320, 145.0)];
    self.mainView.backgroundColor     = BackkGroundColor;
    [self.view addSubview:self.mainView];

    /* textViewImage */
    UIImageView *textViewImage       = [[UIImageView alloc] initWithFrame:CGRectMake(15, 50, 290, 80)];
    textViewImage.image              = [UIImage imageNamed:@"commentContentBG"];
    [self.mainView addSubview:textViewImage];

    /* myTextView */
    self.myTextView                  = [[UITextView alloc]initWithFrame:CGRectMake(15, 50, 290, 80)];
    self.myTextView.font             = TextFont;
    self.myTextView.backgroundColor  = CLEAR;
    [self.mainView addSubview:self.myTextView];

    /* cancelBut */
    UIButton *cancelBut              = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBut.frame                  = CGRectMake(10, 10, 30, 30);
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateHighlighted];
    cancelBut.rac_command            = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        self.bottomButton.hidden     = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame          = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    [self.mainView addSubview:cancelBut];

    /* commentLab */
    self.commentLab                      = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, 240, 25)];
    self.commentLab.text                 = @"评 论";
    self.commentLab.font                 = TextFont;
    self.commentLab.textAlignment        = NSTextAlignmentCenter;
    [self.mainView addSubview:self.commentLab];

    /* sendBut */
    UIButton *sendBut               = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBut.frame                   = CGRectMake(280, 10, 30, 30);
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateHighlighted];
    sendBut.rac_command             = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        /* insert Comment */
        [self insertCommentRequest];
        return [RACSignal empty];
    }];
    [self.mainView addSubview:sendBut];



     /* --- bottomView commentBut ------ */
    UIImage    * detail_bottomView = [UIImage imageNamed:@"detail_bottomView"];
    UIView *commentView            = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight - BARHeight , detail_bottomView.size.width, detail_bottomView.size.height)];
    commentView.backgroundColor    = WHITE;
    [self.view addSubview:commentView];


    UIImageView *imageview         = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, detail_bottomView.size.width, detail_bottomView.size.height)];
    imageview.image                = detail_bottomView;
    [commentView addSubview:imageview];

    UIImage *detail_comment         = [UIImage imageNamed:@"detail_comment"];
    UIButton *commentBut            = [UIButton buttonWithType:UIButtonTypeCustom];
    commentBut.backgroundColor      = CLEAR;
    [commentBut setBackgroundImage:detail_comment forState:UIControlStateNormal];
    [commentBut setBackgroundImage:detail_comment forState:UIControlStateHighlighted];
    commentBut.frame = CGRectMake(ScreenWidth/2 - detail_comment.size.width/2, detail_bottomView.size.height/2 - detail_comment.size.height/2, detail_comment.size.width, detail_comment.size.height);
    commentBut.rac_command          = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        [self.view bringSubviewToFront:self.mainView];
        self.bottomButton.hidden    = NO;
        [self.myTextView becomeFirstResponder];
        return [RACSignal empty];
    }];
    [commentView addSubview:commentBut];

}
/* insert Comment */
-(void)insertCommentRequest{

     NSString *temp = [self.myTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([temp isEqualToString:@""]) {
        [SVProgressHUD showImage:Nil status:@"请输评论内容"];
        return ;
    }

    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_commentObject * content = [[iEver_commentObject alloc] init];

    NSDictionary *dic;

    /* 评论商品详情 */
    if (self.comment_parentId == 0) {

        dic = @{@"itemId": [NSNumber numberWithInt:_type_id],
                @"commentContent": self.myTextView.text,
                @"type": [NSNumber numberWithInt:10],
                };
    }
    /* 回复评论 */
    else {

        dic = @{@"itemId": [NSNumber numberWithInt:_type_id],
                @"parentId": [NSNumber numberWithInt:_comment_parentId],
                @"atUserId": [NSNumber numberWithInt:_comment_atUserId],
                @"commentContent": self.myTextView.text,
                @"type": [NSNumber numberWithInt:11],
                };
    }

    NSLog(@"dic------->%@",dic);
    
    [[[content insertItemComment:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"提交成功"];
             [self fetchData];

         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

     }];
    self.bottomButton.hidden = YES;
    [self.myTextView resignFirstResponder];
    self.commentLab.text = @"评 论";
    self.comment_parentId = 0;
    self.myTextView.text     = @"";
    self.mainView.frame      = CGRectMake(0, ScreenHeight, 320, 145.0);
}

#pragma mark - IEContentViewDelegate *******************播放视频
- (void)videoPlaySelectedWithUrl:(NSString *)url
{
    self.moviePlayer.view.hidden = NO;
    [self.moviePlayer play];
}
/* creat left right bar button */
-(void)addBar{

    __weak iEver_ItemDetailViewController *weakSelf = self;
    [self addLeftBarButtonItem:^(UIButton *sender) {
        [weakSelf.moviePlayer stop];
        [weakSelf.moviePlayer.view removeFromSuperview];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/* creat CoverView As TableViweHeader */
-(void)creatCoverViewAsTableViweHeader{

    /* custom tableView headerView */
    CGFloat gradeDesc_height = 0.0;

    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = WHITE;

    /* 封面图片view */

    UIView *coverView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, ScreenWidth, ScreenWidth)];
    coverView.backgroundColor = CLEAR;

    [headerView addSubview:coverView];

    /* 封面图片 */

    coverImageview = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, ScreenWidth, ScreenWidth)];
    coverImageview.userInteractionEnabled = YES;
    coverImageview.backgroundColor        = CLEAR;

    [coverImageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",_datailOblect._itemCover.itemImg]]
                   placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    [coverView addSubview:coverImageview];


    /* 封面图片-播放按钮 */

    UIImage * main_list_play_big = [UIImage imageNamed:@"main_list_play_big"];

    UIButton *startVideoButton   = [UIButton buttonWithType:UIButtonTypeCustom];
    startVideoButton.hidden      = YES;
    startVideoButton.frame       = CGRectMake(ScreenWidth/2 - main_list_play_big.size.width/2, ScreenWidth/2 - main_list_play_big.size.height/2, main_list_play_big.size.width, main_list_play_big.size.height);
    [startVideoButton setImage:main_list_play_big forState:UIControlStateNormal];
    [startVideoButton setImage:main_list_play_big forState:UIControlStateHighlighted];
    if (_datailOblect._itemCover.V_videoLink.length > 0) {

        coverImageview.hidden        = YES;
        startVideoButton.hidden      = NO;
    }else{
        startVideoButton.hidden      = YES;
        self.moviePlayer.view.hidden = YES;
        coverImageview.hidden        = NO;
    }
    startVideoButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        /* startVideo modthod */

        if (_datailOblect._itemCover.V_videoLink.length >0){
            [self videoPlaySelectedWithUrl:_datailOblect._itemCover.V_videoLink];
        }
        return [RACSignal empty];
    }];

    [coverImageview addSubview:startVideoButton];

    /* 解析不出来用web */

    if (_datailOblect._itemCover.videoLink.length > 0 && _datailOblect._itemCover.V_videoLink.length == 0) {

        webView               = [[UIWebView alloc] initWithFrame:CGRectMake(0.0, 0.0, ScreenWidth, ScreenWidth)];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_datailOblect._itemCover.videoLink]];
        [webView loadRequest:request];

        [coverView addSubview:webView];
    }

    /* 浏览标示图片 */
    UIImage *main_list_video = [UIImage imageNamed:@"main_list_video"];
    UIImage *main_list_number = [UIImage imageNamed:@"main_list_number"];
    UIImageView *browseImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, ScreenWidth - 20, 17.0, 11.0)];
    if (_datailOblect._itemCover.V_videoLink.length > 0) {
        browseImageView.image = main_list_video;

    }else {
        browseImageView.image = main_list_number;
    }
    [coverImageview addSubview:browseImageView];

    /*浏览量*/
    UILabel *browseLabel = [[UILabel alloc] initWithFrame:CGRectMake(32, 315 - 20, 295, 20)];
    browseLabel.backgroundColor  = CLEAR;
    browseLabel.textColor        = WHITE;
    browseLabel.textAlignment    = NSTextAlignmentLeft;
    browseLabel.shadowColor      = GRAY_LIGHT;
    browseLabel.font             = TextDESCFonts;
    browseLabel.text             = [NSString stringWithFormat:@"%d",_datailOblect._itemCover.pvTotal + _datailOblect._itemCover.webPvTotal];

    [coverImageview addSubview:browseLabel];

    /* 放置标题视图 */

    UIView *titleView         = [[UIView alloc] init];
    titleView.backgroundColor = CLEAR;

    [headerView addSubview:titleView];


    /* title */

    UILabel *titleLabel          = [[UILabel alloc] initWithFrame:CGRectMake(25.0, 20, ScreenWidth - 25.0 * 2, 25.0)];
    titleLabel.font              = Title_BigFont;
    titleLabel.textColor         = BLACK;
    titleLabel.backgroundColor   = CLEAR;
    titleLabel.lineBreakMode     = NSLineBreakByTruncatingTail;
    titleLabel.numberOfLines     = 0;
    titleLabel.text              = _datailOblect._itemCover.itemName;
    NSDictionary * titledic      = [NSDictionary dictionaryWithObjectsAndKeys:titleLabel.font, NSFontAttributeName,nil];
    CGSize title_Size =[_datailOblect._itemCover.itemName boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:titledic context:nil].size;

    if (title_Size.width > 270) {
        titleLabel.textAlignment     = NSTextAlignmentCenter;
    }else{
        titleLabel.textAlignment     = NSTextAlignmentLeft;
    }
    titleLabel.frame = CGRectMake(25.0, 20, 270.0, title_Size.height);

    [titleView addSubview:titleLabel];


    /* tag */

    iEver_Item_TagList *tagsView              = [[iEver_Item_TagList alloc] init];
    NSMutableArray *mutableArray_ID = [[NSMutableArray alloc] init];
    for (iEver_item_detail_tagsList_object *tagObject in  _datailOblect._itemCover._tagsList) {
        [mutableArray_ID addObject:[NSNumber numberWithInt:tagObject.tagId]];
    }
    tagsView.IDArray                = mutableArray_ID;
    tagsView.frame                  = CGRectMake(25.0, titleLabel.frame.origin.y + title_Size.height + 10, ScreenWidth - 25.0 * 2, 18.0);
    NSMutableArray *mutableArray     = [[NSMutableArray alloc] init];
    for (iEver_item_detail_tagsList_object *tagObject in  _datailOblect._itemCover._tagsList) {
        [mutableArray addObject:[NSString stringWithFormat:@"#%@#",tagObject.tagName]];
    }
    [tagsView setTags:mutableArray];

    [titleView addSubview:tagsView];

    titleView.frame = CGRectMake(0.0, ScreenWidth, ScreenWidth, tagsView.frame.origin.y + tagsView.frame.size.height + 10.0);


    /* 放置按钮视图 */

    buttonView         = [[UIView alloc] initWithFrame:CGRectMake(0.0 , ScreenWidth + titleView.frame.size.height, ScreenWidth , 85.0)];

    buttonView.backgroundColor = CLEAR;

    [headerView addSubview:buttonView];
    

    /* 喜欢按钮 */

    UIImage *main_list_liked = [UIImage imageNamed:@"main_list_liked"];
    UIImage *main_list_like = [UIImage imageNamed:@"main_list_like"];

    likeButton                     = [MCFireworksButton buttonWithType:UIButtonTypeCustom];
    likeButton.particleImage       = [UIImage imageNamed:@"Sparkle"];
    likeButton.particleScale       = 0.05;
    likeButton.particleScaleRange  = 0.02;
    likeButton.frame = CGRectMake(103.0, 10.0, main_list_like.size.width, main_list_like.size.height);
    if (_datailOblect._itemCover._selected) {
        [likeButton setImage:main_list_liked forState:UIControlStateNormal];
    }else{
        [likeButton setImage:main_list_like forState:UIControlStateNormal];
    }

    iEver_likeActionObject * content = [[iEver_likeActionObject alloc] init];
    likeButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(MCFireworksButton *input) {

        /* like modthod */
        if (!_datailOblect._itemCover._selected) {

            NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_datailOblect._itemCover.Cover_id],
                                  @"type": [NSNumber numberWithInt:20],
                                  };

            [[[content like:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
             subscribeNext:^(NSDictionary *object) {

                 if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                     _datailOblect._itemCover._selected = !_datailOblect._itemCover._selected;
                     _datailOblect._itemCover.likeTotal = _datailOblect._itemCover.likeTotal +1;
                     [input popOutsideWithDuration:0.5];
                     [input setImage:main_list_liked forState:UIControlStateNormal];
                     [itemDetail_likeButton setImage:main_list_liked forState:UIControlStateNormal];
                     likeLabel.text = [NSString stringWithFormat:@"%d喜欢",_datailOblect._itemCover.likeTotal];
                     [input animate];

                 }else{
                     NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                     [SVProgressHUD showImage:Nil status:codeStr];
                 }
             }];
        }
        else {

            NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_datailOblect._itemCover.Cover_id],
                                  @"type": [NSNumber numberWithInt:20],
                                  };

            [[[content unlike:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
             subscribeNext:^(NSDictionary *object) {

                 if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                     _datailOblect._itemCover._selected = !_datailOblect._itemCover._selected;
                     _datailOblect._itemCover.likeTotal = _datailOblect._itemCover.likeTotal -1;
                     [input popInsideWithDuration:0.4];
                     [input setImage:main_list_like forState:UIControlStateNormal];
                     [itemDetail_likeButton setImage:main_list_like forState:UIControlStateNormal];
                     likeLabel.text = [NSString stringWithFormat:@"%d喜欢",_datailOblect._itemCover.likeTotal];
                 }else{
                     NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                     [SVProgressHUD showImage:Nil status:codeStr];
                 }
             }];
        }
        
        
        return [RACSignal empty];
    }];
    
    [buttonView addSubview:likeButton];


    /* 喜欢次数 */

    likeLabel       = [[UILabel alloc] init];
    likeLabel.frame = CGRectMake(90, 54, 70, 20);
    if (_datailOblect._itemCover.likeTotal>9999) {
        likeLabel.text         = [NSString stringWithFormat:@"%.1lf万",_datailOblect._itemCover.likeTotal/10000.0];
    }else{
        likeLabel.text         = [NSString stringWithFormat:@"%d喜欢",_datailOblect._itemCover.likeTotal];
    }
    likeLabel.backgroundColor  = CLEAR;
    likeLabel.textColor        = DAKEBLACK;
    likeLabel.textAlignment = NSTextAlignmentCenter;
    likeLabel.font             = TextFonts;

    [buttonView addSubview:likeLabel];


    /* 分享按钮 */

    UIImage *main_list_share = [UIImage imageNamed:@"main_list_share"];

    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.frame = CGRectMake(180, 10.0, main_list_share.size.width, main_list_share.size.height);
    [shareButton setImage:main_list_share forState:UIControlStateNormal];
    [shareButton setImage:main_list_share forState:UIControlStateHighlighted];
    shareButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        /* share modthod */

        NSString *shareUrl = _datailOblect._itemCover.webUrl;
        NSString *title = _datailOblect._itemCover.itemName;
        UIImage *shareImage = coverImageview.image;

        //设置分享消息类型
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;

        //微信好友
        [UMSocialData defaultData].extConfig.wechatSessionData.url = shareUrl;
        [UMSocialData defaultData].extConfig.wechatSessionData.title = title;

        //微信朋友圈
        [UMSocialData defaultData].extConfig.wechatTimelineData.url = shareUrl;
        [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;

        //Qzone QQ空间分享只支持图文分享（图片文字缺一不可）
        [UMSocialData defaultData].extConfig.qzoneData.url = shareUrl;
        [UMSocialData defaultData].extConfig.qzoneData.title = title;


        NSString *shareText = [NSString stringWithFormat:@"美课美妆给予你们：%@, 详情请见：%@",  title, shareUrl];            //分享内嵌文字

        //分享到新浪微博内容
        [UMSocialData defaultData].extConfig.sinaData.shareText = shareText;


        //如果得到分享完成回调，需要设置delegate为self
        [UMSocialSnsService presentSnsIconSheetView:self appKey:UMAPPKEY shareText:shareText shareImage:shareImage shareToSnsNames:@[UMShareToWechatSession, UMShareToWechatTimeline,UMShareToSina, UMShareToTencent ] delegate:nil];


        return [RACSignal empty];
    }];

    [buttonView addSubview:shareButton];


    /* 分享次数 */


    UILabel *shareLable       = [[UILabel alloc] init];
    shareLable.frame = CGRectMake(165, 54, 70, 20);
    shareLable.backgroundColor  = CLEAR;
    shareLable.textColor        = DAKEBLACK;
    shareLable.text             = [NSString stringWithFormat:@"%d分享",_datailOblect._itemCover.webPvTotal];
    shareLable.textAlignment = NSTextAlignmentCenter;
    shareLable.font             = TextFonts;

    [buttonView addSubview:shareLable];

    /* 描述视图 */

    UIView *descView         = [[UIView alloc] init];
    titleView.backgroundColor = CLEAR;

    [headerView addSubview:descView];

    /* 描述视图_描述 */

    UILabel *descLabel         = [[UILabel alloc] init];
    descLabel.font             = TextFont;
    descLabel.backgroundColor  = CLEAR;
    descLabel.lineBreakMode    = NSLineBreakByTruncatingTail;
    descLabel.numberOfLines    = 0;
    descLabel.textColor        = BLACK;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",_datailOblect._itemCover.itemDesc]];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:3.0];//调整行间距
    [paragraphStyle setFirstLineHeadIndent : descLabel . frame . size . width]; //首行缩进 根据用户昵称宽度在加5个像素
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_datailOblect._itemCover.itemDesc length])];
    descLabel.attributedText = attributedString;
    [descLabel sizeToFit];
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:descLabel.font, NSFontAttributeName,nil];
    CGSize contentSize =[descLabel.text boundingRectWithSize:CGSizeMake(270.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    descLabel.frame = CGRectMake(25.0, 0, contentSize.width, (contentSize.height + ([_datailOblect._itemCover.itemDesc length]+ 20)/19*3.0));

    [descView addSubview:descLabel];

    descView.frame = CGRectMake(0.0,  buttonView.frame.origin.y + buttonView.frame.size.height, ScreenWidth, (contentSize.height + ([_datailOblect._itemCover.itemDesc length]+ 20)/ 19 * 3.0));


    UIView *gradeDescView = [[UIView alloc] init];
    gradeDescView.backgroundColor = CLEAR;
    [headerView addSubview:gradeDescView];

    /* 如果有综合评分 */
    if (_datailOblect._itemCover.startGrade >0) {


        /* 综合评分View_分割线 */
        UIImageView *imageView_line    = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, ScreenWidth - 5*2, 1)];
        imageView_line.backgroundColor = CLEAR;
        imageView_line.image           = [UIImage imageNamed:@"person_line"];
        [gradeDescView addSubview:imageView_line];

        /* 综合评分View_测试图片 */
        UIImage *item_detail_test = [UIImage imageNamed:@"item_detail_test"];
        UIImageView *imageView_itemTest    = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, item_detail_test.size.width, item_detail_test.size.height)];
        imageView_itemTest.backgroundColor = CLEAR;
        imageView_itemTest.image           = item_detail_test;
        [gradeDescView addSubview:imageView_itemTest];

        /* 测试图片_评测分类排名 */
        UILabel *itemRank_lable         = [[UILabel alloc] init];
        itemRank_lable.frame            = CGRectMake(1, 30, 75, 20);
        itemRank_lable.textAlignment    = NSTextAlignmentCenter;
        itemRank_lable.backgroundColor  = CLEAR;
        itemRank_lable.textColor        = BLACK;
        itemRank_lable.font             = TextFont;
        itemRank_lable.text             = [NSString stringWithFormat:@"%d",_datailOblect._itemTop.sort_level];
        [imageView_itemTest addSubview:itemRank_lable];


        /* 测试图片_评测分类名称 */
        UILabel *itemCategory_lable         = [[UILabel alloc] init];
        itemCategory_lable.frame            = CGRectMake(0.0, 45.0, 75.0, 20.0);
        itemCategory_lable.textAlignment    = NSTextAlignmentCenter;
        itemCategory_lable.backgroundColor  = CLEAR;
        itemCategory_lable.textColor        = BLACK;
        itemCategory_lable.font             = TextDESCFonts;
        itemCategory_lable.text             = _datailOblect._itemTop.category_name;
        [imageView_itemTest addSubview:itemCategory_lable];

        /* 评测综合评价文案 */
        UILabel *testStr_lable         = [[UILabel alloc] init];
        testStr_lable.frame            = CGRectMake(90.0, 20, 75.0, 20.0);
        testStr_lable.textAlignment    = NSTextAlignmentCenter;
        testStr_lable.backgroundColor  = CLEAR;
        testStr_lable.textColor        = LIST_TITLE;
        testStr_lable.font             = TitleFont;
        testStr_lable.text             = @"综合评价";
        [gradeDescView addSubview:testStr_lable];


        /* 评测综合评价-- 分数展示 */
        CWStarRateView *starView_test = [[CWStarRateView alloc] initWithFrame:CGRectMake(168.0, 23.0, 80, 15) numberOfStars:5];
        starView_test.scorePercent    = (_datailOblect._itemCover.startGrade * 2)/10.f;
        [gradeDescView addSubview:starView_test];


        /* 评测综合评价-- 分数 */
        UILabel *startGrade           = [[UILabel alloc] initWithFrame:CGRectMake(255.0, 20.0, 40.0, 20.0)];
        startGrade.numberOfLines      = 1;
        startGrade.backgroundColor    = CLEAR;
        startGrade.textAlignment      = NSTextAlignmentLeft;
        startGrade.textColor          = DAKEBLACK;
        startGrade.font               = TextFont;
        startGrade.text               = [NSString stringWithFormat:@"%.1f",_datailOblect._itemCover.startGrade];
        [gradeDescView addSubview:startGrade];


        /* 评测综合评价-- 描述 */
        UILabel *testItemDesc_lable = [[UILabel alloc] init];
        testItemDesc_lable.font             = TextFont;
        testItemDesc_lable.backgroundColor  = CLEAR;
        testItemDesc_lable.lineBreakMode    = NSLineBreakByTruncatingTail;
        testItemDesc_lable.numberOfLines    = 0;
        testItemDesc_lable.textColor        = BLACK;
        NSDictionary * testItemDesc_dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize testItemDesc_Size =[_datailOblect._itemCover.gradeDesc boundingRectWithSize:CGSizeMake(220, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:testItemDesc_dic context:nil].size;
        gradeDesc_height = testItemDesc_Size.height;
        testItemDesc_lable.frame = CGRectMake(90.0, 50.0, 220, testItemDesc_Size.height);
        testItemDesc_lable.text = _datailOblect._itemCover.gradeDesc;
        [gradeDescView addSubview:testItemDesc_lable];


    }else {

    }

    /* 综合评分View */

    if (_datailOblect._itemCover.startGrade >0) {
        if (gradeDesc_height > 38.0) {
            gradeDescView.frame = CGRectMake(0, descView.frame.origin.y + descView.frame.size.height + 20, ScreenWidth, 38.0 + gradeDesc_height +10);
            gradeDescView.hidden = NO;
            headerView.frame = CGRectMake(0.0,  0.0, ScreenWidth, gradeDescView.frame.origin.y + gradeDescView.frame.size.height + 20);
        }else {

            gradeDescView.frame = CGRectMake(0, descView.frame.origin.y + descView.frame.size.height + 20, ScreenWidth,  86.0);
            gradeDescView.hidden = NO;
            headerView.frame = CGRectMake(0.0,  0.0, ScreenWidth, gradeDescView.frame.origin.y + gradeDescView.frame.size.height + 20);
        }
    }else{

        gradeDescView.hidden = YES;
        headerView.frame = CGRectMake(0.0,  0.0, ScreenWidth, descView.frame.origin.y + descView.frame.size.height + 20);

    }
    self.tableView.tableHeaderView = headerView;

    /* creat return like share button  */
    [self navigationButton];
    

}

/*request data*/
-(void)fetchData{

    __block iEver_ItemDetailViewController *weakSelf = self;
    [GiFHUD showWithOverlay];
    iEver_itemDetail_Object *detail_Object = [[iEver_itemDetail_Object alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"/item/queryItemById/%d",_type_id];
    [[[detail_Object acquireDetailData:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_itemDetail_Object *object) {



         if (object.resultCode == 1) {

             _datailOblect = object;
             [weakSelf.tableView reloadData];

             /* creat CoverView As TableViweHeader */
             [weakSelf creatCoverViewAsTableViweHeader];
             [weakSelf creatTextView];
         }
         else if (object.resultCode == -2003) {

             UIAlertView* upgradeAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示", nil)
                                                                    message:NSLocalizedString(@"数据已更新，请刷新列表查看最新", nil)
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"确定", nil)
                                                          otherButtonTitles:nil, nil];

             [upgradeAlert.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {
                 if ([x integerValue] == 0) {

                     [self.navigationController popViewControllerAnimated:YES];
                 }
             }];
             [upgradeAlert show];

         }else{

             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:object.resultCode];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

         [GiFHUD dismiss];
     }];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

	int numberSection = 0;
    if (_datailOblect._arrayCountnumber > 0) {
        numberSection = _datailOblect._arrayCountnumber;
    }
    return numberSection;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0) {
        return [_datailOblect._itemPicList count];
    }
    else if (section == 1) {
        return 1;
    }
    else if (section == 2) {
        return [_datailOblect._itemCommentList count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier;
    UITableViewCell *cell = nil;
    NSInteger section = indexPath.section;
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    switch (section) {
        case 0:
        {
            CellIdentifier = @"ItemDetailPicListCell";
            cell = (iEver_ItemDetail_PicList_TestCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
            break;
        case 1:
        {
            CellIdentifier = @"ItemDetailRecommendItemListCell";
            cell = (iEver_itemDetail_recommendItem_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
            break;
        case 2:
        {
            CellIdentifier = @"ItemDetailCommentItemListCell";
            cell = (iEver_itemDetail_comment_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
            break;

        default:
            break;
    }
    // Configure the cell...
	if (!cell) {

        switch (section) {
            case 0:
            {
                cell = [[iEver_ItemDetail_PicList_TestCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
                break;
            case 1:
            {
                cell = [[iEver_itemDetail_recommendItem_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
                break;
            case 2:
            {
                cell = [[iEver_itemDetail_comment_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
                break;

            default:
                break;
        }
    }

    switch (section) {
        case 0:
        {
            iEver_ItemDetail_PicList_TestCell *pic_cell = (iEver_ItemDetail_PicList_TestCell *)cell;
            iEver_detail_itemPicList_object *object = [_datailOblect._itemPicList objectAtIndex:indexPath.row];
            [pic_cell setObject:object];
        }
            break;
        case 1:
        {
            iEver_itemDetail_recommendItem_cell *recommend_cell = (iEver_itemDetail_recommendItem_cell *)cell;
            iEver_itemDetail_Object *object = (iEver_itemDetail_Object *)_datailOblect;
            [recommend_cell setViewController:self];
            [recommend_cell setObject:object];
        }
            break;
        case 2:
        {
            iEver_itemDetail_comment_cell *item_cell = (iEver_itemDetail_comment_cell *)cell;
            iEver_detail_itemCommentList_object *object = [_datailOblect._itemCommentList objectAtIndex:indexPath.row];
            [item_cell setObject:object];
        }
            break;

        default:
            break;
    }

	return cell;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;

    switch (section) {
        case 0:{
            id object = [_datailOblect._itemPicList objectAtIndex:indexPath.row];
            return [iEver_ItemDetail_PicList_TestCell heightForObject:object atIndexPath:indexPath tableView:tableView];
        }break;
        case 1:{
             return [iEver_itemDetail_recommendItem_cell heightForObject:nil atIndexPath:indexPath tableView:tableView];
        }break;
        case 2:{
            id object = [_datailOblect._itemCommentList objectAtIndex:indexPath.row];
            return [iEver_itemDetail_comment_cell heightForObject:object atIndexPath:indexPath tableView:tableView];
        }break;

        default:
            break;
    }

    return 360.0;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    int sectionHeaderHeight = 0;
    switch (section) {
        case 0:{
            sectionHeaderHeight = .0;
        }break;
        case 1:{
            if ([_datailOblect._recommendItemList count] > 0) {
                sectionHeaderHeight = 50.0;
            }else{

                sectionHeaderHeight = .0;
            }
        }break;

        case 2:{
            if ([_datailOblect._itemCommentList count] > 0) {
                sectionHeaderHeight = 50.0;
            }else{
                sectionHeaderHeight = .0;
            }
        }break;

        default:
            break;
    }
    return sectionHeaderHeight;

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    switch (section) {
        case 0:
        {
        }
            break;
        case 1:
        {
            if ([_datailOblect._recommendItemList count] > 0) {

                UIImage *detail_recommendItem_icon = [UIImage imageNamed:@"detail_recommendItem_icon"];

                UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
                view.backgroundColor = WHITE;
                UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(13, view.frame.size.height/2 - detail_recommendItem_icon.size.height/2 + 5, detail_recommendItem_icon.size.width, detail_recommendItem_icon.size.height)];
                imageview.backgroundColor = CLEAR;
                imageview.image = detail_recommendItem_icon;
                UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(35, 20, 200, 20)];
                lable.numberOfLines = 1;
                lable.text = @"您可能还喜欢的产品";
                lable.backgroundColor = CLEAR;
                lable.textColor = BLACK;
                lable.font = TextFont;

                UIView *line_view = [[UIView alloc] initWithFrame:CGRectMake(15, 49, 290, 0.5)];
                line_view.backgroundColor = GRAY_LIGHT;
                [view addSubview:line_view];


                [view addSubview:lable];
                [view addSubview:imageview];
                return view;


            }else{
                return nil;
                
            }
        }
            break;
        case 2:
        {

            if ([_datailOblect._itemCommentList count] > 0) {

                UIImage * detail_comment_icon = [UIImage imageNamed:@"detail_comment_icon"];
                UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
                view.backgroundColor = WHITE;
                UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(13, view.frame.size.height/2 - detail_comment_icon.size.height/2 + 5, detail_comment_icon.size.width, detail_comment_icon.size.height)];
                imageview.backgroundColor = CLEAR;
                imageview.image = detail_comment_icon;

                NSDictionary* commemt_Style = @{@"gray": [UIColor blackColor],
                                                @"blue": [UIColor blueColor]};

                NSString *name1_strStyle = [NSString stringWithFormat:@"<gray>用户评论</gray><blue>(%d)</blue>",_datailOblect.itemCommentTotal];

                UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(35, 20, 100, 20)];
                lable.numberOfLines = 1;
                lable.attributedText = [name1_strStyle attributedStringWithStyleBook:commemt_Style];
                lable.backgroundColor = CLEAR;
                lable.font = TextFont;

                UIView *line_view = [[UIView alloc] initWithFrame:CGRectMake(15, 49, 290, 0.5)];
                line_view.backgroundColor = GRAY_LIGHT;
                [view addSubview:line_view];


                [view addSubview:lable];
                [view addSubview:imageview];

                if ([_datailOblect._itemCommentList count] > 9) {
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(250, 20, 50, 20);
                    [button setTitle:@"更多" forState:UIControlStateNormal];
                    button.titleLabel.font = TextFont;
                    [button setTitleColor:BLACK forState:UIControlStateNormal];
                    button.titleLabel.textAlignment = NSTextAlignmentRight;
                    button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
                        /* apply modthod */
                        iEver_itemCommentViewController *ViewController = [[iEver_itemCommentViewController alloc] init];
                        ViewController.commentItem_id = _datailOblect._itemCover.Cover_id;
                        [self.navigationController pushViewController:ViewController animated:YES];
                        IOS7POP;
                        return [RACSignal empty];
                    }];

                    [view addSubview:button];
                }

                return view;


            }else{
                return nil;

            }
        }
            break;


        default:
            break;
    }
    return nil;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

        return nil;

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    if (section == 0){

        return 0.0;
    }else{

        return 0.0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"didSelectRowAtIndexPath");
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    if (scrollView == self.tableView)

    {

        CGFloat sectionHeaderHeight = 50.0;

        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {

            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);

        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {

            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
            
        }  
        
    }

    if (scrollView.contentOffset.y >= 320) {
        [self.moviePlayer pause];
    }else{
        if(!self.moviePlayer.view.hidden){

            [self.moviePlayer play];
        }
    }

    if (scrollView.contentOffset.y >= 320 + 65.0) {
        buttonView.hidden = YES;
        itemDetail_likeButton.hidden = NO;
        itemDetail_shareButton.hidden = NO;
    }else{

        buttonView.hidden = NO;
        itemDetail_likeButton.hidden = YES;
        itemDetail_shareButton.hidden = YES;
    }
    
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{

    if (scrollView.contentOffset.y >= 320) {
        [self.moviePlayer pause];
    }else{
        if(!self.moviePlayer.view.hidden){

            [self.moviePlayer play];
        }
    }

    if (scrollView.contentOffset.y >= 320 + 65.0) {
        buttonView.hidden = YES;
        itemDetail_likeButton.hidden = NO;
        itemDetail_shareButton.hidden = NO;
    }else{

        buttonView.hidden = NO;
        itemDetail_likeButton.hidden = YES;
        itemDetail_shareButton.hidden = YES;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}
@end

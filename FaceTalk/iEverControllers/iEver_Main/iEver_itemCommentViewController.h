//
//  iEver_itemCommentViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/4/12.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"

@interface iEver_itemCommentViewController : iEverBaseTableViewController

@property (nonatomic, assign) int                        commentItem_id;       /* id */

-(void)insertCommentToReplyComment;                                     /* 点击评论的评论，进行评论 */
@property (nonatomic, copy  ) NSString       *toReplyCommentName;       /* 评论回复 用户名 */
@property (nonatomic, assign) int               comment_parentId;       /* 评论回复 的parent ID */
@property (nonatomic, assign) int               comment_atUserId;       /* 评论回复 的atUser ID */

@end

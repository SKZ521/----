//
//  iEver_OfficialListViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-18.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_OfficialListViewController.h"
#import "iEver_MessageViewController.h"
#import "iEver_ItemTryViewController.h"
#import "iEver_PersonalTagSettingViewController.h"
#import "iEver_OfficialDetailViewController.h"
#import "iEver_normalUserAnsweredQesViewController.h"
#import "UIViewController+ScrollingNavbar.h"
#import "UIViewController+CWStatusBarNotification.h"
#import "iEver_mainContent.h"
#import "iEver_mainOfficialCell.h"
#import "iEver_defineDicObject.h"
#import "ASFSharedViewTransition.h"
#import "iEver_MainiAdView.h"
#import "iEver_LoginViewController.h"
#import "SYAppStart.h"
#import "IntroControll.h"
#import "iEver_UserMessageObject.h"
#import "iEver_videoBanner.h"
#import "iEver_specialBanner.h"

#import "MFSideMenu.h"

#define IBannerBaseView 180.0f + 94.0f + 510.0


typedef NS_ENUM( NSUInteger, SSDataSourcesReloadType ) {
    SSDataSourcesReloadDefault = 0,
    SSDataSourcesReloadPull,
    SSDataSourcesReloadDrop,
};

typedef NS_ENUM(NSInteger, NJKScrollDirection) {
    NJKScrollDirectionNone,
    NJKScrollDirectionUp,
    NJKScrollDirectionDown,
};

NJKScrollDirection detectScrollDirection(currentOffsetY, previousOffsetY)
{
    return currentOffsetY > previousOffsetY ? NJKScrollDirectionUp   :
    currentOffsetY < previousOffsetY ? NJKScrollDirectionDown :
    NJKScrollDirectionNone;
}


@interface iEver_OfficialListViewController ()<ASFSharedViewTransitionDataSource>{

    float radius;
    float bubbleRadius;
    UIView *bottomView;
    iEver_mainContent *mainObject;

    iEver_mainContent *mainObject_secondCategoryList;

    UIButton *returnButton;

    BOOL _isFirstWillAppear;
    BOOL _isFirstDidAppear;

    UIView *secondCategoryView;
    
    iEver_MainiAdView *_MainiAdView;
    iEver_videoBanner *_videoBanner;
    iEver_specialBanner *_specialBanner;

    int customCount;
    
}
@property (nonatomic) NJKScrollDirection previousScrollDirection;
@property (nonatomic) CGFloat previousOffsetY;
@property (nonatomic) CGFloat accumulatedY;
@property (nonatomic) CGFloat upThresholdY; // up distance until fire. default 0 px.
@property (nonatomic) CGFloat downThresholdY; // down distance until fire. default 200 px.
@property (nonatomic, assign) CGFloat lastContentOffset;
@property (nonatomic, strong)AAShareBubbles *shareBubbles;
@property (nonatomic, assign)SSDataSourcesReloadType reloadType;
@end

@implementation iEver_OfficialListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@""
                                                        image:[UIImage imageNamed:@"tabbar_main_normal"]
                                                selectedImage:[UIImage imageNamed:@"tabbar_main_click"]];

        @weakify(self);
        [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:ReceiveRemoteNotification object:nil] map:^id(NSNotification *notification)
        {
            return notification.userInfo;

        }] subscribeNext:^(NSDictionary *userInfo) {

            @strongify(self);

            [self fetchData];

            if ([[userInfo objectForKey:@"type"] integerValue] == 10)
            {
                iEver_normalUserAnsweredQesViewController *_normalUserAnsweredQesViewController = [[iEver_normalUserAnsweredQesViewController alloc]init];
                _normalUserAnsweredQesViewController.hidesBottomBarWhenPushed                   = YES;
                [self.navigationController pushViewController:_normalUserAnsweredQesViewController animated:YES];
                 IOS7POP;
            }else if ([[userInfo objectForKey:@"type"] integerValue] == 20)
            {

                iEver_ItemTryViewController *_ItemTryViewController = [[iEver_ItemTryViewController alloc]init];
                _ItemTryViewController.hidesBottomBarWhenPushed     = YES;
                [self.navigationController pushViewController:_ItemTryViewController animated:YES];
                IOS7POP;
            }
        }];
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    if (self.category_id == 0) {

        [iEverAppDelegate shareAppDelegate].tabBarController.tabBar.hidden = NO;
    }


}

- (void)viewDidAppear:(BOOL)animated
{

    //[NSThread sleepForTimeInterval:3.0];
    [super viewDidAppear:animated];

    if (self.didSelectIndexPathComment >= 0) {
        NSArray *arr = [self.dataSource allItems];
        iEver_mainContentList_object *object = (iEver_mainContentList_object *)[arr objectAtIndex:self.didSelectIndexPathComment];
        object.pvTotal = self.detail_Object.pvTotal;
        object.likeTotal = self.detail_Object.likeTotal;
        object.commentTotal = self.detail_Object.commentTotal;

        object._selected = self.detail_Object.isLike;
        object.isBrowse = self.detail_Object.isBrowse;
        object.isComment = self.detail_Object.isComment;
        [self.tableView reloadData];
    }
    self.didSelectIndexPathComment = -1;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    returnButton.hidden = YES;
    [iEverAppDelegate shareAppDelegate].openDrawerButton.hidden = YES;

}
- (void)loadView
{
    [super loadView];

        NSString *previousVersion = [[NSUserDefaults standardUserDefaults] objectForKey:PREVIOUSVERSION];
        if (!previousVersion) {
            previousVersion = @"0";
        }
        if ([CURRENTVERSION compare:previousVersion options:NSNumericSearch]) {
            //newVersionFound
            [[NSUserDefaults standardUserDefaults] setObject:CURRENTVERSION forKey:PREVIOUSVERSION];
            [[NSUserDefaults standardUserDefaults] synchronize];

            IntroModel *model1 = [[IntroModel alloc] initWithTitle:@"" description:@"" image:@"1"];
            IntroModel *model2 = [[IntroModel alloc] initWithTitle:@"" description:@"" image:@"2"];
            IntroModel *model3 = [[IntroModel alloc] initWithTitle:@"" description:@"" image:@"3"];

            IntroControll *introView = [[IntroControll alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) pages:@[model1, model2, model3]];
            [[iEverAppDelegate shareAppDelegate].tabBarController.view addSubview:introView];
            [[iEverAppDelegate shareAppDelegate].tabBarController.view bringSubviewToFront:introView];

            UIButton *buttonNext = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonNext.backgroundColor = CLEAR;
            buttonNext.frame     = CGRectMake(100, ScreenHeight -90, 120, 44);
            [[iEverAppDelegate shareAppDelegate].tabBarController.view addSubview:buttonNext];
            [[iEverAppDelegate shareAppDelegate].tabBarController.view bringSubviewToFront:buttonNext];
            buttonNext.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

                /* request data */
                BOOL isNetworkWorking = [[iEver_Global Instance] isExistenceNetwork];
                if (!isNetworkWorking) {
                    UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:@"警告"
                                                                      message:NetWorkError
                                                                     delegate:nil
                                                            cancelButtonTitle:@"确认"
                                                            otherButtonTitles:nil];
                    [myalert show];
                } else {

                    introView.hidden  = YES;
                    buttonNext.hidden = YES;
                    self.page         = 1;
                    [self fetchData];
                }

                return [RACSignal empty];
            }];

        }else{

            /* request data */
            self.page  = 1;
            [self fetchData];
        }

}
/* set tableview scroll with nav scroll */
-(void)setTableViewScrollWithNavScroll{

    // Remember to set the navigation bar to be NOT translucent
	[self.navigationController.navigationBar setTranslucent:YES];

	if ([self.navigationController.navigationBar respondsToSelector:@selector(setBarTintColor:)]) {
        [self.navigationController.navigationBar setBarTintColor:NAV];
    }

    // For better behavior set statusbar style to opaque on iOS < 7.0
    if (([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] == NSOrderedAscending)) {
        // Silence depracation warnings
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
#pragma clang diagnostic pop
    }else {

        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }

    [self followScrollView:self.tableView withDelay:65];
    [self setShouldScrollWhenContentFits:YES];
}
- (void)reset
{
    _previousOffsetY  = 0.0;
    _accumulatedY     = 0.0;
    _downThresholdY   = 0.0;
    _upThresholdY     = 0.0;
    _previousScrollDirection = NJKScrollDirectionNone;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Add Transition
    [ASFSharedViewTransition addTransitionWithFromViewControllerClass:[iEver_OfficialListViewController class]
                                                ToViewControllerClass:[iEver_OfficialDetailViewController class]
                                             WithNavigationController:(UINavigationController *)self.navigationController
                                                         WithDuration:0.5f];

    self.page                                       = 1;
    self.didSelectIndexPathComment                  = -1;
    self.statusBarNotificationLabel.textColor       = [UIColor whiteColor];
    self.statusBarNotificationLabel.backgroundColor = iever;

    [self reset];
    self.view.backgroundColor = [UIColor clearColor];

    [self isCustomApi];

    /* creat left right bar button */
    [self addBar];

    /* configure cell */
    [self configureCell];

    /* add nav imageview  */
    if (self.category_id == 0) {

        [self goTableViewTopView];

        [self addNavImageview];
        
        UIView *bannerBaseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, IBannerBaseView)];
        bannerBaseView.backgroundColor = [UIColor redColor];
        
        /* 基本banner */
        _MainiAdView = [[iEver_MainiAdView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, [iEver_MainiAdView heightForiAd])];
        _MainiAdView.backgroundColor = WHITE;
        _MainiAdView.navigationController = self.navigationController;
        
        [bannerBaseView addSubview:_MainiAdView];
        
        /* 视频banner */
        _videoBanner = [[iEver_videoBanner alloc] initWithFrame:CGRectMake(0, [iEver_MainiAdView heightForiAd], ScreenWidth, [iEver_videoBanner heightForVideoBanner])];
        _videoBanner.backgroundColor = WHITE;
        
        [bannerBaseView addSubview:_videoBanner];
        
        /* 特别banner */
        _specialBanner = [[iEver_specialBanner alloc] initWithFrame:CGRectMake(0, [iEver_MainiAdView heightForiAd] + [iEver_videoBanner heightForVideoBanner], ScreenWidth, [iEver_specialBanner heightForSpecialBanner])];
        _specialBanner.backgroundColor = WHITE;
        
        [bannerBaseView addSubview:_specialBanner];
        
        self.tableView.tableHeaderView = bannerBaseView;
        
    }else{

        /*  setTableViewScrollWithNavScroll */
        //[self setTableViewScrollWithNavScroll];
        [iEverAppDelegate shareAppDelegate].tabBarController.tabBar.hidden = YES;
    }

}


-(void)isCustomApi{

    if (self.category_id == 0) {

        iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
        [[[content userIsCustomMade:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *dic) {

             if ([[dic objectForKey:@"resultCode"] integerValue] == 1) {

                 customCount = [[dic objectForKey:@"customCount"] integerValue] ;

             }
             
         }];

    }
}

/* 一键返回tableview 顶部 */
-(void)goTableViewTopView{

    if (!returnButton) {
        returnButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    returnButton.frame  = CGRectMake(ScreenWidth -50, ScreenHeight -100, 44, 44);
    returnButton.hidden = YES;
    [returnButton setImage:[UIImage imageNamed:@"list_goTop"] forState:UIControlStateNormal];
    [[iEverAppDelegate shareAppDelegate].tabBarController.view addSubview:returnButton];
    [[iEverAppDelegate shareAppDelegate].tabBarController.view bringSubviewToFront:returnButton];
    returnButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        [self scrollToTop];

        return [RACSignal empty];
    }];

}
-(void)addNavImageview{

    UIImageView *imageView  = [[UIImageView alloc] initWithFrame:CGRectMake(127.0f, 234.0f, 66.0f, 26.0f)];
    imageView.contentMode   = UIViewContentModeScaleAspectFit;
    UIImage *image          = [UIImage imageNamed:@"main_nav_logo"];
    [imageView setImage:image];
    self.navigationItem.titleView = imageView;
}
/* creat left right bar button */
-(void)addBar{

    @weakify(self)
    if (self.category_id == 0) {

        [self addRightBarButtonItem:^(UIButton *sender) {
            @strongify(self)
            [self creatBubbles];
        }];

        UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage * NAV_return = [UIImage imageNamed:@"main_list_fenlei"];
        leftButton.frame = CGRectMake(0, 0, NAV_return.size.width, NAV_return.size.height);
        [leftButton setImage:NAV_return forState:UIControlStateNormal];
        leftButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

            [self.menuContainerViewController toggleLeftSideMenuCompletion:^{

            }];
            return [RACSignal empty];
        }];
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
        self.navigationItem.leftBarButtonItem = leftBarButton;
        
    }

}

-(void)creatBubbles{

    if(self.shareBubbles) {
        _shareBubbles = nil;
    }
    CGPoint viewCenter;
    viewCenter.x               = self.view.center.x;
    viewCenter.y               = self.view.center.y - 20.0f;
    radius                     = 130;
    _shareBubbles              = [[AAShareBubbles alloc] initWithPoint:viewCenter radius:radius inView:[iEverAppDelegate shareAppDelegate].window];
    _shareBubbles.delegate     = self;
    bubbleRadius               = 36;
    _shareBubbles.bubbleRadius = bubbleRadius;
    [_shareBubbles show];
    
}

/* configure cell */
-(void)configureCell{

    __typeof (self) __weak weakSelf = self;

    self.dataSource.cellConfigureBlock = ^(iEver_mainOfficialCell *cell,
										   NSNumber *exampleType,
										   UITableView *tableView,
										   NSIndexPath *indexPath) {
        
        [cell configureCell];
        iEver_mainContentList_object *listObject = (iEver_mainContentList_object *)[weakSelf.dataSource itemAtIndexPath:indexPath];
        cell.object =listObject;
    };
    self.dataSource.cellClass = [iEver_mainOfficialCell class];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)delegateMethod{

    [self fetchData];

    [self isCustomApi];
}


-(void)creatsecondCategoryView{

    secondCategoryView                       = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 86.0)];
    //secondCategoryView.backgroundColor       = [UIColor colorWithRed:240/255.0f green:240/255.0f blue:240/255.0f alpha:1.0];
    secondCategoryView.backgroundColor       = WHITE;

    UIScrollView  *scrollView  = [[UIScrollView alloc] initWithFrame:CGRectMake(9.0, 0.0, 302.0, 86.0)];

    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator   = NO;

    [scrollView setContentSize:CGSizeMake(78.0 * [mainObject_secondCategoryList._secondCategoryList count] , 86.0)];

    for (int i = 0; i < [mainObject_secondCategoryList._secondCategoryList count]; i++) {
        iEver_secondCategoryList_object *iAd = [mainObject_secondCategoryList._secondCategoryList objectAtIndex:i];
        [self addImageWithName:iAd atPosition:i scrollView:scrollView];

    }

    [secondCategoryView addSubview:scrollView];


}

/* get request data*/
-(void)fetchData{

    __block iEver_OfficialListViewController *weakSelf = self;

    iEver_mainContent *content = [[iEver_mainContent alloc] init];

    if (self.page == 1) {
        [GiFHUD show];
    }
    if ([[self.dataSource allItems] count] > 0)
    {
        iEver_mainContentList_object *content_object = [[self.dataSource allItems] objectAtIndex:0];
        self.timeDate                                = content_object.releaseTime;

    }else{
        self.timeDate = 0;
    }

    NSString *_pathUrl;
    if (self.tagId >0) {

        _pathUrl = [NSString stringWithFormat:@"article/queryByTagId/%d/%d/%lld",self.tagId,self.page,self.timeDate];
    }
    else if (self.userLike >0) {

        _pathUrl = [NSString stringWithFormat:@"/article/queryByUserLike/%d",self.page];
    }

    else{

        _pathUrl = [NSString stringWithFormat:@"article/queryByCate/%d/%d/%d/%lld",self.categoryType,self.category_id,self.page,self.timeDate];
    }

    [[[content acquireContentListsData:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_mainContent *object) {

         if (self.page == 1 && mainObject_secondCategoryList._secondCategoryList.count == 0) {
             mainObject_secondCategoryList = object;

             [self creatsecondCategoryView];
         }

         if (object.resultCode == 1) {

             if (object.refreshCount > 0 && self.category_id == 0) {
                 [self showStatusBarNotification:[NSString stringWithFormat:@"更新%d条数据",object.refreshCount] forDuration:1.0];
             }

             if (object.pageSize < self.page) {

                 self.tableView.showsInfiniteScrolling = NO;
             }else{

                 mainObject = object;
                 [weakSelf reloadDatasouce:object._contentList];
             }
             [GiFHUD dismiss];
             if (self.category_id == 0) {

                 
                 [_MainiAdView setObject:object];
                 [_videoBanner setObject:object];
                 if (object._specialBannerList > 0 && _specialBanner.subviews > 0) {

                     _specialBanner.pagerView.page          = 0;
                     _specialBanner.crrentSpecialTitle.text = @"";
                     _specialBanner.nextSpecialTitle.text   = @"";
                     _specialBanner.lastSpecialTitle.text   = @"";
                     _specialBanner.baseColorView.backgroundColor = RANKCOLOR_1;
                     [_specialBanner.scrollView setContentOffset:CGPointZero];
                     [_specialBanner.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                 }
                 [_specialBanner setObject:object];

                 [self loadBootstrap];
             }

         }
         else{
             [GiFHUD dismiss];
             iEver_LoginViewController *_loginViewController = [[iEver_LoginViewController alloc]init];
             _loginViewController.delegate                   = self;
             _loginViewController.hidesBottomBarWhenPushed   = YES;
             [self.navigationController pushViewController:_loginViewController animated:YES];
         }
     }];
}

-(void)loadBootstrap {
    NSString *main_previousVersion = [[NSUserDefaults standardUserDefaults] objectForKey:main_PREVIOUSVERSION];
    if (!main_previousVersion) {
        main_previousVersion = @"0";
    }
    if ([main_CURRENTVERSION compare:main_previousVersion options:NSNumericSearch]) {
        //newVersionFound
        [[NSUserDefaults standardUserDefaults] setObject:main_CURRENTVERSION forKey:main_PREVIOUSVERSION];
        [[NSUserDefaults standardUserDefaults] synchronize];

        UIImage *main_Bootstrap = [UIImage imageNamed:@"main_Bootstrap"];
        UIImageView *image_Bootstrap = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,ScreenWidth, ScreenHeight)];
        image_Bootstrap.userInteractionEnabled = YES;
        image_Bootstrap.image = main_Bootstrap;

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(100, ScreenHeight - 60, 120, 40);
        button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

            image_Bootstrap.hidden = YES;
            return [RACSignal empty];
        }];
        [image_Bootstrap addSubview:button];
        
        [[iEverAppDelegate shareAppDelegate].window addSubview:image_Bootstrap];

   }
}
- (void)scrollToTop {

    [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
    [iEverAppDelegate shareAppDelegate].tabBarController.tabBar.hidden = NO;
}


#pragma mark －
#pragma mark Table view

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.separatorStyle             = UITableViewCellSeparatorStyleNone;
    NSArray *arr                         = [self.dataSource allItems];
    iEver_mainContentList_object *object = (iEver_mainContentList_object *)[arr objectAtIndex:indexPath.row];
	return [iEver_mainOfficialCell heightForObject:object atIndexPath:indexPath tableView:tableView];}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    NSArray *arr                                                      = [self.dataSource allItems];
    iEver_mainOfficialCell *cell                                      = (iEver_mainOfficialCell *)[tableView cellForRowAtIndexPath:indexPath];
    iEver_mainContentList_object *object                              = (iEver_mainContentList_object *)[arr objectAtIndex:indexPath.row];
    iEver_OfficialDetailViewController *_officialDetailViewController = [[iEver_OfficialDetailViewController alloc]init];
    _officialDetailViewController.list_Object                         = object;
    _officialDetailViewController.didSelectIndexPath                  = indexPath.row;
    self.didSelectIndexPathComment                                    = indexPath.row;
    _officialDetailViewController.delegate                            = self;
    _officialDetailViewController.image                               = cell.coverImage.image;
    _officialDetailViewController.type_id                             = object.Cover_id;
    _officialDetailViewController.hidesBottomBarWhenPushed = YES;
    self.tableView.frame = CGRectMake(0, 64, 320.0, 504.0);
    [self.navigationController pushViewController:_officialDetailViewController animated:YES];
    IOS7POP;
	//[tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{

    return @"复制";
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{

    return  UITableViewCellEditingStyleNone;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    int sectionHeaderHeight = 0;

    if (mainObject_secondCategoryList._secondCategoryList.count > 0) {
        sectionHeaderHeight  = 86.0;
    }
    return sectionHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    return secondCategoryView;
}


- (void)addImageWithName:(iEver_secondCategoryList_object *)iAd atPosition:(int)position  scrollView:(UIScrollView *)scrollview

{

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame     = CGRectMake(78.0 * position, 9.0, 68.0, 68.0);
    button.tag       = position;
    [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"mainList_secondCategory_0%d",position % 6 + 1]] forState:UIControlStateNormal];
    if (iAd.categoryName.length >4) {
        button.titleLabel.font = TextLIST_Fonts;
    }else {
        button.titleLabel.font = TextDESCFonts;
    }
    [button setTitle:iAd.categoryName forState:UIControlStateNormal];
    [button setTitleColor:WHITE forState:UIControlStateNormal];

    [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {

        self.category_id   = iAd.Category_id;
        self.categoryType  = 11;
        self.page          = 1;
        self.reloadType    = SSDataSourcesReloadPull;

        self.title         = iAd.categoryName;

        [self.dataSource clearItems];
        [self fetchData];
        [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];

    }];
    [scrollview addSubview:button];
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
	// This enables the user to scroll down the navbar by tapping the status bar.
    [self showNavbar];
	return YES;
}

#pragma mark -
#pragma mark scrollview scroll

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{


        CGFloat currentOffsetY = scrollView.contentOffset.y;

        NJKScrollDirection currentScrollDirection = detectScrollDirection(currentOffsetY, _previousOffsetY);
        CGFloat topBoundary = -scrollView.contentInset.top;
        CGFloat bottomBoundary = scrollView.contentSize.height + scrollView.contentInset.bottom;

        BOOL isOverTopBoundary = currentOffsetY <= topBoundary;
        BOOL isOverBottomBoundary = currentOffsetY >= bottomBoundary;

        BOOL isBouncing = (isOverTopBoundary && currentScrollDirection != NJKScrollDirectionDown) || (isOverBottomBoundary && currentScrollDirection != NJKScrollDirectionUp);
        if (isBouncing || !scrollView.isDragging) {
            return;
        }

        CGFloat deltaY = _previousOffsetY - currentOffsetY;
        _accumulatedY += deltaY;

        switch (currentScrollDirection) {
            case NJKScrollDirectionUp:
            {
                BOOL isOverThreshold = _accumulatedY < -_upThresholdY;

                if (isOverThreshold || isOverBottomBoundary)  {
                    if (self.category_id == 0) {

                        [self setTabBarOriginY:0.0 animated:YES];
                        returnButton.hidden = NO;
                        self.tableView.frame = CGRectMake(0, 64, 320.0, 504.0);
                    }else{

                        returnButton.hidden = NO;
                    }
                }
            }
                break;
            case NJKScrollDirectionDown:
            {
                BOOL isOverThreshold = _accumulatedY > _downThresholdY;

                if (isOverThreshold || isOverTopBoundary) {
                    if (self.category_id==0) {

                        [self setTabBarOriginY:0.0 animated:NO];
                    }else{

                        returnButton.hidden = NO;
                    }
                }
            }
                break;
            case NJKScrollDirectionNone:
                
                break;
        }

        // reset acuumulated y when move opposite direction
        if (!isOverTopBoundary && !isOverBottomBoundary && _previousScrollDirection != currentScrollDirection) {
            _accumulatedY = 0;
        }
        
        _previousScrollDirection = currentScrollDirection;
        _previousOffsetY = currentOffsetY;


}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{

        CGFloat currentOffsetY = scrollView.contentOffset.y;
        CGFloat topBoundary    = -scrollView.contentInset.top;
        CGFloat bottomBoundary = scrollView.contentSize.height + scrollView.contentInset.bottom;

        switch (_previousScrollDirection) {
            case NJKScrollDirectionUp:
            {
                BOOL isOverThreshold = _accumulatedY < -_upThresholdY;
                BOOL isOverBottomBoundary = currentOffsetY >= bottomBoundary;

                if (isOverThreshold || isOverBottomBoundary) {

                    if (self.category_id == 0) {

                        [self setTabBarOriginY:0.0 animated:YES];
                        returnButton.hidden = NO;
                    }else{

                        returnButton.hidden = NO;
                    }

                }
                break;
            }
            case NJKScrollDirectionDown:
            {
                BOOL isOverThreshold   = _accumulatedY > _downThresholdY;
                BOOL isOverTopBoundary = currentOffsetY <= topBoundary;

                if (isOverThreshold || isOverTopBoundary) {
                    if (self.category_id == 0) {

                        [self setTabBarOriginY:0.0 animated:NO];
                    }else{

                        returnButton.hidden = NO;
                    }

                }
                break;
            }
            case NJKScrollDirectionNone:
                break;
        }

}

#pragma mark
#pragma mark - photo view appear/disappear
- (void)setTabBarOriginY:(CGFloat)y animated:(BOOL)animated
{
    [UIView animateWithDuration:animated ? 0.1 : 0 animations:^{
        [iEverAppDelegate shareAppDelegate].tabBarController.tabBar.hidden = animated;
    }];
}

#pragma mark -
#pragma mark AAShareBubbles

-(void)aaShareBubbles:(UIButton *)shareBubbles;
{
    NSArray *channelTypes_ID = @[[NSNumber numberWithInt:135],
                                 [NSNumber numberWithInt:31],
                                 [NSNumber numberWithInt:57],
                                 [NSNumber numberWithInt:124],
                                 [NSNumber numberWithInt:122],
                                 [NSNumber numberWithInt:121],
                                 [NSNumber numberWithInt:158],
                                 [NSNumber numberWithInt:0]];


    NSArray *channelTypes =@[@"美妆故事",@"彩 妆",@"护 肤",@"减 肥",@"美 甲",@"美 发",@"场 合"];

    if (shareBubbles.tag == 7) {
        iEver_PersonalTagSettingViewController *_PersonalTagSettingViewController = [[iEver_PersonalTagSettingViewController alloc]init];
        if (customCount > 3) {

            _PersonalTagSettingViewController.userIsCustom =  @"DID";
        }else
        {
            _PersonalTagSettingViewController.userIsCustom =  @"NOT";

        }
        _PersonalTagSettingViewController.delegate                 = self;
        _PersonalTagSettingViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:_PersonalTagSettingViewController animated:YES];
        IOS7POP;
    }else {

        iEver_OfficialListViewController *_OfficialListViewController = [[iEver_OfficialListViewController alloc]init];
        _OfficialListViewController.category_id = [channelTypes_ID[shareBubbles.tag] integerValue];
        _OfficialListViewController.categoryType = 10;
        _OfficialListViewController.title = channelTypes[shareBubbles.tag];
        _OfficialListViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:_OfficialListViewController animated:YES];
        IOS7POP;
    }

}
#pragma mark - ASFSharedViewTransitionDataSource

- (UIView *)sharedView
{

    iEver_mainOfficialCell * cell = (iEver_mainOfficialCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.didSelectIndexPathComment inSection:0]];
    return  cell.coverImage;

}

#pragma mark - shouldAutorotateToInterfaceOrientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
}
@end

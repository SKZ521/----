//
//  iEver_detailCommentViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/23.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"

@interface iEver_detailCommentViewController : iEverBaseTableViewController

@property (nonatomic, assign) int                        commentCover_id;       /* id */

-(void)insertCommentToReplyComment;                                     /* 点击评论的评论，进行评论 */
@property (nonatomic, copy  ) NSString       *toReplyCommentName;       /* 评论回复 用户名 */
@property (nonatomic, assign) int               comment_parentId;       /* 评论回复 的parent ID */
@property (nonatomic, assign) int               comment_atUserId;       /* 评论回复 的atUser ID */

@end

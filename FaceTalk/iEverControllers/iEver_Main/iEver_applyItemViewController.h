//
//  iEver_applyItemViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-31.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

@interface iEver_applyItemViewController : iEverBaseViewController
@property (nonatomic, copy  ) NSString                   *imageview;     /* 图片 */
@property (nonatomic, copy  ) NSString                   *name;          /* 名称 */
@property (nonatomic, copy  ) NSString                   *price;         /* 价格 */
@property (nonatomic, assign) int                        itemTryId;      /* 试用ID */
@property (nonatomic, assign) int                        itemId;         /* 商品 */
@end

//
//  iEver_ItemTryDetailViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-30.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_ItemTryDetailViewController.h"
#import "iEver_tryItemDetailObject.h"
#import "iEver_ItemDetailViewController.h"
#import "iEver_applyItemViewController.h"
#import "iEver_successList_cell.h"
#import "iEver_tryItemDetail_cell.h"
#import "iEver_commentObject.h"

@interface iEver_ItemTryDetailViewController ()
{

    iEver_tryItemDetailObject *_datailOblect;

    UILabel *tryTime_lable;

    UIImageView  *coverImageview;
}
@property (retain, nonatomic) UIView                  *mainView;
@property (retain, nonatomic) UIButton                *bottomButton;
@property (retain, nonatomic) UITextView              *myTextView;

@end

@implementation iEver_ItemTryDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

    /* request data */
    [self fetchData];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}
- (void)loadView
{
    [super loadView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = CLEAR;
    /* creat left right bar button */
    [self addBar];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = NO;

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    self.title = @"试用详情";
//    /* request data */
//    [self fetchData];


    /* comment textview */
    [self creatTextView];

}


// 根据键盘状态，调整_mainView的位置
- (void) changeContentViewPoint:(NSNotification *)notification{

    NSDictionary *userInfo  = [notification userInfo];
    NSValue *value          = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyBoardEndY    = value.CGRectValue.origin.y;  // 得到键盘弹出后的键盘视图所在y坐标
    CGFloat kStateBarHeight = 0.0;

    NSNumber *duration      = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve         = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];

    // 添加移动动画，使视图跟随键盘移动
    [UIView animateWithDuration:duration.doubleValue animations:^{
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationCurve:[curve intValue]];
        self.mainView.center = CGPointMake(self.mainView.center.x, keyBoardEndY - kStateBarHeight - self.mainView.bounds.size.height/2.0 - NAVHeight);   // keyBoardEndY的坐标包括了状态栏的高度，要减去  隐藏导航 NAVHeight
    }];
    
}

-(void)changeKeyboardWillHide:(NSNotification *)notification{

    self.bottomButton.hidden     = YES;
    [self.myTextView resignFirstResponder];
    self.mainView.frame          = CGRectMake(0, ScreenHeight, 320, 145.0);

}

-(void)creatTextView{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentViewPoint:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    /* mainView 灰黑 BG*/
    self.bottomButton             = [UIButton buttonWithType:UIButtonTypeCustom];
    self.bottomButton.frame       = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    self.bottomButton.hidden      = YES;
    self.bottomButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        self.bottomButton.hidden  = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame       = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    self.bottomButton.backgroundColor = BLACK;
    self.bottomButton.alpha           = 0.6;
    [self.view addSubview:self.bottomButton];

    /* mainView */
    self.mainView                     = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight, 320, 145.0)];
    self.mainView.backgroundColor     = BackkGroundColor;
    [self.view addSubview:self.mainView];

    /* textViewImage */
    UIImageView *textViewImage       = [[UIImageView alloc] initWithFrame:CGRectMake(15, 50, 290, 80)];
    textViewImage.image              = [UIImage imageNamed:@"commentContentBG"];
    [self.mainView addSubview:textViewImage];

    /* myTextView */
    self.myTextView                  = [[UITextView alloc]initWithFrame:CGRectMake(15, 50, 290, 80)];
    self.myTextView.font             = TextFont;
    self.myTextView.backgroundColor  = CLEAR;
    [self.mainView addSubview:self.myTextView];

    /* cancelBut */
    UIButton *cancelBut              = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBut.frame                  = CGRectMake(10, 10, 30, 30);
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateHighlighted];
    cancelBut.rac_command            = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        self.bottomButton.hidden     = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame          = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    [self.mainView addSubview:cancelBut];

    /* commentLab */
    UILabel *commentLab             = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, 240, 25)];
    commentLab.text                 = @"试用 心得";
    commentLab.font                 = Title_Font;
    commentLab.textAlignment        = NSTextAlignmentCenter;
    [self.mainView addSubview:commentLab];

    /* sendBut */
    UIButton *sendBut               = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBut.frame                   = CGRectMake(280, 10, 30, 30);
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateHighlighted];
    sendBut.rac_command             = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        /* insert Comment */
        [self insertCommentRequest];
        return [RACSignal empty];
    }];
    [self.mainView addSubview:sendBut];
}

/* creat left right bar button */
-(void)addBar{

    __weak iEver_ItemTryDetailViewController *weakSelf = self;
    [self addLeftBarButtonItem:^(UIButton *sender) {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];

    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 44, 44);
    [rightButton setImage:[UIImage imageNamed:@"mainDetail_share"] forState:UIControlStateNormal];
    int offset = 20;
    UIEdgeInsets imageInset = UIEdgeInsetsMake(0, offset, 0, 0);
    rightButton.imageEdgeInsets = imageInset;
    rightButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        NSString *shareUrl = _datailOblect._itemDetailObject.webUrl;
        NSString *title = _datailOblect._itemDetailObject.itemName;
        UIImage *shareImage = coverImageview.image;

        //设置分享消息类型
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;

        //微信好友
        [UMSocialData defaultData].extConfig.wechatSessionData.url = shareUrl;
        [UMSocialData defaultData].extConfig.wechatSessionData.title = title;

        //微信朋友圈
        [UMSocialData defaultData].extConfig.wechatTimelineData.url = shareUrl;
        [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;

        //Qzone QQ空间分享只支持图文分享（图片文字缺一不可）
        [UMSocialData defaultData].extConfig.qzoneData.url = shareUrl;
        [UMSocialData defaultData].extConfig.qzoneData.title = title;


        NSString *shareText = [NSString stringWithFormat:@"美课美妆给予你们：%@, 详情请见：%@",  title, shareUrl];            //分享内嵌文字

        //分享到新浪微博内容
        [UMSocialData defaultData].extConfig.sinaData.shareText = shareText;


        //如果得到分享完成回调，需要设置delegate为self
        [UMSocialSnsService presentSnsIconSheetView:self appKey:UMAPPKEY shareText:shareText shareImage:shareImage shareToSnsNames:@[UMShareToWechatSession, UMShareToWechatTimeline,UMShareToSina, UMShareToTencent ] delegate:nil];
        return [RACSignal empty];
    }];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/* insert Comment */
-(void)insertCommentRequest{


    NSString *temp = [self.myTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if ([temp isEqualToString:@""]) {
        [SVProgressHUD showImage:Nil status:@"请输评论内容"];
        return ;
    }

    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_commentObject * content = [[iEver_commentObject alloc] init];
    NSString * _pathUrl = [NSString stringWithFormat:@"/itemTryComment/insert"];
    NSDictionary *dic = @{@"itemTryId": [NSNumber numberWithInt:self.type_id],
                          @"commentContent": self.myTextView.text,
                          };

    [[[content insertArticleComment:dic path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"提交成功"];
             [self fetchData];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

     }];
    self.bottomButton.hidden = YES;
    [self.myTextView resignFirstResponder];
    self.myTextView.text = @"";
    self.mainView.frame = CGRectMake(0, ScreenHeight, 320, 145.0);
}


/*request data*/
-(void)fetchData{

    @weakify(self);
    [GiFHUD show];
    iEver_tryItemDetailObject *detail_Object = [[iEver_tryItemDetailObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"/itemTry/queryByItemTryId/%d",self.type_id];
    [[[detail_Object queryByItemTryId:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_tryItemDetailObject *object) {

         @strongify(self);
         _datailOblect = object;
         [self.tableView reloadData];
         [GiFHUD dismiss];
         /* creat CoverView As TableViweHeader */
         [self creatCoverViewAsTableViweHeader];
     }];
}
/* creat CoverView As TableViweHeader */
-(void)creatCoverViewAsTableViweHeader{

    /* Initialization */
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;
    /* custom tableView headerView */
    /* headerView */
    self.tableView.tableHeaderView = ({

        UIView *headerView = [[UIView alloc] init];
        [headerView setBackgroundColor:CLEAR];
        /* 白色背景 */
        [headerView addSubview:({

            UIView *bottomView = [[UIView alloc] init];
            [bottomView setBackgroundColor:CLEAR];

            /* 封面图片 */
            [bottomView addSubview:({

                x = 5.0 ;
                y = 5.0;
                width = 310.0;
                height = 155.0;
                coverImageview = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                coverImageview.backgroundColor = CLEAR;
                [coverImageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/620x",_datailOblect._itemDetailObject.tryImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
                coverImageview;
            })];

            /* 半透明视图 */
            [bottomView addSubview:({

                x = 5.0 ;
                y = 160.0 - 23.0;
                width = 310.0;
                height = 23.0;
                UIView *halfBlackView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                halfBlackView.backgroundColor = BLACK;
                halfBlackView.alpha = 0.6;
                halfBlackView;

            })];

            [bottomView addSubview:({

                x = 5.0 ;
                y = 160.0 - 23.0;
                width = 310.0;
                height = 23.0;
                UIView *halfBlackView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];


                [halfBlackView addSubview:({

                    x = 10.0 ;
                    y = 0.0;
                    width = 50.0;
                    height = 25.0;

                    UILabel *num_label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    num_label.text = @"试用数";
                    num_label.numberOfLines = 1;
                    num_label.backgroundColor = CLEAR;
                    num_label.textColor = WHITE;
                    num_label.font = TextDESCFonts;
                    num_label;

                })];

                [halfBlackView addSubview:({

                    x = 50.0 ;
                    y = 0.0;
                    width = 60.0;
                    height = 25.0;

                    UILabel *num = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    num.numberOfLines = 1;
                    num.text = [NSString stringWithFormat:@"%d份",_datailOblect._itemDetailObject.tryNum];
                    num.backgroundColor = CLEAR;
                    num.textColor = TRY_UPDATE_YELLOY;
                    num.font = TextDESCFonts;
                    num;
                    
                })];

                [halfBlackView addSubview:({

                    x = 95.0 ;
                    y = 0.0;
                    width = 50.0;
                    height = 25.0;

                    UILabel *applyNum_lable = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    applyNum_lable.text = @"申请数";
                    applyNum_lable.numberOfLines = 1;
                    applyNum_lable.backgroundColor = CLEAR;
                    applyNum_lable.textColor = WHITE;
                    applyNum_lable.font = TextDESCFonts;
                    applyNum_lable;

                })];

                [halfBlackView addSubview:({

                    x = 135.0 ;
                    y = 0.0;
                    width = 60.0;
                    height = 25.0;

                    UILabel *applyNum = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    applyNum.numberOfLines = 1;
                    applyNum.text = [NSString stringWithFormat:@"%d份",_datailOblect._itemDetailObject.applyTotal];
                    applyNum.backgroundColor = CLEAR;
                    applyNum.textColor = TRY_UPDATE_YELLOY;
                    applyNum.font = TextDESCFonts;
                    applyNum;

                })];

                [halfBlackView addSubview:({

                    x = 230.0 ;
                    y = 0.0;
                    width = 40;
                    height = 25.0;

                    tryTime_lable = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    tryTime_lable.text = @"剩余";
                    tryTime_lable.numberOfLines = 1;
                    tryTime_lable.backgroundColor = CLEAR;
                    tryTime_lable.textColor = WHITE;
                    tryTime_lable.font = TextDESCFonts;
                    tryTime_lable;
                    
                })];

                [halfBlackView addSubview:({

                    x = 240.0 ;
                    y = 0.0;
                    width = 60.0;
                    height = 25.0;

                    UILabel *tryTime = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    tryTime.numberOfLines = 1;
                    NSString *str = [iEver_Global intervalSinceNow:_datailOblect._itemDetailObject.endTime/1000];
                    if ([str isEqualToString:@"已结束"]) {
                        tryTime_lable.hidden = YES;
                        tryTime_lable.textColor = WHITE;
                    }else{
                        tryTime_lable.hidden = NO;
                        tryTime.textColor = TRY_UPDATE_YELLOY;
                    }
                    tryTime.text = [NSString stringWithFormat:@"%@",str];
                    tryTime.backgroundColor = CLEAR;
                    tryTime.textAlignment = NSTextAlignmentRight;
                    tryTime.textColor = WHITE;
                    tryTime.font = TextDESCFonts;
                    tryTime;
                    
                })];


                halfBlackView;
            })];


            /* Item view */
            [bottomView addSubview:({

                x = 0.0;
                y = 160.0;
                width = 320.0;
                height = 80.0;
                UIView * itemView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                itemView.backgroundColor = WHITE;

                /* 商品图片 */
                [itemView addSubview:({

                    x = 10.0 ;
                    y = 15.0;
                    width = 50.0;
                    height = 50.0;
                    UIImageView  *itemCoverImageview = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    itemCoverImageview.backgroundColor = CLEAR;
                    [itemCoverImageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/100x",_datailOblect._itemDetailObject.itemImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
                    itemCoverImageview.contentMode = UIViewContentModeScaleAspectFill;
                    itemCoverImageview;
                })];

                /* 商品标题 */
                [itemView addSubview:({

                    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
                    CGSize contentSize =[_datailOblect._itemDetailObject.itemName boundingRectWithSize:CGSizeMake(180, 42.0) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
                    x = 70.0;
                    y = 10.0;
                    width = 180.0;
                    height = contentSize.height;
                    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    titleLabel.numberOfLines = 0;
                    titleLabel.textColor = BLACK;
                    titleLabel.font = TextFont;
                    titleLabel.backgroundColor = CLEAR;
                    titleLabel.text = _datailOblect._itemDetailObject.itemName;
                    titleLabel;
                })];

                /* 商品价格 */
                [itemView addSubview:({

                    x = 70.0;
                    y = 45.0;
                    width = 180.0;
                    height = 25.0;
                    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    priceLabel.textColor = BLACK;
                    priceLabel.font = TextDESCFonts;
                    priceLabel.backgroundColor = CLEAR;
                    priceLabel.text = [NSString stringWithFormat:@"RMB%d/%@",_datailOblect._itemDetailObject.price,_datailOblect._itemDetailObject.itemSpec];
                    priceLabel;
                })];

                /* 进入详情箭头图片 */
                [itemView addSubview:({

                    UIImage * tryItem_detail_arrow = [UIImage imageNamed:@"tryItem_detail_arrow"];
                    x = ScreenWidth - 10 - tryItem_detail_arrow.size.width ;
                    y = 35.0 - tryItem_detail_arrow.size.height/2;
                    width = tryItem_detail_arrow.size.width;
                    height = tryItem_detail_arrow.size.height;

                    UIImageView  *itemCoverImageview = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
                    itemCoverImageview.backgroundColor = CLEAR;
                    [itemCoverImageview setImage:tryItem_detail_arrow];
                    itemCoverImageview;
                })];

                /* 进入详情按钮 */
                [itemView addSubview:({

                    x = 0.0;
                    y = 0.0;
                    width = 320.0;
                    height = 70.0;
                    UIButton *enterItemDetailButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    enterItemDetailButton.frame = CGRectMake(x, y, width, height);
                    enterItemDetailButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
                        /* startVideo modthod */
                        iEver_ItemDetailViewController *_itemlDetailViewController = [[iEver_ItemDetailViewController alloc]init];
                        _itemlDetailViewController.type_id = _datailOblect._itemDetailObject.itemId;
                        [self.navigationController pushViewController:_itemlDetailViewController animated:YES];
                        IOS7POP;
                        return [RACSignal empty];
                    }];
                    enterItemDetailButton;
                })];
                itemView;
            })];
            x = 0.0;
            y = 0.0;
            width = 320.0;
            height = 390.0 - 155;
            bottomView.frame = CGRectMake(x, y, width, height);
            bottomView;
            
        })];
        x = 0.0;
        y = 0.0;
        width = 320.0;
        height = 405.0 -165;
        headerView.frame = CGRectMake(x, y, width, height);
        headerView;
    });
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_datailOblect._itemTryNameList >0) {
        return 2;
    }else{

        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [_datailOblect._itemTryDetailArray count];
    }else if (section == 1){
        return [_datailOblect._itemTryNameList count];
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier ;
    UITableViewCell *cell = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    switch (indexPath.section) {
        case 0:
        {
            CellIdentifier = @"tryItemDetailCell";
            cell = (iEver_tryItemDetail_cell *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:CellIdentifier];

        }
            break;
        case 1:
        {
            CellIdentifier = @"tryDetailSuccessListCell";
            cell = (iEver_successList_cell *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:CellIdentifier];

        }
            break;

        default:
            break;
    }
    // Configure the cell...
    if (!cell) {

        switch (indexPath.section) {
            case 0:
            {

                 cell = [[iEver_tryItemDetail_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
                break;
            case 1:
            {

                cell = [[iEver_successList_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
                break;

                
            default:
                break;
        }
    }

    switch (indexPath.section) {
        case 0:
        {

            iEver_tryItemDetail_cell *try_cell = (iEver_tryItemDetail_cell *)cell;
            NSString *object = [_datailOblect._itemTryDetailArray objectAtIndex:indexPath.row];
            [try_cell setObject:object];
        }
            break;
        case 1:
        {
            iEver_successList_cell *name_cell = (iEver_successList_cell *)cell;
            iEver_successList_object *object = [_datailOblect._itemTryNameList objectAtIndex:indexPath.row];
            [name_cell setObject:object];
        }
            break;

        default:
            break;
    }


    return cell;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    switch (section) {
        case 0:
        {

            NSString *object = [_datailOblect._itemTryDetailArray objectAtIndex:indexPath.row];
            return [iEver_tryItemDetail_cell heightForObject:object atIndexPath:indexPath tableView:tableView];
        }
            break;
        case 1:
        {
            iEver_successList_object *object = [_datailOblect._itemTryNameList objectAtIndex:indexPath.row];
            return [iEver_successList_cell heightForObject:object atIndexPath:indexPath tableView:tableView];
        }
            break;

        default:
            break;
    }

    return 0.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    switch (section) {
        case 0:
        {

            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
            bottomView.backgroundColor = CLEAR;

            UIImage *tryDetail_like = [UIImage imageNamed:@"tryDetail_like"];

            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20 - tryDetail_like.size.height/2, tryDetail_like.size.width, tryDetail_like.size.height)];
            imageView.image = tryDetail_like;

            [bottomView addSubview:imageView];

            UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.origin.x + imageView.frame.size.width + 5.0, 20 - 7.5, 60, 15)];
            lable.numberOfLines = 1;
            lable.text = @"活动说明";
            lable.textAlignment = NSTextAlignmentLeft;
            lable.backgroundColor = CLEAR;
            lable.textColor = BLACK;
            lable.font = TextFont;
            [bottomView addSubview:lable];

            return bottomView;


        }
            break;
        case 1:
        {

            if ([_datailOblect._itemTryNameList count] >0) {

                UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
                bottomView.backgroundColor = CLEAR;

                UIImage *tryDetail_scess = [UIImage imageNamed:@"tryDetail_scess"];

                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20 - tryDetail_scess.size.height/2, tryDetail_scess.size.width, tryDetail_scess.size.height)];
                imageView.image = tryDetail_scess;

                [bottomView addSubview:imageView];

                UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.origin.x + imageView.frame.size.width + 5.0, 20 - 7.5, 60, 15)];
                lable.numberOfLines = 1;
                lable.text = @"获奖名单";
                lable.textAlignment = NSTextAlignmentLeft;
                lable.backgroundColor = CLEAR;
                lable.textColor = BLACK;
                lable.font = TextFont;
                [bottomView addSubview:lable];


                UIView *writeBaseView = [[UIView alloc] initWithFrame:CGRectMake(225, 7.5, 80.0, 25)];
                writeBaseView.backgroundColor = TYR_BACKGRAYCOLOR;
                CALayer *layer = [writeBaseView layer];
                writeBaseView.hidden = YES;
                [layer setMasksToBounds:YES];
                [layer setCornerRadius:2.0];
                [layer setBorderWidth:1.0];
                [layer setBorderColor:[TYR_BACKGRAYCOLOR CGColor]];

                UIImage *tryDetail_write = [UIImage imageNamed:@"tryDetail_write"];
                UIImageView *write_image = [[UIImageView alloc] initWithFrame:CGRectMake(7, 25/2 - tryDetail_write.size.height/2, tryDetail_write.size.width, tryDetail_write.size.height)];
                write_image.image = tryDetail_write;

                [writeBaseView addSubview:write_image];

                UILabel *write_lable = [[UILabel alloc] initWithFrame:CGRectMake(write_image.frame.origin.x + write_image.frame.size.width + 3, 8.5, 50, 10)];
                write_lable.numberOfLines = 1;
                write_lable.text = @"写试用心得";
                write_lable.textAlignment = NSTextAlignmentCenter;
                write_lable.backgroundColor = CLEAR;
                write_lable.textColor = BLACK;
                write_lable.font = TextFonts;

                [writeBaseView addSubview:write_lable];

                UIButton *button   = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame       = writeBaseView.bounds;
                button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

                    [self.view bringSubviewToFront:self.mainView];
                    self.bottomButton.hidden    = NO;
                    [self.myTextView becomeFirstResponder];
                    return [RACSignal empty];
                    
                }];


                for (int i = 0; i < [_datailOblect._itemTryNameList count]; i++) {
                    iEver_successList_object *object = [_datailOblect._itemTryNameList objectAtIndex:i];
                    if (object.S_ID == [[[NSUserDefaults standardUserDefaults] objectForKey: @"userId"] integerValue]) {
                        writeBaseView.hidden = NO;
                    }
                }
                
                [writeBaseView addSubview:button];
                [bottomView addSubview:writeBaseView];



                return bottomView;
            }else{
                
                return nil;
            }

        }
            break;

        default:
            break;
    }


    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

    if (section == 0) {
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
        bottomView.backgroundColor = CLEAR;

        UIButton *applyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (_datailOblect._itemDetailObject.isApply>0) {
            [applyButton setTitle:@"已申请 - 修改资料" forState:UIControlStateNormal];
        }else{
            [applyButton setTitle:@"申请" forState:UIControlStateNormal];
        }

        [applyButton setTitleColor:MY_PURPLECOLOR forState:UIControlStateNormal];
        applyButton.backgroundColor = TYR_BACKGRAYCOLOR;
        applyButton.titleLabel.font = TextBigFont;
        applyButton.frame = bottomView.bounds;
        applyButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            /* apply modthod */
            iEver_applyItemViewController *_applyViewController = [[iEver_applyItemViewController alloc] initWithNibName:@"iEver_applyItemViewController" bundle:nil];
            _applyViewController.imageview = _datailOblect._itemDetailObject.itemImg;
            _applyViewController.name = _datailOblect._itemDetailObject.itemName;
            _applyViewController.price = [NSString stringWithFormat:@"RMB%d/%@",_datailOblect._itemDetailObject.price,_datailOblect._itemDetailObject.itemSpec];
            _applyViewController.itemTryId = _datailOblect._itemDetailObject.T_ID;
            _applyViewController.itemId = _datailOblect._itemDetailObject.itemId;
            [self.navigationController pushViewController:_applyViewController animated:YES];
            IOS7POP;
            return [RACSignal empty];
        }];

        [bottomView addSubview:applyButton];
        return bottomView;
    }else{

        return nil;
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    int sectionHeaderHeight = 0;
    switch (section) {
        case 0:{
            sectionHeaderHeight = 40.0;
        }break;
        case 1:{
            if ([_datailOblect._itemTryNameList count] > 0) {
                sectionHeaderHeight = 40.0;
            }else{
                sectionHeaderHeight = .0;
           }
        }break;
        default:
            break;
    }
    return sectionHeaderHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    NSString * timeStr = [iEver_Global intervalSinceNow:_datailOblect._itemDetailObject.endTime/1000];
    if (section == 0 && ![timeStr isEqualToString:@"已结束"]){

        return 40.0;
    }else{

        return 0.0;
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    if (scrollView == self.tableView)

    {

        CGFloat sectionHeaderHeight = 40.0;

        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {

            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);

        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {

            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
            
        }  
        
    }  
    
}
@end

//
//  iEver_askAnswerDetailObject.h
//  FaceTalk
//
//  Created by kevin on 15/5/31.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

/* question object  */
@interface iEver_question_object : NSObject

@property (nonatomic, assign) int                        question_id;                 /* 问题id */
@property (nonatomic, assign) int                        qCategoryId;                 /* 问题分类id */
@property (nonatomic, assign) int                        qUserId;                     /* 提问id */
@property (nonatomic, copy  ) NSString                   *qNickName;                  /* 用户昵称 */
@property (nonatomic, copy  ) NSString                   *qHeadImg;                   /* 用户头像 */
@property (nonatomic, copy  ) NSString                   *qTitle;                     /* 问题标题 */
@property (nonatomic, copy  ) NSString                   *qContent;                   /* 问题内容 */
@property (nonatomic, assign) int                        answerTotal;                 /* 问题总次数 */
@property (nonatomic, assign) int                        qStatus;                     /* 发布状态 */
@property (nonatomic, assign) long long                  releaseTime;                 /* 发布时间 */
@property (nonatomic, retain) NSMutableArray             *_quesPicList;               /* 达人分类数组 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;

@end

/* quesPic object  */
@interface iEver_quesPic_object : NSObject

@property (nonatomic, assign) int                        pic_id;                      /* 图片id */
@property (nonatomic, copy  ) NSString                   *imgUrl;                     /* 图片地址 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;

@end


/* answer object  */
@interface iEver_answer_object : NSObject

@property (nonatomic, assign) int                        answer_id;                  /* 问题id */
@property (nonatomic, assign) int                        qId_id;                     /* 答案回复的问题id */
@property (nonatomic, assign) int                        userId;                     /* 用户id */
@property (nonatomic, copy  ) NSString                   *aNickName;                 /* 用户昵称 */
@property (nonatomic, copy  ) NSString                   *aHeadImg;                  /* 用户头像 */
@property (nonatomic, assign) int                        userType;                   /* 用户类型 */
@property (nonatomic, copy  ) NSString                   *content;                   /* 问题内容 */
@property (nonatomic, assign) int                        commentTotal;               /* 评论次数 */
@property (nonatomic, assign) int                        praiseTotal;                /* 点赞个数 */
@property (nonatomic, assign) long long                  createTime;                 /* 发布时间 */
@property (nonatomic, retain) NSMutableArray             *_answerPicList;            /* 回复图片数组 */
@property (nonatomic, retain) NSMutableArray             *_answerCommentList;        /* 回复评论数组 */
@property (nonatomic, assign) BOOL                       _selected;                  /* 是否点赞 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;

@end

/* answerPicList object  */
@interface iEver_answerPic_object : NSObject

@property (nonatomic, assign) int                        pic_id;                      /* 回复图片id */
@property (nonatomic, copy  ) NSString                   *imgUrl;                     /* 回复图片地址 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;

@end

/* answerComment object  */
@interface iEver_answerComment_object : NSObject

@property (nonatomic, assign) int                        answerComment_id;           /* 回答评论id */
@property (nonatomic, assign) int                        userId;                     /* 用户id */
@property (nonatomic, copy  ) NSString                   *userNickName;              /* 用户昵称 */
@property (nonatomic, assign) int                        atUserId;                   /* @用户id */
@property (nonatomic, copy  ) NSString                   *atUserNickName;            /* @用户昵称 */
@property (nonatomic, copy  ) NSString                   *commentContent;            /* 图片内容 */
@property (nonatomic, assign) long long                  createTime;                 /* 发布时间 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;

@end




@interface iEver_askAnswerDetailObject : NSObject

@property (nonatomic, retain) NSMutableArray             *_answerDetailList;   /* 回答的数组 */
@property (nonatomic, assign) int                        answerPageSize;       /* answerPageSize */
@property (nonatomic, assign) int                        resultCode;           /* resultCode */
@property (nonatomic, retain) iEver_question_object      *questionObject;      /* question_object */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

/* 根据问题id查询问题详情，和第一页的回答评论，页数大于1时则不再查询问题详情 */
- (RACSignal *)expertQuestionQueryById:(NSDictionary *)dic path:(NSString *)pathUrl;


@end




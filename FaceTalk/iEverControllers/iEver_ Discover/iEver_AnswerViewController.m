//
//  iEver_AnswerViewController.m
//  FaceTalk
//
//  Created by kevin on 15/6/1.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_AnswerViewController.h"
#import "QiniuSimpleUploader.h"

#import "iEver_postQuestinTo_V_object.h"
#import "iEver_UserMessageObject.h"

@interface iEver_AnswerViewController ()<UIImagePickerControllerDelegate,
UINavigationControllerDelegate,UITextViewDelegate,QiniuUploadDelegate>
{
    UIImage *currentImage;
    NSString *fileName;
    NSMutableArray *picList;
    NSArray *contentPhoto_array;
    
    int currentButtonIndex;
    
}

@property (strong, nonatomic) QiniuSimpleUploader              *sUploader;
@property (strong, nonatomic) IBOutlet UIScrollView            *scrollView;

/* textView 区域 */
@property (strong) IBOutlet UIView                      *bottom_view;               /* baseTextView */
@property (strong) IBOutlet UITextView                  *textView;                  /* 文本视图 */
@property (strong) IBOutlet UILabel                     *AskStr;                    /* ASK */

/* 添加提问内容图片 区域 */
@property (strong) IBOutlet UIView                      *baseContentPhoto_view;     /* 内容图片View */
@property (strong) IBOutlet UIButton                    *contentPhoto1;             /* 内容图片1 */
@property (strong) IBOutlet UIButton                    *contentPhoto2;             /* 内容图片2 */
@property (strong) IBOutlet UIButton                    *contentPhoto3;             /* 内容图片3 */
@property (strong) IBOutlet UIButton                    *contentPhoto4;             /* 内容图片4 */
@property (strong) IBOutlet UIButton                    *contentPhoto5;             /* 内容图片5 */
@property (strong) IBOutlet UIButton                    *contentPhoto6;             /* 内容图片6 */
@property (strong) IBOutlet UIButton                    *contentPhoto7;             /* 内容图片7 */
@property (strong) IBOutlet UIButton                    *contentPhoto8;             /* 内容图片8 */
@property (strong) IBOutlet UIButton                    *contentPhoto9;             /* 内容图片9 */


- (IBAction)uploadContentPhoto:(id)sender;

@end


@implementation iEver_AnswerViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    /* scrollView  */
    [self backScrollView];
}


-(void)backScrollView {
    _scrollView.scrollEnabled = YES;
    CGSize newSize = CGSizeMake(self.view.frame.size.width, 770.0);
    [_scrollView setContentSize:newSize];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    picList = [[NSMutableArray alloc] initWithCapacity:0];
    self.view.backgroundColor = WHITE;
    self.title = @"ANSWER";
    
    /* fix custom text Font */
    [self customButtonFont];
    
    /* config view */
    self.scrollView.backgroundColor = CLEAR;
    
    /* 导航条左按钮 */
    [self customNavRightButton];
    
    /* 内容提问图片按钮 */
    contentPhoto_array =@[self.contentPhoto1,
                          self.contentPhoto2,
                          self.contentPhoto3,
                          self.contentPhoto4,
                          self.contentPhoto5,
                          self.contentPhoto6,
                          self.contentPhoto7,
                          self.contentPhoto8,
                          self.contentPhoto9];
    
    UIButton *photoBut = contentPhoto_array[0];
    photoBut.hidden = NO;
}

-(void)customNavRightButton {
    
    UIImage *PostAskView_send = [UIImage imageNamed:@"PostAskView_send"];
    UIButton *rightBarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBarBtn.frame = CGRectMake(0, 0, PostAskView_send.size.width, PostAskView_send.size.height);
    [rightBarBtn setImage:PostAskView_send forState:UIControlStateNormal];
    [rightBarBtn setImage:PostAskView_send forState:UIControlStateHighlighted];
    rightBarBtn.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        [self postQuestionTo];
        
        return [RACSignal empty];
    }];
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
}

-(void)customButtonFont {
    
    /* textView */
    self.AskStr.font = TextFont;
    self.textView.font = TextFont;
    CALayer *bottom_view_layer = [self.bottom_view layer];
    [bottom_view_layer setMasksToBounds:YES];
    [bottom_view_layer setCornerRadius:5.0];
    [bottom_view_layer setBorderWidth:0.5];
    [bottom_view_layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
}

- (IBAction)uploadContentPhoto:(id)sender {
    
    UIButton *currentButton = (UIButton *)sender;
    
    currentButtonIndex = currentButton.tag - 20;
    
    /* 调取相机 */
    [self.textView resignFirstResponder];
    @weakify(self);
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"拍照",@"从相册选择", nil];
    [actionSheet.rac_buttonClickedSignal subscribeNext:^(id x) {
        @strongify(self);
        
        NSInteger aIndex = [x integerValue];
        
        UIImagePickerController *myPicker  =[[UIImagePickerController alloc] init];
        myPicker.delegate = self;
        myPicker.allowsEditing = YES;
        
        if (aIndex == 0) {
            myPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            myPicker.navigationController.navigationBarHidden = YES;
            [self presentViewController:myPicker animated:YES completion:^{
                
            }];
        }else if (aIndex == 1) {
            myPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:myPicker animated:YES completion:^{
                
            }];
        }
    }];
    
    [actionSheet showInView:self.view.window];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info
{
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        //来自照相机的image，那么先保存
        UIImage *original_image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        UIImageWriteToSavedPhotosAlbum(original_image, self,
                                       nil,
                                       nil);
    }
    
    [self uploadContent:info];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}

#pragma mark -

- (void)uploadContent:(NSDictionary *)info {
    
    [SVProgressHUD showWithStatus:@"上传中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    @weakify(self);
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/getUpToken/quesImg/png"];
    [[content fetchPictureSiteToken:nil path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
        if ([[dic objectForKey:@"resultCode"] integerValue] == 1) {
            @strongify(self);
            
            self.sUploader = [QiniuSimpleUploader uploaderWithToken:[dic objectForKey:@"uptoken"]];
            self.sUploader.delegate = self;
            
            UIImage *image = [info objectForKey: @"UIImagePickerControllerEditedImage"];
            currentImage = image;
            NSString *key = [NSString stringWithFormat:@"%@", [dic objectForKey:@"fileName"]];
            NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:key];
            
            NSData *data = UIImageJPEGRepresentation(image, 0.5);
            [data writeToFile:filePath atomically:YES];
            [self.sUploader uploadFile:filePath key:[dic objectForKey:@"fullPath"] extra:nil];
            fileName = [dic objectForKey:@"fileName"];
            
        }else{
            
            NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[dic objectForKey:@"resultCode"] integerValue]];
            [SVProgressHUD showImage:Nil status:codeStr];
        }
    }];
}

#pragma mark -
- (void)uploadSucceeded:(NSString *)filePath ret:(NSDictionary *)ret {
    
    [SVProgressHUD showSuccessWithStatus:@"上传成功"];
    DLog(@"uploadSucceeded filePath: %@; ret: %@", filePath, ret);
    
    /* 上传成功 将 filName 写入图片数组，并显示在点击添加按钮上 添加成功后显示下一个按钮  */
    
    if (currentButtonIndex < picList.count) {
        
        [picList replaceObjectAtIndex:currentButtonIndex withObject:fileName];
    }else {
        
        [picList addObject:fileName];
        
        int nextButtonIndex = (picList.count == 9) ? 8 : picList.count;
        
        UIButton *photoBut = contentPhoto_array[nextButtonIndex];
        photoBut.hidden = NO;
        
    }
    [contentPhoto_array[currentButtonIndex] setImage:currentImage forState:UIControlStateNormal];
    
}

- (void)uploadFailed:(NSString *)filePath error:(NSError *)error {
    
    [SVProgressHUD showErrorWithStatus:@"上传失败"];
    DLog(@"uploadSucceeded filePath: %@; error: %@", filePath, error);
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/uploadErr"];
    NSDictionary *dic = @{@"error_log": [NSString stringWithFormat:@"%@",error]
                          };
    [[content uploadErr:dic path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
    }];
    
}
#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    [self.textView resignFirstResponder];
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidChange:(UITextView *)textView {
    
    NSInteger number = [textView.text length];
    if (number == 0) {
        self.AskStr.hidden = NO;
    }
    if (number > 0) {
        self.AskStr.hidden = YES;
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        [self postQuestionTo];
        return NO;
    }
    return YES;
}

#pragma mark
#pragma mark - post question to V
-(void)postQuestionTo{
    
    NSString *temp = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([temp isEqualToString:@""]) {
        [SVProgressHUD showImage:Nil status:@"请输入要咨询的问题内容"];
        return;
    }
    
    
    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_postQuestinTo_V_object * content = [[iEver_postQuestinTo_V_object alloc] init];
    NSDictionary *dic = @{@"qId": [NSNumber numberWithInt:self.q_id],
                          @"aUserType": [NSNumber numberWithInt:self.aUserType],
                          @"aContent": self.textView.text,
                          @"picList": picList
                          };
    NSLog(@"dic-------->%@",dic);
    [[[content answerQuestion:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"提交成功"];
             [self.navigationController popViewControllerAnimated:YES];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }
         
     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end

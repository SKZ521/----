//
//  iEver_VuserHomeViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/6/4.
//  Copyright (c) 2015年 iEver. All rights reserved.
//
//  ********* 达人/用户 主页 *******

// ******入口在base里面*********

#import "iEverBaseViewController.h"

@class iEver_personMessage_object;
@interface iEver_VuserHomeViewController : iEverBaseViewController

- (instancetype)initWithUserInfo:(iEver_personMessage_object *)userInfo userId:(NSString *)uId;

@end

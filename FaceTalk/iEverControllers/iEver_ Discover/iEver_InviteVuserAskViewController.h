//
//  iEver_InviteVuserAskViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/27.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"

#import "iEver_AskViewController.h"

@class iEver_AskViewController;

@interface iEver_InviteVuserAskViewController : iEverBaseTableViewController

@property (nonatomic, strong) iEver_AskViewController   *delegate;             /* 提问VC */

@end

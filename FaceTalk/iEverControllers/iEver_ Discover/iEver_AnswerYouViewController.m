//
//  iEver_AnswerYouViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/25.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_AnswerYouViewController.h"
#import "iEver_postQuestinTo_V_object.h"
#import "iEver_UserMessageObject.h"
#import "QiniuSimpleUploader.h"
#import "iEver_VuserUnAnswerdeViewController.h"
@interface iEver_AnswerYouViewController ()<UIImagePickerControllerDelegate,
UINavigationControllerDelegate,UITextViewDelegate,QiniuUploadDelegate>
{
    UIImage *headImage;
    NSString *fileName;
    NSMutableArray *picList;
}
@property (strong, nonatomic) QiniuSimpleUploader *sUploader;
@property (strong) IBOutlet UIScrollView *scrollView;
@property (strong) IBOutlet UIImageView  *V_personImage;
@property (strong) IBOutlet UILabel      *V_nameLable;

@property (strong) IBOutlet UITextView   *text2_lable;// 问题
@property (strong) IBOutlet UIView       *bottom_view;
@property (strong) IBOutlet UITextView   *textView;
@property (strong) IBOutlet UILabel      *text3_lable;//说点什么...
@property (strong) IBOutlet UILabel      *text4_lable;//140个字

@property (strong) IBOutlet UIButton     *addPhoto_butotn;
@property (strong) IBOutlet UIButton     *public_button;

- (IBAction)takePhotoAction:(id)sender;
- (IBAction)publicAction:(id)sender;

@end

@implementation iEver_AnswerYouViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /* scrollView  */
    [self backScrollView];
}
-(void)backScrollView{

    _scrollView.scrollEnabled = YES;
    CGSize newSize = CGSizeMake(self.view.frame.size.width, 668.0);
    [_scrollView setContentSize:newSize];

    CALayer *photoButton = [self.V_personImage layer];
    [photoButton setMasksToBounds:YES];
    [photoButton setCornerRadius:45.0/2];
    [self.V_personImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/45x",self.image]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    self.V_nameLable.text = [NSString stringWithFormat:@"%@的提问:",self.name];

    self.text2_lable.text  = self.question;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    picList = [[NSMutableArray alloc] initWithCapacity:0];
    self.view.backgroundColor = WHITE;
    self.title = @"ANSWER";

    /* fix custom text Font */

    [self customButtonFont];

    /* config view */
    self.scrollView.backgroundColor = CLEAR;

    [self.textView becomeFirstResponder];

    /*user image modify notification */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(headImageModifySusseed:) name:@"headImageModifySusseedNotification" object:nil];
}

-(void)customButtonFont {

    self.V_nameLable.font = TextFont;
    self.textView.font = TextFont;
    self.text3_lable.font = TextFont;
    self.text4_lable.font = TextDESCFonts;
    self.text2_lable.font = TextFont;
    CALayer *bottom_view_layer = [self.bottom_view layer];
    [bottom_view_layer setMasksToBounds:YES];
    [bottom_view_layer setCornerRadius:5.0];
    [bottom_view_layer setBorderWidth:0.5];
    [bottom_view_layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
}

/* 开启相册/相机 添加图片并且上传拿到图片地址 */
- (IBAction)takePhotoAction:(id)sender{

    [self.textView resignFirstResponder];
    @weakify(self);

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"拍照",@"从相册选择", nil];
    [actionSheet.rac_buttonClickedSignal subscribeNext:^(id x) {
        @strongify(self);

        NSInteger aIndex = [x integerValue];

        UIImagePickerController *myPicker  =[[UIImagePickerController alloc] init];
        myPicker.delegate = self;
        myPicker.allowsEditing = YES;

        if (aIndex == 0) {
            myPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            myPicker.navigationController.navigationBarHidden = YES;
            [self presentViewController:myPicker animated:YES completion:^{

            }];
        }else if (aIndex == 1) {
            myPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:myPicker animated:YES completion:^{

            }];
        }
    }];

    [actionSheet showInView:self.view.window];
}

#pragma mark -

- (void)uploadContent:(NSDictionary *)info
{
    [SVProgressHUD showWithStatus:@"上传中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    @weakify(self);
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/getUpToken/quesImg/png"];
    [[content fetchPictureSiteToken:nil path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
        if ([[dic objectForKey:@"resultCode"] integerValue] == 1) {
            @strongify(self);

            self.sUploader = [QiniuSimpleUploader uploaderWithToken:[dic objectForKey:@"uptoken"]];
            self.sUploader.delegate = self;

            UIImage *image = [info objectForKey: @"UIImagePickerControllerEditedImage"];
            NSString *key = [NSString stringWithFormat:@"%@", [dic objectForKey:@"fileName"]];
            NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:key];

            NSData *data = UIImageJPEGRepresentation(image, 1);
            [data writeToFile:filePath atomically:YES];
            [self.sUploader uploadFile:filePath key:[dic objectForKey:@"fullPath"] extra:nil];
            fileName = [dic objectForKey:@"fileName"];
            [picList addObject:fileName];
        }else{
            NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[dic objectForKey:@"resultCode"] integerValue]];
            [SVProgressHUD showImage:Nil status:codeStr];
        }
    }];
}


#pragma mark -
- (void)uploadSucceeded:(NSString *)filePath ret:(NSDictionary *)ret
{
    [SVProgressHUD showSuccessWithStatus:@"上传成功"];
    DLog(@"uploadSucceeded filePath: %@; ret: %@", filePath, ret);

}

- (void)uploadFailed:(NSString *)filePath error:(NSError *)error
{
    [self.addPhoto_butotn setImage:[UIImage imageNamed:@"fail_pic"] forState:UIControlStateNormal];
    [SVProgressHUD showErrorWithStatus:@"上传失败"];
    DLog(@"uploadSucceeded filePath: %@; error: %@", filePath, error);
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/uploadErr"];
    NSDictionary *dic = @{@"error_log": error
                          };
    [[content uploadErr:dic path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
    }];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info
{
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        //来自照相机的image，那么先保存
        UIImage *original_image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        UIImageWriteToSavedPhotosAlbum(original_image, self,
                                       nil,
                                       nil);
    }

    //获得图片,并上传
    UIImage *image = [info objectForKey: @"UIImagePickerControllerEditedImage"];
    [self uploadContent:info];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"headImageModifySusseedNotification" object:image];
    [self dismissViewControllerAnimated:YES completion:^{

    }];

}

/*user image modify notification  mothod*/
-(void)headImageModifySusseed:(NSNotification *)info{

    headImage = [info object];
    [self.addPhoto_butotn setImage:headImage forState:UIControlStateNormal];


}
/* 确认发布问题 */
- (IBAction)publicAction:(id)sender{

    [self postQuestionTo];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITextView Delegate Methods

- (void)textViewDidChange:(UITextView *)textView {

    NSInteger number = [textView.text length];
    if (number == 0) {
        self.text3_lable.hidden = NO;
    }

    if (number > 0) {
        self.text3_lable.hidden = YES;
    }

    if (number > 140) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"字符个数不能大于140" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        textView.text = [textView.text substringToIndex:140];
        number = 140;
    }
    self.text4_lable.text = [NSString stringWithFormat:@"%d/140个字",number];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        [self postQuestionTo];
        return NO;
    }
    return YES;
}

#pragma mark
#pragma mark - post question to V
-(void)postQuestionTo{

    NSString *temp = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([temp isEqualToString:@""]) {
        [SVProgressHUD showImage:Nil status:@"请输入要回答的内容"];
        return;
    }

    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_postQuestinTo_V_object * content = [[iEver_postQuestinTo_V_object alloc] init];
    NSDictionary *dic = @{@"aContent": self.textView.text,
                          @"id": [NSNumber numberWithInt:self.userId],
                          @"picList": picList
                          };
    [[[content answerQuestion:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"提交成功"];
             [self.navigationController popViewControllerAnimated:YES];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }
         
     }];
}


#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    [self.textView resignFirstResponder];
}
@end

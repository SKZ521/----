//
//  iEver_AskMeViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-10.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

@interface iEver_AskMeViewController : iEverBaseViewController
@property (nonatomic, assign) int                        userId;        /* id */
@property (nonatomic, copy)   NSString                   *name;         /* 达人昵称 */
@property (nonatomic, copy)   NSString                   *image;        /* 达人头像 */
@end

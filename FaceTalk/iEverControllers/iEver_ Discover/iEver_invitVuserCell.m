//
//  iEver_invitVuserCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/27.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_invitVuserCell.h"

@interface iEver_invitVuserCell ()

@property (nonatomic, strong) UIView        *wholeView;                   /* 整体背景 */

@property (nonatomic, strong) UIView        *baseHeadView;                /* 头像背景 */
@property (nonatomic, strong) UIButton      *photoButton;                 /* 头像 */
@property (nonatomic, strong) UIImageView   *userTypeSignImageView;       /* 用户类型 */
@property (nonatomic, strong) UILabel       *nameLable;                   /* 昵称 */
@property (nonatomic, strong) UILabel       *introLable;                  /* 用户简介 */
@property (nonatomic, strong) UILabel       *artAnsTotalLable;            /* 文章回答总量 */

@property (nonatomic, strong) UIView        *footView;                    /* 灰色背景 */

@property (nonatomic, strong) UIImageView   *checkImageView;              /* 是否选择图片 */

@property (nonatomic, strong) iEver_userList_object   *crrentObject;      /* cell当前对象 */

@end

@implementation iEver_invitVuserCell

/* 点击单元格选择达人用户 invitVuserUnChoose */
- (void)setChecked:(BOOL)checked{
    if (checked)
    {
        self.checkImageView.image = [UIImage imageNamed:@"invitVuserUnChoose"];

    }
    else
    {
        self.checkImageView.image = [UIImage imageNamed:@"invitVuserChoose"];
    }

}

- (void) configureCell {

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor whiteColor];

        [self.contentView addSubview:self.wholeView];

        [self.wholeView addSubview:self.baseHeadView];
        [self.wholeView addSubview:self.photoButton];
        [self.wholeView addSubview:self.userTypeSignImageView];
        [self.wholeView addSubview:self.nameLable];
        [self.wholeView addSubview:self.introLable];
        [self.wholeView addSubview:self.artAnsTotalLable];

        [self.wholeView addSubview:self.checkImageView];

        [self.wholeView addSubview:self.footView];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)awakeFromNib {
    // Initialization code
}


- (void)layoutSubviews {
    [super layoutSubviews];
    /*单元格frame */
     const CGRect r = self.contentView.bounds;

    UIImage *invitVuserUnChoose = [UIImage imageNamed:@"invitVuserUnChoose"];
    self.checkImageView.frame = CGRectMake(ScreenWidth - 16.0 - invitVuserUnChoose.size.width, r.size.height/2 - invitVuserUnChoose.size.height/2 - 5.0, invitVuserUnChoose.size.width, invitVuserUnChoose.size.height);
}

- (void)setObject:(iEver_userList_object *)object {

    /* Initialization size */
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 头像背景带边框视图 */
    x = 15.0;
    y = 13.0;
    width =  57.0;
    height = 57.0;
    self.baseHeadView.frame = CGRectMake(x, y, width, height);

    /* 头像按钮 */
    x = 18.0;
    y = 16.0;
    width =  51.0;
    height = 51.0;
    [self.photoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/102x",object.headImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    self.photoButton.frame = CGRectMake(x, y, width, height);


    /* 头像下面达人类型图标 */
    UIImage *MY_VuserType = [UIImage imageNamed:@"MY_VuserType"];

    x = 50.0;
    y = 55.0;
    width =  MY_VuserType.size.width;
    height = MY_VuserType.size.height;

    self.userTypeSignImageView.image = MY_VuserType;
    self.userTypeSignImageView.frame = CGRectMake(x, y, width, height);

    /* 达人昵称 */
    x = 85.0;
    y = 13.0;
    width = 180.0;
    height = 14.0;
    self.nameLable.frame = CGRectMake(x, y, width, height);
    self.nameLable.text = object.nickName;

    /* 达人简介 */
    x = 85.0;
    y = 35.0;
    width = 180.0;
    height = 14.0;
    self.introLable.frame = CGRectMake(x, y, width, height);
    self.introLable.text = object.intro;

    /* 达人相关数据 */
    x = 85.0;
    y = 57.0;
    width = 180.0;
    height = 14.0;
    self.artAnsTotalLable.frame = CGRectMake(x, y, width, height);
    self.artAnsTotalLable.text = [NSString stringWithFormat:@"经验（%d）| 回答（%d）",object.articleTotal,object.answerTotal];


    /* 整体view */
    x = 0.0;
    y = 0.0;
    width =  ScreenWidth;
    height = 90.0;
    self.wholeView.frame = CGRectMake(x, y, width, height);

    /* 底部view */
    x = 0.0;
    y = self.wholeView.frame.size.height - 5.0;
    width =  ScreenWidth;
    height = 5.0;
    self.footView.frame = CGRectMake(x, y, width, height);

    if (object.isSelect) {
        self.checkImageView.image = [UIImage imageNamed:@"invitVuserChoose"];
    }else {
        self.checkImageView.image = [UIImage imageNamed:@"invitVuserUnChoose"];
    }


}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    CGFloat cellHieght = 95.0;
    return cellHieght;
}


# pragma mark - view getters


/* 整体背景视图 */
- (UIView *)wholeView
{
    if (!_wholeView) {
        _wholeView = [[UIView alloc]init];
        _wholeView.backgroundColor = [UIColor whiteColor];
    }
    return _wholeView;
}

/* 头部背景圈圈 */
- (UIView *)baseHeadView
{
    if (!_baseHeadView) {
        _baseHeadView = [[UIView alloc]init];
        _baseHeadView.backgroundColor = [UIColor whiteColor];
        CALayer *baseHeadViewLayer = [_baseHeadView layer];
        [baseHeadViewLayer setMasksToBounds:YES];
        [baseHeadViewLayer setCornerRadius:57.0/2];
        [baseHeadViewLayer setBorderWidth:.5];
        [baseHeadViewLayer setBorderColor:[[UIColor grayColor] CGColor]];
    }
    return _baseHeadView;
}

/* 头像按钮 */
- (UIButton *)photoButton
{
    if (!_photoButton){
        _photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_photoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:51.0/2];
    }
    return _photoButton;
}


/* 头部用户类型标示 */
- (UIImageView *)userTypeSignImageView
{
    if (!_userTypeSignImageView) {
        _userTypeSignImageView = [[UIImageView alloc]init];
        _userTypeSignImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _userTypeSignImageView;
}

/* 昵称 */
- (UILabel *)nameLable
{
    if (!_nameLable){
        _nameLable = [[UILabel alloc] init];
        _nameLable.numberOfLines = 1;
        _nameLable.backgroundColor = CLEAR;
        _nameLable.textColor = BLACK;
        _nameLable.font = TextFont;
    }
    return _nameLable;
}

/* 用户简介 */
- (UILabel *)introLable
{
    if (!_introLable){
        _introLable = [[UILabel alloc] init];
        _introLable.numberOfLines = 1;
        _introLable.backgroundColor = CLEAR;
        _introLable.textColor = DAKEBLACK;
        _introLable.font = TextDESCFonts;
    }
    return _introLable;
}

/* 经验量回答量 */
- (UILabel *)artAnsTotalLable
{
    if (!_artAnsTotalLable){
        _artAnsTotalLable = [[UILabel alloc] init];
        _artAnsTotalLable.numberOfLines = 1;
        _artAnsTotalLable.backgroundColor = CLEAR;
        _artAnsTotalLable.textColor = DAKEBLACK;
        _artAnsTotalLable.font = TextDESCFonts;
    }
    return _artAnsTotalLable;
}

/* cell右部是否选中 */
- (UIImageView *)checkImageView
{
    if (!_checkImageView) {
        _checkImageView = [[UIImageView alloc]init];
        _checkImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _checkImageView;
}

/* 底部背景视图 */
- (UIView *)footView
{
    if (!_footView) {
        _footView = [[UIView alloc]init];
        _footView.backgroundColor = TYR_BACKGRAYCOLOR;
    }
    return _footView;
}
@end

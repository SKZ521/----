//
//  iEver_VuserShareArticleViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/25.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"
@class iEver_VuserCenterViewController;

enum
{
    Vpage_VuserShare_dataType       = 0,
    myPage_VuserShare_dataType      = 1,
    myPage_NuserLikeVuser_dataType  = 2,
    TAGseclect_dataType             = 3
};
typedef NSInteger VuserDataType;


typedef void (^ScrollUpBlock)();
typedef void (^ScrollDownBlock)();

@class iEver_V_userObject;

@interface iEver_VuserShareArticleViewController : iEverBaseTableViewController

@property(nonatomic,strong)iEver_V_userObject *object;

@property (assign) VuserDataType v_dataType;

@property (nonatomic, assign) int                        tagId;

@property(strong)UINavigationController *navigationController;

@property(strong)iEver_VuserCenterViewController *superController;

@property(nonatomic, copy)ScrollUpBlock upBlock;
@property(nonatomic, copy)ScrollDownBlock downBlock;

- (instancetype)initWithUserId:(NSString *)aId;

- (void)selectVuserShareFetchData:(int)VuserNumber;

- (void)resetViewFrameWithRect:(CGRect)rect;

@end

//
//  iEver_VuserCenterViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/25.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <QuartzCore/QuartzCore.h>

#import "ViewPagerController.h"

@interface iEver_VuserCenterViewController : ViewPagerController

- (void)selectVuserFetchData:(int)VuserNumber;

-(void)closeVuserInfoView;

-(void)openVuserInfoView;
@end

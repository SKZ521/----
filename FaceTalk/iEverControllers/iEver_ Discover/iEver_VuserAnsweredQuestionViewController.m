//
//  iEver_VuserAnsweredQuestionViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/25.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_VuserAnsweredQuestionViewController.h"
#import "iEver_V_answeredCell.h"
#import "iEver_V_userObject.h"
#import "iEver_VuserCenterViewController.h"

@interface iEver_VuserAnsweredQuestionViewController ()
{

}
@property(nonatomic, copy)NSString *userId;
@property(nonatomic, strong)NSMutableDictionary *sizeDictionary;
@end

@implementation iEver_VuserAnsweredQuestionViewController

- (instancetype)initWithUserId:(NSString *)aId
{
    if (self = [super init])
    {
        self.userId = aId;

        self.sizeDictionary = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark - View's Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = YES;

    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    [self fetchData];
}

- (void)resetViewFrameWithRect:(CGRect)rect
{
    self.view.frame = rect;

    CGRect tempFrame = self.tableView.frame;
    tempFrame.size.height = rect.size.height;
    self.tableView.frame = tempFrame;
}

/* get request data*/
- (void)fetchData
{
    // expertQuestion/queryAnswered/{answerUserId}/{currPage}
    __weak typeof(self) weakSelf = self;
    iEver_V_answeredQuestionObject *content = [[iEver_V_answeredQuestionObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"expertQuestion/queryAnswered/%@/%d",_userId,self.page];
    [[[content queryAnsweredByUser:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_V_answeredQuestionObject *object) {
         if (object.pageSize < weakSelf.page) {

             weakSelf.tableView.showsInfiniteScrolling = NO;
         }else{

             [weakSelf addDataToDataSource:object._quesList];
             [weakSelf.tableView reloadData];
         }
         [GiFHUD dismiss];
     }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Vuser_answerQuestionCell";
    iEver_V_answeredCell *cell      = (iEver_V_answeredCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (!cell)
    {
        cell = [[iEver_V_answeredCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    iEver_V_quesList_object * object = [self.dataSourceArray  objectAtIndex:indexPath.row];
    [cell setObject:object];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sizeKey = [NSString stringWithFormat:@"kIndex%d",[indexPath row]];
    NSNumber *heightNumber = [_sizeDictionary objectForKey:sizeKey];
    if (heightNumber)
    {
        return [heightNumber floatValue];
    } else {
        id object = [self.dataSourceArray objectAtIndex:indexPath.row];
        CGFloat height = [iEver_V_answeredCell heightForObject:object atIndexPath:indexPath tableView:tableView];

        [_sizeDictionary setObject:[NSNumber numberWithFloat:height] forKey:sizeKey];
        
        return height;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (scrollView.contentOffset.y <= - 12)
    {
        if (_downBlock)
            _downBlock();
    }

    if (scrollView.contentOffset.y >= 12)
    {
        if (_upBlock)
            _upBlock();
    }
}

@end

//
//  iEver_inviteVuserObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/27.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_inviteVuserObject : NSObject

@property (nonatomic, retain) NSMutableArray             *_userList;       /* 达人用户对象数组 */
@property (nonatomic, retain) NSMutableArray             *_categoryList;   /* 达人分类数组 */
@property (nonatomic, assign) int                        pageSize;         /* pageSize */
@property (nonatomic, assign) int                        resultCode;       /* resultCode */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

/* 根据达人问题回答量和文章浏览量排序查询所有达人 */
- (RACSignal *)expertUserQueryAll:(NSDictionary *)dic path:(NSString *)pathUrl;


@end

/* userList object  */
@interface iEver_userList_object : NSObject

@property (nonatomic, assign) int                        user_id;                      /* 用户id */
@property (nonatomic, copy  ) NSString                   *nickName;                    /* 用户昵称 */
@property (nonatomic, copy  ) NSString                   *headImg;                     /* 用户头像 */
@property (nonatomic, copy  ) NSString                   *intro;                       /* 用户简介 */
@property (nonatomic, assign) int                        answerTotal;                  /* 回答问题总次数 */
@property (nonatomic, assign) int                        pvAllTotal;                   /* 当前用户所有文章总浏览量 */
@property (nonatomic, assign) int                        articleTotal;                 /* 当前用户文章总数 */
@property (nonatomic, assign) int                        sortLevel;                    /* 达人排序值 */

@property (nonatomic, assign) BOOL                       isSelect;                     /* 是否选择 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;

@end


/* categoryList object  */
@interface iEver_categoryList_object : NSObject

@property (nonatomic, assign) int                        category_id;                      /* 分组id */
@property (nonatomic, copy  ) NSString                   *categoryName;                    /* 分组名称 */
@property (nonatomic, assign) int                        sortLevel;                        /* 分组排序值 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
@end

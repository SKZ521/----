//
//  iEver_AskViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/27.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_AskViewController.h"

#import "iEver_postQuestinTo_V_object.h"
#import "iEver_UserMessageObject.h"
#import "QiniuSimpleUploader.h"

#import "CNPGridMenu.h"

#import "iEver_InviteVuserAskViewController.h"

@interface iEver_AskViewController ()<UIImagePickerControllerDelegate,
UINavigationControllerDelegate,UITextViewDelegate,QiniuUploadDelegate,CNPGridMenuDelegate>
{
    UIImage *currentImage;
    NSString *fileName;
    NSMutableArray *picList;
    NSArray *contentPhoto_array;

    int currentButtonIndex;

    NSArray *topicName;
    int qCategoryId;
}

@property (strong, nonatomic) QiniuSimpleUploader              *sUploader;
@property (strong, nonatomic) IBOutlet UIScrollView            *scrollView;

/* 邀请用户回答view */
@property (strong) IBOutlet UIView                      *bsaeInviteVuserView;       /* 请求达人回答区域 */
@property (strong) IBOutlet UILabel                     *inviteAnswerStr;           /* 邀请回答文本 */
@property (strong) IBOutlet UILabel                     *inviteAnswerUserCountStr;  /* 邀请回答用户数量 */
@property (strong) IBOutlet UIButton                    *inviteAnswerUserPhotoBut1; /* 邀请用户头像1 */
@property (strong) IBOutlet UIButton                    *inviteAnswerUserPhotoBut2; /* 邀请用户头像2 */
@property (strong) IBOutlet UIButton                    *inviteAnswerUserPhotoBut3; /* 邀请用户头像3 */

/* 选择话题圈子区域 */
@property (strong) IBOutlet UIView                      *bsaeSelectTopicView;       /* 选择话题区域 */
@property (strong) IBOutlet UILabel                     *bsaeSelectTopicStr;        /* 选择话题文本 */
@property (strong) IBOutlet UILabel                     *bsaeSelectTopicLable;      /* 选中的话题 */

/* textView 区域 */
@property (strong) IBOutlet UIView                      *bottom_view;               /* baseTextView */
@property (strong) IBOutlet UITextView                  *textView;                  /* 文本视图 */
@property (strong) IBOutlet UILabel                     *AskStr;                    /* ASK */

/* 添加提问内容图片 区域 */
@property (strong) IBOutlet UIView                      *baseContentPhoto_view;     /* 内容图片View */
@property (strong) IBOutlet UIButton                    *contentPhoto1;             /* 内容图片1 */
@property (strong) IBOutlet UIButton                    *contentPhoto2;             /* 内容图片2 */
@property (strong) IBOutlet UIButton                    *contentPhoto3;             /* 内容图片3 */
@property (strong) IBOutlet UIButton                    *contentPhoto4;             /* 内容图片4 */
@property (strong) IBOutlet UIButton                    *contentPhoto5;             /* 内容图片5 */
@property (strong) IBOutlet UIButton                    *contentPhoto6;             /* 内容图片6 */
@property (strong) IBOutlet UIButton                    *contentPhoto7;             /* 内容图片7 */
@property (strong) IBOutlet UIButton                    *contentPhoto8;             /* 内容图片8 */
@property (strong) IBOutlet UIButton                    *contentPhoto9;             /* 内容图片9 */


- (IBAction)intoVuserInviteVCAction:(id)sender;

- (IBAction)intoSeclectTopicVCAction:(id)sender;

- (IBAction)uploadContentPhoto:(id)sender;

@end

@implementation iEver_AskViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    /* scrollView  */
    [self backScrollView];
}

-(void)delegateWithInviteVC {

    NSArray *inviteAnswerUserPhotoButArray = @[self.inviteAnswerUserPhotoBut1,
                                               self.inviteAnswerUserPhotoBut2,
                                               self.inviteAnswerUserPhotoBut3];
    
    for (int i = 0; i < self.inviteVuserHeadimage.count; i++) {

        CALayer *layer = [inviteAnswerUserPhotoButArray[i] layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:37.0/2];
        [inviteAnswerUserPhotoButArray[i] setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/74x",self.inviteVuserHeadimage[i]]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    }
}

-(void)backScrollView {
    _scrollView.scrollEnabled = YES;
    CGSize newSize = CGSizeMake(self.view.frame.size.width, 770.0);
    [_scrollView setContentSize:newSize];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    picList = [[NSMutableArray alloc] initWithCapacity:0];
    self.inviteVuserHeadimage = [[NSMutableArray alloc] initWithCapacity:0];
    self.inviteVuserID = [[NSMutableArray alloc] initWithCapacity:0];
    self.view.backgroundColor = WHITE;
    self.title = @"ASK";

    /* fix custom text Font */
    [self customButtonFont];

    /* config view */
    self.scrollView.backgroundColor = CLEAR;

    /* 导航条左按钮 */
    [self customNavRightButton];

    /* 内容提问图片按钮 */
    contentPhoto_array =@[self.contentPhoto1,
                          self.contentPhoto2,
                          self.contentPhoto3,
                          self.contentPhoto4,
                          self.contentPhoto5,
                          self.contentPhoto6,
                          self.contentPhoto7,
                          self.contentPhoto8,
                          self.contentPhoto9];

    UIButton *photoBut = contentPhoto_array[0];
    photoBut.hidden = NO;

}

-(void)customNavRightButton {

    UIImage *PostAskView_send = [UIImage imageNamed:@"PostAskView_send"];
    UIButton *rightBarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBarBtn.frame = CGRectMake(0, 0, PostAskView_send.size.width, PostAskView_send.size.height);
    [rightBarBtn setImage:PostAskView_send forState:UIControlStateNormal];
    [rightBarBtn setImage:PostAskView_send forState:UIControlStateHighlighted];
    rightBarBtn.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        [self postQuestionTo];

        return [RACSignal empty];
    }];

    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
}

-(void)customButtonFont {

    /* 邀请用户回答区域 */
    self.bsaeInviteVuserView.backgroundColor = TYR_BACKGRAYCOLOR;
    self.inviteAnswerStr.font = TextFont;
    self.inviteAnswerUserCountStr.font = TextDESCFonts;

    /* 选择问题话题 */
    self.bsaeSelectTopicView.backgroundColor = TYR_BACKGRAYCOLOR;
    self.bsaeSelectTopicStr.font = TextFont;
    self.bsaeSelectTopicLable.font = TextFont;

    /* textView */
     self.AskStr.font = TextFont;
    self.textView.font = TextFont;
    CALayer *bottom_view_layer = [self.bottom_view layer];
    [bottom_view_layer setMasksToBounds:YES];
    [bottom_view_layer setCornerRadius:5.0];
    [bottom_view_layer setBorderWidth:0.5];
    [bottom_view_layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
}


- (IBAction)intoVuserInviteVCAction:(id)sender {

    [self.textView resignFirstResponder];

    iEver_InviteVuserAskViewController *_askMeViewController = [[iEver_InviteVuserAskViewController alloc]init];

    [self.inviteVuserHeadimage removeAllObjects];
    [self.inviteVuserID removeAllObjects];

    NSArray *inviteAnswerUserPhotoButArray = @[self.inviteAnswerUserPhotoBut1,
                                               self.inviteAnswerUserPhotoBut2,
                                               self.inviteAnswerUserPhotoBut3];

    for (int i = 0; i < inviteAnswerUserPhotoButArray.count; i++) {

        [inviteAnswerUserPhotoButArray[i] setImage:[UIImage imageNamed:@"PostAskView_addV"] forState:UIControlStateNormal];
    }
    _askMeViewController.delegate = self;
    _askMeViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:_askMeViewController animated:YES];
    IOS7POP;

}

- (IBAction)intoSeclectTopicVCAction:(id)sender {

    [self.textView resignFirstResponder];

    NSMutableArray * itemArray = [[NSMutableArray alloc] init];
             topicName = @[@"长草拔草",
                           @"体重不过百",
                           @"治理大油田",
                           @"拯救干燥肌",
                           @"拯救赤痘粽",
                           @"脸你怎么了",
                           @"驾驭敏感肌肤",
                           @"混合肌分区护理",
                           @"美白是王道",
                           @"明眸看世界",
                           @"小白爱打扮",
                           @"脑洞太大",
                           @"路人转粉",
                           @"平价党专区",
                           @"字母霜爱好者",
                           @"隔绝脏空气",
                           @"心机唇妆",
                           @"生命解码",
                           @"肌肤细节不粗糙",
                           @"底妆全攻略"];

    for (int i = 0; i< [topicName count]; i++) {
        CNPGridMenuItem *topicItem = [[CNPGridMenuItem alloc] init];
        topicItem.icon = [UIImage imageNamed:[NSString stringWithFormat:@"topic_icon%d",i+1]];
        topicItem.title = topicName[i];
        [itemArray addObject:topicItem];
    }

    CNPGridMenu *gridMenu = [[CNPGridMenu alloc] initWithMenuItems:itemArray];
    gridMenu.delegate = self;
    [self presentGridMenu:gridMenu animated:YES completion:^{
        NSLog(@"Grid Menu Presented");
    }];


}

- (IBAction)uploadContentPhoto:(id)sender {

    UIButton *currentButton = (UIButton *)sender;

    currentButtonIndex = currentButton.tag - 20;

    /* 调取相机 */
    [self.textView resignFirstResponder];
    @weakify(self);

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"拍照",@"从相册选择", nil];
    [actionSheet.rac_buttonClickedSignal subscribeNext:^(id x) {
        @strongify(self);

        NSInteger aIndex = [x integerValue];

        UIImagePickerController *myPicker  =[[UIImagePickerController alloc] init];
        myPicker.delegate = self;
        myPicker.allowsEditing = YES;

        if (aIndex == 0) {
            myPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            myPicker.navigationController.navigationBarHidden = YES;
            [self presentViewController:myPicker animated:YES completion:^{

            }];
        }else if (aIndex == 1) {
            myPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:myPicker animated:YES completion:^{

            }];
        }
    }];
    
    [actionSheet showInView:self.view.window];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info
{
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        //来自照相机的image，那么先保存
        UIImage *original_image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        UIImageWriteToSavedPhotosAlbum(original_image, self,
                                       nil,
                                       nil);
    }

    [self uploadContent:info];
    [self dismissViewControllerAnimated:YES completion:^{

    }];
    
}

#pragma mark -

- (void)uploadContent:(NSDictionary *)info {

    [SVProgressHUD showWithStatus:@"上传中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    @weakify(self);
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/getUpToken/quesImg/png"];
    [[content fetchPictureSiteToken:nil path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
        if ([[dic objectForKey:@"resultCode"] integerValue] == 1) {
            @strongify(self);

            self.sUploader = [QiniuSimpleUploader uploaderWithToken:[dic objectForKey:@"uptoken"]];
            self.sUploader.delegate = self;

            UIImage *image = [info objectForKey: @"UIImagePickerControllerEditedImage"];
            currentImage = image;
            NSString *key = [NSString stringWithFormat:@"%@", [dic objectForKey:@"fileName"]];
            NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:key];

            NSData *data = UIImageJPEGRepresentation(image, 0.5);
            [data writeToFile:filePath atomically:YES];
            [self.sUploader uploadFile:filePath key:[dic objectForKey:@"fullPath"] extra:nil];
            fileName = [dic objectForKey:@"fileName"];

        }else{

            NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[dic objectForKey:@"resultCode"] integerValue]];
            [SVProgressHUD showImage:Nil status:codeStr];
        }
    }];
}

#pragma mark -
- (void)uploadSucceeded:(NSString *)filePath ret:(NSDictionary *)ret {

    [SVProgressHUD showSuccessWithStatus:@"上传成功"];
    DLog(@"uploadSucceeded filePath: %@; ret: %@", filePath, ret);

     /* 上传成功 将 filName 写入图片数组，并显示在点击添加按钮上 添加成功后显示下一个按钮  */

    if (currentButtonIndex < picList.count) {

        [picList replaceObjectAtIndex:currentButtonIndex withObject:fileName];
    }else {

        [picList addObject:fileName];

        int nextButtonIndex = (picList.count == 9) ? 8 : picList.count;

        UIButton *photoBut = contentPhoto_array[nextButtonIndex];
        photoBut.hidden = NO;

    }
    [contentPhoto_array[currentButtonIndex] setImage:currentImage forState:UIControlStateNormal];

}

- (void)uploadFailed:(NSString *)filePath error:(NSError *)error {

    [SVProgressHUD showErrorWithStatus:@"上传失败"];
    DLog(@"uploadSucceeded filePath: %@; error: %@", filePath, error);
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/uploadErr"];
    NSDictionary *dic = @{@"error_log": [NSString stringWithFormat:@"%@",error]
                          };
    [[content uploadErr:dic path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
    }];

}

#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    [self.textView resignFirstResponder];
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidChange:(UITextView *)textView {

    NSInteger number = [textView.text length];
    if (number == 0) {
        self.AskStr.hidden = NO;
    }
    if (number > 0) {
        self.AskStr.hidden = YES;
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        [self postQuestionTo];
        return NO;
    }
    return YES;
}

#pragma mark
#pragma mark - post question to V
-(void)postQuestionTo{

    NSString *temp = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([temp isEqualToString:@""]) {
        [SVProgressHUD showImage:Nil status:@"请输入要咨询的问题内容"];
        return;
    }

    if (qCategoryId == 0) {
        [SVProgressHUD showImage:Nil status:@"请选择问题所属的圈子"];
        return;
    }

    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_postQuestinTo_V_object * content = [[iEver_postQuestinTo_V_object alloc] init];
    NSDictionary *dic = @{@"qCategoryId": [NSNumber numberWithInt:qCategoryId],
                          @"qContent": self.textView.text,
                          @"inviteeUserId": self.inviteVuserID,
                          @"picList": picList
                          };
    [[[content expertQuestion:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"提交成功"];
             [self.navigationController popViewControllerAnimated:YES];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

     }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -
#pragma mark - CNPGridMenuDelegate

- (void)gridMenuDidTapOnBackground:(CNPGridMenu *)menu {
    [self dismissGridMenuAnimated:YES completion:^{
        NSLog(@"Grid Menu Dismissed With Background Tap");
    }];
}

- (void)gridMenu:(CNPGridMenu *)menu didTapOnItem:(CNPGridMenuItem *)item {
    [self dismissGridMenuAnimated:YES completion:^{
        NSLog(@"Grid Menu Did Tap On Item: %@", item.title);
        self.bsaeSelectTopicLable.text = item.title;
        qCategoryId = [topicName indexOfObject:item.title];
    }];
}


@end

//
//  iEver_AskViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/27.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

@interface iEver_AskViewController : iEverBaseViewController

@property (nonatomic, strong) NSMutableArray   *inviteVuserHeadimage;      /* 邀请达人头像 */

@property (nonatomic, strong) NSMutableArray   *inviteVuserID;             /* 邀请达人ID */

-(void)delegateWithInviteVC;

@end

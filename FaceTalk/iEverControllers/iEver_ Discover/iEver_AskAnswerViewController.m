//
//  iEver_AskAnswerViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/23.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_AskAnswerViewController.h"
#import "DAPagesContainer.h"
#import "REMenu.h"
#import "CNPGridMenu.h"

#import "iEver_AskAswerPagesTableViewController.h"

#import "iEver_AskViewController.h"

@interface iEver_AskAnswerViewController () <DAPagesContainerDelegate,CNPGridMenuDelegate>

{

    UILabel *askTypeLable;
}

@property (strong, nonatomic) DAPagesContainer *pagesContainer;

@property (strong, nonatomic) REMenu *menu;

@property (strong, nonatomic) iEver_AskAswerPagesTableViewController *currentTableViewController;

@property (nonatomic, strong) CNPGridMenu *gridMenu;

@end

@implementation iEver_AskAnswerViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"问答";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@""
                                                        image:[UIImage imageNamed:@"tabbar_discover_normal"]
                                                selectedImage:[UIImage imageNamed:@"tabbar_discover_click"]];
    }
    return self;
}

- (void)showGridMenu:(id)sender {

    NSMutableArray * itemArray = [[NSMutableArray alloc] init];
    NSArray *topicName = @[@"长草拔草",
                           @"体重不过百",
                           @"治理大油田",
                           @"拯救干燥肌",
                           @"拯救赤痘粽",
                           @"脸你怎么了",
                           @"驾驭敏感肌肤",
                           @"混合肌分区护理",
                           @"美白是王道",
                           @"明眸看世界",
                           @"小白爱打扮",
                           @"脑洞太大",
                           @"路人转粉",
                           @"平价党专区",
                           @"字母霜爱好者",
                           @"隔绝脏空气",
                           @"心机唇妆",
                           @"生命解码",
                           @"肌肤细节不粗糙",
                           @"底妆全攻略"];

    for (int i = 0; i< [topicName count]; i++) {
        CNPGridMenuItem *topicItem = [[CNPGridMenuItem alloc] init];
        topicItem.icon = [UIImage imageNamed:[NSString stringWithFormat:@"topic_icon%d",i+1]];
        topicItem.title = topicName[i];
        [itemArray addObject:topicItem];
    }

    CNPGridMenu *gridMenu = [[CNPGridMenu alloc] initWithMenuItems:itemArray];
    gridMenu.delegate = self;
    [self presentGridMenu:gridMenu animated:YES completion:^{
        NSLog(@"Grid Menu Presented");
    }];

}


- (void)TapcenterBarView:(UITapGestureRecognizer *)gesture {

    if (_menu.isOpen)
        return [_menu close];

    REMenuItem *allItem = [[REMenuItem alloc] initWithTitle:@"全 部"
                                                   subtitle:nil
                                                      image:nil
                                           highlightedImage:nil
                                                     action:^(REMenuItem *item) {
                                                         NSLog(@"Item: %@", item);
                                                         askTypeLable.text = item.title;
                                                         [self.currentTableViewController.dataSourceArray removeAllObjects];

                                                         [self.currentTableViewController.tableView setContentOffset:CGPointMake(0,0) animated:NO];
                                                         self.currentTableViewController.page = 1;
                                                         [self.currentTableViewController fetchData];
                                                     }];

    REMenuItem *noReplyItem = [[REMenuItem alloc] initWithTitle:@"未回答"
                                                       subtitle:nil
                                                          image:nil
                                               highlightedImage:nil
                                                         action:^(REMenuItem *item) {
                                                             NSLog(@"Item: %@", item);
                                                             askTypeLable.text = item.title;
                                                             [self.currentTableViewController.dataSourceArray removeAllObjects];
                                                             [self.currentTableViewController.tableView setContentOffset:CGPointMake(0,0) animated:NO];
                                                             self.currentTableViewController.page = 1;
                                                             [self.currentTableViewController fetchData];
                                                         }];

    REMenuItem *haveReplyItem = [[REMenuItem alloc] initWithTitle:@"已回答"
                                                         subtitle:nil
                                                            image:nil
                                                 highlightedImage:nil
                                                           action:^(REMenuItem *item) {
                                                               NSLog(@"Item: %@", item);
                                                               askTypeLable.text = item.title;
                                                               [self.currentTableViewController.dataSourceArray removeAllObjects];
                                                               [self.currentTableViewController.tableView setContentOffset:CGPointMake(0,0) animated:NO];
                                                               self.currentTableViewController.page = 1;
                                                               [self.currentTableViewController fetchData];
                                                           }];

    allItem.tag = 0;
    noReplyItem.tag = 1;
    haveReplyItem.tag = 2;
    _menu = [[REMenu alloc] initWithItems:@[allItem, noReplyItem, haveReplyItem]];


    [_menu showFromNavigationController:self.navigationController];

}

-(void)addNavBar {

    /* 标题“问答” */
    UIView *centerBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 44)];
    UILabel *askTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 40, 25)];
    askTitleLable.backgroundColor = CLEAR;
    askTitleLable.font = TextBigFont;
    askTitleLable.textColor = BLACK;
    askTitleLable.text = @"问答";
    [centerBarView addSubview:askTitleLable];

    /* 标题傍边的选择回答类型 */
    askTypeLable = [[UILabel alloc] initWithFrame:CGRectMake(45, 12, 45, 25)];
    askTypeLable.backgroundColor = CLEAR;
    askTypeLable.font = TextFont;
    askTypeLable.textColor = BLACK;
    askTypeLable.text = @"全 部";
    [centerBarView addSubview:askTypeLable];

    /* 标题傍边的指示小箭头 */
    UIImage *VuserCenterNav_arrow = [UIImage imageNamed:@"VuserCenterNav_arrow"];
    UIImageView *titleSignImageView = [[UIImageView alloc] initWithFrame:CGRectMake(87, 25, VuserCenterNav_arrow.size.width, VuserCenterNav_arrow.size.height)];
    titleSignImageView.image = VuserCenterNav_arrow;
    [centerBarView addSubview:titleSignImageView];

    centerBarView.backgroundColor = CLEAR;
    self.navigationItem.titleView = centerBarView;

    centerBarView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapcenterBarView:)];
    tapGesture.numberOfTapsRequired = 1;
    [centerBarView addGestureRecognizer:tapGesture];

    /* 导航条右按钮 */
    UIImage *askAnswerNav_ask = [UIImage imageNamed:@"askAnswerNav_ask"];
    UIButton *rightBarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBarBtn.frame = CGRectMake(0, 0, askAnswerNav_ask.size.width, askAnswerNav_ask.size.height);
    [rightBarBtn setImage:askAnswerNav_ask forState:UIControlStateNormal];
    [rightBarBtn setImage:askAnswerNav_ask forState:UIControlStateHighlighted];
    rightBarBtn.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {


        iEver_AskViewController *_askMeViewController = [[iEver_AskViewController alloc]init];

        _askMeViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:_askMeViewController animated:YES];
         IOS7POP;

        return [RACSignal empty];
    }];

    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;

}

-(void)topicPagesContainer {

    self.pagesContainer = [[DAPagesContainer alloc] init];
    [self.pagesContainer willMoveToParentViewController:self];
    self.pagesContainer.view.frame = self.view.bounds;

    self.pagesContainer.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pagesContainer.delegate = self;
    [self.view addSubview:self.pagesContainer.view];

    /* 展拉开选择更多话题分类 */
    UIView *selectTopicView = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth - 44.0, 0, 44.0, 36.0)];

    UIImage *askAnswerMain_moreTopic_image = [UIImage imageNamed:@"askAnswerMain_moreTopic"];
    UIButton *selectTopicButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectTopicButton.frame  = CGRectMake(8., 0., 36.0, 36.0);
    [selectTopicButton setImage:askAnswerMain_moreTopic_image forState:UIControlStateNormal];
    selectTopicButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        [self showGridMenu:input];
        return [RACSignal empty];
    }];
    [selectTopicView addSubview:selectTopicButton];


    selectTopicView.backgroundColor = SpecialBannerCOLOR_1;
    [self.pagesContainer.view addSubview:selectTopicView];

    [self.pagesContainer didMoveToParentViewController:self];

    NSMutableArray *pageViewControllers = [[NSMutableArray alloc] initWithCapacity:0];
    NSArray *pagesArray = @[@"全部",
                            @"长草拔草死循环",
                            @"体重不过百",
                            @"治理大油田",
                            @"拯救干燥肌",
                            @"拒绝赤痘粽",
                            @"驾驭敏感肌",
                            @"混合肌分区护理",
                            @"美白是王道",
                            @"明眸看世界",
                            @"小白爱打扮",
                            @"脑洞太大",
                            @"路人转粉",
                            @"平价党专属",
                            @"字母霜爱好者",
                            @"隔绝脏空气",
                            @"心机唇妆必看",
                            @"生命解码",
                            @"肌肤细节不粗糙",
                            @"底妆全攻略",];
    int pages = [pagesArray count];

    for (int i = 0; i < pages; i++) {
        iEver_AskAswerPagesTableViewController *pageViewController = [[iEver_AskAswerPagesTableViewController alloc] initWithNibName:nil
                                                                                                                              bundle:nil
                                                                                                                              rootVC:self];
        pageViewController.title = [NSString stringWithFormat:@"%@", pagesArray[i]];

        if (i == 0) {
            self.currentTableViewController = pageViewController;
        }
        [pageViewControllers addObject:pageViewController];
    }

    self.pagesContainer.viewControllers = pageViewControllers;

    [self.currentTableViewController fetchData];

}


- (void)viewDidLoad {

    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.navigationItem.leftBarButtonItem = nil;

    [self addNavBar];

    [self topicPagesContainer];

    self.view.backgroundColor = WHITE;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DAPagesContainerDelegate

- (void)refreshData:(NSInteger)page
{
    NSLog(@"current page ----> %d",page);

    self.currentTableViewController = (iEver_AskAswerPagesTableViewController *)[self.pagesContainer.viewControllers objectAtIndex:page];

    //因为接口写的顺序比较二,这里只能二的转换一次
    if (page == 0) {
        page = 7;
    }else if (page == 1) {
        page = 0;
    }else if (page == 2) {
        page = 1;
    }else if (page == 3) {
        page = 6;
    }else if (page == 4) {
        page = 2;
    }else if (page == 5) {
        page = 4;
    }else if (page == 6) {
        page = 3;
    }else if (page == 7) {
        page = 8;
    }

    self.currentTableViewController.currentType = page + 1;
    [self.currentTableViewController manualRefreh];
}

#pragma mark -
#pragma mark - CNPGridMenuDelegate

- (void)gridMenuDidTapOnBackground:(CNPGridMenu *)menu {
    [self dismissGridMenuAnimated:YES completion:^{
        NSLog(@"Grid Menu Dismissed With Background Tap");
    }];
}

- (void)gridMenu:(CNPGridMenu *)menu didTapOnItem:(CNPGridMenuItem *)item {
    [self dismissGridMenuAnimated:YES completion:^{
        NSLog(@"Grid Menu Did Tap On Item: %@", item.title);
    }];
}


#pragma mark -	EVENT

- (void)notifyAction:(UIButton *)button
{
    [self.pagesContainer setSelectedIndex:7 animated:YES];
    [self refreshData:7];
}


@end

//
//  iEver_AskAswerPagesTableViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/23.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_AskAswerPagesTableViewController.h"
#import "iEver_AskAnswerViewController.h"
#import "iEver_AskAnswerDetailViewController.h"

#import "iEver_expertQuestionObject.h"
#import "iEver_PageAskAnswerCell.h"

@interface iEver_AskAswerPagesTableViewController ()
{
    iEver_AskAnswerViewController *_askAnswerVC;
}

@end

@implementation iEver_AskAswerPagesTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil rootVC:(UIViewController *)rootVC
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _askAnswerVC = (iEver_AskAnswerViewController *)rootVC;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //全部
//    _currentType = 8;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.dataSourceArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdetify = @"PageAskAnswerCell";
    iEver_PageAskAnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdetify];
    if (!cell) {
            cell = [[iEver_PageAskAnswerCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdetify];
    }

    if ([self.dataSourceArray count] > 0) {
        iEver_quesList_object *content_object = (iEver_quesList_object *)[self.dataSourceArray objectAtIndex:indexPath.row];
        [cell setObject:content_object];
    }
    return cell;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    iEver_quesList_object *content_object = (iEver_quesList_object *)[self.dataSourceArray objectAtIndex:indexPath.row];

    return [iEver_PageAskAnswerCell heightForObject:content_object atIndexPath:indexPath tableView:tableView];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    iEver_quesList_object *content_object = (iEver_quesList_object *)[self.dataSourceArray objectAtIndex:indexPath.row];
    iEver_AskAnswerDetailViewController *_AskAnswerDetailViewController = [[iEver_AskAnswerDetailViewController alloc]init];
    _AskAnswerDetailViewController.title = [NSString stringWithFormat:@"%@的问题",content_object.qNickName];
    _AskAnswerDetailViewController.question_id = content_object.ques_id;
    _AskAnswerDetailViewController.hidesBottomBarWhenPushed = YES;
    if (_currentType == iEverPageShowTypeDefault)
    {
        [self.navigationController pushViewController:_AskAnswerDetailViewController animated:YES];
    }
    else {

        [_askAnswerVC.navigationController pushViewController:_AskAnswerDetailViewController animated:YES];
    }

    IOS7POP;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"didSelectRowAtIndexPath");
}

 //刷新数据
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)fetchData
{
    [super fetchData];

     @weakify(self);
    iEver_expertQuestionObject *expertQuestionObject = [[iEver_expertQuestionObject alloc] init];
    NSString *_pathUrl = nil;
    if (_currentType == iEverPageShowTypeDefault)
    {
        _pathUrl = [NSString stringWithFormat:@"expertQuestion/queryAskByUser/%@/%d",[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],self.page];
    } else {
        _pathUrl = [NSString stringWithFormat:@"/expertQuestion/queryAll/0/0/1"];
    }
    [[[expertQuestionObject expertQuestionQueryAll:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_expertQuestionObject *object) {


         @strongify(self);
         if (object.pageSize < self.page) {
             self.tableView.showsInfiniteScrolling = NO;
             self.tableView.showsPullToRefresh = NO;
         }else{

             [self addDataToDataSource:object._quesList];
             [self.tableView reloadData];
             
         }

         [self nothingView];

     }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

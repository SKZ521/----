//
//  iEver_VuserShareArticleViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/25.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_VuserShareArticleViewController.h"
#import "iEver_V_ListDetailViewController.h"
#import "iEver_PerCener_nana_cell.h"
#import "iEver_V_userObject.h"
#import "iEver_VuserCenterViewController.h"

@interface iEver_VuserShareArticleViewController ()
{

    long long                timeDate;
    iEver_V_userObject     * _object;
    
    int                     _VuserNumber;

    NSString * combination_URL;

    CGFloat currentOffset;// 记录列表当前滚动的位置
}

@property(nonatomic, copy)NSString *userId;
@end

@implementation iEver_VuserShareArticleViewController

- (instancetype)initWithUserId:(NSString *)aId
{
    if (self = [super init])
    {
        self.userId = aId;
    }
    return self;
}

#pragma mark - View's Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = YES;

    self.tableView.separatorColor = CLEAR;

    [self fetchData];
}

- (void)resetViewFrameWithRect:(CGRect)rect
{
    self.view.frame = rect;
    
    CGRect tempFrame = self.tableView.frame;
    tempFrame.size.height = rect.size.height;
    self.tableView.frame = tempFrame;
}

- (void)setObject:(iEver_V_userObject *)object
{
    _object = object;
    
    _VuserNumber  = 0;

    @weakify(self);

    if ([self.dataSourceArray count] > 0 )
    {
        iEver_discoverContentList_object *content_object = [self.dataSourceArray objectAtIndex:0];
        timeDate                                         = content_object.releaseTime;

    }else{
        timeDate = 0;
    }

    iEver_discover_userList_object * userObject = (iEver_discover_userList_object *)[object._userList objectAtIndex:_VuserNumber];

    iEver_discoverObject *content = [[iEver_discoverObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"expertArticle/queryByExpertUser/%d/%d/%lld",userObject.User_id,self.page,timeDate];
    [[[content expertArticle:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_discoverObject *object) {
         @strongify(self);

         if (object.pageSize < self.page) {
             self.tableView.showsInfiniteScrolling = NO;
         }else{

             [self addDataToDataSource:object._discoverList];

             [self.tableView  reloadData];
         }

         [GiFHUD dismiss];
     }
     ];
}

/* get request data*/
- (void)fetchData
{
    @weakify(self);

    // 第一页第一条的发布时间
    if ([self.dataSourceArray count] > 0 )
    {
        iEver_discoverContentList_object *content_object = [self.dataSourceArray objectAtIndex:0];
        timeDate                                         = content_object.releaseTime;
    } else {
        timeDate = 0.0;
    }

    iEver_discoverObject *content = [[iEver_discoverObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"/article/queryByUser/%@/%d/%lld",_userId,self.page,timeDate];
    [[[content expertArticle:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_discoverObject *object) {
         @strongify(self);

         if (object.pageSize < self.page)
         {
             self.tableView.showsInfiniteScrolling = NO;
         } else {

             [self addDataToDataSource:object._discoverList];

             [self.tableView  reloadData];
         }
         [GiFHUD dismiss];
     }];
}

/* get request data*/
- (void)selectVuserShareFetchData:(int)VuserNumber
{    
    @weakify(self);

    self.page = 1;
    _VuserNumber = VuserNumber;
    
    [self.dataSourceArray removeAllObjects];

    [GiFHUD showWithOverlay];
    
    if ([self.dataSourceArray count] > 0 )
    {
        iEver_discoverContentList_object *content_object = [self.dataSourceArray objectAtIndex:0];
        timeDate                                         = content_object.releaseTime;
        
    }else{
        timeDate = 0;
    }
    
    iEver_discover_userList_object * userObject = (iEver_discover_userList_object *)[_object._userList objectAtIndex:VuserNumber];
    
    iEver_discoverObject *content = [[iEver_discoverObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"expertArticle/queryByExpertUser/%d/%d/%lld",userObject.User_id,self.page,timeDate];
    [[[content expertArticle:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_discoverObject *object) {
         @strongify(self);
         
         if (object.pageSize < self.page) {
             self.tableView.showsInfiniteScrolling = NO;
         }else{
             
             [self addDataToDataSource:object._discoverList];

             [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
             
             [self.tableView  reloadData];

             [self nothingView];
         }
         
         [GiFHUD dismiss];
     }
     ];
}
- (void)nothingView
{
    if (self.dataSourceArray.count == 0) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_nothing_"]];
        self.tableView.tableFooterView = imageView;
    }else {
        self.tableView.tableFooterView = nil;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Vuser_shareArticleCell";
    iEver_PerCener_nana_cell *cell  = (iEver_PerCener_nana_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    // Configure the cell...
    if (!cell) {

        cell = [[iEver_PerCener_nana_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    iEver_discoverContentList_object * object = [self.dataSourceArray  objectAtIndex:indexPath.row];
    object.didSelectIndexPath                 = indexPath.row;
    [cell setObject:object];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.dataSourceArray objectAtIndex:indexPath.row];
    return [iEver_PerCener_nana_cell heightForObject:object atIndexPath:indexPath tableView:tableView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    iEver_discoverContentList_object * object                       = [self.dataSourceArray objectAtIndex:indexPath.row];
    iEver_V_ListDetailViewController *_V_ListDetailViewController = [[iEver_V_ListDetailViewController alloc]init];
    _V_ListDetailViewController.list_Object                       = object;
    _V_ListDetailViewController.didSelectIndexPath                = indexPath.row;
    _V_ListDetailViewController.type_id                           = object.Cover_id;
    _V_ListDetailViewController.hidesBottomBarWhenPushed          = YES;
    [self.navigationController pushViewController:_V_ListDetailViewController animated:YES];
    IOS7POP;

    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (scrollView.contentOffset.y <= - 12)
    {
        if (_downBlock)
            _downBlock();
    }

    if (scrollView.contentOffset.y >= 12)
    {
        if (_upBlock)
            _upBlock();
    }
}

@end

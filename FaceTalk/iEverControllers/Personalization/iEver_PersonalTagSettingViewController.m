//
//  iEver_PersonalTagSettingViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/14.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_PersonalTagSettingViewController.h"
#import "JMBackgroundCameraView.h"
#import "iEver_UserMessageObject.h"
#import "iEver_OfficialListViewController.h"

enum
{
    face    = 0,
    eyeBrow = 1,
    eye     = 2,
    mouth   = 3,
    skin    = 4,
};
typedef NSInteger FirstCategory;

enum
{
    face_CHANG = 0,
    face_JIAN  = 1,
    face_YUAN  = 2,
    face_FANG  = 3,
    face_LING  = 4,
};
typedef NSInteger FaceType;

enum
{
    eyeBrow_NONG = 0,
    eyeBrow_XI   = 1,

};
typedef NSInteger EyeBrow_Type;


enum
{
    eye_DAN         = 0,
    eye_SHUANG      = 1,
    eye_NEISHUANG   = 2,

};
typedef NSInteger Eye_Type;

enum
{
    mouth_BAO      = 0,
    mouth_HOU      = 1,
    mouth_SHI      = 2,

};
typedef NSInteger Mouth_Type;


enum
{
    skin_HUNHE      = 0,
    skin_MINGAN     = 1,
    skin_GANXING    = 2,
    skin_YOUXING    = 3,
    skin_DOUDOU     = 4,

};
typedef NSInteger Skin_Type;



@interface iEver_PersonalTagSettingViewController ()

{

    JMBackgroundCameraView *deviceView;
    UIView *B_AreaView;
    UIImageView * B_SelectBottomView;

    UIView *face_View;
    UIView *eyeBrow_View;
    UIView *eye_View;
    UIView *mouth_View;
    UIView *skin_View;

    UIImageView  *selectArea_A_face_View;
    UIImageView  *selectArea_A_eyeBrow_View;
    UIImageView  *selectArea_A_eye_View;
    UIImageView  *selectArea_A_mouth_View;
    UIImageView  *selectArea_A_skin_View;

    NSArray * selectTitle;

    NSString *face_string;
    NSString *eyeBrow_string;
    NSString *eye_string;
    NSString *mouth_string;
    NSString *skin_string;

    NSString *face_key;
    NSString *eyeBrow_key;
    NSString *eye_key;
    NSString *mouth_key;
    NSString *skin_key;


    NSDictionary *baseCustomDic;

    iEver_UserMessageObject *_object;

    NSDictionary *type_dictionary;


}


@property (assign) FirstCategory first_Category;
@property (assign) FaceType      face_Type;
@property (assign) EyeBrow_Type  eyeBrow_Type;
@property (assign) Eye_Type      eye_Type;
@property (assign) Mouth_Type    mouth_Type;
@property (assign) Skin_Type     skin_Type;
@end

@implementation iEver_PersonalTagSettingViewController


-(void)creatScanningAreaView{


    UIImageView *scanAreaView            = [[UIImageView alloc] initWithFrame:CGRectMake(20, 36, 280, 280)];
    scanAreaView.image                   = [UIImage imageNamed:@"personalTag_scanArea"];

    selectArea_A_face_View               = [[UIImageView alloc] initWithFrame:CGRectMake(20, 36, 280, 280)];
    selectArea_A_face_View.image         = [UIImage imageNamed:[NSString stringWithFormat:@"personalTag_A_face0%d",self.face_Type +1]];
    NSLog(@"11%@",[NSString stringWithFormat:@"personalTag_A_face0%d",self.face_Type +1]);

    selectArea_A_eyeBrow_View            = [[UIImageView alloc] initWithFrame:CGRectMake(20, 36, 280, 280)];
    selectArea_A_eyeBrow_View.image      = [UIImage imageNamed:[NSString stringWithFormat:@"personalTag_A_eyeBrow0%d",self.eyeBrow_Type +1]];
    NSLog(@"22%@",[NSString stringWithFormat:@"personalTag_A_eyeBrow0%d",self.eyeBrow_Type +1]);

    selectArea_A_eye_View                = [[UIImageView alloc] initWithFrame:CGRectMake(20, 36, 280, 280)];
    selectArea_A_eye_View.image          = [UIImage imageNamed:[NSString stringWithFormat:@"personalTag_A_eye0%d",self.eye_Type +1]];
    NSLog(@"33%@",[NSString stringWithFormat:@"personalTag_A_eye0%d",self.eye_Type +1]);

    selectArea_A_mouth_View              = [[UIImageView alloc] initWithFrame:CGRectMake(20, 36, 280, 280)];
    selectArea_A_mouth_View.image        = [UIImage imageNamed:[NSString stringWithFormat:@"personalTag_A_mouth0%d",self.mouth_Type +1]];
    NSLog(@"44%@",[NSString stringWithFormat:@"personalTag_A_mouth0%d",self.mouth_Type +1]);

    selectArea_A_skin_View               = [[UIImageView alloc] initWithFrame:CGRectMake(20, 36, 280, 280)];
    selectArea_A_skin_View.image         = [UIImage imageNamed:[NSString stringWithFormat:@"personalTag_A_skin0%d",self.skin_Type +1]];
    NSLog(@"55%@",[NSString stringWithFormat:@"personalTag_A_skin0%d",self.skin_Type +1]);


    [self.view addSubview:selectArea_A_face_View];
    [self.view addSubview:selectArea_A_eyeBrow_View];
    [self.view addSubview:selectArea_A_eye_View];
    [self.view addSubview:selectArea_A_mouth_View];
    [self.view addSubview:selectArea_A_skin_View];
    [self.view addSubview:scanAreaView];

    
}


-(void)creatSelectFirstCategoryAreaView{


    UIView *C_AreaView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 64 - 44, self.view.frame.size.width, 64)];
    C_AreaView.backgroundColor = WHITE;

    UIImageView * C_SelectBottomView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 64.0, 65.0)];
    C_SelectBottomView.image         = [UIImage imageNamed:@"personalTag_C_Select"];

    [C_AreaView addSubview:C_SelectBottomView];


    NSArray *imageArray              = @[@"personalTag_C_face" ,
                                         @"personalTag_C_eyeBrow" ,
                                         @"personalTag_C_eye" ,
                                         @"personalTag_C_mouth" ,
                                         @"personalTag_C_skin"];

    NSArray *imageSelect_Array       = @[@"personalTag_C_Select_face" ,
                                         @"personalTag_C_Select_eyeBrow" ,
                                         @"personalTag_C_Select_eye" ,
                                         @"personalTag_C_Select_mouth" ,
                                         @"personalTag_C_Select_skin"];

    NSArray *titleArray              = @[@"脸型" ,
                                         @"眉型" ,
                                         @"眼型" ,
                                         @"唇形" ,
                                         @"肤质"];

    float x = 0;

    @weakify(self);
    for (int i = 0; i < imageArray.count; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [button setTitle:titleArray[i] forState:UIControlStateNormal];
        [button setTitleColor:BLACK forState:UIControlStateNormal];
        button.titleLabel.font = TextFonts;
        int offset = 25;
        UIEdgeInsets titleInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
        button.titleEdgeInsets = titleInset;
        if (i == 0) {
            [button setBackgroundImage:[UIImage imageNamed:imageSelect_Array[i]] forState:UIControlStateNormal];
            [button setTitleColor:WHITE forState:UIControlStateNormal];
        }
        [button setBackgroundColor:CLEAR];
        button.tag = i;
        button.frame = CGRectMake(x, 0, 64.0, 65.0);
        x += 64.0;
        [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
            @strongify(self);

            if (self.first_Category == sender.tag) {
                return;
            }
            self.first_Category = sender.tag;
            switch (self.first_Category) {

                case face:
                {

                    face_View.hidden    = NO;
                    eyeBrow_View.hidden = YES;
                    eye_View.hidden     = YES;
                    mouth_View.hidden   = YES;
                    skin_View.hidden    = YES;

                    selectArea_A_face_View.hidden      = NO;
                    selectArea_A_eyeBrow_View.hidden   = YES;
                    selectArea_A_eye_View.hidden       = YES;
                    selectArea_A_mouth_View.hidden     = YES;
                    selectArea_A_skin_View.hidden      = YES;


                }
                    break;

                case eyeBrow:
                {

                    face_View.hidden    = YES;
                    eyeBrow_View.hidden = NO;
                    eye_View.hidden     = YES;
                    mouth_View.hidden   = YES;
                    skin_View.hidden    = YES;

                    selectArea_A_face_View.hidden      = YES;
                    selectArea_A_eyeBrow_View.hidden   = NO;
                    selectArea_A_eye_View.hidden       = YES;
                    selectArea_A_mouth_View.hidden     = YES;
                    selectArea_A_skin_View.hidden      = YES;

                    }
                    break;

                case eye:
                {

                    face_View.hidden    = YES;
                    eyeBrow_View.hidden = YES;
                    eye_View.hidden     = NO;
                    mouth_View.hidden   = YES;
                    skin_View.hidden    = YES;

                    selectArea_A_face_View.hidden      = YES;
                    selectArea_A_eyeBrow_View.hidden   = YES;
                    selectArea_A_eye_View.hidden       = NO;
                    selectArea_A_mouth_View.hidden     = YES;
                    selectArea_A_skin_View.hidden      = YES;


                }
                    break;

                case mouth:
                {

                    face_View.hidden    = YES;
                    eyeBrow_View.hidden = YES;
                    eye_View.hidden     = YES;
                    mouth_View.hidden   = NO;
                    skin_View.hidden    = YES;

                    selectArea_A_face_View.hidden      = YES;
                    selectArea_A_eyeBrow_View.hidden   = YES;
                    selectArea_A_eye_View.hidden       = YES;
                    selectArea_A_mouth_View.hidden     = NO;
                    selectArea_A_skin_View.hidden      = YES;


                }
                    break;

                case skin:
                {

                    face_View.hidden    = YES;
                    eyeBrow_View.hidden = YES;
                    eye_View.hidden     = YES;
                    mouth_View.hidden   = YES;
                    skin_View.hidden    = NO;

                    selectArea_A_face_View.hidden      = YES;
                    selectArea_A_eyeBrow_View.hidden   = YES;
                    selectArea_A_eye_View.hidden       = YES;
                    selectArea_A_mouth_View.hidden     = YES;
                    selectArea_A_skin_View.hidden      = NO;

                }
                    break;
                default:
                    break;
            }

            for (UIView *sub in C_AreaView.subviews) {

                if ([sub isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)sub;
                    if (sender == btn) {
                        [sender setTitleColor:WHITE forState:UIControlStateNormal];
                        [sender setBackgroundImage:[UIImage imageNamed:imageSelect_Array[sender.tag]] forState:UIControlStateNormal];
                    }else {
                        [btn setTitleColor:BLACK forState:UIControlStateNormal];
                        [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                    }
                }
            }

            [UIView animateWithDuration:.3 animations:^{
                /* C区域选中色块 */
                [C_SelectBottomView setFrame:CGRectMake(64.0 * i, 0,  64.0, 65.0)];
            }];

        }];
        [C_AreaView addSubview:button];
    }


    [self.view addSubview:C_AreaView];
    
    
}

-(void)creatSelectSecondCategoryAreaView{


    B_AreaView = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 64 - 97.0, 640, 97.0)];
    B_AreaView.backgroundColor = CLEAR;

    UIImageView *B_bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 640, 97.0)];
    B_bottomView.image        = [UIImage imageNamed:@"personalTag_B_bottom"];

    [B_AreaView addSubview:B_bottomView];


    /* 脸部 */

    face_View = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 640, 97.0)];

    [B_AreaView addSubview:face_View];

    [self creat_Face_SelectView];

    /* 眉部 */

    eyeBrow_View = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 640, 97.0)];

    [B_AreaView addSubview:eyeBrow_View];

    [self creat_EyeBrow_SelectView];

    /* 眼部 */

    eye_View = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 640, 97.0)];

    [B_AreaView addSubview:eye_View];

    [self creat_Eye_SelectView];

    /* 唇部 */

    mouth_View = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 640, 97.0)];

    [B_AreaView addSubview:mouth_View];

    [self creat_mouth_SelectView];

    /* 肤质 */

    skin_View = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 640, 97.0)];

    [B_AreaView addSubview:skin_View];

    [self creat_skin_SelectView];


    [self.view addSubview:B_AreaView];
    
}


/* 脸部 */

-(void)creat_Face_SelectView{


    NSArray *imageArray              = @[@"personalTag_B_face01" ,
                                         @"personalTag_B_face02" ,
                                         @"personalTag_B_face03" ,
                                         @"personalTag_B_face04" ,
                                         @"personalTag_B_face05"];

    NSArray *titleArray              = @[@"长脸" ,
                                         @"尖脸" ,
                                         @"圆脸" ,
                                         @"方脸" ,
                                         @"菱形脸"];


    NSArray *imageSelect_Array       = @[@"personalTag_B_Select_face01" ,
                                         @"personalTag_B_Select_face02" ,
                                         @"personalTag_B_Select_face03" ,
                                         @"personalTag_B_Select_face04" ,
                                         @"personalTag_B_Select_face05"];


    NSArray *imageArray_A_face_Select     = @[@"personalTag_A_face01" ,
                                              @"personalTag_A_face02" ,
                                              @"personalTag_A_face03" ,
                                              @"personalTag_A_face04" ,
                                              @"personalTag_A_face05"];


    UIImageView * B_face_SelectBottomView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 64.0, 97.0)];
    B_face_SelectBottomView.image           = [UIImage imageNamed:@"personalTag_B_Select"];

    [face_View addSubview:B_face_SelectBottomView];


    float x = 0;

    @weakify(self);
    for (int i = 0; i < imageArray.count; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [button setTitle:titleArray[i] forState:UIControlStateNormal];
        [button setTitleColor:WHITE forState:UIControlStateNormal];
        button.titleLabel.font = TextFonts;
        int offset = 40;
        UIEdgeInsets titleInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
        button.titleEdgeInsets = titleInset;
        if (i == 0) {
            [button setBackgroundImage:[UIImage imageNamed:imageSelect_Array[i]] forState:UIControlStateNormal];
            [button setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
        }
        [button setBackgroundColor:CLEAR];
        button.tag = i;
        button.frame = CGRectMake(x, 0, 64.0, 97.0);
        x += 64.0;
        [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
            @strongify(self);

            self.face_Type = sender.tag;

            for (UIView *sub in face_View.subviews) {

                if ([sub isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)sub;
                    if (sender == btn) {
                        [sender setBackgroundImage:[UIImage imageNamed:imageSelect_Array[sender.tag]] forState:UIControlStateNormal];
                        [sender setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                        face_string = [baseCustomDic objectForKey:[NSString stringWithFormat:@"1-%d",sender.tag + 1]];
                        face_key    =  [NSString stringWithFormat:@"1-%d",sender.tag + 1];
                    }else {
                        [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                        [btn setTitleColor:WHITE forState:UIControlStateNormal];
                    }
                }
            }

            [UIView animateWithDuration:.3 animations:^{
                [B_face_SelectBottomView setFrame:CGRectMake(64.0 * i, 0,  64.0, 97.0)];
                selectArea_A_face_View.image    = [UIImage imageNamed:imageArray_A_face_Select[i]];
            }];
            
        }];
        [face_View addSubview:button];
    }


    for (UIView *sub in face_View.subviews) {

        if ([sub isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)sub;
            if (btn.tag == self.face_Type) {
                [btn setBackgroundImage:[UIImage imageNamed:imageSelect_Array[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                [B_face_SelectBottomView setFrame:CGRectMake(64.0 * self.face_Type, 0,  64.0, 97.0)];
            }else {
                [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:WHITE forState:UIControlStateNormal];
            }
        }
    }

}

/* 眉部 */

-(void)creat_EyeBrow_SelectView{

    NSArray *imageArray              = @[@"personalTag_B_eyeBrow01" ,
                                         @"personalTag_B_eyeBrow02"];

    NSArray *titleArray              = @[@"浓眉" ,
                                         @"稀疏眉" ];

    NSArray *imageSelect_Array       = @[@"personalTag_B_Select_eyeBrow01" ,
                                         @"personalTag_B_Select_eyeBrow02"];

    NSArray *imageArray_A_eyeBrow_Select     = @[@"personalTag_A_eyeBrow01" ,
                                                 @"personalTag_A_eyeBrow02"];


    UIImageView * B_eyeBrow_SelectBottomView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 64.0, 97.0)];
    B_eyeBrow_SelectBottomView.image           = [UIImage imageNamed:@"personalTag_B_Select"];

    [eyeBrow_View addSubview:B_eyeBrow_SelectBottomView];


    float x = 0;

    @weakify(self);
    for (int i = 0; i < imageArray.count; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [button setTitle:titleArray[i] forState:UIControlStateNormal];
        [button setTitleColor:WHITE forState:UIControlStateNormal];
        button.titleLabel.font = TextFonts;
        int offset = 40;
        UIEdgeInsets titleInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
        button.titleEdgeInsets = titleInset;
        if (i == 0) {
            [button setBackgroundImage:[UIImage imageNamed:imageSelect_Array[i]] forState:UIControlStateNormal];
            [button setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
        }
        [button setBackgroundColor:CLEAR];
        button.tag = i;
        button.frame = CGRectMake(x, 0, 64.0, 97.0);
        x += 64.0;
        [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
            @strongify(self);

            self.eyeBrow_Type = sender.tag;

            for (UIView *sub in eyeBrow_View.subviews) {

                if ([sub isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)sub;
                    if (sender == btn) {
                        [sender setBackgroundImage:[UIImage imageNamed:imageSelect_Array[sender.tag]] forState:UIControlStateNormal];
                        [sender setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                        eyeBrow_string = [baseCustomDic objectForKey:[NSString stringWithFormat:@"2-%d",sender.tag + 1]];
                        eyeBrow_key    =  [NSString stringWithFormat:@"2-%d",sender.tag + 1];
                    }else {
                        [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                        [btn setTitleColor:WHITE forState:UIControlStateNormal];
                    }
                }
            }

            [UIView animateWithDuration:.3 animations:^{
                [B_eyeBrow_SelectBottomView setFrame:CGRectMake(64.0 * i, 0,  64.0, 97.0)];
                selectArea_A_eyeBrow_View.image    = [UIImage imageNamed:imageArray_A_eyeBrow_Select[i]];
            }];
            
        }];
        [eyeBrow_View addSubview:button];
    }

    for (UIView *sub in eyeBrow_View.subviews) {

        if ([sub isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)sub;
            if (btn.tag == self.eyeBrow_Type) {
                [btn setBackgroundImage:[UIImage imageNamed:imageSelect_Array[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                [B_eyeBrow_SelectBottomView setFrame:CGRectMake(64.0 * self.eyeBrow_Type, 0,  64.0, 97.0)];
            }else {
                [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:WHITE forState:UIControlStateNormal];
            }
        }
    }

}

/* 眼部 */

-(void)creat_Eye_SelectView{

    NSArray *imageArray              = @[@"personalTag_B_eye01" ,
                                         @"personalTag_B_eye02" ,
                                         @"personalTag_B_eye03"];

    NSArray *titleArray              = @[@"单眼皮" ,
                                         @"双眼皮",
                                         @"内双眼皮"];

    NSArray *imageSelect_Array       = @[@"personalTag_B_Select_eye01" ,
                                         @"personalTag_B_Select_eye02" ,
                                         @"personalTag_B_Select_eye03"];

    NSArray *imageArray_A_eye_Select     = @[@"personalTag_A_eye01" ,
                                             @"personalTag_A_eye02",
                                             @"personalTag_A_eye03"];


    UIImageView * B_eye_SelectBottomView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 64.0, 97.0)];
    B_eye_SelectBottomView.image           = [UIImage imageNamed:@"personalTag_B_Select"];

    [eye_View addSubview:B_eye_SelectBottomView];


    float x = 0;

    @weakify(self);
    for (int i = 0; i < imageArray.count; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [button setTitle:titleArray[i] forState:UIControlStateNormal];
        [button setTitleColor:WHITE forState:UIControlStateNormal];
        button.titleLabel.font = TextFonts;
        int offset = 40;
        UIEdgeInsets titleInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
        button.titleEdgeInsets = titleInset;
        if (i == 0) {
            [button setBackgroundImage:[UIImage imageNamed:imageSelect_Array[i]] forState:UIControlStateNormal];
            [button setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
        }

        [button setBackgroundColor:CLEAR];
        button.tag = i;
        button.frame = CGRectMake(x, 0, 64.0, 97.0);
        x += 64.0;
        [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
            @strongify(self);

           
            self.eye_Type = sender.tag;

            for (UIView *sub in eye_View.subviews) {

                if ([sub isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)sub;
                    if (sender == btn) {
                        [sender setBackgroundImage:[UIImage imageNamed:imageSelect_Array[sender.tag]] forState:UIControlStateNormal];
                        [sender setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                        eye_string = [baseCustomDic objectForKey:[NSString stringWithFormat:@"3-%d",sender.tag + 1]];
                        eye_key    =  [NSString stringWithFormat:@"3-%d",sender.tag + 1];
                    }else {
                        [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                        [btn setTitleColor:WHITE forState:UIControlStateNormal];
                    }
                }
            }

            [UIView animateWithDuration:.3 animations:^{
                [B_eye_SelectBottomView setFrame:CGRectMake(64.0 * i, 0,  64.0, 97.0)];
                selectArea_A_eye_View.image    = [UIImage imageNamed:imageArray_A_eye_Select[i]];
            }];
            
        }];
        [eye_View addSubview:button];
    }
    

    for (UIView *sub in eye_View.subviews) {

        if ([sub isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)sub;
            if (btn.tag == self.eye_Type) {
                [btn setBackgroundImage:[UIImage imageNamed:imageSelect_Array[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                [B_eye_SelectBottomView setFrame:CGRectMake(64.0 * self.eye_Type, 0,  64.0, 97.0)];
            }else {
                [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:WHITE forState:UIControlStateNormal];
            }
        }
    }

}

/* 唇部 */

-(void)creat_mouth_SelectView{

    NSArray *imageArray              = @[@"personalTag_B_mouth01" ,
                                         @"personalTag_B_mouth02" ,
                                         @"personalTag_B_mouth03"];

    NSArray *titleArray              = @[@"薄唇" ,
                                         @"厚唇",
                                         @"适中唇"];

    NSArray *imageSelect_Array       = @[@"personalTag_B_Select_mouth01" ,
                                         @"personalTag_B_Select_mouth02" ,
                                         @"personalTag_B_Select_mouth03"];

    NSArray *imageArray_A_mouth_Select     = @[@"personalTag_A_mouth01" ,
                                               @"personalTag_A_mouth02",
                                               @"personalTag_A_mouth03"];


    UIImageView * B_mouth_SelectBottomView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 64.0, 97.0)];
    B_mouth_SelectBottomView.image           = [UIImage imageNamed:@"personalTag_B_Select"];

    [mouth_View addSubview:B_mouth_SelectBottomView];


    float x = 0;

    @weakify(self);
    for (int i = 0; i < imageArray.count; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [button setTitle:titleArray[i] forState:UIControlStateNormal];
        [button setTitleColor:WHITE forState:UIControlStateNormal];
        button.titleLabel.font = TextFonts;
        int offset = 40;
        UIEdgeInsets titleInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
        button.titleEdgeInsets = titleInset;
        if (i == 0) {
            [button setBackgroundImage:[UIImage imageNamed:imageSelect_Array[i]] forState:UIControlStateNormal];
            [button setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
        }

        [button setBackgroundColor:CLEAR];
        button.tag = i;
        button.frame = CGRectMake(x, 0, 64.0, 97.0);
        x += 64.0;
        [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
            @strongify(self);

           
            self.mouth_Type = sender.tag;

            for (UIView *sub in mouth_View.subviews) {

                if ([sub isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)sub;
                    if (sender == btn) {
                        [sender setBackgroundImage:[UIImage imageNamed:imageSelect_Array[sender.tag]] forState:UIControlStateNormal];
                        [sender setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                        mouth_string = [baseCustomDic objectForKey:[NSString stringWithFormat:@"4-%d",sender.tag + 1]];
                        mouth_key =  [NSString stringWithFormat:@"4-%d",sender.tag + 1];
                    }else {
                        [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                        [btn setTitleColor:WHITE forState:UIControlStateNormal];
                    }
                }
            }

            [UIView animateWithDuration:.3 animations:^{
                [B_mouth_SelectBottomView setFrame:CGRectMake(64.0 * i, 0,  64.0, 97.0)];
                selectArea_A_mouth_View.image    = [UIImage imageNamed:imageArray_A_mouth_Select[i]];
            }];
            
        }];
        [mouth_View addSubview:button];
    }
    

    for (UIView *sub in mouth_View.subviews) {

        if ([sub isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)sub;
            if (btn.tag == self.mouth_Type) {
                [btn setBackgroundImage:[UIImage imageNamed:imageSelect_Array[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                [B_mouth_SelectBottomView setFrame:CGRectMake(64.0 * self.mouth_Type, 0,  64.0, 97.0)];
            }else {
                [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:WHITE forState:UIControlStateNormal];
            }
        }
    }

}

/* 肤质 */

-(void)creat_skin_SelectView{



    NSArray *imageArray              = @[@"personalTag_B_skin01" ,
                                         @"personalTag_B_skin02" ,
                                         @"personalTag_B_skin03" ,
                                         @"personalTag_B_skin04" ,
                                         @"personalTag_B_skin05"];

    NSArray *titleArray              = @[@"混合肌肤" ,
                                         @"敏感肌肤",
                                         @"干性肌肤",
                                         @"油性肌肤",
                                         @"痘痘肌肤"];

    NSArray *imageSelect_Array       = @[@"personalTag_B_Select_skin01" ,
                                         @"personalTag_B_Select_skin02" ,
                                         @"personalTag_B_Select_skin03" ,
                                         @"personalTag_B_Select_skin04" ,
                                         @"personalTag_B_Select_skin05"];

    NSArray *imageArray_A_skin_Select     = @[@"personalTag_A_skin01" ,
                                              @"personalTag_A_skin02",
                                              @"personalTag_A_skin03",
                                              @"personalTag_A_skin04",
                                              @"personalTag_A_skin05"];


    UIImageView * B_skin_SelectBottomView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 64.0, 97.0)];
    B_skin_SelectBottomView.image           = [UIImage imageNamed:@"personalTag_B_Select"];

    [skin_View addSubview:B_skin_SelectBottomView];


    float x = 0;

    @weakify(self);
    for (int i = 0; i < imageArray.count; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [button setTitle:titleArray[i] forState:UIControlStateNormal];
        [button setTitleColor:WHITE forState:UIControlStateNormal];
        button.titleLabel.font = TextFonts;
        int offset = 40;
        UIEdgeInsets titleInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
        button.titleEdgeInsets = titleInset;
        if (i == 0) {
            [button setBackgroundImage:[UIImage imageNamed:imageSelect_Array[i]] forState:UIControlStateNormal];
            [button setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
        }

        [button setBackgroundColor:CLEAR];
        button.tag = i;
        button.frame = CGRectMake(x, 0, 64.0, 97.0);
        x += 64.0;
        [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
            @strongify(self);

           
            self.skin_Type = sender.tag;

            for (UIView *sub in skin_View.subviews) {

                if ([sub isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)sub;
                    if (sender == btn) {
                        [sender setBackgroundImage:[UIImage imageNamed:imageSelect_Array[sender.tag]] forState:UIControlStateNormal];
                        [sender setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                        skin_string = [baseCustomDic objectForKey:[NSString stringWithFormat:@"5-%d",sender.tag + 1]];
                        skin_key    =  [NSString stringWithFormat:@"5-%d",sender.tag + 1];
                    }else {
                        [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                        [btn setTitleColor:WHITE forState:UIControlStateNormal];
                    }
                }
            }

            [UIView animateWithDuration:.3 animations:^{
                [B_skin_SelectBottomView setFrame:CGRectMake(64.0 * i, 0,  64.0, 97.0)];
                selectArea_A_skin_View.image    = [UIImage imageNamed:imageArray_A_skin_Select[i]];
            }];
            
        }];
        [skin_View addSubview:button];
    }

    for (UIView *sub in skin_View.subviews) {

        if ([sub isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)sub;
            if (btn.tag == self.skin_Type) {
                [btn setBackgroundImage:[UIImage imageNamed:imageSelect_Array[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:GRAY_LIGHT forState:UIControlStateNormal];
                [B_skin_SelectBottomView setFrame:CGRectMake(64.0 * self.skin_Type, 0,  64.0, 97.0)];
            }else {
                [btn setBackgroundImage:[UIImage imageNamed:imageArray[btn.tag]] forState:UIControlStateNormal];
                [btn setTitleColor:WHITE forState:UIControlStateNormal];
            }
        }
    }



}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"个性化设置";

    type_dictionary =                [NSDictionary dictionaryWithObjectsAndKeys:
                                     [NSNumber numberWithInt:face_CHANG      ],@"1-1",
                                     [NSNumber numberWithInt:face_JIAN       ],@"1-2",
                                     [NSNumber numberWithInt:face_YUAN       ],@"1-3",
                                     [NSNumber numberWithInt:face_FANG       ],@"1-4",
                                     [NSNumber numberWithInt:face_LING       ],@"1-5",
                                     [NSNumber numberWithInt:eyeBrow_NONG    ],@"2-1",
                                     [NSNumber numberWithInt:eyeBrow_XI      ],@"2-2",
                                     [NSNumber numberWithInt:eye_DAN         ],@"3-1",
                                     [NSNumber numberWithInt:eye_SHUANG      ],@"3-2",
                                     [NSNumber numberWithInt:eye_NEISHUANG   ],@"3-3",
                                     [NSNumber numberWithInt:mouth_BAO       ],@"4-1",
                                     [NSNumber numberWithInt:mouth_HOU       ],@"4-2",
                                     [NSNumber numberWithInt:mouth_SHI       ],@"4-3",
                                     [NSNumber numberWithInt:skin_HUNHE      ],@"5-1",
                                     [NSNumber numberWithInt:skin_MINGAN     ],@"5-2",
                                     [NSNumber numberWithInt:skin_GANXING    ],@"5-3",
                                     [NSNumber numberWithInt:skin_YOUXING    ],@"5-4",
                                     [NSNumber numberWithInt:skin_DOUDOU     ],@"5-5",
                                     nil];
    [self addBar];

    /* 用户是否定制 */
    [self getUserCustomMessage];

    face_string    = [[NSString alloc] init];
    eyeBrow_string = [[NSString alloc] init];
    eye_string     = [[NSString alloc] init];
    mouth_string   = [[NSString alloc] init];
    skin_string    = [[NSString alloc] init];

    face_key       = [[NSString alloc] init];
    eyeBrow_key    = [[NSString alloc] init];
    eye_key        = [[NSString alloc] init];
    mouth_key      = [[NSString alloc] init];
    skin_key       = [[NSString alloc] init];

    deviceView = [[JMBackgroundCameraView alloc] initWithFrame:self.view.frame positionDevice:DevicePositonFront blur:UIBlurEffectStyleDark];
    [deviceView removeBlurEffect];
    [self.view addSubview:deviceView];

    [self creatSelectFirstCategoryAreaView];

}

-(void)setConfige{



    if ([self.userIsCustom isEqualToString:@"DID"]) {


        self.face_Type      = [[type_dictionary objectForKey:((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:0]).customKey] integerValue];
        self.eyeBrow_Type   = [[type_dictionary objectForKey:((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:1]).customKey] integerValue];
        self.eye_Type       = [[type_dictionary objectForKey:((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:2]).customKey] integerValue];
        self.mouth_Type     = [[type_dictionary objectForKey:((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:3]).customKey] integerValue];
        self.skin_Type      = [[type_dictionary objectForKey:((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:4]).customKey] integerValue];



        face_key            = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:0]).customKey;
        eyeBrow_key         = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:1]).customKey;
        eye_key             = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:2]).customKey;
        mouth_key           = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:3]).customKey;
        skin_key            = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:4]).customKey;

        face_string         = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:0]).customName;
        eyeBrow_string      = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:1]).customName;
        eye_string          = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:2]).customName;
        mouth_string        = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:3]).customName;
        skin_string         = ((iEver_userCustomList_object *)[_object._customedListArray objectAtIndex:4]).customName;



    }else{

        //face_string         = @"长脸";

        self.face_Type      = face_CHANG ;
        self.eyeBrow_Type   = eyeBrow_NONG ;
        self.eye_Type       = eye_DAN;
        self.mouth_Type     = mouth_BAO ;
        self.skin_Type      = skin_HUNHE ;


    }

    selectTitle = [[NSMutableArray alloc] init];


    [self creatScanningAreaView];


    [self creatSelectSecondCategoryAreaView];

    face_View.hidden    = NO;
    eyeBrow_View.hidden = YES;
    eye_View.hidden     = YES;
    mouth_View.hidden   = YES;
    skin_View.hidden    = YES;

    selectArea_A_face_View.hidden      = NO;
    selectArea_A_eyeBrow_View.hidden   = YES;
    selectArea_A_eye_View.hidden       = YES;
    selectArea_A_mouth_View.hidden     = YES;
    selectArea_A_skin_View.hidden      = YES;


}

-(void)addBar{


    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 10, 44, 44);
    rightButton.titleLabel.font = TextFont;
    [rightButton setTitle:@"提交" forState:UIControlStateNormal];
    [rightButton setTitleColor:BLACK forState:UIControlStateNormal];
    rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    rightButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        if ([face_string isEqualToString:@""]) {

            UIAlertView* upgradeAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"个性化设定", nil)
                                                                   message:NSLocalizedString(@"脸型还没选择", nil)
                                                                  delegate:self
                                                         cancelButtonTitle:NSLocalizedString(@"取消", nil)
                                                         otherButtonTitles:nil, nil];

            [upgradeAlert.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {

            }];
            [upgradeAlert show];

        }

        else if ([eyeBrow_string isEqualToString:@""]) {

            UIAlertView* upgradeAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"个性化设定", nil)
                                                                   message:NSLocalizedString(@"眉型还没选择", nil)
                                                                  delegate:self
                                                         cancelButtonTitle:NSLocalizedString(@"取消", nil)
                                                         otherButtonTitles:nil, nil];

            [upgradeAlert.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {

            }];
            [upgradeAlert show];

        }

        else if ([eye_string isEqualToString:@""]) {

            UIAlertView* upgradeAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"个性化设定", nil)
                                                                   message:NSLocalizedString(@"眼型还没选择", nil)
                                                                  delegate:self
                                                         cancelButtonTitle:NSLocalizedString(@"取消", nil)
                                                         otherButtonTitles:nil, nil];

            [upgradeAlert.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {

            }];
            [upgradeAlert show];

        }

        else if ([mouth_string isEqualToString:@""]) {

            UIAlertView* upgradeAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"个性化设定", nil)
                                                                   message:NSLocalizedString(@"唇形还没选择", nil)
                                                                  delegate:self
                                                         cancelButtonTitle:NSLocalizedString(@"取消", nil)
                                                         otherButtonTitles:nil, nil];

            [upgradeAlert.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {

            }];
            [upgradeAlert show];

        }
        else if ([skin_string isEqualToString:@""]) {

            UIAlertView* upgradeAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"个性化设定", nil)
                                                                   message:NSLocalizedString(@"肤质还没选择", nil)
                                                                  delegate:self
                                                         cancelButtonTitle:NSLocalizedString(@"取消", nil)
                                                         otherButtonTitles:nil, nil];

            [upgradeAlert.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {

            }];
            [upgradeAlert show];

        }

        else if (![face_string isEqualToString:@""] && ![eyeBrow_string isEqualToString:@""] &&![eye_string isEqualToString:@""] && ![mouth_string isEqualToString:@""] &&![skin_string isEqualToString:@""]) {


            NSString *str = [NSString stringWithFormat:@"您选定的是:\n%@ \n%@ \n%@ \n%@ \n%@ ",face_string , eyeBrow_string , eye_string , mouth_string , skin_string];
            UIAlertView* upgradeAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"个性化设定", nil)
                                                                   message:NSLocalizedString(str, nil)
                                                                  delegate:self
                                                         cancelButtonTitle:NSLocalizedString(@"取消", nil)
                                                         otherButtonTitles:NSLocalizedString(@"确定", nil), nil];

            [upgradeAlert.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {
                
                if ([x integerValue] == 1) {

                    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];

                    NSArray *customNameArray         = @[face_string ,
                                                         eyeBrow_string ,
                                                         eye_string,
                                                         mouth_string ,
                                                         skin_string];

                    NSArray *customKeyArray          = @[face_key ,
                                                         eyeBrow_key ,
                                                         eye_key,
                                                         mouth_key ,
                                                         skin_key];


                    NSMutableArray * contentArray = [[NSMutableArray alloc] init];

                    for (int i = 0; i < 5; i++) {

                        NSDictionary *customListdic = @{@"customName": customNameArray[i],
                                                        @"customKey": customKeyArray[i]
                                                        };
                        [contentArray addObject:customListdic];
                    }

                    NSDictionary *dic = @{@"customList": contentArray
                                          };
                    [[[content postUserCustomMessageModel:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
                     subscribeNext:^(NSDictionary *object) {

                         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                             [SVProgressHUD showImage:Nil status:@"定制成功，您的定制数据正在生成中。。。"];

                              [self performSelector:@selector(delayMethod) withObject:nil afterDelay:2];

                         }else{
                             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                             [SVProgressHUD showImage:Nil status:codeStr];
                         }
                         
                     }];
                }
            }];
            [upgradeAlert show];
            
        }

        return [RACSignal empty];
    }];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;

}
-(void)delayMethod {

    if ([self.delegate respondsToSelector:@selector(delegateMethod)]) {
        [self.delegate delegateMethod];
    }

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/* 获取定制模板 */
-(void)getUserCustomMessage{

    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];

    if ([self.userIsCustom isEqualToString:@"NOT"]) {
        [[[content questUserCustomMessageModel:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *dic) {

             if ([[dic objectForKey:@"resultCode"] integerValue] == 1) {

                 baseCustomDic = [dic objectForKey:@"baseCustom"];

                 [self setConfige];

                 [self loadBootstrap];

             }else{

                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[dic objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];

             }

         }];

    }else if ([self.userIsCustom isEqualToString:@"DID"]){

        [[[content questUserCustomMessage:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(iEver_UserMessageObject *object) {

             if (object.resultCode == 1) {

                 _object       = object;

                 baseCustomDic = object._baseCustom;

                 [self setConfige];

             }else{

                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:object.resultCode];
                 [SVProgressHUD showImage:Nil status:codeStr];
                 
             }

         }];
    }
}

-(void)loadBootstrap {
    NSString *person_previousVersion = [[NSUserDefaults standardUserDefaults] objectForKey:person_PREVIOUSVERSION];
    if (!person_previousVersion) {
        person_previousVersion = @"0";
    }
    if ([person_CURRENTVERSION compare:person_previousVersion options:NSNumericSearch]) {
    //newVersionFound
    [[NSUserDefaults standardUserDefaults] setObject:person_CURRENTVERSION forKey:person_PREVIOUSVERSION];
    [[NSUserDefaults standardUserDefaults] synchronize];

    UIImage *main_Bootstrap = [UIImage imageNamed:@"person_Bootstrap"];
    UIImageView *image_Bootstrap = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,ScreenWidth, ScreenHeight)];
    image_Bootstrap.userInteractionEnabled = YES;
    image_Bootstrap.image = main_Bootstrap;

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(210, ScreenHeight - 80, 120, 40);
    button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        image_Bootstrap.hidden = YES;
        return [RACSignal empty];
    }];
    [image_Bootstrap addSubview:button];

    [[iEverAppDelegate shareAppDelegate].window addSubview:image_Bootstrap];

    }
}

@end

//
//  QQSectionHeaderView.h
//  TQQTableView
//
//  Created by Futao on 11-6-22.
//  Copyright 2011 ftkey.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QQSectionHeaderViewDelegate;

@interface QQSectionHeaderView : UIView {

}
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *subtitleLabel;
@property (nonatomic, retain) UIButton *disclosureButton;
@property (nonatomic, retain) UIImageView *imageview;
@property (nonatomic, retain) UIImageView *smallImageview;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) BOOL opened;

@property (nonatomic, assign) id <QQSectionHeaderViewDelegate> delegate;
-(id)initWithFrame:(CGRect)frame title:(NSString*)title imagestring:(NSString *)imagestr section:(NSInteger)sectionNumber personNum:(NSInteger)personNumber opened:(BOOL)isOpened delegate:(id<QQSectionHeaderViewDelegate>)adelegate;

@end

@protocol QQSectionHeaderViewDelegate <NSObject> 

@optional
-(void)sectionHeaderView:(QQSectionHeaderView*)sectionHeaderView sectionClosed:(NSInteger)section;

-(void)sectionHeaderView:(QQSectionHeaderView*)sectionHeaderView sectionOpened:(NSInteger)section;
@end

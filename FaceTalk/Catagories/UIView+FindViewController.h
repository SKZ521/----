//
//  UIView+FindViewController.h
//  ZGChat
//
//  Created by 'Sooongz on 14-2-14.
//  Copyright (c) 2014年 Sooongz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FindViewController)

- (UIViewController *)firstAvailableUIViewController;

@end

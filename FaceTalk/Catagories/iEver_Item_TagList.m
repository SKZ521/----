//
//  iEver_Item_TagList.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/5.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_Item_TagList.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+FindViewController.h"
#import "iEver_TagQuery_ItemViewController.h"

#define CORNER_RADIUS 5.0f
#define LABEL_MARGIN 4.0f
#define BOTTOM_MARGIN 2.0f
#define FONT_SIZE 10.0f
#define HORIZONTAL_PADDING 4.0f
#define VERTICAL_PADDING 2.0f
#define BACKGROUND_COLOR [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.00]
#define TEXT_COLOR NAV
#define TEXT_SHADOW_COLOR [UIColor clearColor]
#define TEXT_SHADOW_OFFSET CGSizeMake(0.0f, 0.0f)
#define BORDER_COLOR [UIColor colorWithRed:217/255.f green:217/255.f blue:217/255.f alpha:1.00].CGColor
#define BORDER_WIDTH 0.0f

@implementation iEver_Item_TagList

@synthesize view, textArray,IDArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:view];
    }
    return self;
}

- (void)setTags:(NSArray *)array
{
    textArray = [[NSArray alloc] initWithArray:array];
    sizeFit = CGSizeZero;
    [self display];
}

- (void)setLabelBackgroundColor:(UIColor *)color
{
    lblBackgroundColor = color;
    [self display];
}

- (void)display
{
    for (UILabel *subview in [self subviews]) {
        [subview removeFromSuperview];
    }
    float totalHeight = 0;
    CGRect previousFrame = CGRectZero;
    BOOL gotPreviousFrame = NO;
    for (int i= 0 ; i < [textArray count]; i++) {

        NSString *text      = [textArray objectAtIndex:i];
        NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:FONT_SIZE], NSFontAttributeName,nil];
        CGSize textSize     =[text boundingRectWithSize:CGSizeMake(self.frame.size.width, 1500)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:tdic context:nil].size;

        textSize.width     += HORIZONTAL_PADDING*2;
        textSize.height    += VERTICAL_PADDING*2;
        UILabel *label      = nil;
        if (!gotPreviousFrame) {
            label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textSize.width, textSize.height)];
            totalHeight = textSize.height;
        } else {
            CGRect newRect = CGRectZero;
            if (previousFrame.origin.x + previousFrame.size.width + textSize.width + LABEL_MARGIN > self.frame.size.width) {
                newRect.origin = CGPointMake(0, previousFrame.origin.y + textSize.height + BOTTOM_MARGIN);
                totalHeight += textSize.height + BOTTOM_MARGIN;
            } else {
                newRect.origin = CGPointMake(previousFrame.origin.x + previousFrame.size.width + LABEL_MARGIN, previousFrame.origin.y);
            }
            newRect.size = textSize;
            label = [[UILabel alloc] initWithFrame:newRect];
        }
        previousFrame = label.frame;
        gotPreviousFrame = YES;
        label.font = TextFonts;
        if (!lblBackgroundColor) {
            [label setBackgroundColor:CLEAR];
        } else {
            [label setBackgroundColor:lblBackgroundColor];
        }
        UIViewController *superVC  = (UIViewController *)[self firstAvailableUIViewController];
        if ([superVC isKindOfClass:[iEver_TagQuery_ItemViewController class]]) {
            [label setTextColor:BLACK];
        }else{
            [label setTextColor:MY_PURPLECOLOR];
        }
        [label setText:text];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label.layer setBorderWidth:BORDER_WIDTH];
        [label.layer setBorderColor:BORDER_COLOR];
        [label.layer setMasksToBounds:YES];
        label.tag = i;
        label.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapTagLable:)];
        tapGesture.numberOfTapsRequired = 1;
        [label addGestureRecognizer:tapGesture];
        [self addSubview:label];
    }
    sizeFit = CGSizeMake(self.frame.size.width, totalHeight);
}

- (CGSize)fittedSize
{
    return sizeFit;
}

- (void)TapTagLable:(UITapGestureRecognizer *)gesture {

    NSLog(@"TapTagLable---->%d",[self.IDArray count]);

    if ([self.IDArray count] >0) {

        iEver_TagQuery_ItemViewController *_TagQueryViewController = [[iEver_TagQuery_ItemViewController alloc]init];
        _TagQueryViewController.tagId                         = [[self.IDArray objectAtIndex:gesture.view.tag] integerValue];
        _TagQueryViewController.title                         = [self.textArray objectAtIndex:gesture.view.tag];
        _TagQueryViewController.hidesBottomBarWhenPushed      = YES;
        UIViewController *superVC                             = (UIViewController *)[self firstAvailableUIViewController];
        [superVC.navigationController pushViewController:_TagQueryViewController animated:YES];
        if ([superVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            superVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
}

@end

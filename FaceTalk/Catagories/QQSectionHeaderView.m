//
//  QQSectionHeaderView.m
//  TQQTableView
//
//  Created by Futao on 11-6-22.
//  Copyright 2011 ftkey.com. All rights reserved.
//

#import "QQSectionHeaderView.h"


@implementation QQSectionHeaderView
@synthesize titleLabel, disclosureButton, delegate, section , opened;

-(id)initWithFrame:(CGRect)frame title:(NSString*)title imagestring:(NSString *)imagestr section:(NSInteger)sectionNumber personNum:(NSInteger)personNumber opened:(BOOL)isOpened delegate:(id<QQSectionHeaderViewDelegate>)adelegate{
    if (self = [super initWithFrame:frame]) {
		
		UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
																					  action:@selector(toggleAction:)];
		[self addGestureRecognizer:tapGesture];
		self.userInteractionEnabled = YES;
        /* setion head image */
        _imageview = [[UIImageView alloc] initWithFrame:frame];
        _imageview.image = [UIImage imageNamed:imagestr];
        [self addSubview:_imageview];
		section = sectionNumber;
		delegate = adelegate;
		opened = isOpened;

        /* setion head title */
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0, 3.0, 80.0, 40.0)];
        titleLabel.text = title;
        titleLabel.font = [UIFont systemFontOfSize:14.0];
        titleLabel.textColor = [UIColor darkGrayColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:titleLabel];

        /* setion head title */
        _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(110.0, 3.0, 40.0, 40.0)];
        _subtitleLabel.text = [NSString stringWithFormat:@"%d人",personNumber];
        _subtitleLabel.font = [UIFont systemFontOfSize:12.0];
        _subtitleLabel.textColor = [UIColor darkGrayColor];
        _subtitleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_subtitleLabel];

        _smallImageview = [[UIImageView alloc] initWithFrame:CGRectMake(30.0, 15.0, 11.0, 16.0)];
        _smallImageview.image = [UIImage imageNamed:@"discover_list_photoImage"];
        [self addSubview:_smallImageview];
		
		
		disclosureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        disclosureButton.frame = CGRectMake(10.0, 15.0, 15.0, 15.0);
		[disclosureButton setImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];
		[disclosureButton setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateSelected];
		disclosureButton.userInteractionEnabled = NO;
		disclosureButton.selected = isOpened;
        [self addSubview:disclosureButton];
		
		self.backgroundColor = [UIColor clearColor];
	}
	return self;
}

-(IBAction)toggleAction:(id)sender {
	
	disclosureButton.selected = !disclosureButton.selected;
	
	if (disclosureButton.selected) {
		if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
			[delegate sectionHeaderView:self sectionOpened:section];
		}
	}
	else {
		if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
			[delegate sectionHeaderView:self sectionClosed:section];
		}
	}

}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code.
 }
 */


@end

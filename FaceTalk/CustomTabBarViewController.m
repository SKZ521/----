//
//  CustomTabBarViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/11.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "CustomTabBarViewController.h"

@interface CustomTabBarViewController ()

@end

@implementation CustomTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation

{

    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);

}

- (BOOL)shouldAutorotate

{

    return NO;

}

- (NSUInteger)supportedInterfaceOrientations

{

    return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
    
}
@end

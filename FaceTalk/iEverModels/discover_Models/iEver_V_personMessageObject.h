//
//  iEver_V_personMessageObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/19.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_V_personMessageObject : NSObject

@property (nonatomic, assign) int                        U_id;                 /* 用户ID */
@property (nonatomic, copy  ) NSString                   *categoryName;        /* 用户昵称 */
@property (nonatomic, copy  ) NSString                   *headImg;             /* 用户头像 */
@property (nonatomic, copy  ) NSString                   *nickName;            /* 用户昵称 */
@property (nonatomic, copy  ) NSString                   *intro;               /* 用户简介 */
@property (nonatomic, assign) int                        coverCount;           /* 封面数量 */
@property (nonatomic, assign) int                        quesCount;            /* 问答数量 */
/* 查询达人个人信息 */
- (RACSignal *)expertUser:(NSDictionary *)dic path:(NSString *)pathUrl;
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end

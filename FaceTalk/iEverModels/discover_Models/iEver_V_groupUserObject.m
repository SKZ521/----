//
//  iEver_V_groupUserObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-14.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_V_groupUserObject.h"

@implementation iEver_V_groupUserObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }

    /* coverList object  */
    NSMutableArray *array_expertUser_object = [[NSMutableArray alloc] init];
    array_expertUser_object                 = [attributes objectForKey:@"expertUserList"];
    self._expertUserList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_expertUser_object count]; i++) {
        iEver_V_userList_object *user_array_object = [[iEver_V_userList_object alloc] initWithAttributes:[array_expertUser_object objectAtIndex:i]];
        [self._expertUserList addObject:user_array_object];
    }

    return self;
}
/* 达人分组列表接口 */
- (RACSignal *)queryByGroup:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL /expertUser/queryByGroup */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:@"/expertUser/queryByGroup"
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_V_groupUserObject * object = [[iEver_V_groupUserObject alloc] initWithAttributes:responseObject];
                return object;
            }];
}
@end


/* userList object  */
@implementation iEver_V_userList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.opened                       = NO;
    self.categoryName                 = [attributes objectForKey:@"categoryName"];
    /* coverList object  */
    NSMutableArray *array_user_object = [[NSMutableArray alloc] init];
    array_user_object                 = [attributes objectForKey:@"userList"];
    self._userList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_user_object count]; i++) {
        iEver_V_user_object *user_array_object = [[iEver_V_user_object alloc] initWithAttributes:[array_user_object objectAtIndex:i]];
        [self._userList addObject:user_array_object];
    }

	return self;
}
@end


/* user object  */
@implementation iEver_V_user_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.User_id             = [[attributes objectForKey:@"id"] integerValue];
    self.nickName            = [attributes objectForKey:@"nickName"];
    self.headImg             = [attributes objectForKey:@"headImg"];
    self.intro               = [attributes objectForKey:@"intro"];
    self.answerTotal         = [[attributes objectForKey:@"answerTotal"] integerValue];
	return self;
}
@end
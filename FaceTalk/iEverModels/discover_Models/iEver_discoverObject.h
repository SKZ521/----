//
//  iEver_discoverObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-9.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_discoverObject : NSObject
/*  content LIST Object array */
@property (nonatomic, retain) NSMutableArray             *_discoverList;
@property (nonatomic, assign) int                        pageSize;         /* pageSize */
@property (nonatomic, assign) int                        resultCode;       /* resultCode */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
/*  expertArticle data mothod */
- (RACSignal *)expertArticle:(NSDictionary *)dic path:(NSString *)pathUrl;

@end

/* coverList object  */
@interface iEver_discoverContentList_object : NSObject

@property (nonatomic, assign) int                        Cover_id;       /* id */
@property (nonatomic, assign) BOOL                       _selected;       /* 是否点赞 */
@property (nonatomic, copy  ) NSString                   *articleTitle;  /* 文章标题 */
@property (nonatomic, copy  ) NSString                   *articleDesc;   /* 文章描述 */
@property (nonatomic, copy  ) NSString                   *coverImg;      /* 封面图片 */
@property (nonatomic, copy  ) NSString                   *coverDesc;     /* 封面描述 */
@property (nonatomic, copy  ) NSString                   *tags;          /* 封面描述 */
@property (nonatomic, assign) int                        releaseStatus;  /* 发布状态 */
@property (nonatomic, assign) long long                  timerDate;      /* 定时时间 */
@property (nonatomic, assign) int                        userId;         /* 用户ID */
@property (nonatomic, assign) int                        status;         /* 状态 */
@property (nonatomic, assign) long long                  releaseTime;    /* 发布时间 */
@property (nonatomic, assign) long long                  createTime;     /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;     /* 更新时间 */
@property (nonatomic, assign) int                        pvTotal;        /* 浏览次数  */
@property (nonatomic, assign) int                        likeTotal;      /* 商品ID  */
@property (nonatomic, assign) int                        commentTotal;   /* 商品ID  */
@property (nonatomic, assign) int                        isLike;         /* 是否喜欢  */
@property (nonatomic, assign) int                        isBrowse;       /* 是否浏览  */
@property (nonatomic, assign) int                        isComment;      /* 是否评论  */
@property (nonatomic, copy  ) NSString                   *nickName;      /* 作者昵称 */
@property (nonatomic, copy  ) NSString                   *headImg;       /* 视频链接 */
@property (nonatomic, copy  ) NSString                   *categoryName;     /* 视频描述 */
@property (nonatomic, assign) int                        didSelectIndexPath;/* 选中cell的indexpath  */

@property (nonatomic, copy  ) NSString                   *videoLink;      /* 作者昵称 */
@property (nonatomic, copy  ) NSString                   *mVideoLink;

/*  tagsList Object array */
@property (nonatomic, retain) NSMutableArray            *_tagsList;

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
@end


/* _tagsList object */
@interface iEver_V_PersonList_tagsList_object : NSObject

@property (nonatomic, assign) int                        tagId;         /* tagid */
@property (nonatomic, assign) int                        businessId;    /* 事物id */
@property (nonatomic, copy  ) NSString                   *tagName;      /* 标签名称 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end

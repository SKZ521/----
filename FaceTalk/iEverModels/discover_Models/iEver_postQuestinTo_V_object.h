//
//  iEver_postQuestinTo_V_object.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-16.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_postQuestinTo_V_object : NSObject

/* 向达人提问 */
- (RACSignal *)expertQuestion:(NSDictionary *)dic path:(NSString *)pathUrl;
/* 达人回答问题 */
- (RACSignal *)answerQuestion:(NSDictionary *)dic path:(NSString *)pathUrl;

@end

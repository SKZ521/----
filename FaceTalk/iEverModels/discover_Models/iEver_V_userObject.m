//
//  iEver_V_userObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-10.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_V_userObject.h"

@implementation iEver_V_userObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.pageSize                           = 1;
    self.resultCode                         = [[attributes objectForKey:@"resultCode"] integerValue];
    /* coverList object  */
    NSMutableArray *array_content_object = [[NSMutableArray alloc] init];
    array_content_object                 = [attributes objectForKey:@"userList"];
    self._userList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_content_object count]; i++) {
        iEver_discover_userList_object *user_object = [[iEver_discover_userList_object alloc] initWithAttributes:[array_content_object objectAtIndex:i]];
        [self._userList addObject:user_object];
    }
	return self;
}


/* 达人根据回答量排序 */
- (RACSignal *)queryOrderByAnswer:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL expertUser/queryOrderByAnswer */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:@"expertUser/queryOrderByAnswer"
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_V_userObject * object = [[iEver_V_userObject alloc] initWithAttributes:responseObject];
                return object;
            }];
    
}

@end


/* coverList object  */
@implementation iEver_discover_userList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.User_id             = [[attributes objectForKey:@"id"] integerValue];
    self.nickName            = [attributes objectForKey:@"nickName"];
    self.headImg             = [attributes objectForKey:@"headImg"];
    self.intro               = [attributes objectForKey:@"intro"];
    self.answerTotal         = [[attributes objectForKey:@"answerTotal"] integerValue];
    self.articleTotal        = [[attributes objectForKey:@"articleTotal"] integerValue];
	return self;
}
@end

//
//  iEver_V_answeredQuestionObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_V_answeredQuestionObject : NSObject
/*  _quesList  array */
@property (nonatomic, retain) NSMutableArray             *_quesList;
@property (nonatomic, assign) int                        pageSize;         /* pageSize */
@property (nonatomic, assign) int                        resultCode;       /* resultCode */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
/* 达人已回答问题列表 */
- (RACSignal *)queryAnsweredByUser:(NSDictionary *)dic path:(NSString *)pathUrl;
@end

/* quesList object  */
@interface iEver_V_quesList_object : NSObject
// 提问者相关
@property (nonatomic, assign) int                        ques_id;        /* 1、问题ID */
@property (nonatomic, assign) int                        qCategory_id;   /* 2、问题分类ID */
@property (nonatomic, assign) int                        qUserId;        /* 3、问题用户ID */
@property (nonatomic, copy  ) NSString                   *qContent;      /* 4、问题内容 */
@property (nonatomic, assign) long long                  createTime;     /* 5、创建时间 */
@property (nonatomic, copy  ) NSString                   *qNickName;     /* 6、问者昵称 */
@property (nonatomic, copy  ) NSString                   *qHeadImg;      /* 7、问者头像 */
@property (nonatomic, assign) int                        answerTotal;    /* 8、回答总数 */
@property (nonatomic, retain) NSMutableArray             *_QquesPicList; /* 9、提问者图片 */

// 回答者相关
@property (nonatomic, assign) int                        aId;            /* 1、回答Id */
@property (nonatomic, assign) int                        aUserId;        /* 2、回答用户id */
@property (nonatomic, copy  ) NSString                   *aNickName;     /* 3、答者昵称 */
@property (nonatomic, copy  ) NSString                   *aHeadImg;      /* 4、答者头像 */
@property (nonatomic, assign) int                        aUserType;      /* 5、回答用户类型 10普通用户 20达人用户 */
@property (nonatomic, copy  ) NSString                   *aContent;      /* 6、回答内容 */
@property (nonatomic, assign) long long                  aCreateTime;    /* 7、回答时间 */
@property (nonatomic, assign) BOOL                       isPraise;       /* 8、回答是否被点赞 */
@property (nonatomic, assign) int                        praiseTotal;    /* 9、点赞总数 */
@property (nonatomic, retain) NSMutableArray             *_AquesPicList; /* 10、回答者图片 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end

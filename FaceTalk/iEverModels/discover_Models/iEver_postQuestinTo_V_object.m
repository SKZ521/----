//
//  iEver_postQuestinTo_V_object.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-16.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_postQuestinTo_V_object.h"

@implementation iEver_postQuestinTo_V_object

/* 向达人提问 */
- (RACSignal *)expertQuestion:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* 线上：/expertQuestion/insert 检测接口修改成：expertQuestion/insert */
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/expertQuestion/asking"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}
/* 达人回答问题 */
- (RACSignal *)answerQuestion:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/expertQuestion/answer"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}
@end

//
//  iEver_discoverObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-9.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_discoverObject.h"

@implementation iEver_discoverObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }

    /* coverList object  */
    NSMutableArray *array_content_object  = [[NSMutableArray alloc] init];
    array_content_object                  = [attributes objectForKey:@"coverList"];
    self.pageSize                         = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    self._discoverList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_content_object count]; i++) {
        iEver_discoverContentList_object *content_object = [[iEver_discoverContentList_object alloc] initWithAttributes:[array_content_object objectAtIndex:i]];
        [self._discoverList addObject:content_object];
    }

	return self;
}
/*  expertArticle data mothod */
- (RACSignal *)expertArticle:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL article/queryCoverByRootCate/ */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_discoverObject * object = [[iEver_discoverObject alloc] initWithAttributes:responseObject];
                return object;
            }];

}

@end


/* coverList object  */
@implementation iEver_discoverContentList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Cover_id             = [[attributes objectForKey:@"id"] integerValue];
    self.articleTitle         = [attributes objectForKey:@"articleTitle"];
    self.coverImg             = [attributes objectForKey:@"coverImg"];
    self.coverDesc            = [attributes objectForKey:@"coverDesc"];
    self.tags                 = [attributes objectForKey:@"tags"];
    self.releaseStatus        = [[attributes objectForKey:@"releaseStatus"] integerValue];
    self.timerDate            = [[attributes objectForKey:@"timerDate"] longLongValue];
    self.userId               = [[attributes objectForKey:@"userId"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.releaseTime          = [[attributes objectForKey:@"releaseTime"] longLongValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.pvTotal              = [[attributes objectForKey:@"pvTotal"] integerValue];
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];
    self.commentTotal         = [[attributes objectForKey:@"commentTotal"] integerValue];
    if ([[attributes objectForKey:@"isLike"] integerValue] >0) {
        self._selected            = YES;
    }else{
        self._selected            = NO;
    }
    self.isBrowse             = [[attributes objectForKey:@"isBrowse"] integerValue];
    self.isComment            = [[attributes objectForKey:@"isComment"] integerValue];
    self.nickName             = [attributes objectForKey:@"nickName"];
    self.headImg              = [attributes objectForKey:@"headImg"];
    self.categoryName         = [attributes objectForKey:@"categoryName"];
    self.videoLink            = [attributes objectForKey:@"videoLink"];
    self.mVideoLink            = [attributes objectForKey:@"mVideoLink"];


    /* _tagsList object */
    NSMutableArray *array_tags_object = [[NSMutableArray alloc] init];
    array_tags_object                 = [attributes objectForKey:@"tagsList"];
    self._tagsList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_tags_object count]; i++) {
        iEver_V_PersonList_tagsList_object *tags_object = [[iEver_V_PersonList_tagsList_object alloc] initWithAttributes:[array_tags_object objectAtIndex:i]];
        [self._tagsList addObject:tags_object];
    }

	return self;
}
@end

/* _tagsList object */
@implementation iEver_V_PersonList_tagsList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.tagId                = [[attributes objectForKey:@"tagId"] integerValue];
    self.businessId           = [[attributes objectForKey:@"businessId"] integerValue];
    self.tagName              = [attributes  objectForKey:@"tagName"];

    return self;
}

@end


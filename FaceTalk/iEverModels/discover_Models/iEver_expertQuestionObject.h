//
//  iEver_expertQuestionObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/24.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_expertQuestionObject : NSObject

@property (nonatomic, retain) NSMutableArray             *_quesList;
@property (nonatomic, assign) int                        pageSize;         /* pageSize */
@property (nonatomic, assign) int                        resultCode;       /* resultCode */
@property (nonatomic, copy  ) NSString                   *judgmentDate;    /* 判断统一日期 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

/* 查询全部或者分类下的问题列表 */
- (RACSignal *)expertQuestionQueryAll:(NSDictionary *)dic path:(NSString *)pathUrl;

@end


/* quesList object  */
@interface iEver_quesList_object : NSObject

@property (nonatomic, assign) int                        ques_id;                      /* 问题ID */
@property (nonatomic, assign) int                        qCategoryId;                  /* 问题分类ID */
@property (nonatomic, assign) int                        qUserId;                      /* 问题用户ID */
@property (nonatomic, copy  ) NSString                   *qTitle;                      /* 问题标题 */
@property (nonatomic, copy  ) NSString                   *qContent;                    /* 问题内容 */
@property (nonatomic, assign) int                        qStatus;                      /* 问题状态 */
@property (nonatomic, assign) int                        status;                       /* 状态 */
@property (nonatomic, assign) long long                  createTime;                   /* 创建时间 */
@property (nonatomic, copy  ) NSString                   *qNickName;                   /* 问者昵称 */
@property (nonatomic, assign) long long                  updateTime;                   /* 更新时间 */
@property (nonatomic, copy  ) NSString                   *creeatTimeFormatStr;         /* 时间格式字符 */
@property (nonatomic, copy  ) NSString                   *qHeadImg;                    /* 问者头像 */
@property (nonatomic, assign) int                        answerTotal;                  /* 问题数量 */
@property (nonatomic, assign) BOOL                       isSectionFirstData;           /* 是否点赞 */
/*  quesList  image array */
@property (nonatomic, retain) NSMutableArray             *_quesPicList;                /* 问题图片 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
@end

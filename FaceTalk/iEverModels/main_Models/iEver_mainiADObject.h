//
//  iEver_mainiADObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_mainiADObject : NSObject
@property (nonatomic, strong) NSMutableArray *adsList;
@property (nonatomic, strong) NSMutableArray *activityList;
@property (nonatomic, assign) int            resultCode;       /* resultCode */
//获取首页广告栏
- (RACSignal *)acquireIADSListsData:(NSDictionary *)dic;
@end

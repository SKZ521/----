//
//  iEver_IAD.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_IAD : NSObject

@property (assign, nonatomic) NSInteger id;
@property (assign, nonatomic) NSInteger homeType;/*10文章；11达人；12活动；13商品；20文章分类；21榜单；22活动列表*/
@property (assign, nonatomic) NSInteger businessId;/*业务ID*/
@property (copy, nonatomic)   NSString *coverImg;/*封面图片*/
@property (assign, nonatomic) NSInteger sortLevel;/*排序值*/

- (id)initWithAttributes:(NSDictionary *)attributes;
@end

//
//  iEver_IAD.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_IAD.h"
#import "NSObject+Reflection.h"
@implementation iEver_IAD

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
	[self reflectDataFromObject: attributes];
	return self;
}

@end

//
//  iEver_commentObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/1.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_commentObject : NSObject

/* 插入商品评论 */
- (RACSignal *)insertItemComment:(NSDictionary *)dic path:(NSString *)pathUrl;
/* 插入文章评论 */
- (RACSignal *)insertArticleComment:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 插入活动评论 */
- (RACSignal *)insertTryComment:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 插入问题的中回答的评论 */
- (RACSignal *)insertAnswerComment:(NSDictionary *)dic path:(NSString *)pathUrl;

@end

//
//  iEver_defineDicObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-20.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_defineDicObject : NSObject
/*  Color LIST Object array */
@property (nonatomic, retain) NSMutableDictionary        *_ColorDic;

/*  name LIST Object array */
@property (nonatomic, retain) NSMutableDictionary        *_nameDic;
@end

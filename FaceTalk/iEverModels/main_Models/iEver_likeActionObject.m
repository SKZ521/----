//
//  iEver_likeActionObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_likeActionObject.h"

@implementation iEver_likeActionObject
/* 喜欢接口 */
- (RACSignal *)like:(NSDictionary *)dic path:(NSString *)pathUrl {

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/like/insert"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];

}

/* 取消喜欢接口 */
- (RACSignal *)unlike:(NSDictionary *)dic path:(NSString *)pathUrl {

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/like/deleteLike"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}


/* 对问题下回答的点赞 (此接口必须登录) */
- (RACSignal *)answerPraise:(NSDictionary *)dic path:(NSString *)pathUrl {

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/expertQuestion/answerPraise"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];

}

/* 取消回答下自己的点赞 (此接口必须登录，且只能删除自己的点赞) */
- (RACSignal *)delAnswerPraise:(NSDictionary *)dic path:(NSString *)pathUrl {

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/expertQuestion/delAnswerPraise"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

@end

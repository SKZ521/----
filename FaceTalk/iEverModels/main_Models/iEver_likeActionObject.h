//
//  iEver_likeActionObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_likeActionObject : NSObject

/* 喜欢接口 */
- (RACSignal *)like:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 取消喜欢接口 */
- (RACSignal *)unlike:(NSDictionary *)dic path:(NSString *)pathUrl;


/* 对问题下回答的点赞 (此接口必须登录) */
- (RACSignal *)answerPraise:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 取消回答下自己的点赞 (此接口必须登录，且只能删除自己的点赞) */
- (RACSignal *)delAnswerPraise:(NSDictionary *)dic path:(NSString *)pathUrl;

@end

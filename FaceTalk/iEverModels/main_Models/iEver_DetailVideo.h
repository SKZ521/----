//
//  iEver_DetailVideo.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-14.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_DetailVideo : NSObject
- (RACSignal *)acquireDetailVideoData:(NSDictionary *)dic path:(NSString *)pathUrl;
@end

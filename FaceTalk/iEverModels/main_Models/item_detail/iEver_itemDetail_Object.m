//
//  iEver_itemDetail_Object.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-23.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_itemDetail_Object.h"
#import "iEver_imageAFAPIClient.h"
#import "JSONKit.h"
#import "AFJSONRequestOperation.h"
#import "AFNetworking.h"
@implementation iEver_itemDetail_Object
- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self._arrayCountnumber = 0;

    self.pageSize                         = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    self.itemCommentTotal              = [[attributes objectForKey:@"itemCommentTotal"] integerValue];
    
    /* _itemList object */
    NSMutableArray *array_item_object = [[NSMutableArray alloc] init];
    array_item_object                 = [attributes objectForKey:@"recommendItemList"];
    self._itemList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_item_object count]; i++) {
        iEver_detail_recommendItemList_object *category_object = [[iEver_detail_recommendItemList_object alloc] initWithAttributes:[array_item_object objectAtIndex:i]];
        [self._itemList addObject:category_object];
    }

    if ([self._itemList count] > 0) {
        self._arrayCountnumber ++;
    }

    /* _itemCover object  */

    iEver_detail_itemCover_object *itemCover_object = [[iEver_detail_itemCover_object alloc] initWithAttributes:[attributes objectForKey:@"item"]];
    self._itemCover = itemCover_object;

    /* _itemCover object  */

    if ([[attributes objectForKey:@"itemTop"] isKindOfClass:[NSDictionary class]]) {
        iEver_item_detail_itemTop_object *itemTop_object = [[iEver_item_detail_itemTop_object alloc] initWithAttributes:[attributes objectForKey:@"itemTop"]];
        self._itemTop = itemTop_object;
    }
    /* _itemPicList object  */
    NSMutableArray *array_itemPicList_object = [[NSMutableArray alloc] init];
    array_itemPicList_object                 = [attributes objectForKey:@"itemPicList"];
    self._itemPicList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_itemPicList_object count]; i++) {
        iEver_detail_itemPicList_object *content_object = [[iEver_detail_itemPicList_object alloc] initWithAttributes:[array_itemPicList_object objectAtIndex:i]];

        if (i == [array_itemPicList_object count] - 1) {
            content_object.endCell = 2;
        }else{
            content_object.endCell = 1;
        }
        [self._itemPicList addObject:content_object];
    }
    if ([self._itemPicList count] > 0) {
        self._arrayCountnumber ++;
    }

    /* recommendCoverList object  */
    NSMutableArray *array_recommendItemList_object = [[NSMutableArray alloc] init];
    array_recommendItemList_object                 = [attributes objectForKey:@"recommendItemList"];
    self._recommendItemList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_recommendItemList_object count]; i++) {
        iEver_detail_recommendItemList_object *content_object = [[iEver_detail_recommendItemList_object alloc] initWithAttributes:[array_recommendItemList_object objectAtIndex:i]];
        [self._recommendItemList addObject:content_object];
    }
    if ([self._recommendItemList count] > 0) {
        self._arrayCountnumber ++;
    }

    /* itemCommentList object  */
    NSMutableArray *array_itemCommentList_object = [[NSMutableArray alloc] init];
    array_itemCommentList_object                 = [attributes objectForKey:@"itemCommentList"];
    self._itemCommentList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_itemCommentList_object count]; i++) {
        iEver_detail_itemCommentList_object *content_object = [[iEver_detail_itemCommentList_object alloc] initWithAttributes:[array_itemCommentList_object objectAtIndex:i]];
        [self._itemCommentList addObject:content_object];
    }
    if ([self._itemCommentList count] > 0) {
        self._arrayCountnumber ++;
    }
    self._arrayCountnumber = 3;
	return self;
}
/*  content LIST Object array */
- (RACSignal *)acquireDetailData:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL /item/queryItemById/4 */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_itemDetail_Object * object = [[iEver_itemDetail_Object alloc] initWithAttributes:responseObject];
                return object;
            }];
}

@end

/* _itemCover object  */
@implementation iEver_detail_itemCover_object

- (BOOL)isNilObj:(id)obj {

    if (!obj) {
        return NO;
    }
    if ([obj  isKindOfClass:[NSNull  class]]) {
        return NO;
    }
    NSString  *str = [NSString  stringWithFormat:@"%@",obj];
    if ([str length] > 0) {
        return YES;
    }
    return NO;

}
- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Cover_id             = [[attributes objectForKey:@"id"] integerValue];
    self.rootCategoryId       = [[attributes objectForKey:@"rootCategoryId"] integerValue];
    self.secondCategoryId     = [[attributes objectForKey:@"secondCategoryId"] integerValue];
    self.threeCategoryId      = [[attributes objectForKey:@"threeCategoryId"] integerValue];
    self.leafLevel            = [[attributes objectForKey:@"leafLevel"] integerValue];
    self.itemName             = [attributes objectForKey:@"itemName"];
    self.itemDesc             = [attributes objectForKey:@"itemDesc"];
    self.itemColor            = [attributes objectForKey:@"itemColor"];
    self.itemSpec             = [attributes objectForKey:@"itemSpec"];
    self.itemImg              = [attributes objectForKey:@"itemImg"];
    self.price                = [[attributes objectForKey:@"price"] integerValue];
    self.itemLink             = [attributes objectForKey:@"itemLink"];
    self.videoLink            = [attributes objectForKey:@"videoLink"];
    self.webUrl               = [attributes objectForKey:@"webUrl"];
    self.webPvTotal           = [[attributes objectForKey:@"webPvTotal"] integerValue];


    if (self.videoLink.length > 3) {

        NSString  *key = [[NSString  stringWithFormat:@"%@letv&flvcd",self.videoLink] MD5Digest];
        NSString   *requestURL = [[NSString  stringWithFormat:@"http://vpsea.flvcd.com/parse-le.php?url=%@&format=高清&key=%@",self.videoLink,key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *request = [[iEver_imageAFAPIClient sharedClient:requestURL] requestWithMethod:@"GET" path:nil parameters:nil];
        AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [requestOperation start];
        [requestOperation waitUntilFinished];
        NSDictionary *result = [requestOperation.responseString objectFromJSONStringWithParseOptions:JKParseOptionLooseUnicode];
        if([[result  objectForKey:@"TYPE"]  isEqualToString:@"DIRECT"]) {
            NSArray  *playList = [result  objectForKey:@"V"];
            if ([playList  count] >= 1) {

                NSDictionary  *playUnit = [playList  lastObject];
                if ([self  isNilObj:[playUnit   objectForKey:@"U"]]) {
                    self.V_videoLink = [playUnit   objectForKey:@"U"];
                    if (self.V_videoLink.length == 0) {
                        self.V_videoLink = [attributes objectForKey:@"mVideoLink"];
                    }
                }
            }
        }
    }else{
        self.V_videoLink = [attributes objectForKey:@"mVideoLink"];
    }
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.itemTryId            = [[attributes objectForKey:@"itemTryId"] integerValue];
    self.pvTotal              = [[attributes objectForKey:@"pvTotal"] integerValue];
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];
    self.commentTotal         = [[attributes objectForKey:@"commentTotal"] integerValue];
    if ([[attributes objectForKey:@"isLike"] integerValue] >0) {
        self._selected            = YES;
    }else{
        self._selected            = NO;
    }

    /* _tagsList object */
    NSMutableArray *array_tags_object = [[NSMutableArray alloc] init];
    array_tags_object                 = [attributes objectForKey:@"tagsList"];
    self._tagsList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_tags_object count]; i++) {
        iEver_item_detail_tagsList_object *tags_object = [[iEver_item_detail_tagsList_object alloc] initWithAttributes:[array_tags_object objectAtIndex:i]];
        [self._tagsList addObject:tags_object];
    }

    //评测添加字段
    self.startGrade               = [[attributes objectForKey:@"startGrade"] floatValue];
    self.gradeDesc                = [attributes objectForKey:@"gradeDesc"];


	return self;
}
@end

/* _itemTop object */
@implementation iEver_item_detail_itemTop_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.sort_level                = [[attributes objectForKey:@"sort_level"] integerValue];
    self.category_name             = [attributes  objectForKey:@"category_name"];

    return self;
}

@end


/* _tagsList object */
@implementation iEver_item_detail_tagsList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.tagId                = [[attributes objectForKey:@"tagId"] integerValue];
    self.businessId           = [[attributes objectForKey:@"businessId"] integerValue];
    self.tagName              = [attributes  objectForKey:@"tagName"];

    return self;
}

@end

/* _itemPicList object  */
@implementation iEver_detail_itemPicList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Pic_id               = [[attributes objectForKey:@"id"] integerValue];
    self.itemId       = [[attributes objectForKey:@"itemId"] integerValue];
    self.imgTitle               = [attributes objectForKey:@"imgTitle"];
    self.imgUrl               = [attributes objectForKey:@"imgUrl"];
    /* 获得图片的高度 商品图片统一 不再需要 先注释 */
    if (self.imgUrl.length > 300) {
        NSString *str = [NSString stringWithFormat:@"%@?imageInfo",self.imgUrl];

        NSMutableURLRequest *request = [[iEver_imageAFAPIClient sharedClient:str] requestWithMethod:@"GET" path:nil parameters:nil];
        AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [requestOperation start];
        [requestOperation waitUntilFinished];
        NSDictionary *result = [requestOperation.responseString objectFromJSONStringWithParseOptions:JKParseOptionLooseUnicode];
        self.image_width = [[result objectForKey:@"width"] integerValue];
        self.image_height= [[result objectForKey:@"height"] integerValue];
    }else{
        self.image_width = 0;
        self.image_height= 0;
    }
    self.imgDesc              = [attributes objectForKey:@"imgDesc"];
    self.startGrade           = [[attributes objectForKey:@"startGrade"] floatValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
	return self;
}
@end

/* _recommendItemList object */
@implementation iEver_detail_recommendItemList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Item_id           = [[attributes objectForKey:@"id"] integerValue];
    self.rootCategoryId    = [[attributes objectForKey:@"rootCategoryId"] integerValue];
    self.secondCategoryId  = [[attributes objectForKey:@"secondCategoryId"] integerValue];
    self.threeCategoryId   = [[attributes objectForKey:@"threeCategoryId"] integerValue];
    self.leafLevel         = [[attributes objectForKey:@"leafLevel"] integerValue];
    self.itemName          = [attributes objectForKey:@"itemName"];
    self.itemDesc          = [attributes objectForKey:@"itemDesc"];
    self.itemColor         = [attributes objectForKey:@"itemColor"];
    self.itemSpec          = [attributes objectForKey:@"itemSpec"];
    self.itemImg           = [attributes objectForKey:@"itemImg"];
    self.price             = [[attributes objectForKey:@"price"] integerValue];
    self.itemLink          = [attributes objectForKey:@"itemLink"];
    self.videoLink         = [attributes objectForKey:@"videoLink"];
    self.videoDesc         = [attributes objectForKey:@"videoDesc"];
    self.sortLevel         = [attributes objectForKey:@"sortLevel"];
    self.status            = [[attributes objectForKey:@"status"] integerValue];
    self.createTime        = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime        = [[attributes objectForKey:@"updateTime"] longLongValue];
	return self;
}
@end

/* _itemCommentList object */
@implementation iEver_detail_itemCommentList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Comment_id           = [[attributes objectForKey:@"id"] integerValue];
    self.itemId               = [[attributes objectForKey:@"itemId"] integerValue];
    self.userId               = [[attributes objectForKey:@"userId"] integerValue];
    self.commentContent       = [attributes objectForKey:@"commentContent"];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.replyTotal           = [[attributes objectForKey:@"replyTotal"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.nickName             = [attributes objectForKey:@"nickName"];
    self.feature              = [attributes objectForKey:@"feature"];
    self.headImg              = [attributes objectForKey:@"headImg"];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    if ([[attributes objectForKey:@"isLike"] integerValue] >0) {
        self._selected            = YES;
    }else{
        self._selected            = NO;
    }
    self.likeTotal               = [[attributes objectForKey:@"likeTotal"] integerValue];

    /* _replyList object  */
    NSMutableArray *array_replyList_object = [[NSMutableArray alloc] init];
    array_replyList_object                 = [attributes objectForKey:@"replyList"];
    self._replyList                      = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_replyList_object count]; i++) {
        iEver_itemDetail_comment_ReplyList_object *_comment_ReplyList_object = [[iEver_itemDetail_comment_ReplyList_object alloc] initWithAttributes:[array_replyList_object objectAtIndex:i]];
        [self._replyList addObject:_comment_ReplyList_object];
    }


	return self;
}

@end

/* _replyList object */
@implementation iEver_itemDetail_comment_ReplyList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.ReplyComment_id      = [[attributes objectForKey:@"id"] integerValue];
    self.cover_id             = [[attributes objectForKey:@"coverId"] integerValue];
    self.parent_id            = [[attributes objectForKey:@"parentId"] integerValue];
    self.userId               = [[attributes objectForKey:@"userId"] integerValue];
    self.atUserId             = [[attributes objectForKey:@"atUserId"] integerValue];
    self.commentContent       = [attributes  objectForKey:@"commentContent"];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.nickName             = [attributes  objectForKey:@"nickName"];
    self.atNickName           = [attributes  objectForKey:@"atNickName"];
    self.headImg              = [attributes  objectForKey:@"headImg"];
    self.feature              = [attributes  objectForKey:@"feature"];
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];

    return self;
}

@end


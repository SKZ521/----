//
//  iEver_tryItemDetailObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-30.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>


/* itemTry object */
@interface iEver_itemTry_object : NSObject

@property (nonatomic, assign) int                        T_ID;                 /* 数据id */
@property (nonatomic, copy  ) NSString                   *tryImg;              /* 活动图片 */
@property (nonatomic, copy  ) NSString                   *tryName;             /* 活动名称 */
@property (nonatomic, assign) int                        itemId;               /* 商品ID */
@property (nonatomic, assign) long long                  startTime;            /* 开始时间 */
@property (nonatomic, assign) long long                  endTime;              /* 结束时间 */
@property (nonatomic, assign) int                        tryNum;               /* 试用个数 */
@property (nonatomic, copy  ) NSString                   *tryExplain;          /* 活动描述 */
@property (nonatomic, assign) int                        status;               /* 活动状态 */
@property (nonatomic, assign) int                        applyTotal;           /* 申请个数 */
@property (nonatomic, assign) int                        isApply;              /* 是否申请 */
@property (nonatomic, copy  ) NSString                   *itemName;            /* 商品名称 */
@property (nonatomic, copy  ) NSString                   *itemImg;             /* 商品图片 */
@property (nonatomic, assign) int                        price;                /* 使用价格 */
@property (nonatomic, copy  ) NSString                   *itemSpec;            /* 商品图片 */
@property (nonatomic, copy  ) NSString                   *webUrl;

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end



/* successList object */
@interface iEver_successList_object : NSObject

@property (nonatomic, assign) int                        S_ID;                 /* 成功申请ID */
@property (nonatomic, copy  ) NSString                   *nickName;            /* 用户昵称 */
@property (nonatomic, copy  ) NSString                   *headImg;             /* 用户头像 */
@property (nonatomic, assign) int                        point;                /* 用户积分 */
@property (nonatomic, copy  ) NSString                   *intro;               /* 用户简介 */
@property (nonatomic, assign) long long                  createTime;           /* 成功时间 */
@property (nonatomic, copy  ) NSString                   *feature;             /* 用户个性标签 */
@property (nonatomic, copy  ) NSString                   *commentContent;      /* 用户评论 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end


@interface iEver_tryItemDetailObject : NSObject

/* 查询试用申请信息 */
- (RACSignal *)itemTryApplyQueryByUserId:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 根据试用id查询 */
- (RACSignal *)queryByItemTryId:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 申请试用 */
- (RACSignal *)itemTryApply:(NSDictionary *)dic path:(NSString *)pathUrl;

/*  试用名单 */
@property (nonatomic, retain) NSMutableArray             *_itemTryDetailArray;
/*  试用名单 */
@property (nonatomic, retain) NSMutableArray             *_itemTryNameList;

/*  试用详情 */
@property (nonatomic, retain) iEver_itemTry_object      *_itemDetailObject;
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end

/* successList object */
@interface iEver_itemTryApply_object : NSObject

@property (nonatomic, copy  ) NSString                   *itemTryApply;         /* 用户昵称 */
@property (nonatomic, copy  ) NSString                   *realName;            /* 用户昵称 */
@property (nonatomic, copy  ) NSString                   *address;             /* 用户头像 */
@property (nonatomic, copy  ) NSString                   *phone;               /* 用户简介 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end



//
//  iEver_mainDetail_Object.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-6.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

/* articleCover object */
@interface iEver_detail_articleCover_object : NSObject

@property (nonatomic, assign) int                        Cover_id;       /* id */
@property (nonatomic, assign) int                        rootCategoryId;  /* 一级分类 */
@property (nonatomic, assign) int                        secondCategoryId;/* 二级分类 */
@property (nonatomic, assign) int                        threeCategoryId; /* 三级分类 */
@property (nonatomic, copy  ) NSString                   *articleTitle;  /* 文章标题 */
@property (nonatomic, copy  ) NSString                   *webUrl;
@property (nonatomic, copy  ) NSString                   *articleDesc;   /* 文章描述 */
@property (nonatomic, copy  ) NSString                   *coverImg;      /* 封面图片 */
@property (nonatomic, copy  ) NSString                   *coverDesc;     /* 封面描述 */
@property (nonatomic, assign) int                        releaseStatus;  /* 发布状态 */
@property (nonatomic, assign) long long                  timerDate;      /* 定时时间 */
@property (nonatomic, assign) int                        type;           /* 10官方文章、20达人文章 */
@property (nonatomic, assign) int                        authorUserId;   /* 作者ID */
@property (nonatomic, copy  ) NSString                   *authorNickName;/* 作者昵称 */
@property (nonatomic, assign) int                        status;         /* 文章状态 */
@property (nonatomic, copy  ) NSString                   *videoLink;     /* 视频链接 */
@property (nonatomic, copy  ) NSString                   *V_videoLink;   /* 真实链接 */
@property (nonatomic, copy  ) NSString                   *videoDesc;     /* 视频描述 */
@property (nonatomic, assign) int                        itemId;         /* 商品ID  */
@property (nonatomic, assign) long long                  createTime;     /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;     /* 更新时间 */
@property (nonatomic, assign) int                        pvTotal;        /* 播放次数  */
@property (nonatomic, assign) int                        likeTotal;      /* 喜欢次数  */
@property (nonatomic, assign) int                        commentTotal;   /* 评论次数  */
@property (nonatomic, assign) BOOL                       _selected;      /* 是否点赞 */
@property (nonatomic, assign) int                        isLike;         /* 是否喜欢  */
@property (nonatomic, assign) int                        isBrowse;       /* 评论次数  */
@property (nonatomic, assign) int                        isComment;      /* 评论次数  */
@property (nonatomic, assign) int                        webPvTotal;

/* V_person add */
@property (nonatomic, copy  ) NSString                   *tags;          /* 文章所属标签 */
@property (nonatomic, assign) int                        userId;         /* 作者ID */
@property (nonatomic, assign) long long                  releaseTime;    /* 发布时间 */
@property (nonatomic, copy  ) NSString                   *nickName;      /* 达人昵称 */
@property (nonatomic, copy  ) NSString                   *headImg;       /* 达人头像 */
@property (nonatomic, copy  ) NSString                   *categoryName;  /* 达人类别 */

/*  tagsList Object array */
@property (nonatomic, retain) NSMutableArray            *_tagsList;
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end


/* _tagsList object */
@interface iEver_detail_tagsList_object : NSObject

@property (nonatomic, assign) int                        tagId;         /* tagid */
@property (nonatomic, assign) int                        businessId;    /* 事物id */
@property (nonatomic, copy  ) NSString                   *tagName;      /* 标签名称 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end



/* _picList object */
@interface iEver_detail_picList_object : NSObject

@property (nonatomic, assign) int                        Pic_id;         /* id */
@property (nonatomic, assign) int                        articleCoverId; /* 属文章id */
@property (nonatomic, assign) int                        image_width;    /* 图片宽度 */
@property (nonatomic, assign) int                        image_height;   /* 图片高度 */
@property (nonatomic, copy  ) NSString                   *imgUrl;        /* 图片地址 */
@property (nonatomic, copy  ) NSString                   *imgDesc;       /* 图片描述 */
@property (nonatomic, copy  ) NSString                   *imgTitle;      /* 图片标题 */
@property (nonatomic, assign) int                        sortLevel;      /* 文章状态 */
@property (nonatomic, assign) int                        status;         /* 短级别 */
@property (nonatomic, assign) long long                  createTime;     /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;     /* 更新时间 */
@property (nonatomic, assign) int                        endCell;         /* 时候最后一个cell */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end

/* itemList object */
@interface iEver_detail_itemList_object : NSObject

@property (nonatomic, assign) int                        Item_id;          /* 推荐商品id */
@property (nonatomic, assign) int                        rootCategoryId;   /* 一级分类 */
@property (nonatomic, assign) int                        secondCategoryId; /* 二级分类 */
@property (nonatomic, assign) int                        threeCategoryId;  /* 三级分类 */
@property (nonatomic, assign) int                        leafLevel;        /* 级别*/
@property (nonatomic, copy  ) NSString                   *itemName;        /* 商品名称 */
@property (nonatomic, copy  ) NSString                   *itemDesc;        /* 商品描述 */
@property (nonatomic, copy  ) NSString                   *itemColor;       /* 商品颜色 */
@property (nonatomic, copy  ) NSString                   *itemSpec;        /* 商品 */
@property (nonatomic, copy  ) NSString                   *itemImg;         /* 商品图片 */
@property (nonatomic, assign) int                        price;            /* 商品价格 */
@property (nonatomic, copy  ) NSString                   *itemLink;        /* 商品链接 */
@property (nonatomic, copy  ) NSString                   *videoLink;       /* 视频链接 */
@property (nonatomic, copy  ) NSString                   *videoDesc;       /* 视频描述 */
@property (nonatomic, copy  ) NSString                   *sortLevel;       /* 短级别 */
@property (nonatomic, assign) int                        status;           /* 商品状态 */
@property (nonatomic, assign) int                        pvTotal;          /* 浏览个数 */
@property (nonatomic, assign) int                        itemTryId;        /* 使用ID */
@property (nonatomic, assign) long long                  createTime;       /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;       /* 更新时间 */
@property (nonatomic, assign) int                        endCell;          /* 推荐商品单元格展现形式 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end

/* recommendCoverList object */
@interface iEver_detail_recommendCoverList_object : NSObject

@property (nonatomic, assign) int                        RecomCover_id;    /* 推荐文章id */
@property (nonatomic, assign) int                        rootCategoryId;   /* 一级分类 */
@property (nonatomic, assign) int                        secondCategoryId; /* 二级分类 */
@property (nonatomic, assign) int                        threeCategoryId;  /* 三级分类 */
@property (nonatomic, copy  ) NSString                   *articleTitle;    /* 文章标题 */
@property (nonatomic, copy  ) NSString                   *articleDesc;     /* 文章描述 */
@property (nonatomic, copy  ) NSString                   *coverImg;        /* 封面图片 */
@property (nonatomic, copy  ) NSString                   *coverDesc;       /* 封面描述 */
@property (nonatomic, assign) int                        releaseStatus;    /* 文章状态 */
@property (nonatomic, assign) long long                  timerDate;        /* 时间日期 */
@property (nonatomic, assign) int                        type;             /* 文章类型 */
@property (nonatomic, assign) int                        authorUserId;     /* 作者ID */
@property (nonatomic, copy  ) NSString                   *authorNickName;  /* 作者名称 */
@property (nonatomic, assign) int                        status;           /* 文章状态 */
@property (nonatomic, copy  ) NSString                   *videoLink;       /* 视频链接 */
@property (nonatomic, copy  ) NSString                   *videoDesc;       /* 视频描述 */
@property (nonatomic, assign) int                        itemId;           /* 商品ID */
@property (nonatomic, assign) long long                  createTime;       /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;       /* 更新时间 */
@property (nonatomic, assign) int                        pvTotal;           /* 视频次数 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end


/* _articleCommentList object */
@interface iEver_detail_articleCommentList_object : NSObject

@property (nonatomic, assign) int                        Comment_id;       /* 评论id */
@property (nonatomic, assign) int                        cover_id;         /* 封面id */
@property (nonatomic, assign) int                        parent_id;        /* 父数据id */
@property (nonatomic, assign) int                        userId;           /* 用户ID */
@property (nonatomic, assign) int                        atUserId;         /* @用户ID */
@property (nonatomic, copy  ) NSString                   *commentContent;  /* 评论内容 */
@property (nonatomic, assign) int                        type;             /* 类型 */
@property (nonatomic, assign) int                        status;           /* 状态 */
@property (nonatomic, assign) long long                  createTime;       /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;       /* 更新时间 */
@property (nonatomic, assign) int                        replyTotal;       /* 评论数据 */
@property (nonatomic, copy  ) NSString                   *nickName;        /* 昵称 */
@property (nonatomic, copy  ) NSString                   *atNickName;      /* @用户昵称 */
@property (nonatomic, copy  ) NSString                   *feature;
@property (nonatomic, copy  ) NSString                   *headImg;         /* 头像图片 */
@property (nonatomic, assign) int                        point;            /* 积分 */
@property (nonatomic, assign) int                        isLike;           /* 是否点赞 */
@property (nonatomic, assign) int                        likeTotal;        /* 点赞个数 */
@property (nonatomic, assign) BOOL                       _selected;        /* 是否点赞 */
@property (nonatomic, assign) int                        endCell;          /* 是否最后一个单元格 */

@property (nonatomic, retain) NSMutableArray             *_replyList;      /* 评论回复的数组 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end


/* _articleCommentList object */
@interface iEver_detail_comment_ReplyList_object : NSObject

@property (nonatomic, assign) int                        ReplyComment_id;  /* 评论id */
@property (nonatomic, assign) int                        cover_id;         /* 封面id */
@property (nonatomic, assign) int                        parent_id;        /* 父数据id */
@property (nonatomic, assign) int                        userId;           /* 用户ID */
@property (nonatomic, assign) int                        atUserId;         /* @用户ID */
@property (nonatomic, copy  ) NSString                   *commentContent;  /* 评论内容 */
@property (nonatomic, assign) int                        type;             /* 类型 */
@property (nonatomic, assign) int                        status;           /* 状态 */
@property (nonatomic, assign) long long                  createTime;       /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;       /* 更新时间 */
@property (nonatomic, copy  ) NSString                   *nickName;        /* 昵称 */
@property (nonatomic, copy  ) NSString                   *atNickName;      /* @用户昵称 */
@property (nonatomic, copy  ) NSString                   *feature;
@property (nonatomic, copy  ) NSString                   *headImg;         /* 头像图片 */
@property (nonatomic, assign) int                        point;            /* 积分 */
@property (nonatomic, assign) int                        isLike;           /* 是否点赞 */
@property (nonatomic, assign) int                        likeTotal;        /* 点赞个数 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end




@interface iEver_mainDetail_Object : NSObject

@property (nonatomic, assign) int                        pageSize;         /* pageSize */
@property (nonatomic, assign) int                        resultCode;       /* resultCode */
@property (nonatomic, assign) int                        articlecommentTotal;
/*  Commodity Object array */
@property (nonatomic, retain) NSMutableArray                     *_itemList;

/*  articleCover Object  */
@property (nonatomic, retain) iEver_detail_articleCover_object   *_articleCover;

/*  Process Object array */
@property (nonatomic, retain) NSMutableArray                     *_picList;

/*  recommendCoverList Object array */
@property (nonatomic, retain) NSMutableArray                     *_recommendCoverList;

/*  _articleCommentList Object array */
@property (nonatomic, retain) NSMutableArray                     *_commentList;

/*  Statistics content array >0 */
@property (nonatomic, assign) int                                _arrayCountnumber;


/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
- (RACSignal *)acquireDetailData:(NSDictionary *)dic path:(NSString *)pathUrl;
@end

//
//  iEver_DetailVideo.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-14.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_DetailVideo.h"

@implementation iEver_DetailVideo

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}


- (RACSignal *)acquireDetailVideoData:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL article/queryCoverByRootCate/ */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return nil;
            }];

}
@end

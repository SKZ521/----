//
//  iEver_mainContent.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-19.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_mainContent.h"
#import "iEver_AppPostJsonAPIClient.h"
#import "iEver_defineDicObject.h"
@implementation iEver_mainContent

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.refreshCount                       = [[attributes objectForKey:@"refreshCount"] integerValue];
    self.pageSize                           = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                         = [[attributes objectForKey:@"resultCode"] integerValue];

    /* _bannerList object  */
    NSMutableArray *array_bannerList_object = [[NSMutableArray alloc] init];
    array_bannerList_object                 = [attributes objectForKey:@"bannerList"];
    self._bannerList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_bannerList_object count]; i++) {
        iEver_bannerList_object *category_object = [[iEver_bannerList_object alloc] initWithAttributes:[array_bannerList_object objectAtIndex:i]];
        [self._bannerList addObject:category_object];
    }
    
    /* _videoBannerList object  */
    NSMutableArray *array_videoBannerList_object = [[NSMutableArray alloc] init];
    array_videoBannerList_object                 = [attributes objectForKey:@"videoBannerList"];
    self._videoBannerList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_videoBannerList_object count]; i++) {
        iEver_videoBannerList_object *videoBannerList_object = [[iEver_videoBannerList_object alloc] initWithAttributes:[array_videoBannerList_object objectAtIndex:i]];
        [self._videoBannerList addObject:videoBannerList_object];
    }
    
    /* _specialBannerList object  */
    NSMutableArray *array_specialBannerList_object = [[NSMutableArray alloc] init];
    array_specialBannerList_object                 = [attributes objectForKey:@"specialBannerList"];
    self._specialBannerList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_specialBannerList_object count]; i++) {
        iEver_specialBannerList_object *_specialBannerList_object = [[iEver_specialBannerList_object alloc] initWithAttributes:[array_specialBannerList_object objectAtIndex:i]];
        [self._specialBannerList addObject:_specialBannerList_object];
    }


    /* coverList object  */
    NSMutableArray *array_content_object = [[NSMutableArray alloc] init];
    array_content_object                 = [attributes objectForKey:@"coverList"];
    self._contentList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_content_object count]; i++) {
        iEver_mainContentList_object *content_object = [[iEver_mainContentList_object alloc] initWithAttributes:[array_content_object objectAtIndex:i]];
        [self._contentList addObject:content_object];
    }

    NSMutableArray *array_secondCategoryList_object = [[NSMutableArray alloc] init];
    array_secondCategoryList_object                 = [attributes objectForKey:@"secondCategoryList"];
    self._secondCategoryList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_secondCategoryList_object count]; i++) {
        iEver_secondCategoryList_object *secondCategory_object = [[iEver_secondCategoryList_object alloc] initWithAttributes:[array_secondCategoryList_object objectAtIndex:i]];
        [self._secondCategoryList addObject:secondCategory_object];
    }


	return self;
}

/*  content LIST Object array */
- (RACSignal *)acquireContentListsData:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL /article/queryCoverByRootCate/ */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                      path:pathUrl
                                                                parameters:dic
			  ]
			 map:^id(NSDictionary *responseObject) {
				 if ([responseObject isKindOfClass:[NSNull class]]) {
					 return nil;
				 }
                 iEver_mainContent * object = [[iEver_mainContent alloc] initWithAttributes:responseObject];
				 return object;
			 }];
}

/*  CoverBySubCate data mothod */
- (RACSignal *)acquireCoverBySubCateListsData:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL /article/queryCoverBySubCate */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_mainContent * object = [[iEver_mainContent alloc] initWithAttributes:responseObject];
                return object;
            }];
}
@end

/* bannerList object */
@implementation iEver_bannerList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Banner_id           = [[attributes objectForKey:@"id"] integerValue];
    self.homeType            = [[attributes objectForKey:@"homeType"] integerValue];
    self.businessId          = [[attributes objectForKey:@"businessId"] integerValue];
    self.coverImg            = [attributes objectForKey:@"coverImg"];
    self.sortLevel           = [[attributes objectForKey:@"sortLevel"] integerValue];
	return self;
}

@end

/* _videoBannerList object */
@implementation iEver_videoBannerList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.articleId           = [[attributes objectForKey:@"businessId"] integerValue];
    self.coverImg            = [attributes objectForKey:@"coverImg"];
    self.homeType            = [[attributes objectForKey:@"homeType"] integerValue];
    return self;
}

@end


/* _specialBannerList object */
@implementation iEver_specialBannerList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.businessId              = [[attributes objectForKey:@"businessId"] integerValue];
    self.specialTitle            = [attributes objectForKey:@"specialTitle"];
    self.coverImg                = [attributes objectForKey:@"coverImg"];
    self.rootCategoryName        = [attributes objectForKey:@"rootCategoryName"];
    self.secondCategoryName      = [attributes objectForKey:@"secondCategoryName"];
    self.articleTitle            = [attributes objectForKey:@"articleTitle"];
    self.videoLink               = [attributes objectForKey:@"videoLink"];
    self.mVideoLink              = [attributes objectForKey:@"mVideoLink"];
    self.expertUserNickName      = [attributes objectForKey:@"expertUserNickName"];
    self.homeType                = [[attributes objectForKey:@"homeType"] integerValue];
    self.pvTotal                 = [[attributes objectForKey:@"pvTotal"] integerValue];
    self.webPvTotal              = [[attributes objectForKey:@"webPvTotal"] integerValue];
    self.bannerReleaseTime       = [[attributes objectForKey:@"bannerReleaseTime"] longLongValue];
    return self;
}

@end



/* coverList object  */
@implementation iEver_mainContentList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Cover_id             = [[attributes objectForKey:@"id"] integerValue];
    self.rootCategoryId       = [[attributes objectForKey:@"rootCategoryId"] integerValue];
    self.threeCategoryId      = [[attributes objectForKey:@"threeCategoryId"] integerValue];
    self.secondCategoryId     = [[attributes objectForKey:@"secondCategoryId"] integerValue];
    self.rootCategoryName     = [attributes objectForKey:@"rootCategoryName"];
    self.secondCategoryName   = [attributes objectForKey:@"secondCategoryName"];
    self.threeCategoryName    = [attributes objectForKey:@"threeCategoryName"];
    /* 只有一级目录 */
    if (self.rootCategoryId!=0&&self.secondCategoryId==0&&self.threeCategoryId==0) {
        self.useCategoryId = self.rootCategoryId;
        self.categoryType  = 10;
        self.useCategoryColor = [attributes objectForKey:@"rootCategoryColor"];
        self.buttonName       = self.rootCategoryName;
        self.tagCategoryName  = [NSMutableString stringWithFormat:@""];

    }/* 一级二级目录 */
    else if (self.rootCategoryId!=0&&self.secondCategoryId!=0&&self.threeCategoryId==0){

        self.useCategoryId = self.secondCategoryId;
        self.categoryType  = 11;
        self.useCategoryColor = [attributes objectForKey:@"secondCategoryColor"];
        self.buttonName       = self.secondCategoryName;
        self.tagCategoryName  = [NSMutableString stringWithFormat:@""];
    }/* 一级二级三级目录 */
    else if (self.rootCategoryId!=0&&self.secondCategoryId!=0&&self.threeCategoryId!=0){

        self.useCategoryId = self.secondCategoryId;
        self.categoryType  = 11;
        self.useCategoryColor = [attributes objectForKey:@"secondCategoryColor"];
        self.buttonName       = self.secondCategoryName;
        self.tagCategoryName  = self.threeCategoryName;
    }
    self.articleTitle         = [attributes objectForKey:@"articleTitle"];

    if ([[attributes objectForKey:@"isLike"] integerValue] >0) {
        self._selected            = YES;
    }else{
        self._selected            = NO;
    }
    self.isBrowse             = [[attributes objectForKey:@"isBrowse"] integerValue];
    self.isComment            = [[attributes objectForKey:@"isComment"] integerValue];
    self.articleDesc          = [attributes objectForKey:@"articleDesc"];
    self.coverImg             = [attributes objectForKey:@"coverImg"];
    self.coverDesc            = [attributes objectForKey:@"coverDesc"];
    self.releaseStatus        = [[attributes objectForKey:@"releaseStatus"] integerValue];
    self.timerDate            = [[attributes objectForKey:@"timerDate"] longLongValue];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.authorUserId         = [[attributes objectForKey:@"authorUserId"] integerValue];
    self.authorNickName       = [attributes objectForKey:@"authorNickName"];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.videoLink            = [attributes objectForKey:@"videoLink"];
    self.mVideoLink           = [attributes objectForKey:@"mVideoLink"];
    self.videoDesc            = [attributes objectForKey:@"videoDesc"];
    self.itemId               = [[attributes objectForKey:@"itemId"] integerValue];
    self.releaseTime          = [[attributes objectForKey:@"releaseTime"] longLongValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.pvTotal              = [[attributes objectForKey:@"pvTotal"] integerValue];
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];
    self.commentTotal         = [[attributes objectForKey:@"commentTotal"] integerValue];

    self.webUrl               = [attributes objectForKey:@"webUrl"];

    self.commentTotal         = [[attributes objectForKey:@"webPvTotal"] integerValue];

    /* _tagsList object */
    NSMutableArray *array_tags_object = [[NSMutableArray alloc] init];
    array_tags_object                 = [attributes objectForKey:@"tagsList"];
    self._tagsList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_tags_object count]; i++) {
        iEver_mainList_tagsList_object *tags_object = [[iEver_mainList_tagsList_object alloc] initWithAttributes:[array_tags_object objectAtIndex:i]];
        [self._tagsList addObject:tags_object];
    }
	return self;
}


@end


/* _secondCategoryList object */
@implementation iEver_secondCategoryList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.Category_id                = [[attributes objectForKey:@"id"] integerValue];
    self.categoryName               = [attributes  objectForKey:@"categoryName"];
    self.parentCategoryId           = [[attributes objectForKey:@"parentCategoryId"] integerValue];
    self.secondCategoryId           = [[attributes objectForKey:@"secondCategoryId"] integerValue];
    self.type                       = [[attributes objectForKey:@"type"] integerValue];
    self.status                     = [[attributes objectForKey:@"status"] integerValue];
    self.color                      = [attributes  objectForKey:@"color"];

    return self;
}

@end



/* _tagsList object */
@implementation iEver_mainList_tagsList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.tagId                = [[attributes objectForKey:@"tagId"] integerValue];
    self.businessId           = [[attributes objectForKey:@"businessId"] integerValue];
    self.tagName              = [attributes  objectForKey:@"tagName"];

    return self;
}

@end

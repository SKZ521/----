//
//  iEver_normalUserUnAnswerObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_normalUserUnAnswerObject : NSObject
/*  _quesList  array */
@property (nonatomic, retain) NSMutableArray             *_quesList;
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
/* 普通用户提问未回答 */
- (RACSignal *)queryQuestByUnanswered:(NSDictionary *)dic path:(NSString *)pathUrl;
@end

/* quesList object  */
@interface iEver_N_quesList_object : NSObject

@property (nonatomic, assign) int                        ques_id;        /* 问题ID */
@property (nonatomic, assign) int                        qUserId;        /* 问题用户ID */
@property (nonatomic, assign) int                        aUserId;        /* 回答问题ID */
@property (nonatomic, copy  ) NSString                   *qTitle;        /* 问题标题 */
@property (nonatomic, copy  ) NSString                   *qContent;      /* 问题内容 */
@property (nonatomic, copy  ) NSString                   *aContent;      /* 回答内容 */
@property (nonatomic, assign) int                        qStatus;        /* 问题状态 */
@property (nonatomic, assign) int                        status;         /* 状态 */
@property (nonatomic, assign) long long                  createTime;     /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;     /* 更新时间 */
@property (nonatomic, copy  ) NSString                   *qNickName;     /* 问者昵称 */
@property (nonatomic, copy  ) NSString                   *qHeadImg;      /* 问者头像 */
@property (nonatomic, copy  ) NSString                   *aNickName;     /* 答者昵称 */
@property (nonatomic, copy  ) NSString                   *aHeadImg;      /* 答者头像 */
@property (nonatomic, assign) int                        likeTotal;      /* 问题点赞数量 */
@property (nonatomic, assign) int                        isLike;         /* 问题是否点赞 */
@property (nonatomic, assign) BOOL                       _selected;       /* 是否点赞 */
/*  expertUserList  array */
@property (nonatomic, retain) NSMutableArray             *_QquesPicList; /* 提问者图片 */
@property (nonatomic, retain) NSMutableArray             *_AquesPicList; /* 回答者图片 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
@end

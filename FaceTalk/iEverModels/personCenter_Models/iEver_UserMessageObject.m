//
//  iEver_UserMessageObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_UserMessageObject.h"
#import "iEver_AppPostJsonAPIClient.h"
@implementation iEver_UserMessageObject


- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    self._customedListArray               = [[NSMutableArray alloc] initWithCapacity:0];
    NSMutableArray *array_customed_object = [[NSMutableArray alloc] init];
    array_customed_object                 = [attributes objectForKey:@"userCustomList"];
    self._baseCustom                      = [attributes objectForKey:@"baseCustom"];
    for (int i = 0; i < [array_customed_object count]; i ++) {
        iEver_userCustomList_object * content_object = [[iEver_userCustomList_object alloc] initWithAttributes:[array_customed_object objectAtIndex:i]];
        [self._customedListArray addObject:content_object];
    }
    
    
    return self;
}

/* 发送七牛上传图片错误 */
- (RACSignal *)uploadErr:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

/* 发送推送token接口 */
- (RACSignal *)sendPushToken:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

/* 登录接口 */
- (RACSignal *)userLogin:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}

/* 手机注册接口 */
- (RACSignal *)mobileRegister:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/mobileRegister"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}

/* 邮箱注册接口 */
- (RACSignal *)emailRegister:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/emailRegister"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}


/* 判断用户是否定制 */
- (RACSignal *)userIsCustomMade:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:@"/custom/isCustom"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}
/* 强制用户更新版本 */
- (RACSignal *)updateUserApp:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:@"version/getVersion"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}

/* 获取用户定制模板 */
- (RACSignal *)questUserCustomMessageModel:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:@"/custom/getCustom"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}
/* 获取用户定制信息 */
- (RACSignal *)questUserCustomMessage:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:@"/custom/getUserCustom"
                                                                     parameters:dic
             ]
            
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_UserMessageObject * object = [[iEver_UserMessageObject alloc] initWithAttributes:responseObject];
                return object;
            }];
}
/* POST用户定制信息 */
- (RACSignal *)postUserCustomMessageModel:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/custom/setUserCustom"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}


/* POST 发送手机号码 获取验证码 */
- (RACSignal *)postSendVerifyCode:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/sendVerifyCode"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}
/* POST 验证短信校验码 */
- (RACSignal *)postVerifyMobile:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/verifyMobile"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

/* POST 手机找回密码发送验证码 */
- (RACSignal *)send2mobile:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/forgot/send2mobile"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

/* POST 邮件找回密码发送验证码 */
- (RACSignal *)send2email:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/forgot/send2email"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

/* POST 找回密码修改密码 */
- (RACSignal *)forgotUpdatePasswd:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/forgot/updatePasswd"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

/* email用户注册，填写手机号发送验证短信 */
- (RACSignal *)updateMobile:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/updateMobile"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}

/* email用户注册，验证保存手机号 */
- (RACSignal *)updateMobileVerify:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/updateMobileVerify"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}


/* 查询用户基本信息 */
- (RACSignal *)queryUserById:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:@"/user/queryById"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_personMessage_object * object = [[iEver_personMessage_object alloc] initWithAttributes:[responseObject objectForKey:@"user"]];
                object.customStatus = [[responseObject objectForKey:@"customStatus"] boolValue];
                return object;
            }];
}

/* 根据用户id查询其他用户信息 */
- (RACSignal *)queryUserInfo:(NSDictionary *)dic path:(NSString *)pathUrl
{
    // /user/queryUserId/{userId}
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_personMessage_object * object = [[iEver_personMessage_object alloc] initWithAttributes:[responseObject objectForKey:@"user"]];
                object.customStatus = [[responseObject objectForKey:@"customStatus"] boolValue];
                object.articleTotal = [[responseObject objectForKey:@"articleTotal"] integerValue];
                object.answerTotal = [[responseObject objectForKey:@"answerTotal"] integerValue];
                return object;
            }];
}

/* email发送邮件验证链接 */
- (RACSignal *)sendBindEmail:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:@"/userBind/sendBindEmail"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

/* 修改用户密码 */
- (RACSignal *)updatePasswd:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/updatePasswd"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}

/* 修改用户昵称 */
- (RACSignal *)updateNickName:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/updateNickName"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}
/* 修改用户性别 */
- (RACSignal *)updateGender:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/updateGender"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}
/* 修改用户简介 */
- (RACSignal *)updateIntro:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/updateIntro"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

/* 修改用户邮箱 */
- (RACSignal *)updateEmail:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/updateEmail"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}

/* 注册上传头像相关 */
- (RACSignal *)fetchPictureSiteToken:(NSDictionary *)dic path:(NSString *)pathUrl
{
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                           path:pathUrl
                                                                     parameters:nil
             ]
            map:^id(NSDictionary *responseObject) {
                return responseObject ;
            }] ;
}

/* 修改上传头像相关 */
- (RACSignal *)fixUserPictureSiteToken:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/user/updateHeadImg"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                return responseObject ;
            }] ;
}
@end


/* categoryList object */
@implementation iEver_userCustomList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.customKey             = [attributes objectForKey:@"customKey"];
    self.customName            = [attributes objectForKey:@"customName"];
    return self;
}

@end

/* userMessage object */
@implementation iEver_personMessage_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.point                 = [[attributes objectForKey:@"point"] integerValue];
    self.registerType          = [[attributes objectForKey:@"registerType"] integerValue];
    self.headImg               = [attributes objectForKey:@"headImg"];
    self.bindStatus            = [[attributes objectForKey:@"bindStatus"] integerValue];
    self.mobilePhone           = [attributes objectForKey:@"mobilePhone"];
    self.email                 = [attributes objectForKey:@"email"];
    self.nickName              = [attributes objectForKey:@"nickName"];
    self.gender                = [[attributes objectForKey:@"gender"] integerValue];
    self.intro                 = [attributes objectForKey:@"intro"];
    self.feature               = [attributes objectForKey:@"feature"];
    self.userType              = [[attributes objectForKey:@"userType"] integerValue];
    self.articleTotal          = [[attributes objectForKey:@"articleTotal"] integerValue];
    self.answerTotal           = [[attributes objectForKey:@"answerTotal"] integerValue];
    return self;
}

@end


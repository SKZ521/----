//
//  iEver_UserMessageObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_UserMessageObject : NSObject

/* 发送七牛上传图片错误 */
- (RACSignal *)uploadErr:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 发送推送token接口 */
- (RACSignal *)sendPushToken:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 登录接口 */
- (RACSignal *)userLogin:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 手机注册接口 */
- (RACSignal *)mobileRegister:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 邮箱注册接口 */
- (RACSignal *)emailRegister:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 判断用户时候定制 */
- (RACSignal *)userIsCustomMade:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 获取用户定制模板 */
- (RACSignal *)questUserCustomMessageModel:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 获取用户定制信息 */
- (RACSignal *)questUserCustomMessage:(NSDictionary *)dic path:(NSString *)pathUrl;

/* POST用户定制信息 */
- (RACSignal *)postUserCustomMessageModel:(NSDictionary *)dic path:(NSString *)pathUrl;

/* POST 发送手机号码 获取验证码 */
- (RACSignal *)postSendVerifyCode:(NSDictionary *)dic path:(NSString *)pathUrl;

/* POST 验证短信校验码 */
- (RACSignal *)postVerifyMobile:(NSDictionary *)dic path:(NSString *)pathUrl;

/* POST 手机找回密码发送验证码 */
- (RACSignal *)send2mobile:(NSDictionary *)dic path:(NSString *)pathUrl;

/* POST 邮件找回密码发送验证码 */
- (RACSignal *)send2email:(NSDictionary *)dic path:(NSString *)pathUrl;

/* POST 找回密码修改密码 */
- (RACSignal *)forgotUpdatePasswd:(NSDictionary *)dic path:(NSString *)pathUrl;


/* email用户注册，填写手机号发送验证短信 */
- (RACSignal *)updateMobile:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 查询用户基本信息 */
- (RACSignal *)queryUserById:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 根据用户id查询其他用户信息 */
- (RACSignal *)queryUserInfo:(NSDictionary *)dic path:(NSString *)pathUrl;

/* email用户注册，验证保存手机号 */
- (RACSignal *)updateMobileVerify:(NSDictionary *)dic path:(NSString *)pathUrl;

/* email发送邮件验证链接 */
- (RACSignal *)sendBindEmail:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 修改用户密码 */
- (RACSignal *)updatePasswd:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 修改用户昵称 */
- (RACSignal *)updateNickName:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 修改用户简介 */
- (RACSignal *)updateIntro:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 修改用户性别 */
- (RACSignal *)updateGender:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 修改用户邮箱 */
- (RACSignal *)updateEmail:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 上传头像相关 */
- (RACSignal *)fetchPictureSiteToken:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 修改上传头像相关 */
- (RACSignal *)fixUserPictureSiteToken:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 强制用户更新版本 */
- (RACSignal *)updateUserApp:(NSDictionary *)dic path:(NSString *)pathUrl;

/*  已经定制信息数组 */
@property (nonatomic, retain) NSMutableArray             *_customedListArray;

@property (nonatomic, retain) NSMutableDictionary        *_baseCustom;

@property (nonatomic, assign) int                        resultCode;       /* resultCode */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end


/* userCustomList object */
@interface iEver_userCustomList_object : NSObject

@property (nonatomic, copy  ) NSString                   *customKey;           /* 自定义值 */
@property (nonatomic, copy  ) NSString                   *customName;          /* 自定义键 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end

/* userMessage object */
@interface iEver_personMessage_object : NSObject

@property (nonatomic, assign) int                        point;                 /* 用户积分 */
@property (nonatomic, assign) int                        registerType;          /* 注册类型 */
@property (nonatomic, copy  ) NSString                   *headImg;              /* 用户头像 */
@property (nonatomic, assign) int                        bindStatus;            /* 绑定状态 */
@property (nonatomic, copy  ) NSString                   *mobilePhone;          /* 手机 */
@property (nonatomic, copy  ) NSString                   *email;                /* 邮箱 */
@property (nonatomic, copy  ) NSString                   *nickName;             /* 用户名 */
@property (nonatomic, assign) int                        gender;                /* 性别 */
@property (nonatomic, copy  ) NSString                   *intro;                /* 用户简介 */
@property (nonatomic, assign) int                        userType;              /* 用户类型 */
@property (nonatomic, assign) BOOL                       customStatus;          /* 用户定制状态 */
@property (nonatomic, assign) int                        section3EndCell;       /* 圆角单元格展现形式 */
@property (nonatomic, assign) int                        section4EndCell;       /* 圆角单元格展现形式 */
@property (nonatomic, assign) BOOL                       VorNuser;              /* 用户定制状态 */
@property (nonatomic, copy  ) NSString                   *section3Title;        /* 单元格title */
@property (nonatomic, copy  ) NSString                   *section3image;        /* 单元格title */
@property (nonatomic, copy  ) NSString                   *section4Title;        /* 单元格title */
@property (nonatomic, copy  ) NSString                   *section4image;        /* 单元格title */
@property (nonatomic, assign) BOOL                       lineHidden;            /* 单元格线是否隐藏 */
@property (nonatomic, copy  ) NSString                   *feature;              /* 单元格title */
@property (nonatomic, assign) int                        articleTotal;          /* 用户发表文章总数 */
@property (nonatomic, assign) int                        answerTotal;           /* 用户回答问题总数 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end


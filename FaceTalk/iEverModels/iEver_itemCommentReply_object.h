//
//  iEver_itemCommentReply_object.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/18.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_itemCommentReply_object : NSObject

@property (nonatomic, assign) int                                pageSize;         /* pageSize */
@property (nonatomic, assign) int                                resultCode;       /* resultCode */
@property (nonatomic, retain) NSMutableArray                     *_icList;
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
/* 查询商品文章详情  评论回复 */
- (RACSignal *)queryItemCommentReply:(NSDictionary *)dic path:(NSString *)pathUrl;

@end


@interface iEver_icList_object : NSObject

@property (nonatomic, assign) int                        icObject_id;      /* 评论id */
@property (nonatomic, assign) int                        cover_id;         /* 封面id */
@property (nonatomic, assign) int                        parent_id;        /* 父数据id */
@property (nonatomic, assign) int                        userId;           /* 用户ID */
@property (nonatomic, assign) int                        atUserId;         /* @用户ID */
@property (nonatomic, copy  ) NSString                   *commentContent;  /* 评论内容 */
@property (nonatomic, assign) int                        type;             /* 类型 */
@property (nonatomic, assign) int                        status;           /* 状态 */
@property (nonatomic, assign) long long                  createTime;       /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;       /* 更新时间 */
@property (nonatomic, assign) int                        replyTotal;       /* 评论数据 */
@property (nonatomic, copy  ) NSString                   *nickName;        /* 昵称 */
@property (nonatomic, copy  ) NSString                   *atNickName;      /* @用户昵称 */
@property (nonatomic, copy  ) NSString                   *feature;
@property (nonatomic, copy  ) NSString                   *headImg;         /* 头像图片 */
@property (nonatomic, assign) int                        point;            /* 积分 */
@property (nonatomic, assign) int                        isLike;           /* 是否点赞 */
@property (nonatomic, assign) int                        likeTotal;        /* 点赞个数 */
@property (nonatomic, assign) BOOL                       _selected;        /* 是否点赞 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end

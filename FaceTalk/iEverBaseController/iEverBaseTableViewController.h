//
//  iEverBaseTableViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

@interface iEverBaseTableViewController : iEverBaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic          ) UITableView               *tableView;
@property (strong, nonatomic          ) NSMutableArray            *dataSourceArray;
@property (nonatomic, strong          ) NSMutableArray            *filteredActivities;
@property (nonatomic, strong          ) NSMutableArray            *activities;
@property (nonatomic, assign          ) int                       page;
@property (nonatomic, strong, readonly) UISearchBar               *searchBar;
@property (nonatomic, strong, readonly) UISearchDisplayController *strongSearchDisplayController;
@property (nonatomic, strong          ) UIImageView               *nothingImageView;

- (void)fetchData;
- (void)fetchDataWithResultBlock:(void (^)(id data, NSError *error))block;
- (void)addDataToDataSource:(id)objects;
- (void)manualRefreh;
- (void)showsSVPullScrolling;
- (void)hidePullToRefresh;
- (void)nothingView;

@end

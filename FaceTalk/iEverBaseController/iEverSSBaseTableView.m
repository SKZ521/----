//
//  iEverSSBaseTableView.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverSSBaseTableView.h"
#import "SVPullToRefresh.h"

typedef NS_ENUM( NSUInteger, SSDataSourcesReloadType ) {
    SSDataSourcesReloadDefault = 0,
    SSDataSourcesReloadPull,
    SSDataSourcesReloadDrop,
};

@interface iEverSSBaseTableView()
@property (assign)SSDataSourcesReloadType reloadType;
@end
@implementation iEverSSBaseTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code


		//初始化刷新状态
		self.page = 1;
		self.reloadType = SSDataSourcesReloadPull;

		//上啦刷新 下拉加载更多
		__weak iEverSSBaseTableView *weakSelf = self;
		// setup pull-to-refresh 刷新
        		[self addPullToRefreshWithActionHandler:^{
                weakSelf.reloadType = SSDataSourcesReloadPull;
        			weakSelf.page = 1;
        			NSLog(@"ni da ye ");
        			[weakSelf manualRefreh];
        		}];

		//加载更多
		[self addInfiniteScrollingWithActionHandler:^{
			weakSelf.reloadType = SSDataSourcesReloadDrop;
			weakSelf.page += 1;
			[weakSelf manualRefreh];
		}];

		self.showsInfiniteScrolling = YES;
		self.arrayDtaSource = [[SSArrayDataSource alloc] initWithItems:nil];
		self.dataSource = self.dataSource;
		self.arrayDtaSource.tableView = self;
    }
    return self;
}

#pragma mark - 刷新数据

//刷新
- (void)manualRefreh
{
    [self fetchData];
}

//请求数据
- (void)fetchData
{

}

//data source
- (void)reloadDatasouce:(NSArray *)objects
{
    [self refreshViewStopAnimating];
    switch (self.reloadType) {
        case SSDataSourcesReloadPull: {
            [self.arrayDtaSource updateItems:objects];
        }
            break;
        case SSDataSourcesReloadDrop: {
            [self.arrayDtaSource appendItems:objects];
        }
            break;

        default:
            break;
    }

    if (objects.count < ONEPAGECOUNT) {
		self.showsInfiniteScrolling = NO;
	}else {
		self.showsInfiniteScrolling = YES;
	}

    //没有数据
    [self nothingView];
}

//加载数据完成 停止动画
- (void)refreshViewStopAnimating
{
	int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		__weak iEverSSBaseTableView *weakSelf = self;
		[weakSelf.pullToRefreshView stopAnimating];
		[weakSelf.infiniteScrollingView stopAnimating];
    });
}

//下拉刷新数据（动画）
- (void)triggerPullToRefresh
{
    [self triggerPullToRefresh];
}

//空数据替换view
- (void)nothingView
{
	if ([self.arrayDtaSource allItems].count == 0) {
		UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_nothing_"]];
		self.tableFooterView = imageView;
	}else {
		self.tableFooterView = nil;
	}
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView

           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewCellEditingStyleNone;

}
@end

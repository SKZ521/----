//
//  iEverBaseTableView.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseTableView.h"
#import "SVPullToRefresh.h"
#define CountOfOnePage 20

enum
{
    ReloadingDefault = 0, //
	ReloadingPull = 1, //刷新
	ReloadingMore      //加载更多
};
typedef NSUInteger ReloadType;

@interface iEverBaseTableView ()<UITableViewDataSource,UITableViewDelegate>
{
}
@property (assign) ReloadType reloadType;
@end
@implementation iEverBaseTableView


- (id)initWithFrame:(CGRect)frame reloadData:(void (^)(id data, NSError *error))block
{
	self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.delegate = self;
		self.dataSource = self;

		self.dataSourceArray = [[NSMutableArray alloc] initWithCapacity:0];
		self.page = 1;

		__weak iEverBaseTableView *weakSelf = self;
		// setup pull-to-refresh 刷新
		//		[self addPullToRefreshWithActionHandler:^{
		//			weakSelf.reloadType = ReloadingPull;
		//			[weakSelf manualRefreh];
		//		}];

		//加载更多
		[self addInfiniteScrollingWithActionHandler:^{
			weakSelf.reloadType = ReloadingMore;
            //			[weakSelf manualRefreh];
			if (weakSelf.reloadType == ReloadingPull) {
				if (weakSelf.dataSourceArray.count > 0) {
					[weakSelf.dataSourceArray removeAllObjects];
				}
				weakSelf.page = 1;
				//		[self fetchData];
			}else if (weakSelf.reloadType == ReloadingMore) {
				weakSelf.page++;
			}else {

			}

			if (weakSelf.dataSourceArray.count == 0 || weakSelf.reloadType == ReloadingMore) {
				return block(nil, nil);
			}
		}];
    }
	self.showsInfiniteScrolling = NO;
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark
#pragma mark - 刷新数据

- (void)manualRefreh
{
	if (_reloadType == ReloadingPull) {
        if (self.dataSourceArray.count > 0) {
            [self.dataSourceArray removeAllObjects];
        }
		_page = 1;
		//		[self fetchData];
	}else if (_reloadType == ReloadingMore) {
        _page++;
	}else {

	}

	if (self.dataSourceArray.count == 0 || _reloadType == ReloadingMore) {
		[self fetchData];
	}
}

- (void)fetchData
{

}

- (void)refreshViewStopAnimating
{
	int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		__weak iEverBaseTableView *weakSelf = self;
		[weakSelf.pullToRefreshView stopAnimating];
		[weakSelf.infiniteScrollingView stopAnimating];
    });
}

//加载数据
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)addDataToDataSource:(NSArray *)objects
{
	[self refreshViewStopAnimating];
	NSArray *dataList = [NSArray arrayWithArray:objects];

	[self.dataSourceArray addObjectsFromArray:dataList];

	if (dataList.count < CountOfOnePage) {
		self.showsInfiniteScrolling = NO;
	}else {
		self.showsInfiniteScrolling = YES;
	}

	_reloadType = ReloadingDefault;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    // Configure the cell...

    return cell;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView

           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewCellEditingStyleNone;

}
@end

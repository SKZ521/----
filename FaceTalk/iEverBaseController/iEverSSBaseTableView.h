//
//  iEverSSBaseTableView.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SSDataSources.h>

@interface iEverSSBaseTableView : UITableView

@property (assign) NSInteger page;
@property (strong) SSArrayDataSource *arrayDtaSource;

//deprecated property; use SSArrayDataSource * dataSource instead;
@property (strong, nonatomic) NSMutableArray *dataSourceArray;
@property(nonatomic, strong) NSMutableArray *filteredActivities;
@property(nonatomic, strong) NSMutableArray *activities;

//add data source
- (void)reloadDatasouce:(NSArray *)objects;

//请求数据
- (void)fetchData;

//start pull refresh with animation
- (void)triggerPullToRefresh;
@end

//
//  iEverBaseViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

#import "iEver_UserMessageObject.h"
#import "iEver_VuserHomeViewController.h"

@interface iEverBaseViewController ()

@end

@implementation iEverBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithBarType:(NavBarType)barType
{
	self = [super init];
	if (self) {
		self.navBarType = barType;
	}
	return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [GiFHUD dismiss];
    [super viewWillDisappear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = CLEAR;
   #ifdef __IPHONE_7_0
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        //self.automaticallyAdjustsScrollViewInsets = NO;
        //self.extendedLayoutIncludesOpaqueBars = NO;
		self.view.frame =  CGRectMake(0, 0, ScreenWidth, ScreenHeight - 20);
	}
   #endif

    __typeof (self) __weak weakSelf = self;

	UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *return_image = [UIImage imageNamed:@"NAV_return"];
	if (self.navBarType) {
		leftButton.frame = CGRectMake(0, 0, 44, 44);
		[leftButton setTitleColor:MY_PURPLECOLOR forState:UIControlStateNormal];
		[leftButton setTitle:@"取消" forState:UIControlStateNormal];
	}else {
        leftButton.frame            = CGRectMake(0, 0, return_image.size.width, return_image.size.height);
		if (iOS7) {
			[leftButton setImage:return_image forState:UIControlStateNormal];
		}else {
			[leftButton setImage:return_image forState:UIControlStateNormal];
		}
	}
	[[leftButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
		if (weakSelf.navBarType) {
			[weakSelf dismissViewControllerAnimated:YES completion:^{

			}];
		}else {
			[weakSelf.view endEditing:YES];
			[weakSelf.navigationController popViewControllerAnimated:YES];
		}
	}];
	UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
	self.navigationItem.leftBarButtonItem = leftBarButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)addLeftBarButtonItem:(void (^)(UIButton *sender))block
{
	UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage * NAV_return = [UIImage imageNamed:@"NAV_return"];
	leftButton.frame = CGRectMake(0, 0, NAV_return.size.width, NAV_return.size.height);
    [leftButton setImage:NAV_return forState:UIControlStateNormal];
	[[leftButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
		return block(sender);
	}];
	UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
	self.navigationItem.leftBarButtonItem = leftBarButton;
}

- (void)addRightBarButtonItem:(void (^)(UIButton *sender))block
{
	UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
	rightButton.frame = CGRectMake(0, 0, 44, 44);
	[rightButton setTitle:@"" forState:UIControlStateNormal];
	[rightButton setTitleColor:WHITE forState:UIControlStateNormal];
	rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
	[[rightButton rac_signalForControlEvents:UIControlEventTouchUpInside ] subscribeNext:^(UIButton *sender) {
		return block(sender);
	}];
	UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
	self.navigationItem.rightBarButtonItem = rightBarButton;
}

#pragma mark - private
// 用户/达人主页入口
- (void)gotoUserHomeWithUserId:(NSString *)userId
{
    [GiFHUD show];
    @weakify(self);
    NSString *path = [NSString stringWithFormat:@"/user/queryUserId/%@",userId];
    iEver_UserMessageObject *detail_Object = [[iEver_UserMessageObject alloc] init];
    [[[detail_Object queryUserInfo:nil path:path] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_personMessage_object *object) {
         [GiFHUD dismiss];

         @strongify(self);

         if (!object) return;
         
         iEver_VuserHomeViewController *homeViewController = [[iEver_VuserHomeViewController alloc] initWithUserInfo:object userId:userId];
//         self.navigationController.delegate = homeViewController;
         [self.navigationController pushViewController:homeViewController animated:YES];
     }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}

@end

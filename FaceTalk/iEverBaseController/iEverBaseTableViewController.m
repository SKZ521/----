//
//  iEverBaseTableViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"
#import "SVPullToRefresh.h"
#define CountOfOnePage 10

enum
{
    ReloadingDefault = 0, //
	ReloadingPull = 1, //刷新
	ReloadingMore      //加载更多
};
typedef NSUInteger ReloadType;

@interface iEverBaseTableViewController ()
{
	NSMutableArray *dataList;
	NSMutableArray *data;
    BOOL isSearch;
}
@property (nonatomic, assign) ReloadType reloadType;

@end

@implementation iEverBaseTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [GiFHUD dismiss];
    [super viewWillDisappear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = CLEAR;

    /* set default backgroundView */
    UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    _backImageView.image = [UIImage imageNamed:@"backImage"];
    self.tableView.backgroundView = _backImageView;

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    CGRect tFrame = self.view.bounds;
	tFrame.size.height = tFrame.size.height - 44.0;
	self.tableView = [[UITableView alloc] initWithFrame:tFrame];
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];

    dataList = [[NSMutableArray alloc] initWithCapacity:0];
	self.dataSourceArray = [[NSMutableArray alloc] initWithCapacity:0];
	self.filteredActivities = [[NSMutableArray alloc] initWithCapacity:0];
    self.activities = [[NSMutableArray alloc] initWithCapacity:0];
	self.page = 1;

    __weak iEverBaseTableViewController *weakSelf = self;
	/*  setup pull-to-refresh  */
	[self.tableView addPullToRefreshWithActionHandler:^{
		weakSelf.reloadType = ReloadingPull;
		[weakSelf manualRefreh];
	}];

	/*  get more */
	[self.tableView addInfiniteScrollingWithActionHandler:^{
		weakSelf.reloadType = ReloadingMore;
		[weakSelf manualRefreh];
    }];
	self.tableView.showsInfiniteScrolling = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)nothingView
{
	if (self.dataSourceArray.count == 0) {
		UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_nothing_"]];
		self.tableView.tableFooterView = imageView;
	}else {
		self.tableView.tableFooterView = nil;
	}
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[self.view endEditing:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearch) {
      	if (tableView != self.tableView) {
            return self.filteredActivities.count;
        }
        return self.activities.count;
    }

    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    // Configure the cell...

    return cell;
}

#pragma mark - 刷新数据

- (void)manualRefreh
{
	if (_reloadType == ReloadingPull) {
        if (self.dataSourceArray.count > 0) {
            [self.dataSourceArray removeAllObjects];
        }
        if (self.activities.count > 0) {
            [self.activities removeAllObjects];
        }
		_page = 1;
        [self fetchData];
	}else if (_reloadType == ReloadingMore) {
        _page++;
        [self fetchData];
	}else {

	}
    
	if (self.dataSourceArray.count == 0 || _reloadType == ReloadingMore) {

		[self fetchData];
	}
}

- (void)fetchDataWithResultBlock:(void (^)(id data, NSError *error))block
{
    if (block) {
        DLog(@"success !!!!!!!!!!");
    }
}

- (void)fetchData
{
	//		[self refreshViewStopAnimating];
    [self.tableView.pullToRefreshView startAnimating];
}

- (void)refreshViewStopAnimating
{
	int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		__weak iEverBaseTableViewController *weakSelf = self;
		[weakSelf.tableView.pullToRefreshView stopAnimating];
		[weakSelf.tableView.infiniteScrollingView stopAnimating];
    });
}

//加载数据
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)addDataToDataSource:(NSArray *)objects
{
	[self refreshViewStopAnimating];
	NSArray *dataLists = [NSArray arrayWithArray:objects];

	[self.dataSourceArray addObjectsFromArray:dataLists];
	if (dataLists.count < CountOfOnePage) {
		self.tableView.showsInfiniteScrolling = NO;
	}else {
		self.tableView.showsInfiniteScrolling = YES;
	}

	_reloadType = ReloadingDefault;

    [self nothingView];
}

- (void)hidePullToRefresh
{
	self.tableView.showsPullToRefresh = NO;
}

- (void)showsSVPullScrolling
{
	self.tableView.showsPullToRefresh = NO;
	self.tableView.showsInfiniteScrolling = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView

           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewCellEditingStyleNone;

}
@end

//
//  CustomNavigationController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/11.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "CustomNavigationController.h"

@interface CustomNavigationController ()

@end

@implementation CustomNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}


//-(BOOL)shouldAutorotate
//{
//    return NO;
//
//}
//
//
//-(NSUInteger)supportedInterfaceOrientations{
//    return self.orietation;
//}
//
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return self.orietation;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation

{

    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);

}

- (BOOL)shouldAutorotate

{

    return NO;

}

- (NSUInteger)supportedInterfaceOrientations

{

    return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
    
}
@end

#!/bin/sh
set -e

RESOURCES_TO_COPY=${PODS_ROOT}/resources-to-copy-${TARGETNAME}.txt
> "$RESOURCES_TO_COPY"

install_resource()
{
  case $1 in
    *.storyboard)
      echo "ibtool --reference-external-strings-file --errors --warnings --notices --output-format human-readable-text --compile ${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename \"$1\" .storyboard`.storyboardc ${PODS_ROOT}/$1 --sdk ${SDKROOT}"
      ibtool --reference-external-strings-file --errors --warnings --notices --output-format human-readable-text --compile "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename \"$1\" .storyboard`.storyboardc" "${PODS_ROOT}/$1" --sdk "${SDKROOT}"
      ;;
    *.xib)
        echo "ibtool --reference-external-strings-file --errors --warnings --notices --output-format human-readable-text --compile ${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename \"$1\" .xib`.nib ${PODS_ROOT}/$1 --sdk ${SDKROOT}"
      ibtool --reference-external-strings-file --errors --warnings --notices --output-format human-readable-text --compile "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename \"$1\" .xib`.nib" "${PODS_ROOT}/$1" --sdk "${SDKROOT}"
      ;;
    *.framework)
      echo "mkdir -p ${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
      mkdir -p "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
      echo "rsync -av ${PODS_ROOT}/$1 ${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
      rsync -av "${PODS_ROOT}/$1" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
      ;;
    *.xcdatamodel)
      echo "xcrun momc \"${PODS_ROOT}/$1\" \"${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1"`.mom\""
      xcrun momc "${PODS_ROOT}/$1" "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1" .xcdatamodel`.mom"
      ;;
    *.xcdatamodeld)
      echo "xcrun momc \"${PODS_ROOT}/$1\" \"${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1" .xcdatamodeld`.momd\""
      xcrun momc "${PODS_ROOT}/$1" "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1" .xcdatamodeld`.momd"
      ;;
    *.xcassets)
      ;;
    /*)
      echo "$1"
      echo "$1" >> "$RESOURCES_TO_COPY"
      ;;
    *)
      echo "${PODS_ROOT}/$1"
      echo "${PODS_ROOT}/$1" >> "$RESOURCES_TO_COPY"
      ;;
  esac
}
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieBackward.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieBackward@2x.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieBackwardSelected.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieBackwardSelected@2x.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieEndFullscreen.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieEndFullscreen@2x.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieForward.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieForward@2x.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieForwardSelected.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieForwardSelected@2x.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieFullscreen.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/movieFullscreen@2x.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/moviePause.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/moviePause@2x.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/moviePlay.png"
install_resource "ALMoviePlayerController/ALMoviePlayerController/Images/moviePlay@2x.png"
install_resource "JCGridMenu/Universal/Images/Close/Close.png"
install_resource "JCGridMenu/Universal/Images/Close/Close@2x.png"
install_resource "JCGridMenu/Universal/Images/CloseSelected/CloseSelected.png"
install_resource "JCGridMenu/Universal/Images/CloseSelected/CloseSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/Comments/Comments.png"
install_resource "JCGridMenu/Universal/Images/Comments/Comments@2x.png"
install_resource "JCGridMenu/Universal/Images/CommentsSelected/CommentsSelected.png"
install_resource "JCGridMenu/Universal/Images/CommentsSelected/CommentsSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/Email/Email.png"
install_resource "JCGridMenu/Universal/Images/Email/Email@2x.png"
install_resource "JCGridMenu/Universal/Images/EmailSelected/EmailSelected.png"
install_resource "JCGridMenu/Universal/Images/EmailSelected/EmailSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/Facebook/Facebook.png"
install_resource "JCGridMenu/Universal/Images/Facebook/Facebook@2x.png"
install_resource "JCGridMenu/Universal/Images/FacebookSelected/FacebookSelected.png"
install_resource "JCGridMenu/Universal/Images/FacebookSelected/FacebookSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/Favourite/Favourite.png"
install_resource "JCGridMenu/Universal/Images/Favourite/Favourite@2x.png"
install_resource "JCGridMenu/Universal/Images/FavouriteSelected/FavouriteSelected.png"
install_resource "JCGridMenu/Universal/Images/FavouriteSelected/FavouriteSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/FavouriteSmall/FavouriteSmall.png"
install_resource "JCGridMenu/Universal/Images/FavouriteSmall/FavouriteSmall@2x.png"
install_resource "JCGridMenu/Universal/Images/FavouriteSmallSelected/FavouriteSmallSelected.png"
install_resource "JCGridMenu/Universal/Images/FavouriteSmallSelected/FavouriteSmallSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/Pocket/Pocket.png"
install_resource "JCGridMenu/Universal/Images/Pocket/Pocket@2x.png"
install_resource "JCGridMenu/Universal/Images/PocketSelected/PocketSelected.png"
install_resource "JCGridMenu/Universal/Images/PocketSelected/PocketSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/Search/Search.png"
install_resource "JCGridMenu/Universal/Images/Search/Search@2x.png"
install_resource "JCGridMenu/Universal/Images/SearchSelected/SearchSelected.png"
install_resource "JCGridMenu/Universal/Images/SearchSelected/SearchSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/Share/Share.png"
install_resource "JCGridMenu/Universal/Images/Share/Share@2x.png"
install_resource "JCGridMenu/Universal/Images/ShareSelected/ShareSelected.png"
install_resource "JCGridMenu/Universal/Images/ShareSelected/ShareSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/Spam/Spam.png"
install_resource "JCGridMenu/Universal/Images/Spam/Spam@2x.png"
install_resource "JCGridMenu/Universal/Images/SpamSelected/SpamSelected.png"
install_resource "JCGridMenu/Universal/Images/SpamSelected/SpamSelected@2x.png"
install_resource "JCGridMenu/Universal/Images/Twitter/Twitter.png"
install_resource "JCGridMenu/Universal/Images/Twitter/Twitter@2x.png"
install_resource "JCGridMenu/Universal/Images/TwitterSelected/TwitterSelected.png"
install_resource "JCGridMenu/Universal/Images/TwitterSelected/TwitterSelected@2x.png"
install_resource "RETableViewManager/RETableViewManager/RETableViewManager.bundle"
install_resource "SVProgressHUD/SVProgressHUD/SVProgressHUD.bundle"
install_resource "UMengFeedback/UMFeedback_iOS_1.4.2/umFeedback.bundle"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/UMSocialSDKResourcesNew.bundle"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_Extra_Frameworks/TencentOpenAPI/TencentOpenApi_IOS_Bundle.bundle"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/SocialSDKXib/UMSCommentDetailController.xib"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/SocialSDKXib/UMSCommentInputController.xib"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/SocialSDKXib/UMSCommentInputControlleriPad.xib"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/SocialSDKXib/UMShareEditViewController.xib"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/SocialSDKXib/UMShareEditViewControlleriPad.xib"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/SocialSDKXib/UMSLoginViewController.xib"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/SocialSDKXib/UMSnsAccountViewController.xib"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/SocialSDKXib/UMSShareListController.xib"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/en.lproj"
install_resource "UMengSocial/umeng_ios_social_sdk_4.1_arm64_custom/UMSocial_Sdk_4.1/zh-Hans.lproj"
install_resource "iVersion/iVersion/iVersion.bundle"

rsync -avr --copy-links --no-relative --exclude '*/.svn/*' --files-from="$RESOURCES_TO_COPY" / "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"
if [[ "${ACTION}" == "install" ]]; then
  rsync -avr --copy-links --no-relative --exclude '*/.svn/*' --files-from="$RESOURCES_TO_COPY" / "${INSTALL_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"
fi
rm -f "$RESOURCES_TO_COPY"

if [[ -n "${WRAPPER_EXTENSION}" ]] && [ `xcrun --find actool` ] && [ `find . -name '*.xcassets' | wc -l` -ne 0 ]
then
  case "${TARGETED_DEVICE_FAMILY}" in 
    1,2)
      TARGET_DEVICE_ARGS="--target-device ipad --target-device iphone"
      ;;
    1)
      TARGET_DEVICE_ARGS="--target-device iphone"
      ;;
    2)
      TARGET_DEVICE_ARGS="--target-device ipad"
      ;;
    *)
      TARGET_DEVICE_ARGS="--target-device mac"
      ;;  
  esac 
  find "${PWD}" -name "*.xcassets" -print0 | xargs -0 actool --output-format human-readable-text --notices --warnings --platform "${PLATFORM_NAME}" --minimum-deployment-target "${IPHONEOS_DEPLOYMENT_TARGET}" ${TARGET_DEVICE_ARGS} --compress-pngs --compile "${BUILT_PRODUCTS_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"
fi
